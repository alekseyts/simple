#if !defined(__C_BINARY_FILE)
#define __NENET_H

#if !defined(__cplusplus)
#error NENet.h requires C++ compilation (use a .cpp suffix) 
#endif 

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000