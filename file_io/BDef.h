#if !defined(__BDEF_H)
#define __BDEF_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(NO_NAMESPACE)
#define _BIN_BEGIN \
namespace BIN {
#else
#define _BIN_BEGIN
#endif 


#if !defined(NO_NAMESPACE)
#define _BIN_END \
} //namespace BIN
#else
#define _BIN_END
#endif

#if defined(_MSC_VER) /* MicroSoft compiler ? */
//disable warnings on extern before template instantiation
#pragma warning (disable : 4231 4251)
// To disable warning about long identifiers with STL
#pragma warning (disable:   4786 4788)
#ifdef BIN_COMPILING   
    #    define BIN_TEMPLATE    
#else    
    #    define BIN_TEMPLATE extern    
#endif
#else // not a Microsoft compiler
    #    define BIN_TEMPLATE    
#endif // defined(_MSC_VER)

/* a macro to export library symbols (BIN_EXPORT) 
   and the type for exported functions (BIN_API) */   
#if defined(WIN32) /* Compiling for 32 bit Windows  ? */
#if defined(_MSC_VER) /* MicroSoft compiler ? */
#if defined(BIN_COMPILING) /* Compiling the DLL ? */
#define BIN_EXPORT __declspec(dllexport) 
#else // using the DLL
#define BIN_EXPORT __declspec(dllimport) 
#endif /* defined(OCR_COMPILING) */
#define BIN_API __stdcall
#else /* Other compilers */
#define BIN_EXPORT export
#define BIN_API pascal
#endif /* _MSC_VER */
#else /* WIN32 */
#define BIN_EXPORT
#define BIN_API
#endif /* WIN32 */

#include <CRSmartp.h>

#endif // __BDEF_H
