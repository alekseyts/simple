#include "stdafx.h"
#include "CFileBinary.h"

#include <sstream>
#include <vector>
#include <limits>
#include <cassert>
#include "boost/lexical_cast.hpp" 

using boost::lexical_cast;

#ifdef min
#undef min
#endif

_BIN_BEGIN

class BinFile
{
public:
	BinFile(std::string fileName):
		m_fileName(fileName)
	{

	};
	virtual ~BinFile() {};
	bool IsValid() {return m_File.m_hFile != CFile::hFileNull;};
	
protected:
	std::string m_fileName;
	CFile m_File;
};	

class BinInputFile : public CInputBinFile,public BinFile
{
public:
	BinInputFile(std::string fileName);
	virtual ~BinInputFile();
	virtual void  Read(char* pBeg, std::streamsize nCount);
	virtual void  Read(unsigned char* pBeg, std::streamsize nCount);
	virtual void  Read(unsigned char& uch);
	virtual std::string GetFilename() {return m_fileName;};
	virtual void SetPointerPosition(__int64 offset);
	virtual void OffsetPointerPosition(__int64 offset);
	virtual __int64 GetPointerPosition() {return m_File.GetPosition();};
	
};

BinInputFile::BinInputFile(std::string fileName):
	BinFile(fileName)
{
	if(m_File.Open(fileName.c_str(),CFile::modeRead | CFile::typeBinary) == 0){
			std::string sErrMsg = "Couldn't open file '";
			sErrMsg += fileName;
			sErrMsg += "' for binary read";
			throw std::runtime_error(sErrMsg);
	}
}

BinInputFile::~BinInputFile()
{

}

void  BinInputFile::Read(char* pBeg, std::streamsize nCount)
{
	    __int64 pos = GetPointerPosition();
		assert(nCount >= 0);
		const UINT nRead =
			m_File.Read(pBeg, sizeof(char) * nCount);

		if(nRead == 0){
			std::string sErrMsg = "End of file encountered while reading.";
			throw std::runtime_error(sErrMsg);
		}

		if(nRead != nCount) {
			std::stringstream ss;
			ss
				<< "Couldn't read '"
				<< nCount
				<< "' characters from file. "
				<< std::strerror(NULL);
			throw std::runtime_error(ss.str());
		}
	
}

void  BinInputFile::Read(unsigned char* pBeg, std::streamsize nCount)
{
		assert(nCount >= 0);
		std::vector<char> buff(nCount);
		this->Read(&*buff.begin(), buff.size());

		const int nCharMin = std::numeric_limits<char>::min();
		const int nUCharMin = std::numeric_limits<unsigned char>::min();
		for (int i = 0; i < nCount; ++i) {
			int nCh = buff[i];
			pBeg[i] = static_cast<unsigned char>(nCh - nCharMin + nUCharMin);
		}
}

void BinInputFile::Read(unsigned char& uch)
{
		char ch(0);
		this->Read(&ch, sizeof(char));

		const int nCharMin = std::numeric_limits<char>::min();
		const int nUCharMin = std::numeric_limits<unsigned char>::min();

		int nCh = ch;
		uch = static_cast<unsigned char>(nCh - nCharMin + nUCharMin);
}

void BinInputFile::OffsetPointerPosition(__int64 offset)
{
	m_File.Seek(offset,CFile::current);
}

void BinInputFile::SetPointerPosition(__int64 offset)
{
	m_File.Seek(offset,CFile::begin);
}
//---------------------------------------------------------------------
class BinOutputFile:public COutputBinFile,public BinFile
{
public:
	BinOutputFile(std::string fileName,std::ios_base::openmode mode);	
	virtual ~BinOutputFile();
	virtual void Write(const char* const pBeg, std::streamsize nCount);
	virtual void Write(const unsigned char* const pBeg, std::streamsize nCount);
	virtual void Write(unsigned char uch);
	virtual void Append(std::string const& sFileName);
	virtual void Append(CInputBinFile* ibf);
	virtual std::string GetFilename() {return m_fileName;};
};

BinOutputFile::BinOutputFile(std::string fileName,std::ios_base::openmode mode):
	BinFile(fileName)
{
	UINT iOpenFlags = 
		CFile::modeWrite | CFile::modeCreate  | CFile::typeBinary;
	if(mode == std::ios_base::app){
		 iOpenFlags |= CFile::modeNoTruncate;
	} 
	CFileException fError;
	if(m_File.Open(fileName.c_str(),iOpenFlags,&fError) == 0){
			std::string sErrMsg = "Couldn't open file '";
			sErrMsg += fileName;
			sErrMsg += "' for binary write \n";
			sErrMsg += "Error status = ";
			sErrMsg += lexical_cast<std::string>(fError.m_cause);

			throw std::runtime_error(sErrMsg);
	}
	m_File.SeekToEnd();
}

BinOutputFile::~BinOutputFile()
{
	
}

void BinOutputFile::Write(const char* const pBeg, std::streamsize nCount)
{
	    __int64 pos = m_File.GetPosition();
		assert(nCount >= 0);
		try {
			m_File.Write(pBeg, sizeof(char) * nCount);
		} catch(...){
			std::string sErrMsg = "End of file encountered while writing.";
			throw std::runtime_error(sErrMsg);
		}
}

void BinOutputFile::Write(const unsigned char* const pBeg, std::streamsize nCount)
{
		assert(nCount >= 0);
		std::vector<char> buff(nCount);
		const int nCharMin = std::numeric_limits<char>::min();
		const int nUCharMin = std::numeric_limits<unsigned char>::min();
		for (int i = 0; i < nCount; ++i) {
			int nCh = pBeg[i];
			buff[i] = static_cast<char>(nCh + nCharMin - nUCharMin);
		}
		this->Write(&*buff.begin(), buff.size());
}

void BinOutputFile::Write(unsigned char uch)
{
		const int nCharMin = std::numeric_limits<char>::min();
		const int nUCharMin = std::numeric_limits<unsigned char>::min();

		int nCh = uch;
		char ch = static_cast<char>(nCh + nCharMin - nUCharMin);

		this->Write(&ch, sizeof(char));
}

void BinOutputFile::Append(std::string const& sFileName)
{
		BinInputFile ibf(sFileName);
		try{
			unsigned char ch;
			while(true){
				ibf.Read(ch);
				Write(ch);
			}
		} catch(std::exception &e)
		{
				int k=0;
		}
}

void BinOutputFile::Append(CInputBinFile* ibf)
	{
		try{
			unsigned char ch;
			while(true){
				ibf->Read(ch);
				Write(ch);
			}
		} catch(std::exception &e)
		{
			int k=0;	
		}
	}
//---------------------------------------------------------------------
CInputBinFile* GetBinFileIn(std::string	fileName)
{
    BinInputFile* ptr = new BinInputFile(fileName);
	if(ptr->IsValid()){
		return ptr;
	} else {
		delete ptr;
		return NULL;
	}
}

BIN_EXPORT COutputBinFile* GetBinFileOut(std::string fileName,std::ios_base::openmode mode)
{
 	BinOutputFile* ptr = new BinOutputFile(fileName,mode);
	if(ptr->IsValid()){
		return ptr;
	} else {
		delete ptr;
		return NULL;
	}
}



_BIN_END