#if !defined(__C_BINARY_FILE)
#define __C_BINARY_FILE

#if !defined(__cplusplus)
#error CFileBinary.h requires C++ compilation (use a .cpp suffix) 
#endif 

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "BDef.h"
#include <string>

#if !defined(NO_NAMESPACE)
namespace BinaryFile
{ 
#endif 


class BIN_EXPORT CInputBinFile
{
public:
	virtual ~CInputBinFile() {};
	virtual void Read(char* pBeg, std::streamsize nCount) = 0;
	virtual void Read(unsigned char* pBeg, std::streamsize nCount) = 0;
	virtual std::string GetFilename() = 0;
	virtual __int64 GetPointerPosition() = 0;
	virtual void OffsetPointerPosition(__int64 offset) = 0;
	virtual void SetPointerPosition(__int64 offset) = 0;

	template <typename TOutIter>
	void Read(TOutIter beg, std::streamsize nCount) {
			assert(nCount >= 0);
			while(nCount-- != 0) {
				this->Read(*beg);
				++beg;
			}
		}

	template <typename TOutIter>
	void Read(TOutIter beg, TOutIter end) {
			while(beg != end) {
				this->Read(*beg);
				++beg;
			}
		}
	    
	template <typename T>
	//T must be a POD type
	void Read(T& obj) {
			this->Read(reinterpret_cast<char*>(&obj), sizeof(T));
	}

	virtual void Read(unsigned char& uch) = 0;
};

class BIN_EXPORT COutputBinFile
{
public:
	virtual ~COutputBinFile() {};
	virtual void Write(const char* const pBeg, std::streamsize nCount)=0;
	void Write(const char* const pBeg, const char* const pEnd) {
			Write(pBeg, std::distance(pBeg, pEnd));
	}
	virtual void Write(const unsigned char* const pBeg, std::streamsize nCount)=0;
	void Write(const unsigned char* const pBeg, const unsigned char* const pEnd) {
			Write(pBeg, std::distance(pBeg, pEnd));
		};

	template <typename TFwdIter>
		void Write(TFwdIter beg, std::streamsize nCount) {
			assert(nCount >= 0);
			while(nCount-- != 0) {
				this->Write(*beg);
				++beg;
			}
		}
	template <typename TFwdIter>
		void Write(TFwdIter beg, TFwdIter end) {
			while(beg != end) {
				this->Write(*beg);
				++beg;
			}
		}

	template <typename T>
		//T must be a POD type
		void Write(T const& obj) {
			this->Write(reinterpret_cast<const char* const>(&obj), sizeof(T));
		}

	virtual void Write(unsigned char uch)=0;
	virtual void Append(std::string const& sFileName) = 0;
	virtual void Append(CInputBinFile* ibf) = 0;

	virtual std::string GetFilename() = 0;

};

BIN_EXPORT CInputBinFile* GetBinFileIn(std::string	fileName);
BIN_EXPORT COutputBinFile* GetBinFileOut(std::string fileName,std::ios_base::openmode mode = std::ios_base::app);


#if !defined(NO_NAMESPACE)
} //end of namespace BinaryFile
#endif

#endif // __C_BINARY_FILE