// file_io.h : main header file for the file_io DLL
//

#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols


// Cfile_ioApp
// See file_io.cpp for the implementation of this class
//

class Cfile_ioApp : public CWinApp
{
public:
	Cfile_ioApp();

// Overrides
public:
	virtual BOOL InitInstance();

	DECLARE_MESSAGE_MAP()
};
