# Microsoft Developer Studio Generated NMAKE File, Based on imgtk.dsp
!IF "$(CFG)" == ""
CFG=imgtk - Win32 Debug
!MESSAGE No configuration specified. Defaulting to imgtk - Win32 Debug.
!ENDIF 

!IF "$(CFG)" != "imgtk - Win32 Release" && "$(CFG)" != "imgtk - Win32 Debug"
!MESSAGE Invalid configuration "$(CFG)" specified.
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "imgtk.mak" CFG="imgtk - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "imgtk - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "imgtk - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 
!ERROR An invalid configuration is specified.
!ENDIF 

!IF "$(OS)" == "Windows_NT"
NULL=
!ELSE 
NULL=nul
!ENDIF 

CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "imgtk - Win32 Release"

OUTDIR=.\..\Release
INTDIR=.\Release
# Begin Custom Macros
OutDir=.\..\Release
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\imgtk.dll"

!ELSE 

ALL : "$(OUTDIR)\imgtk.dll"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\Blit.obj"
	-@erase "$(INTDIR)\Color.obj"
	-@erase "$(INTDIR)\ColorPalette.obj"
	-@erase "$(INTDIR)\Convert.obj"
	-@erase "$(INTDIR)\CTiff.obj"
	-@erase "$(INTDIR)\DataStream.obj"
	-@erase "$(INTDIR)\Dither.obj"
	-@erase "$(INTDIR)\Extract.obj"
	-@erase "$(INTDIR)\Histo.obj"
	-@erase "$(INTDIR)\Image.obj"
	-@erase "$(INTDIR)\ImageInfo.obj"
	-@erase "$(INTDIR)\ImgTkException.obj"
	-@erase "$(INTDIR)\Invert.obj"
	-@erase "$(INTDIR)\OptThr.obj"
	-@erase "$(INTDIR)\Position.obj"
	-@erase "$(INTDIR)\Quantize.obj"
	-@erase "$(INTDIR)\Rotate.obj"
	-@erase "$(INTDIR)\Scale.obj"
	-@erase "$(INTDIR)\Skew.obj"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(OUTDIR)\imgtk.dll"
	-@erase "$(OUTDIR)\imgtk.exp"
	-@erase "$(OUTDIR)\imgtk.lib"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MD /W3 /GX /O2 /I "..\tiff" /D "NDEBUG" /D "WIN32" /D\
 "_WINDOWS" /D "IMGTK_COMPILING" /Fp"$(INTDIR)\imgtk.pch" /YX"ImgTk.h"\
 /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Release/
CPP_SBRS=.
MTL_PROJ=/nologo /D "NDEBUG" /mktyplib203 /o NUL /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\imgtk.bsc" 
BSC32_SBRS= \
	
LINK32=link.exe
LINK32_FLAGS=..\Release\tiff.lib kernel32.lib user32.lib gdi32.lib winspool.lib\
 comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib\
 odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:no\
 /pdb:"$(OUTDIR)\imgtk.pdb" /machine:I386 /def:".\imgtk.def"\
 /out:"$(OUTDIR)\imgtk.dll" /implib:"$(OUTDIR)\imgtk.lib" 
DEF_FILE= \
	".\imgtk.def"
LINK32_OBJS= \
	"$(INTDIR)\Blit.obj" \
	"$(INTDIR)\Color.obj" \
	"$(INTDIR)\ColorPalette.obj" \
	"$(INTDIR)\Convert.obj" \
	"$(INTDIR)\CTiff.obj" \
	"$(INTDIR)\DataStream.obj" \
	"$(INTDIR)\Dither.obj" \
	"$(INTDIR)\Extract.obj" \
	"$(INTDIR)\Histo.obj" \
	"$(INTDIR)\Image.obj" \
	"$(INTDIR)\ImageInfo.obj" \
	"$(INTDIR)\ImgTkException.obj" \
	"$(INTDIR)\Invert.obj" \
	"$(INTDIR)\OptThr.obj" \
	"$(INTDIR)\Position.obj" \
	"$(INTDIR)\Quantize.obj" \
	"$(INTDIR)\Rotate.obj" \
	"$(INTDIR)\Scale.obj" \
	"$(INTDIR)\Skew.obj"

"$(OUTDIR)\imgtk.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

OUTDIR=.\..\Debug
INTDIR=.\Debug
# Begin Custom Macros
OutDir=.\..\Debug
# End Custom Macros

!IF "$(RECURSE)" == "0" 

ALL : "$(OUTDIR)\imgtk.dll" "$(OUTDIR)\imgtk.bsc"

!ELSE 

ALL : "$(OUTDIR)\imgtk.dll" "$(OUTDIR)\imgtk.bsc"

!ENDIF 

CLEAN :
	-@erase "$(INTDIR)\Blit.obj"
	-@erase "$(INTDIR)\Blit.sbr"
	-@erase "$(INTDIR)\Color.obj"
	-@erase "$(INTDIR)\Color.sbr"
	-@erase "$(INTDIR)\ColorPalette.obj"
	-@erase "$(INTDIR)\ColorPalette.sbr"
	-@erase "$(INTDIR)\Convert.obj"
	-@erase "$(INTDIR)\Convert.sbr"
	-@erase "$(INTDIR)\CTiff.obj"
	-@erase "$(INTDIR)\CTiff.sbr"
	-@erase "$(INTDIR)\DataStream.obj"
	-@erase "$(INTDIR)\DataStream.sbr"
	-@erase "$(INTDIR)\Dither.obj"
	-@erase "$(INTDIR)\Dither.sbr"
	-@erase "$(INTDIR)\Extract.obj"
	-@erase "$(INTDIR)\Extract.sbr"
	-@erase "$(INTDIR)\Histo.obj"
	-@erase "$(INTDIR)\Histo.sbr"
	-@erase "$(INTDIR)\Image.obj"
	-@erase "$(INTDIR)\Image.sbr"
	-@erase "$(INTDIR)\ImageInfo.obj"
	-@erase "$(INTDIR)\ImageInfo.sbr"
	-@erase "$(INTDIR)\ImgTkException.obj"
	-@erase "$(INTDIR)\ImgTkException.sbr"
	-@erase "$(INTDIR)\Invert.obj"
	-@erase "$(INTDIR)\Invert.sbr"
	-@erase "$(INTDIR)\OptThr.obj"
	-@erase "$(INTDIR)\OptThr.sbr"
	-@erase "$(INTDIR)\Position.obj"
	-@erase "$(INTDIR)\Position.sbr"
	-@erase "$(INTDIR)\Quantize.obj"
	-@erase "$(INTDIR)\Quantize.sbr"
	-@erase "$(INTDIR)\Rotate.obj"
	-@erase "$(INTDIR)\Rotate.sbr"
	-@erase "$(INTDIR)\Scale.obj"
	-@erase "$(INTDIR)\Scale.sbr"
	-@erase "$(INTDIR)\Skew.obj"
	-@erase "$(INTDIR)\Skew.sbr"
	-@erase "$(INTDIR)\vc50.idb"
	-@erase "$(INTDIR)\vc50.pdb"
	-@erase "$(OUTDIR)\imgtk.bsc"
	-@erase "$(OUTDIR)\imgtk.dll"
	-@erase "$(OUTDIR)\imgtk.exp"
	-@erase "$(OUTDIR)\imgtk.ilk"
	-@erase "$(OUTDIR)\imgtk.lib"
	-@erase "$(OUTDIR)\imgtk.pdb"

"$(OUTDIR)" :
    if not exist "$(OUTDIR)/$(NULL)" mkdir "$(OUTDIR)"

"$(INTDIR)" :
    if not exist "$(INTDIR)/$(NULL)" mkdir "$(INTDIR)"

CPP_PROJ=/nologo /MDd /W3 /Gm /Gi /GX /Zi /Od /Gy /I "..\tiff" /D "_DEBUG" /D\
 "WIN32" /D "_WINDOWS" /D "IMGTK_COMPILING" /FR"$(INTDIR)\\"\
 /Fp"$(INTDIR)\imgtk.pch" /YX"ImgTk.h" /Fo"$(INTDIR)\\" /Fd"$(INTDIR)\\" /FD /c 
CPP_OBJS=.\Debug/
CPP_SBRS=.\Debug/
MTL_PROJ=/nologo /D "_DEBUG" /mktyplib203 /o NUL /win32 
BSC32=bscmake.exe
BSC32_FLAGS=/nologo /o"$(OUTDIR)\imgtk.bsc" 
BSC32_SBRS= \
	"$(INTDIR)\Blit.sbr" \
	"$(INTDIR)\Color.sbr" \
	"$(INTDIR)\ColorPalette.sbr" \
	"$(INTDIR)\Convert.sbr" \
	"$(INTDIR)\CTiff.sbr" \
	"$(INTDIR)\DataStream.sbr" \
	"$(INTDIR)\Dither.sbr" \
	"$(INTDIR)\Extract.sbr" \
	"$(INTDIR)\Histo.sbr" \
	"$(INTDIR)\Image.sbr" \
	"$(INTDIR)\ImageInfo.sbr" \
	"$(INTDIR)\ImgTkException.sbr" \
	"$(INTDIR)\Invert.sbr" \
	"$(INTDIR)\OptThr.sbr" \
	"$(INTDIR)\Position.sbr" \
	"$(INTDIR)\Quantize.sbr" \
	"$(INTDIR)\Rotate.sbr" \
	"$(INTDIR)\Scale.sbr" \
	"$(INTDIR)\Skew.sbr"

"$(OUTDIR)\imgtk.bsc" : "$(OUTDIR)" $(BSC32_SBRS)
    $(BSC32) @<<
  $(BSC32_FLAGS) $(BSC32_SBRS)
<<

LINK32=link.exe
LINK32_FLAGS=..\Debug\tiff.lib kernel32.lib user32.lib gdi32.lib winspool.lib\
 comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib\
 odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /incremental:yes\
 /pdb:"$(OUTDIR)\imgtk.pdb" /debug /machine:I386 /def:".\imgtk.def"\
 /out:"$(OUTDIR)\imgtk.dll" /implib:"$(OUTDIR)\imgtk.lib" /pdbtype:sept 
DEF_FILE= \
	".\imgtk.def"
LINK32_OBJS= \
	"$(INTDIR)\Blit.obj" \
	"$(INTDIR)\Color.obj" \
	"$(INTDIR)\ColorPalette.obj" \
	"$(INTDIR)\Convert.obj" \
	"$(INTDIR)\CTiff.obj" \
	"$(INTDIR)\DataStream.obj" \
	"$(INTDIR)\Dither.obj" \
	"$(INTDIR)\Extract.obj" \
	"$(INTDIR)\Histo.obj" \
	"$(INTDIR)\Image.obj" \
	"$(INTDIR)\ImageInfo.obj" \
	"$(INTDIR)\ImgTkException.obj" \
	"$(INTDIR)\Invert.obj" \
	"$(INTDIR)\OptThr.obj" \
	"$(INTDIR)\Position.obj" \
	"$(INTDIR)\Quantize.obj" \
	"$(INTDIR)\Rotate.obj" \
	"$(INTDIR)\Scale.obj" \
	"$(INTDIR)\Skew.obj"

"$(OUTDIR)\imgtk.dll" : "$(OUTDIR)" $(DEF_FILE) $(LINK32_OBJS)
    $(LINK32) @<<
  $(LINK32_FLAGS) $(LINK32_OBJS)
<<

!ENDIF 

.c{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_OBJS)}.obj::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.c{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cpp{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<

.cxx{$(CPP_SBRS)}.sbr::
   $(CPP) @<<
   $(CPP_PROJ) $< 
<<


!IF "$(CFG)" == "imgtk - Win32 Release" || "$(CFG)" == "imgtk - Win32 Debug"
SOURCE=.\Blit.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_BLIT_=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Blit.obj" : $(SOURCE) $(DEP_CPP_BLIT_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_BLIT_=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Blit.obj"	"$(INTDIR)\Blit.sbr" : $(SOURCE) $(DEP_CPP_BLIT_)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Color.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_COLOR=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Color.obj" : $(SOURCE) $(DEP_CPP_COLOR) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_COLOR=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Color.obj"	"$(INTDIR)\Color.sbr" : $(SOURCE) $(DEP_CPP_COLOR)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\ColorPalette.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_COLORP=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\ColorPalette.obj" : $(SOURCE) $(DEP_CPP_COLORP) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_COLORP=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\ColorPalette.obj"	"$(INTDIR)\ColorPalette.sbr" : $(SOURCE)\
 $(DEP_CPP_COLORP) "$(INTDIR)"


!ENDIF 

SOURCE=.\Convert.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_CONVE=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Convert.obj" : $(SOURCE) $(DEP_CPP_CONVE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_CONVE=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Convert.obj"	"$(INTDIR)\Convert.sbr" : $(SOURCE) $(DEP_CPP_CONVE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\CTiff.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_CTIFF=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\CTiff.obj" : $(SOURCE) $(DEP_CPP_CTIFF) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_CTIFF=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\CTiff.obj"	"$(INTDIR)\CTiff.sbr" : $(SOURCE) $(DEP_CPP_CTIFF)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\DataStream.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_DATAS=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\DataStream.obj" : $(SOURCE) $(DEP_CPP_DATAS) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_DATAS=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\DataStream.obj"	"$(INTDIR)\DataStream.sbr" : $(SOURCE)\
 $(DEP_CPP_DATAS) "$(INTDIR)"


!ENDIF 

SOURCE=.\Dither.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_DITHE=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Dither.obj" : $(SOURCE) $(DEP_CPP_DITHE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_DITHE=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Dither.obj"	"$(INTDIR)\Dither.sbr" : $(SOURCE) $(DEP_CPP_DITHE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Extract.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_EXTRA=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Extract.obj" : $(SOURCE) $(DEP_CPP_EXTRA) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_EXTRA=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Extract.obj"	"$(INTDIR)\Extract.sbr" : $(SOURCE) $(DEP_CPP_EXTRA)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Histo.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_HISTO=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Histo.obj" : $(SOURCE) $(DEP_CPP_HISTO) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_HISTO=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Histo.obj"	"$(INTDIR)\Histo.sbr" : $(SOURCE) $(DEP_CPP_HISTO)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Image.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_IMAGE=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Image.obj" : $(SOURCE) $(DEP_CPP_IMAGE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_IMAGE=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Image.obj"	"$(INTDIR)\Image.sbr" : $(SOURCE) $(DEP_CPP_IMAGE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\ImageInfo.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_IMAGEI=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\ImageInfo.obj" : $(SOURCE) $(DEP_CPP_IMAGEI) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_IMAGEI=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\ImageInfo.obj"	"$(INTDIR)\ImageInfo.sbr" : $(SOURCE)\
 $(DEP_CPP_IMAGEI) "$(INTDIR)"


!ENDIF 

SOURCE=.\ImgTkException.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_IMGTK=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\ImgTkException.obj" : $(SOURCE) $(DEP_CPP_IMGTK) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_IMGTK=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\ImgTkException.obj"	"$(INTDIR)\ImgTkException.sbr" : $(SOURCE)\
 $(DEP_CPP_IMGTK) "$(INTDIR)"


!ENDIF 

SOURCE=.\Invert.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_INVER=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Invert.obj" : $(SOURCE) $(DEP_CPP_INVER) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_INVER=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Invert.obj"	"$(INTDIR)\Invert.sbr" : $(SOURCE) $(DEP_CPP_INVER)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\OptThr.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_OPTTH=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\OptThr.obj" : $(SOURCE) $(DEP_CPP_OPTTH) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_OPTTH=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\OptThr.obj"	"$(INTDIR)\OptThr.sbr" : $(SOURCE) $(DEP_CPP_OPTTH)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Position.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_POSIT=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Position.obj" : $(SOURCE) $(DEP_CPP_POSIT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_POSIT=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Position.obj"	"$(INTDIR)\Position.sbr" : $(SOURCE) $(DEP_CPP_POSIT)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Quantize.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_QUANT=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Quantize.obj" : $(SOURCE) $(DEP_CPP_QUANT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_QUANT=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Quantize.obj"	"$(INTDIR)\Quantize.sbr" : $(SOURCE) $(DEP_CPP_QUANT)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Rotate.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_ROTAT=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Rotate.obj" : $(SOURCE) $(DEP_CPP_ROTAT) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_ROTAT=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Rotate.obj"	"$(INTDIR)\Rotate.sbr" : $(SOURCE) $(DEP_CPP_ROTAT)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Scale.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_SCALE=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Scale.obj" : $(SOURCE) $(DEP_CPP_SCALE) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_SCALE=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Scale.obj"	"$(INTDIR)\Scale.sbr" : $(SOURCE) $(DEP_CPP_SCALE)\
 "$(INTDIR)"


!ENDIF 

SOURCE=.\Skew.cpp

!IF  "$(CFG)" == "imgtk - Win32 Release"

DEP_CPP_SKEW_=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Skew.obj" : $(SOURCE) $(DEP_CPP_SKEW_) "$(INTDIR)"


!ELSEIF  "$(CFG)" == "imgtk - Win32 Debug"

DEP_CPP_SKEW_=\
	"..\tiff\tiff.h"\
	"..\tiff\tiffio.h"\
	".\Color.h"\
	".\ColorPalette.h"\
	".\CTiff.h"\
	".\DataStream.h"\
	".\Histo.h"\
	".\Image.h"\
	".\ImageInfo.h"\
	".\ImageIterator.h"\
	".\ImgTk.h"\
	".\ImgTkException.h"\
	".\Position.h"\
	

"$(INTDIR)\Skew.obj"	"$(INTDIR)\Skew.sbr" : $(SOURCE) $(DEP_CPP_SKEW_)\
 "$(INTDIR)"


!ENDIF 


!ENDIF 

