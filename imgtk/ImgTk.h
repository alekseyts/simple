//////////////////////////////////////////////////////////////////////
// @doc IMAGE
// ImgTk.h: All the de'finitions for imaging ToolKit
//
//////////////////////////////////////////////////////////////////////

#if !defined(__IMGTK_H)
#define __IMGTK_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/* A moins que l'on demande spe'cifiquement une imple'mentation
   ge'ne'rique, on compile en utilisant l'imple'mentation spe'cifique
   a` la plateforme de compilation */
#if !defined(_GENERIC_IMPLEMENTATION)
#if defined(WIN32)
#define _WIN32_IMPLEMENTATION 
#endif /* defined(WIN32) */
#endif /* !defined(_GENERIC_IMPLEMENTATION) */

/* Si aucune plateforme spe'cifique n'a e'te' de'tecte'e, on utilise
   l'imple'mentation ge'ne'rique */
#if !defined(_WIN32_IMPLEMENTATION) && !defined(_GENERIC_IMPLEMENTATION)
#define _GENERIC_IMPLEMENTATION
#endif /* !defined(_WIN32_IMPLEMENTATION) */

#if defined(_WIN32_IMPLEMENTATION)
// definition needed for including files like windef.h
// without including the whole huge windows.h

#ifndef WINVER
// only Windows 2000/XP/2003 is supported
#define WINVER 0x0500
#endif

#if !defined(_68K_) && !defined(_MPPC_) && !defined(_PPC_) && !defined(_ALPHA_) && !defined(_MIPS_) && !defined(_X86_) && defined(_M_IX86)
#define _X86_
#endif

#if !defined(_68K_) && !defined(_MPPC_) && !defined(_PPC_) && !defined(_ALPHA_) && !defined(_X86_) && !defined(_MIPS_) && defined(_M_MRX000)
#define _MIPS_
#endif

#if !defined(_68K_) && !defined(_MPPC_) && !defined(_PPC_) && !defined(_ALPHA_) && !defined(_X86_) && !defined(_MIPS_) && defined(_M_ALPHA)
#define _ALPHA_
#endif

#if !defined(_68K_) && !defined(_MPPC_) && !defined(_PPC_) && !defined(_ALPHA_) && !defined(_X86_) && !defined(_MIPS_) && defined(_M_PPC)
#define _PPC_
#endif

#if !defined(_68K_) && !defined(_MPPC_) && !defined(_PPC_) && !defined(_ALPHA_) && !defined(_X86_) && !defined(_MIPS_) && defined(_M_M68K)
#define _68K_
#endif

#if !defined(_68K_) && !defined(_MPPC_) && !defined(_PPC_) && !defined(_ALPHA_) && !defined(_X86_) && !defined(_MIPS_) && defined(_M_MPPC)
#define _MPPC_
#endif

#ifndef _MAC
#if defined(_68K_) || defined(_MPPC_)
#define _MAC
#endif
#endif
#endif // defined(_WIN32_IMPLEMENTATION)


#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


#if !defined(M_PI)
#define M_PI       3.1415926535897932385
#endif
#if !defined(M_PI_2)
#define M_PI_2     1.5707963267948966192
#endif
#if !defined(M_PI_4)
#define M_PI_4     7.8539816339744830962E-1
#endif
#if !defined(M_SQRT2)
#define M_SQRT2    1.4142135623730950488
#endif
#if !defined(M_SQRT1_2)
#define M_SQRT1_2  7.0710678118654752440E-1
#endif


/* la macro pour exporter des symbo^les de la librairie (IMGTK_EXPORT) 
   et le type des fonctions exporte's (IMGTK_API) */   
#if defined(WIN32) /* on compile pour Windows 32 bits ? */
#if defined(_MSC_VER) /* compilateur MicroSoft ? */
#if defined(IMGTK_COMPILING) /* compilation de la DLL */
#define IMGTK_EXPORT __declspec(dllexport) 
#else
#define IMGTK_EXPORT __declspec(dllimport) 
#endif /* defined(IMGTK_COMPILING) */
#define IMGTK_API __stdcall
#else /* Autres compilateurs */
#define IMGTK_EXPORT export
#define IMGTK_API pascal
#endif /* _MSC_VER */
#else /* WIN32 */
#define IMGTK_EXPORT
#define IMGTK_API
#endif /* WIN32 */


//@type TSample | Type for an image sample between 0 and 255
typedef unsigned char TSample;

//@type TFSample | Type for an image sample stored as a real number
typedef float TFSample;

//@type TByte | Type for a chunk of 8 bits
typedef unsigned char TByte;

//@type TBitmap | Type for a pointer to a bitmap
typedef unsigned char * TBitmap;

//@type TCBitmap | Type for a constant pointer to a bitmap
typedef const unsigned char * TCBitmap;

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 


#endif // !defined(__IMGTK_H)
