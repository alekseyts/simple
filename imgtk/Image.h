//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#if !defined(__IMAGE_H)
#define __IMAGE_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ImgTk.h"
#include "ImageInfo.h"
#include "ImageException.h"

class CInputBinaryFile;
class COutputBinaryFile;

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

class CImageIterator;


//@class Image
class IMGTK_EXPORT CImage 
{
public:
  //@cmember,menum Color reduction methods
  enum EColorReductionMethod {
      Thresholding,         // @@emem Thresholding
      FloydSteinberg,       // @@emem Error diffusion using Floyd-Steinberg       
      Dither8,              // @@emem Dithering matrix 8x8 
      Cluster3,             // @@emem Cluster 3x3 
      Cluster4,             // @@emem Cluster 4x4 
      Cluster8,             // @@emem Cluster 8x8 
      LineArt,              // @@emem LineArt 1 
      LineArt2,             // @@emem LineArt 2 
      LineArt3,             // @@emem LineArt 3 
      Hilbert               // @@emem Hilbert's snakes 
  };

public: // public methods

  //@access Constructors & destructor	
  //@cmember Default constructor
  CImage();
  //@cmember Constructor
  CImage(const CImageInfo &imgInfo);
  //@cmember Constructor
  CImage(int width,int height,CImageInfo::EImageType type,int xres = CImageInfo::DefaultYResolution,int yres = CImageInfo::DefaultYResolution, CColorPalette *palette = NULL);
  //@cmember Constructor
  CImage(const CImage &img);
  
#if defined(_WIN32_IMPLEMENTATION)
  //@cmember Constructor from a DIB
  CImage(HGLOBAL hDib);
  //@cmember Constructor from a DDB
  CImage(HBITMAP hBmp, HPALETTE hPal = NULL, int xres = CImageInfo::DefaultYResolution,
		int yres = CImageInfo::DefaultYResolution);  
#endif
  //@cmember Destructor
  ~CImage();

	IMGTK_EXPORT friend bool operator== (CImage const& lh, CImage const& rh);
	IMGTK_EXPORT friend bool operator!= (CImage const& lh, CImage const& rh) {
		return !(lh == rh);
	}

  //@access Properties accessors
  //@cmember,mfunc Get associated image info block of type <c CImageInfo >
  const CImageInfo & GetImageInfo() const { return mInfo; }
  //@cmember,mfunc Get image width
  int GetWidth()   const { return mInfo.GetWidth(); }
  //@cmember,mfunc Get image height
  int GetHeight()  const { return mInfo.GetHeight(); }
  //@cmember,mfunc Get image type (cf. <t EImageType>)
  EImageType GeEImageType() const { return mInfo.GeEImageType(); }
  //@cmember,mfunc Get horizontal resolution in DPI
  int GetXResolution() const { return mInfo.GetXResolution(); }
  //@cmember,mfunc Get vertical resolution in DPI
  int GetYResolution() const { return mInfo.GetYResolution(); }
  //@cmember,mfunc Set horizontal resolution in DPI
  void SetXResolution(int val) { mInfo.SetXResolution(val); }
  //@cmember,mfunc Set vertical resolution in DPI
  void SetYResolution(int val) { mInfo.SetYResolution(val); }
  //@cmember,mfunc Get a constant pointer to the associated color palette, if any
  const CColorPalette *GetColorPalette() const { return mInfo.GetColorPalette(); }
  //@cmember,mfunc Get associated color palette, if any
  CColorPalette *GetColorPalette() { return mInfo.GetColorPalette(); }
  //@cmember,mfunc Set associated color palette
  void SetColorPalette(CColorPalette *palette) {
    mInfo.SetColorPalette(palette);
  }
  //@cmember,mfunc Get the size in bytes of an image row
  int GetRowSize() const { return mInfo.GetRowSize(); }
  //@cmember,mfunc Get the size in byte for the whole bitmap
  int GetBitmapSize() const { return GetRowSize()*GetHeight(); }
  //@cmember,mfunc Get the number of bits used for storing a pixel
  int GetBitCount() const { return mInfo.GetBitCount(); }
  //@cmember,mfunc Get the number of bits used to store a sample
  int GetBitsPerSample() const  { return mInfo.GetBitsPerSample(); }
  //@cmember,mfunc Get the number of samples per pixel
  int GetSamplesPerPixel() const  { return mInfo.GetSamplesPerPixel(); }
    
#if defined(_WIN32_IMPLEMENTATION)
  //@cmember,mfunc Get the HBITMAP object associated with the image, if any
  HBITMAP GetHBitmap() const { return mHBitmap; }
#endif
  //@cmember,mfunc Get a constant pointer to the bitmap bits
  TCBitmap GetBitmap() const { return mBitmap; }
  //@cmember,mfunc Get a pointer to the bitmap bits
  TBitmap GetBitmap() { return mBitmap; }
  //@cmember,mfunc Is it a standard image ?
  bool IsStandard() const { return mInfo.IsStandard(); }
  //@cmember,mfunc Get a constant pointer to the bits encoding the nth row of an standard image
  TCBitmap GetRow(int n) const { 
	  if ((n < 0) || (n >= GetHeight())) {
		  throw CImageException("GetRow", "Incorrect row number");
	  }
    assert(IsStandard());  
#if defined(_WIN32_IMPLEMENTATION)
    return mFirstRow-n*GetRowSize();
#else
    return mBitmap+n*GetRowSize();
#endif
  }
  //@cmember,mfunc Get a pointer to the bits encoding the nth row of an standard image
  TBitmap GetRow(int n) { 
    if ((n < 0) || (n >= GetHeight())) {
		throw CImageException("GetRow", "Incorrect row number");
	}
    assert(IsStandard());  
#if defined(_WIN32_IMPLEMENTATION)
    return mFirstRow-n*GetRowSize();
#else
    return mBitmap+n*GetRowSize();
#endif
  }
  //@cmember,mfunc Get a constant pointer to the bits encoding the nth row of an float image
  const TFSample *GetFRow(int n) const {
	if ((n < 0) || (n >= GetHeight())) {
		throw CImageException("GetRow", "Incorrect row number");
	}
    assert(!IsStandard());  
    return (TFSample *) (mBitmap+n*GetRowSize());
  }
  //@cmember,mfunc Get a pointer to the bits encoding the nth row of an float image
  TFSample *GetFRow(int n)  {
	if ((n < 0) || (n >= GetHeight())) {
		throw CImageException("GetRow", "Incorrect row number");
	}
    assert(!IsStandard());  
    return (TFSample *) (mBitmap+n*GetRowSize());
  }
  //@cmember,mfunc Get the number of bytes between two rows
  int GetRowOffset() const {
#if defined(_WIN32_IMPLEMENTATION)
    if (IsStandard())
      return -GetRowSize();
    else
      return GetRowSize();
#else
    return GetRowSize();
#endif
  }
  
  //@access Basic operations
  //@cmember,mfunc Copy the resolution values from another image
  void CopyResolution(const CImage &img) { 
    SetXResolution(img.GetXResolution());
    SetYResolution(img.GetYResolution());
  }

  //@cmember Get the luminosity at a given point
  TSample   GetLuma	(int x, int y) const;
  //@cmember Get the luminosity at a given point as a real sample
  TFSample  GetFLuma	(int x, int y) const;
  //@cmember Set the luminosity at a given point
  void	  SetLuma	(int x, int y, TSample lum);
  //@cmember Set the luminosity at a given point as a real sample
  void	  SetFLuma	(int x, int y, TFSample lum);
  //@cmember Get the color at a given point
  void	  GetColor (int x, int y, CColor &color) const ;
  //@cmember Get the color at a given point, as real samples
  void	  GetFColor(int x, int y, TFSample &r, TFSample &g, TFSample &b) const;
  //@cmember Set the color at a given point
  void	  SetColor (int x, int y, const CColor &color);
  //@cmember Set the color at a given point, as real samples
  void	  SetFColor(int x, int y, TFSample r, TFSample g, TFSample b);  
  //@cmember Fill the whole image with a given luminosity (default, white)
  void	  Erase(TSample luma = 255);
  //@cmember Fill the whole image with a given color 
  void	  Erase(const CColor &color);
  //@cmember blit an image
  void Blit(int x,int y,const CImage *src);  
  //@cmember blit a portion of an image
  void Blit(int x,int y,const CImage *src,int xSrc,int ySrc,int widthSrc,int heightSrc);
  ////@access I/O
  ////@cmember Reads an image from a stream
  //void ReadFromStream (istream& stream);
  ////@cmember Write an image to a stream
  //void WriteToStream  (ostream& stream) const;

	void ReadFromFile(CInputBinaryFile& ibf);
	void ReadFromFile(shared_ptr<CInputBinFile> ibf);
	void IgnoreRead(CInputBinaryFile& ibf);
	void IgnoreRead(shared_ptr<CInputBinFile> ibf);
	void WriteToFile(COutputBinaryFile& obf) const;
	void WriteToFile(shared_ptr<COutputBinFile> obf) const;

	void WriteAsTIFF(std::string fileName);

  //@access Convertions to system images
#if defined(_WIN32_IMPLEMENTATION)
  //@cmember Build a BITMAPINFOHEADER structure from an image
  LPBITMAPINFOHEADER ConvertToBitmapInfoHeader() const;
  //@cmember Build a DIB structure from an image
  HGLOBAL	ConvertToDIB() const;
  //@cmember Build a HBITMAP structure from an image
  HBITMAP	ConvertToDDB() const;
#endif
  
  //@access Operations

  //@cmember Invertion
  void Invert();
  //@cmember Compute optimal threshold for converting an image to a bilevel image
  TSample OptimalThreshold() const;
   //@cmember Convert an image
  CImage *Convert(EImageType newtype) const;
  
#if defined(_WIN32_IMPLEMENTATION)
  void RGBToBGR();
#endif

  //@cmember Rotation
  CImage *Rotate(double angle,const CColor &fill = CColor(255,255,255) ) const;

  //@cmember Convert and resize an image
  CImage *ConvertAndResize(EImageType newtype,  int neww, int newh) const;
    //@cmember Resize an image
  CImage *Resize(int neww, int newh) const;

  //@cmember Skewing
  double FindSkewing() const;

  //@cmember Extraction
  CImage *ExtractArea(int x,int y,int w,int h) const;
  //@cmember Extract an image area
  CImage *Crop(TSample threshold = 255) const;
  //@cmember get extract area coordinates
  void GetExtractAreaCoordinates(int& startx, int& starty, int& neww, int& newh, TSample threshold = 255) const;
  //@member do thinning
  CImage *Thin() const; 
  //@member do double image in horiz
  CImage *DoubleHorizontal() const;  


  //@cmember Create an image iterator
  CImageIterator *CreateIterator() const;
public: //@access operators
  //@cmember assignment operator
  const CImage& operator= (const CImage& img);    

public: //@access public class methods
  //@cmember Specify the color reduction method to use
  static EColorReductionMethod UseColorReductionMethod(EColorReductionMethod method);
  //@cmember Specify the threshold value to use
  static TSample UseThreshold(TSample threshold);  
  //@cmember "Cast" a real sample to a normal sample
  static TSample CLUMP(TFSample v) {
    return v<(TFSample)0.0 ? 0 : (v>=(TFSample)256.0 ? 255 : (TSample) v);
  }
#if defined(_WIN32_IMPLEMENTATION)
  //@cmember,mfunc Use DIB sections or not
  static bool UseDIBSections(bool flag) { 
    bool oldMode=mUseDIBSections;
    mUseDIBSections = flag; 
    return oldMode;
  }
#endif

public: // public class fields
  static const int ROffset;
  static const int GOffset;
  static const int BOffset;

  static const int FROffset;
  static const int FGOffset;
  static const int FBOffset;

private: // instance fields

  CImageInfo mInfo;
  TBitmap mBitmap;

#if defined(_WIN32_IMPLEMENTATION)
  TBitmap mFirstRow;
  HBITMAP mHBitmap;
#endif

private: // private methods  
  void Init();
  void Free();
  void Duplicate(const CImage &image);
  void AllocBitmap();
#if defined(_WIN32_IMPLEMENTATION)
  void AllocDIBSection();
#endif
  void AllocMemory();
  void InvertBW();
  void Invert4();
  void Invert8();
  void InvertFloat();
  CImage *FSDithering(TSample Threshold) const;
  CImage *MatrixDithering(const int *matrix, int wsize, int hsize, int maxval, TSample threshold) const;
  
  CImage *HilbertDithering() const;
  CImage *Dither(EColorReductionMethod algo,TSample threshold) const;
  
  CImage *GRAY16ToBW() const;
  CImage *GRAY256ToBW() const;
  CImage *COLOR16ToBW() const;
  CImage *COLOR256ToBW() const;
  CImage *RGBToBW() const;
  CImage *GRAYFLOATToBW() const;
  CImage *RGBFLOATToBW() const;
  
  CImage *BWToGRAY16() const;
  CImage *GRAY256ToGRAY16() const;
  CImage *COLOR16ToGRAY16() const;
  CImage *COLOR256ToGRAY16() const;
  CImage *RGBToGRAY16() const;
  CImage *GRAYFLOATToGRAY16() const;
  CImage *RGBFLOATToGRAY16() const;
  
  CImage *GRAY16ToGRAY256() const;
  CImage *BWToGRAY256() const;
  CImage *COLOR16ToGRAY256() const;
  CImage *COLOR256ToGRAY256() const;
  CImage *RGBToGRAY256() const;
  CImage *GRAYFLOATToGRAY256() const;
  CImage *RGBFLOATToGRAY256() const;
    
  CImage *GRAY16ToCOLOR16() const;
  CImage *GRAY256ToCOLOR16() const;
  CImage *BWToCOLOR16() const;
  CImage *COLOR256ToCOLOR16() const;
  CImage *RGBToCOLOR16() const;
  CImage *RGBToCOLOR16Dither() const;
  CImage *GRAYFLOATToCOLOR16() const;
  CImage *RGBFLOATToCOLOR16() const;
  CImage *RGBFLOATToCOLOR16Dither() const;
  
  CImage *GRAY16ToCOLOR256() const;
  CImage *GRAY256ToCOLOR256() const;
  CImage *COLOR16ToCOLOR256() const;
  CImage *BWToCOLOR256() const;
  CImage *RGBToCOLOR256() const;
  CImage *RGBToCOLOR256Dither() const;
  CImage *GRAYFLOATToCOLOR256() const;
  CImage *RGBFLOATToCOLOR256() const;
  CImage *RGBFLOATToCOLOR256Dither() const;
  
  CImage *GRAY16ToRGB() const;
  CImage *GRAY256ToRGB() const;
  CImage *COLOR16ToRGB() const;
  CImage *COLOR256ToRGB() const;
  CImage *BWToRGB() const;
  CImage *GRAYFLOATToRGB() const;
  CImage *RGBFLOATToRGB() const;
  
  CImage *GRAY16ToGRAYFLOAT() const;
  CImage *GRAY256ToGRAYFLOAT() const; 
  CImage *COLOR16ToGRAYFLOAT() const;
  CImage *COLOR256ToGRAYFLOAT() const;
  CImage *BWToGRAYFLOAT() const;
  CImage *RGBToGRAYFLOAT() const;
  CImage *RGBFLOATToGRAYFLOAT() const;
  
  CImage *GRAY16ToRGBFLOAT() const;
  CImage *GRAY256ToRGBFLOAT() const;
  CImage *COLOR16ToRGBFLOAT() const;
  CImage *COLOR256ToRGBFLOAT() const;
  CImage *BWToRGBFLOAT() const;
  CImage *RGBToRGBFLOAT() const;
  CImage *GRAYFLOATToRGBFLOAT() const;

  CImage *ToDither(EColorReductionMethod algo) const;
  CImage *ToBW(EColorReductionMethod algo) const;
  CImage *ToGRAY16() const;
  CImage *ToGRAY256() const;
  CImage *ToCOLOR16(EColorReductionMethod algo) const;
  CImage *ToCOLOR256(EColorReductionMethod algo) const;
  CImage *ToRGB() const;
  CImage *ToGRAYFLOAT() const;
  CImage *ToRGBFLOAT() const;
  
  CImage *Quantize(int,bool) const;	

  // Optimal thresholding calculus
  void ComputePg(double *pg) const;
  void BWNormalize();
  void BWDenormalize();

  // Resize
  CImage *HalfSizeToGrayscale() const;
  CImage *ShrinkBWToGRAY256(int neww,int newh) const;
  CImage *ResizeGRAY256(int neww,int newh) const;
  CImage *ResizeGRAYFLOAT(int neww,int newh) const;
  CImage *ResizeRGB(int neww,int newh) const;
  CImage *ResizeCOLORFLOAT(int neww,int newh) const;
  CImage *TransformAux(EImageType tp,int neww,int newh) const;

  // Rotation
  CImage *Rotate90() const;
  CImage *Rotatem90() const;
  CImage *Rotate180() const;
  CImage *FastRotateBW(double angle) const;

  // Extract
  CImage *ExtractAreaOther(int x,int y,int w,int h) const;
  CImage *ExtractAreaNibble(int x,int y,int w,int h) const;
  CImage *ExtractAreaBilevel(int x,int y,int w,int h) const;
  CImage *CropBilevel() const;
  CImage *CropGray256(TSample threshold) const;
  CImage *CropOther(TSample threshold) const;

  CImage *Thinning() const; 

  void GetExtractAreaCoordinatesBilevel(int& startx, int& starty, int& neww, int& newh) const;
  void GetExtractAreaCoordinatesGray256(int& startx, int& starty, int& neww, int& newh, TSample threshold) const;
  void GetExtractAreaCoordinatesOther(int& startx, int& starty, int& neww, int& newh, TSample threshold) const;
    
private: // private class fields

  static EColorReductionMethod mColorReductionMethod;
  static TSample mThreshold;
  // Dithering matrices
  static int dither6[8][8];
  static int dither8[16][16];
  static int cluster3[6][6];
  static int cluster4[8][8];
  static int cluster8[16][16];
  static int lineart[7][1];
  static int lineart2[6][6];
  static int lineart3[4][4];
#if defined(_WIN32_IMPLEMENTATION)
  static bool mUseDIBSections;
#endif  


private: // private class methods

#if defined(_WIN32_IMPLEMENTATION)
  // DIB Conversion
  static TSample ApplyMask(DWORD val,DWORD mask,int maskstart,int maskend);
  static void MaskLimits(DWORD mask,int *pstart,int *pend);
  static int WidthBytes(int bits);
#endif

  // Optimal thresholding calculus
  static double ComputeMicro0(int T,const double *pg);
  static double ComputeMicro1(int T,const double *pg);
  static void ComputeMicro(const double *pg,double *micro0,double *micro1);
  static double ComputeEx(const double *pg);
  static double ComputeExx(const double *pg);
  static double ComputeEy(const double *pg,const double *micro0,const double *micro1,int T);
  static double ComputeEyy(const double *pg,const double *micro0,const double *micro1,int T);
  static double ComputeExy(const double *pg,const double *micro0,const double *micro1,int T);  
  static double ComputeCorrelation(int T,const double *pg,const double *micro0,const double *micro1,double Ex,double Vx);

  //checks whether specified point is inside image; if not this method throws CImageException
  void CheckPointInside(const string& sData, const int& x, const int& y) const;

  enum ECreationState {
	  CS_NotUsingDIBSection,
	  CS_UsingDIBSection
  };

  //////////////////////////////////////////////////////////////////////////
  // Member m_eCreationState stores state of mUseDIBSections, 
  // which was setted at the moment of this CImage object creation.
  // (It is necessary to store it, because destructor and method Free depend on this value. We need to know how
  // mBitmap member was created - via new[] operator or via CreateDIBSection in order to use appropriate way 
  // to destroy mBitmap. Such solution is not good, but changing mUseDIBSections and mBitmap implementation is 
  // labour-intensive and cannot be provided now, because a lot of code use this mechanism.)
  ECreationState m_eCreationState;
};

typedef CImage::EColorReductionMethod EColorReductionMethod;

//IMGTK_EXPORT istream& operator>> (istream& s, CImage& o);
//IMGTK_EXPORT ostream& operator<< (ostream& s, const CImage& o);

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__IMAGE_H)
