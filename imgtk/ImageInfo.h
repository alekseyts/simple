//////////////////////////////////////////////////////////////////////
// @doc IMAGEINFO

#if !defined(__IMAGE_INFO_H)
#define __IMAGE_INFO_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ImgTk.h"
#include "ColorPalette.h"
#include <iostream>

class CInputBinaryFile;
class COutputBinaryFile;

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


//@class Image information block
class IMGTK_EXPORT CImageInfo
{
public:
  //@cmember,menum Image types
  enum EImageType {
    Bilevel	= 0,  //@@emem Black and white 
      Gray16	= 1,  //@@emem 16 levels of gray
      Gray256	= 2,  //@@emem 256 levels of gray
      Color16	= 3,  //@@emem 16 colors
      Color256	= 4,  //@@emem 256 colors 
      ColorRGB	= 5,  //@@emem 24 bits True color
      GrayFloat	= 6,  //@@emem Grayscale with real samples 
      ColorFloat= 7,  //@@emem Color with real samples 
  };
public: //@access Constructors & Destructors
  //@cmember Constructor
  CImageInfo();
  //@cmember Constructor
  CImageInfo(int width,int height, EImageType type, int xres = CImageInfo::DefaultXResolution, 
		int yres = CImageInfo::DefaultYResolution, CColorPalette *palette = NULL);
  //@cmember Copy constructor
  CImageInfo(const CImageInfo& info) ;
  //@cmember Destructor
  ~CImageInfo();
  
public: //@access Accessors
  //@cmember Get image width 
  int GetWidth() const { return mWidth; }
  //@cmember Get image height
  int GetHeight() const { return mHeight; }
  //@cmember Get image type (cf. <t EImageType>)
  EImageType GeEImageType() const { return mType; }
  //@cmember Get horizontal resolution in DPI
  int GetXResolution() const { return mXResolution; }
  //@cmember Get vertical resolution in DPI
  int GetYResolution() const { return mYResolution; }  
  //@cmember Get a constant pointer to the associated color palette, if any
  const CColorPalette *GetColorPalette() const { return mPalette; }
  //@cmember Get associated color palette, if any
  CColorPalette *GetColorPalette() { return mPalette; }
  //@cmember get the size in bytes of an image row
  int GetRowSize() const { return mRowSize; }
  //@cmember Get the number of bits used for storing a pixel
  int GetBitCount() const { return mBitCount; }
  //@cmember Get the number of bits used to store a sample
  int GetBitsPerSample() const { return GetBitCount()/GetSamplesPerPixel(); } 
  //@cmember Get the number of samples per pixel
  int GetSamplesPerPixel() const;  
  //@cmember Set horizontal resolution in DPI
  void SetXResolution(int value) { mXResolution=value; Init(mWidth, mHeight, mType, mXResolution, mYResolution, mPalette); }
  //@cmember Set vertical resolution in DPI
  void SetYResolution(int value) { mYResolution=value; Init(mWidth, mHeight, mType, mXResolution, mYResolution, mPalette); }
  //@cmember Is it a standard image ?  
  bool IsStandard() const { return GeEImageType()<GrayFloat; }
  
public: // Operations
#if defined(_WIN32_IMPLEMENTATION)
  //@cmember Build a BITMAPINFOHEADER structure from an image info block
  LPBITMAPINFOHEADER ConvertToBitmapInfoHeader() const;  
#endif

    void ConfinedRead(shared_ptr<CInputBinFile> ibf);
	void ReadFromFile(CInputBinaryFile& ibf);
	void ReadFromFile(shared_ptr<CInputBinFile> ibf);
	void WriteToFile(COutputBinaryFile& obf) const;
	void WriteToFile(shared_ptr<COutputBinFile> obf) const;

  ////@cmember Reads an image information from a stream
  //void ReadFromStream (istream& stream);
  ////@cmember Write an image information to a stream
  //void WriteToStream  (ostream& stream) const;
  
public: // Operators
  //@cmember assignment operator
  const CImageInfo&  operator= (const CImageInfo& info);

  IMGTK_EXPORT friend bool operator== (CImageInfo const& lh, CImageInfo const& rh);

public: // Class data
  
  static int DefaultXResolution;
  static int DefaultYResolution;
  
protected:
  
  void SetWidth(int value) { mWidth=value; Init(mWidth, mHeight, mType, mXResolution, mYResolution, mPalette); }  
  void SetHeight(int value) { mHeight=value; Init(mWidth, mHeight, mType, mXResolution, mYResolution, mPalette); }  
  void SeEImageType(EImageType value) { mType=value; Init(mWidth, mHeight, mType, mXResolution, mYResolution, mPalette); }
  void SetColorPalette(CColorPalette *palette)  { 
    if (mPalette) delete mPalette;
    mPalette=palette; 
	Init(mWidth, mHeight, mType, mXResolution, mYResolution, mPalette);
  }

private: // Private members
  
  int mWidth;
  int mHeight;
  EImageType mType;
  int mXResolution;
  int mYResolution;
  CColorPalette *mPalette;
  int mBitCount;
  int mRowSize;

private: // Private methods

  int GetNumColors() const ;
  friend class	CImage;
  void Init(int width,int height,EImageType type,int xres,int yres, CColorPalette *palette);
  void Free();
  void Duplicate(const CImageInfo& palette);
};

typedef CImageInfo::EImageType EImageType;

//IMGTK_EXPORT istream& operator>> (istream& s, CImageInfo& o);
//IMGTK_EXPORT ostream& operator<< (ostream& s, const CImageInfo& o);
//
//IMGTK_EXPORT istream& operator>> (istream& s, CImageInfo::EImageType& type);
//IMGTK_EXPORT ostream& operator<< (ostream& s, CImageInfo::EImageType type);
//
#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__IMAGE_INFO_H)

