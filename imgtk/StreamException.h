//////////////////////////////////////////////////////////////////////
// @doc IMAGE
#if !defined(__STREAM_EXCEPTION_H)
#define __STREAM_EXCEPTION_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif

#if !defined(__IMGTK_EXCEPTION_H)
#include "ImgTkException.h"
#endif

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


//@class Image exception
//@base public | CImgTkException
class IMGTK_EXPORT CStreamException : public CImgTkException {
public:
  CStreamException(const char *data, const char *message) :
    CImgTkException(data, message) {};
    
};

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__STREAM_EXCEPTION_H)
