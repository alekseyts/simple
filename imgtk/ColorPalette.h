//////////////////////////////////////////////////////////////////////
// @doc COLORPALETTE

#if !defined(__COLOR_PALETTE_H)
#define __COLOR_PALETTE_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ImgTk.h"
#include "Color.h"
#include <iostream>
#include <cassert>
#include <CFileBinary.h>
#include <CRSmartp.h>

#if defined(_WIN32_IMPLEMENTATION) && !defined(__WINDOWS_H)
#include <windows.h>
#define __WINDOWS_H
#endif

#if defined(_WIN32_IMPLEMENTATION) && !defined(__WINDEF_H)
#include <windef.h> // for HPALETTE
#define __WINDEF_H
#endif

class CInputBinaryFile;
class COutputBinaryFile;

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

using namespace std;

//@class This class implements a color palette. A color palette is mainly used
// by 16 color and 256 color images
class IMGTK_EXPORT CColorPalette 
{
public: //@access Constructors & destructors
  //@cmember  Default constructor
  CColorPalette(); 
  //@cmember  Constructor for a CColorPalette object  
  CColorPalette(int size); 
  //@cmember  Constructor for a CColorPalette object  
  CColorPalette(HPALETTE hPal);
  //@cmember  Copy Constructor   
  CColorPalette(const CColorPalette& palette);
  //@cmember Destructor 
  ~CColorPalette();

public: //@access Accessors
  
  //@cmember,mfunc Returns a constant reference on the i th color  
  const CColor& GetColor(int i) const {
    assert(i>=0 && i<mSize);
    return mColors[i];
  }
  //@cmember,mfunc Returns a reference on the i th color  
  CColor& GetColor(int i) {
    assert(i>=0 && i<mSize);
    return mColors[i];
  }

  //@cmember,mfunc Returns the number of colors in the color palette
  int GetSize() const { return mSize; }

public: //@access Operations
  
  //@cmember Returns the luminosity of the i th colors  
  TSample GetLuma(int i) const ;    
  //@cmember Returns the index of the most similar color  
  int GetNearestColorIndex(const CColor &color) const;
  //@cmember,mfunc Returns a constant reference on the most similar color  
  const CColor &GetNearestColor(const CColor &color) const {return GetColor(GetNearestColorIndex(color)); }
  //@cmember,mfunc Returns a reference on the most similar color  
  CColor &GetNearestColor(const CColor &color) {return GetColor(GetNearestColorIndex(color)); }
  
  //@cmember Returns the index of color which has the closest luminosity to a given value  
  int GetNearestLumaIndex(TSample luma) const;
  //@cmember,mfunc Returns a constant reference on the color which has the closest luminosity to a given value  
  const CColor &GetNearestLuma(TSample luma) const {return GetColor(GetNearestLumaIndex(luma)); }
  //@cmember,mfunc Returns a reference on the color which has the closest luminosity to a given value  
  CColor &GetNearestLuma(TSample luma) {return GetColor(GetNearestLumaIndex(luma)); }

#if defined(_WIN32_IMPLEMENTATION)  
  //@cmember Builds and return an HPALETTE object corresponding to the palette
  HPALETTE ConvertToHPalette() const;
#endif

    void JustRead(shared_ptr<CInputBinFile> ibf);
	void ReadFromFile(CInputBinaryFile& ibf);
	void ReadFromFile(shared_ptr<CInputBinFile> ibf);
	void WriteToFile(COutputBinaryFile& obf) const;
	void WriteToFile(shared_ptr<COutputBinFile> obf) const;

  ////@cmember Reads a palette from a stream
  //void ReadFromStream (istream& stream);
  ////@cmember Writes a palette to a stream
  //void WriteToStream  (ostream& stream) const;

public: //@access Operators
  
  //@cmember,mfunc operator for retrieving a constant reference on the i th color
  const CColor& operator[](int i) const { return GetColor(i); }
    //@cmember,mfunc operator for retrieving a reference on the i th color
  CColor& operator[](int i) { return GetColor(i); }  
  //@cmember Compare for equality
  bool operator== (const CColorPalette& palette) const;
  //@cmember,mfunc Compare for inequality
  bool operator!= (const CColorPalette& palette) const {
    return !(*this==palette); };
  //@cmember Assignment operator
  const CColorPalette& operator= (const CColorPalette& palette);    

private:
  void Init(int size);
  void Free();
  void Duplicate(const CColorPalette& palette);
private:
  // Palette size
  int mSize;
  // Color array
  CColor *mColors;
};

//IMGTK_EXPORT istream& operator>> (istream& s, CColorPalette& o);
//IMGTK_EXPORT ostream& operator<< (ostream& s, const CColorPalette& o);

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__COLOR_PALETTE_H)
