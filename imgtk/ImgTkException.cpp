//////////////////////////////////////////////////////////////////////
// @doc TKEXCEPTION

#include "ImgTkException.h"

#include <cstring> // for NULL
#include <stdio.h>
#include <assert.h>
#include <sstream>

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

std::string CImgTkException::format_int(int nValue) const
{
	std::ostringstream ss;
	ss << nValue;
	return ss.str();
}

CImgTkException::CImgTkException(const std::string sData, const std::string sMessage) :
	m_sMessage(sMessage),
	m_sData(sData) {}

CImgTkException::CImgTkException(const std::string sData, const std::string sMessage, int nMinValue, int nMaxValue) :
	m_sMessage(
		sMessage +
		" The minimum value is '" +
		format_int(nMinValue) +
		"'. The maximum value is '" +
		format_int(nMaxValue) +
		"'."
	),
	m_sData(sData) {}

CImgTkException::CImgTkException(const std::string sData, const std::string sMessage, int nValue): 
	m_sMessage(
		sMessage +
		" The value is '" +
		format_int(nValue) +
		"'."
	),
	m_sData(sData) {}

CImgTkException::CImgTkException(const std::string sData, const std::string sMessage, const std::string sDescription) :
	m_sMessage(
		sMessage + " " +
		sDescription
	),
	m_sData(sData) {}

//const CImgTkException& CImgTkException::operator= (const CImgTkException& ex)
//{
//  if (this == &ex) return *this; 
//  Free();
//  Duplicate(ex);
//  return *this;
//}
//  
//
//CImgTkException::~CImgTkException()
//{
//  Free();
//}
//
//void CImgTkException::Free()
//{
//  if (mData)  delete [] (char *)mData;
//  if (mMessage) delete [] (char *)mMessage;
//}
//
//void CImgTkException::Duplicate(const CImgTkException& ex)
//{
//  Init(ex.mData,ex.mMessage);  
//}
//
//void CImgTkException::Init(const char *data, const char *message)
//{
//  Free();
//  if (data != NULL) {
//    int lenData = strlen(data) + 1;
//    mData = new char[lenData];
//    strncpy((char *)mData, data, lenData);
//  } else
//    mData = NULL;
//  
//  if (message != NULL) {
//    int lenMessage = strlen(message)+1;
//    mMessage = new char[lenMessage];
//    strncpy((char *)mMessage, message, lenMessage);  
//  } else
//    mMessage=NULL;
//}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
