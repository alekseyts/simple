
//////////////////////////////////////////////////////////////////////
// System: SimpleOCR
// Author: Alexey Tsarenko
// Created: 11.07.2007
// Purpose: This component provides realization of thinning algorithm.

#include "Image.h"
#include "CTiff.h"
#include <atlstr.h>

#ifndef  NO_NAMESPACE
namespace ImgTk
{
#endif

unsigned char g_aEliminateIndex_Huang_Wan_Liu[] = {
		6,12,24,48,96,192,129,3,
		7,14,28,56,112,224,193,131,194,26,11,134,
		15,30,60,120,240,225,195,135,27,198,
		31,62,124,248,241,227,199,143,
		63,126,252,249,243,231,207,159,
		223,127,253,247
};
unsigned char g_aElimArr[256];
unsigned char g_pPreserveIndex_Huang_Wan_Liu[] = { 248, 3, 48, 56, 24, 62, 192 };
unsigned char g_pMask_p[] = {5,16,1,1,65,4};


class CFileScopeInitializator
{
public:
	CFileScopeInitializator();	

} g_FileSopeInitializator;

CFileScopeInitializator::CFileScopeInitializator()
{
	memset(g_aElimArr,0,256);

	for(int i=0; i < sizeof(g_aEliminateIndex_Huang_Wan_Liu); i++) {
		g_aElimArr[g_aEliminateIndex_Huang_Wan_Liu[i]] = 1;
    }
}

int CalcIndex(int n,TCBitmap pUp,TCBitmap pCentral,TCBitmap pDown)
{   
	int n_ret = 0;
	int i=0;

	n_ret |= (1<<i++)*(pUp[n-1]);
	n_ret |= (1<<i++)*(pUp[n]);	
	n_ret |= (1<<i++)*(pUp[n+1]);
	n_ret |= (1<<i++)*(pCentral[n+1]);
	n_ret |= (1<<i++)*(pDown[n+1]);
	n_ret |= (1<<i++)*(pDown[n]);
    n_ret |= (1<<i++)*(pDown[n-1]);
	n_ret |= (1<<i++)*(pCentral[n-1]);

	return n_ret;
}

bool TemplateSeven(int n,int index,TCBitmap* aPTLines)
{
	int sum = 0;
	sum = aPTLines[1][n+2]+aPTLines[2][n+2]+aPTLines[3][n+2]+aPTLines[4][n+2];
	sum += aPTLines[4][n+1];
	sum	+= aPTLines[4][n];
	sum += aPTLines[4][n-1]; 

	return index == 56 && sum == 0 ? true : false;
}

bool TemplateTwo(int n,int index,TCBitmap* aPTLines)
{
	int sum = 0;
	sum	+= aPTLines[0][n];
	sum += aPTLines[0][n+1];
		
	unsigned char chIndex = index;
	chIndex &= ~(g_pMask_p[1]);
	return chIndex == 3 && sum == 0 ? true : false;
}

bool TemplateOneAndThree(int n,int index,TCBitmap* aPTLines)
{
	int sum = 0;
	unsigned char chIndex = index;
	chIndex &= ~(g_pMask_p[0]);

	if(chIndex == 248) {
		if(aPTLines[4][n] == 0) {
			return true;
		}
	}

	chIndex = index;
	chIndex &= ~(g_pMask_p[2]);
	if(chIndex == 48) {
		if(aPTLines[4][n] == 0 && aPTLines[4][n-1]==0) {
			return true;
		}
	}

	return false;
}

bool TemplateSix(int n,int index,TCBitmap* aPTLines)
{
	unsigned char chIndex = index;
	chIndex &= ~(g_pMask_p[5]);

	int sum = 0;
	if(chIndex == 192) {
		sum += aPTLines[2][n-2]+aPTLines[1][n-2];	
		if(sum==0) {
			return true;
		}
	}
	return false;
}

bool TemplateFourAndFive(int n,int index,TCBitmap* aPTLines)
{
	unsigned char chIndex = index;
	chIndex &= ~(g_pMask_p[3]);

	int sum = 0;
	if(chIndex == 24) {
		sum += aPTLines[2][n+2]+aPTLines[1][n+2];	
		if(sum==0) {
			return true;
		}
	}

	chIndex = index;
	chIndex &= ~(g_pMask_p[4]);
	if(chIndex == 62) {
		sum += aPTLines[2][n+2];	
		if(sum==0) {
			return true;
		}
	}

	return false;
}

bool CheckPreservation(int n,int index,TCBitmap* aPTLines)
{
	bool b_ret = false;

	char chHeightThickness = 0;
	for(int i=0; i<5; i++) {
		chHeightThickness |= ((1<<i)*aPTLines[i][n]);
	}
	
	char chWidthThickness = 0;
	for(int i=0; i<5; i++) {
		chWidthThickness |= ((1<<i)*aPTLines[2][n+(i-2)]);
	}
	
	char chIndex = index;
	char chIndex1 = index;

	char chMask_Height_1_3_7 = 30;
	char chMask_Width_4_5_7 = 30;
	char chMask_Height_2 = 15;
	char chMask_Width_6 = 15;
	
	if((chHeightThickness & chMask_Height_1_3_7) == 12) {
		if((chWidthThickness & chMask_Width_4_5_7) == 12) {
			return TemplateSeven(n,index,aPTLines);
		} else {
			return TemplateOneAndThree(n,index,aPTLines);
		}
	} else if((chHeightThickness & chMask_Height_2) == 6) {
			return TemplateTwo(n,index,aPTLines);
	} else if((chWidthThickness & chMask_Width_4_5_7) == 12) {
			return TemplateFourAndFive(n,index,aPTLines);
	} else if((chWidthThickness & chMask_Width_6) == 6) {
			return TemplateSix(n,index,aPTLines);
	}
	
	return false;
}


CImage * CImage::Thinning() const 
{
	CImage* imgSrc = 0;
	CImage* imgDest = 0;

	int w = GetWidth();
	int h = GetHeight();

	if(GeEImageType() != CImageInfo::Bilevel) {
		CImageException("CImage::Thinning", "Should be bilevel image");
	}

	imgSrc = new CImage(w+4,h+4,CImageInfo::Gray256,GetXResolution(),GetYResolution());  
	if (0 == imgSrc) {
	  throw CImageException("CImage::Thinning", "Out of memory imgSrc");
    }
	imgSrc->Erase(TSample(255));
    imgSrc->Blit(2,2,this);
	imgSrc->BWNormalize();

	imgDest = new CImage(*imgSrc);  
	if (0 == imgDest) {
	  throw CImageException("CImage::Thinning", "Out of memory imgDest");
    }

	TCBitmap pUpRow;
    TCBitmap pCentralRow;
    TCBitmap pBottomRow;
	TBitmap  pDestRow;

	TCBitmap aPTLines[5];

	CImage* pRet = 	new CImage(w,h,CImageInfo::Bilevel,GetXResolution(),GetYResolution());  
	if (0 == pRet) {
	  throw CImageException("CImage::Thinning", "Out of memory pRet");
    }

	bool bEliminationDone = true;
	int nIter = 0;

	while(bEliminationDone) {
	bEliminationDone = false;
	for(int i=2; i<2+h; i++) { 
		pDestRow = imgDest->GetRow(i);
		aPTLines[2] = pCentralRow = imgSrc->GetRow(i);
		aPTLines[3] = pBottomRow = imgSrc->GetRow(i+1);
		aPTLines[1] = pUpRow = imgSrc->GetRow(i-1);
		aPTLines[0] = imgSrc->GetRow(i-2);
		aPTLines[4] = imgSrc->GetRow(i+2);

		for(int j=2; j<2+w; j++) {
			if(pCentralRow[j] == 0) {
				continue;
			}
			int index = CalcIndex(j,pUpRow,pCentralRow,pBottomRow);
			if(g_aElimArr[index] == 0) {
				continue;
			} else {
				bool bPreserve = CheckPreservation(j,index,aPTLines);
				if(bPreserve == false) {
					pDestRow[j] = 0;
					bEliminationDone = true;
				}
			}
		}
	}
	imgSrc->Blit(0,0,imgDest);
//	imgSrc->BWDenormalize();
//	pRet->Blit(0,0,imgSrc,2,2,w,h);
//			CTiff tiff;
//			CString FName;
//			FName.Format("C:\\temp\\SampleThinning_Iter_%d.tif",nIter);
//			tiff.Open((LPCTSTR)FName,"w");
//			tiff.WriteImage(pRet);
//			tiff.Flush();
//			tiff.Close();
//
			nIter++;
//	imgSrc->Blit(0,0,imgDest);
	}

	delete imgSrc;
	imgDest->BWDenormalize();
	pRet->Blit(0,0,imgDest,2,2,w,h);
	delete imgDest;

	return pRet; 
}

void CImage::BWNormalize()
{
	if(GeEImageType() != CImageInfo::Gray256) {
		CImageException("CImage::BWNormalize", "Should be bilevel image");
		return;
	}

	int w=GetWidth();
    int h=GetHeight();

	for(int i=0; i<h; i++) { 
		TBitmap pRow = GetRow(i);
		for(int j=0; j<w; j++) {
			if(pRow[j]==0) {
				pRow[j] = 1;
			} else {
				pRow[j] = 0;
			}
		}
	}
}

void CImage::BWDenormalize()
{
	if(GeEImageType() != CImageInfo::Gray256) {
		CImageException("CImage::BWNormalize", "Should be bilevel image");
		return;
	}

	int w=GetWidth();
    int h=GetHeight();

	for(int i=0; i<h; i++) { 
		TBitmap pRow = GetRow(i);
		for(int j=0; j<w; j++) {
			if(pRow[j]==1) {
				pRow[j] = 0;
			} else {
				pRow[j] = 255;
			}
		}
	}
}

CImage *CImage::Thin() const
{
	return Thinning();
}

#ifndef  NO_NAMESPACE
}
#endif
