# Autoduck MAKEFILE
#
# Eric Artzt, Program Manager
# Consumer Division, Kids Software Group
# Internet  :  erica@microsoft.com
#

OUTDIR  = $(ProjDir)\Autoduck
TARGET  = $(Project)
TITLE   = $(TARGET) Help
DOCHDR  = $(TARGET) API Reference
AD      = $(AdDir)\autoduck.exe
ADTOC   = .\Contents.D
ADTAB   = 8
ADHLP   = /RH    /t$(ADTAB) /O$(OUTDIR)\$(TARGET).RTF /D "title=$(TITLE)"
ADDOC   = /RD    /t$(ADTAB) /O$(OUTDIR)\$(TARGET).DOC /D "doc_header=$(DOCHDR)"
ADHTM   = /RHTML /f$(AdDir)\HTML.FMT /t$(ADTAB) /O$(OUTDIR)\$(TARGET).HTM /D "title=$(TITLE)"
HC      = hcw /a /e /c
SOURCE  = *.cpp *.h

# Help and Doc targets

target ::
!if !EXIST("$(OUTDIR)")
    md $(OUTDIR)
! endif

target :: $(TARGET).hlp $(TARGET).doc $(TARGET).htm

clean:
    if exist $(OUTDIR)\*.rtf del $(OUTDIR)\*.rtf
    if exist $(OUTDIR)\$(TARGET).doc del $(OUTDIR)\$(TARGET).doc
    if exist $(OUTDIR)\$(TARGET).hlp del $(OUTDIR)\$(TARGET).hlp

# Generate a Help file

$(TARGET).rtf : $(SOURCE) $(ADTOC)
    $(AD) $(ADHLP) $(ADTOC) $(SOURCE)

$(TARGET).hlp : $(TARGET).rtf
    $(HC) $(OUTDIR)\$(TARGET).HPJ

# Generate a print documentation file

$(TARGET).doc :  $(SOURCE)
    $(AD) $(ADDOC) $(SOURCE)

# Generate an HTML page

$(TARGET).htm :  $(SOURCE)
    $(AD) $(ADHTM) $(ADTOC) $(SOURCE)

