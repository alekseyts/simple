//////////////////////////////////////////////////////////////////////
// @doc COLORPALETTE


#include "ColorPalette.h"

#if defined(_WIN32_IMPLEMENTATION) && !defined(__WINDOWS_H)
#include <windows.h>
#define __WINDOWS_H
#endif

#include "ImgTkException.h"
#include <CBinaryFile.h>


#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

using std::istream;
using std::ostream;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CColorPalette::CColorPalette(): mSize(0), mColors(NULL)
{
  Init(256);
}

CColorPalette::CColorPalette(int size): mSize(0), mColors(NULL)
{
  Init(size);
}

CColorPalette::CColorPalette(const CColorPalette& palette): mSize(0), mColors(NULL)
{
  Duplicate(palette);
}

CColorPalette::CColorPalette(HPALETTE hPal): mSize(0), mColors(NULL)
{
	DWORD nPaletteSize = 0;
	int nRes = 0;
	nRes = GetObject(hPal, sizeof(DWORD), &nPaletteSize);
	if (0 == nRes) {
		throw CImgTkException("CColorPalette::CColorPalette", "An error occured while creating palette object");
	}
	Init(nPaletteSize);

	LPLOGPALETTE lpPal;
	HANDLE pal;
	pal=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE, sizeof(LOGPALETTE)+nPaletteSize*sizeof(PALETTEENTRY));
	if (!pal) {
		throw CImgTkException("CColorPalette::CColorPalette", "An error occured while creating palette object");
	}

	lpPal=(LPLOGPALETTE) GlobalLock(pal);
	if (!lpPal) {
		GlobalUnlock(pal);
		GlobalFree(pal);
		throw CImgTkException("CColorPalette::CColorPalette", "An error occured while creating palette object");
	}

	nRes = GetPaletteEntries(hPal, 0, nPaletteSize - 1, &lpPal->palPalEntry[0]);
	if (0 == nRes) {
		GlobalUnlock(pal);
		GlobalFree(pal);
		throw CImgTkException("CColorPalette::CColorPalette", "An error occured while creating palette object");
	}

	CColor curColor;
	int N = nPaletteSize;
	for(int i=0; i < N; ++i) {
		curColor.SetRed(lpPal->palPalEntry[i].peRed);
		curColor.SetGreen(lpPal->palPalEntry[i].peGreen);
		curColor.SetBlue(lpPal->palPalEntry[i].peBlue);
		mColors[i] = curColor;
	}
	GlobalUnlock(pal);
	GlobalFree(pal);
}

CColorPalette::~CColorPalette()
{
  Free();
}

TSample CColorPalette::GetLuma(int i) const 
{
  return GetColor(i).GetLuma();
}

int CColorPalette::GetNearestColorIndex(const CColor &color) const
{
  int i;
  int bestdistance=3*255*255+1, bestcolor,distance;
  CColor currentColor;
  TSample r,g,b;
  TSample red,green,blue;

  red=color.GetRed();
  green=color.GetGreen();
  blue=color.GetBlue();
  for(i=0;i<GetSize();i++) {
    currentColor=GetColor(i);
    r=currentColor.GetRed();
    g=currentColor.GetGreen();
    b=currentColor.GetBlue();
    distance=(r-red)*(r-red) + (g-green)*(g-green) + (b-blue)*(b-blue);
    if (distance<bestdistance) {
      bestdistance=distance;
      bestcolor=i;
    }
  }
  return bestcolor;
}

int CColorPalette::GetNearestLumaIndex(TSample luma) const
{
  int i;
  int bestdistance=255*255+1, bestcolor,distance;
  TSample l;
    
  for(i=0;i<GetSize();i++) {
    l=GetLuma(i);    
    if (l>luma) distance=l-luma; else distance=luma-l;
    if (distance<bestdistance) {
      bestdistance=distance;
      bestcolor=i;
    }
  }
  return bestcolor;
}

#if defined(_WIN32_IMPLEMENTATION)

HPALETTE CColorPalette::ConvertToHPalette() const
{
  LPLOGPALETTE lpPal;
  HANDLE pal;
  int i;
  HPALETTE hPal;
  
  pal=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,
      sizeof(LOGPALETTE)+GetSize()*sizeof(PALETTEENTRY));
  if (!pal) return NULL;
  lpPal=(LPLOGPALETTE) GlobalLock(pal);
  if (!lpPal) return NULL;

  lpPal->palVersion=0x300;
  lpPal->palNumEntries=(WORD) GetSize();
  for(i=0;i<GetSize();i++) {
    lpPal->palPalEntry[i].peRed=GetColor(i).GetRed();
    lpPal->palPalEntry[i].peGreen=GetColor(i).GetGreen();
    lpPal->palPalEntry[i].peBlue=GetColor(i).GetBlue();
    lpPal->palPalEntry[i].peFlags=0;
  }

  hPal=CreatePalette(lpPal);
  
  GlobalUnlock(pal);
  GlobalFree(pal);
  return hPal;
}

#endif

void CColorPalette::JustRead(shared_ptr<CInputBinFile> ibf)
{
	int nSize(0);
	ibf->Read(nSize);

	CColor tmp;
	for(int i = 0; i < nSize; ++i) {
		tmp.ReadFromFile(ibf);
	}
}

void CColorPalette::ReadFromFile(CInputBinaryFile& ibf)
{
	int nSize(0);
	ibf.Read(nSize);

	if (GetSize() != nSize) {
		Free();
		Init(nSize);
	}

	for(int i = 0; i < nSize; ++i) {
		mColors[i].ReadFromFile(ibf);
	}
}

void CColorPalette::ReadFromFile(shared_ptr<CInputBinFile> ibf)
{
	int nSize(0);
	ibf->Read(nSize);

	if (GetSize() != nSize) {
		Free();
		Init(nSize);
	}

	for(int i = 0; i < nSize; ++i) {
		mColors[i].ReadFromFile(ibf);
	}
}

void CColorPalette::WriteToFile(COutputBinaryFile& obf) const
{
	obf.Write(GetSize());
	for(int i = 0; i < GetSize(); ++i) {
		mColors[i].WriteToFile(obf);
	}
}

void CColorPalette::WriteToFile(shared_ptr<COutputBinFile> obf) const
{
	obf->Write(GetSize());
	for(int i = 0; i < GetSize(); ++i) {
		mColors[i].WriteToFile(obf);
	}
}

//void CColorPalette::ReadFromStream (istream& stream)
//{
//  int size;
//
//  stream >> size;
//  
//  if (GetSize()!=size) {
//    Free();
//    Init(size);
//  }
//
//  for(int i=0;i<size;i++) {
//    stream >> mColors[i] ;
//  }  
//}

//void CColorPalette::WriteToStream  (ostream& stream) const
//{
//  stream << GetSize() << endl;
//  for(int i=0;i<GetSize();i++) 
//    stream << mColors[i];  
//}
//
//istream& operator>> (istream& s, CColorPalette& o)
//{
//  o.ReadFromStream(s);
//  return s;
//}
//
//ostream& operator<< (ostream& s, const CColorPalette& o)
//{
//  o.WriteToStream(s);
//  return s;
//}

bool CColorPalette::operator== (const CColorPalette& palette) const
{
	bool bRes = true;
	if (mColors != 0 && palette.mColors != 0) {
		bRes =
			GetSize()==palette.GetSize() &&
			memcmp(mColors,palette.mColors,GetSize()*sizeof(CColor))==0;
	} else if (mColors != 0 || palette.mColors != 0) {
		bRes = false;
	}

	return bRes;
}

const CColorPalette& CColorPalette::operator= (const CColorPalette& palette)
{
  if (this == &palette) return *this; 

  Duplicate(palette);
  return *this;
}

void CColorPalette::Init(int size)
{
  assert(size>0);
  mSize=size;
  mColors=new CColor[mSize];
}

void CColorPalette::Free()
{
	if (NULL != mColors) {
		delete [] mColors;
	}
}

void CColorPalette::Duplicate(const CColorPalette& palette)
{
  if (GetSize()!=palette.GetSize()) {
    Free();  
    mSize=palette.GetSize();
    mColors=new CColor[mSize];
  }
  memcpy(mColors,palette.mColors,mSize*sizeof(CColor));
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
