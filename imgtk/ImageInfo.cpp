//////////////////////////////////////////////////////////////////////
// @doc IMAGEINFO

#include "ImageInfo.h"
#include "ImageException.h"

#include <CBinaryFile.h>
#include <memory>
#include "boost/lexical_cast.hpp" 

#if !defined(min) 
#define min(a,b) ((a) < (b) ? (a) : (b) )
#endif

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


int CImageInfo::DefaultXResolution = 300;
int CImageInfo::DefaultYResolution = 300;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageInfo::CImageInfo(): mWidth(0), mHeight(0), mType(Bilevel), mXResolution(0), mYResolution(0),
		mPalette(NULL), mBitCount(0), mRowSize(0)
{
  Init(1,1,Bilevel,DefaultXResolution,DefaultYResolution,NULL);
}


CImageInfo::CImageInfo(int width,int height,EImageType type,int xres ,int yres, CColorPalette *palette):
		mWidth(width), mHeight(height), mType(type), mXResolution(xres), mYResolution(yres),
		mPalette(palette), mBitCount(0), mRowSize(0)
{
  Init(width,height,type,xres,yres,palette);
}

CImageInfo::CImageInfo(const CImageInfo& info):
		mWidth(info.GetWidth()), mHeight(info.GetHeight()), mType(info.GeEImageType()), 
		mXResolution(info.GetXResolution()), mYResolution(info.GetYResolution()),
		mPalette(NULL), mBitCount(0), mRowSize(0)
{
  Duplicate(info);
}

int CImageInfo::GetNumColors() const 
{
  switch (GeEImageType()) {
  case Bilevel:     /* Noir & Blanc */
    return 2;	    
  case Gray16:     /* 16 niveaux de gris */
    return 16;
  case Gray256:    /* 256 niveaux de gris */
    return 256;
  case Color16:    /* 16 couleurs */
    return 16;
  case Color256:   /* 256 couleurs */
    return 256;
  case ColorRGB:   /* 24 bits True color */
    return 0;
  case GrayFloat:  /* Niveaux de gris a` e'chantillons flottants */
    return 0;
  case ColorFloat: /* Couleur a` e'chantillons flottants */
    return 0;
  }
  throw CImageException("CImageInfo::GetNumColors","Unknown Image type");
  return 0;
}

void CImageInfo::Init(int width,int height,EImageType type,int xres,int yres,CColorPalette *palette)
{
  mWidth=width;
  mHeight=height;
  mType=type;
  mXResolution=xres;
  mYResolution=yres;
  if (palette && type!=Color16 && type!=Color256) 
    throw CImageException("CImageInfo::Init","This type of image doesn't need a color palette");
  mPalette=palette;
  switch (GeEImageType()) {
  case Bilevel:    /* Noir & Blanc */
    mBitCount=1;
    break;
  case Gray16:     /* 16 niveaux de gris */
    mBitCount=4;	    
    break;
  case Gray256:    /* 256 niveaux de gris */
    mBitCount=8;	    
    break;
  case Color16:    /* 16 couleurs */
    mBitCount=4;	    
    if (mPalette==NULL) {
      mPalette=new CColorPalette(16);
      (*mPalette)[ 0].SetRGB(0,0,0);
      (*mPalette)[ 1].SetRGB(128,0,0);    
      (*mPalette)[ 2].SetRGB(0,128,0);        
      (*mPalette)[ 3].SetRGB(0,0,128);    
      (*mPalette)[ 4].SetRGB(128,128,0);    
      (*mPalette)[ 5].SetRGB(128,0,128);        
      (*mPalette)[ 6].SetRGB(0,128,128);                
      (*mPalette)[ 7].SetRGB(192,192,192);                
      (*mPalette)[ 8].SetRGB(128,128,128);                    
      (*mPalette)[ 9].SetRGB(255,0,0);                        
      (*mPalette)[10].SetRGB(0,255,0);                        
      (*mPalette)[11].SetRGB(0,0,255);                        
      (*mPalette)[12].SetRGB(255,255,0);                        
      (*mPalette)[13].SetRGB(255,0,255);
      (*mPalette)[14].SetRGB(0,255,255);                        
      (*mPalette)[15].SetRGB(255,255,255);                        
    }
       
    break;
  case Color256:   /* 256 couleurs */
    mBitCount=8;	    
    if (mPalette == NULL) {
      int r,g,b,i;
      mPalette=new CColorPalette(256);
      /* Cre'e une palette "Arc en ciel" */
      r=g=b=0;
      for (i=0;i<255;i++) {
	(*mPalette)[i].SetRGB((TSample)min(r,255),(TSample)min(g,255),(TSample)min(b,255));
	if (r>=64 || r<192) r+=32;
	else r+=64;
	if (r>256) {
	  r=0;
	  g+=32;								
	  if (g>256) {
	    g=0;
	    b+=64;
	  }
	} 
	(*mPalette)[255].SetRGB(255,255,255);
      }
    }
    break;
  case ColorRGB:   /* 24 bits True color */
    mBitCount=24;	    
    break;
  case GrayFloat:  /* Niveaux de gris a` e'chantillons flottants */
    mBitCount=sizeof(TFSample)*8;	    
    break;
  case ColorFloat: /* Couleur a` e'chantillons flottants */
    mBitCount=sizeof(TFSample)*8*3;	    
    break;
  default:
    throw CImageException("CImageInfo::Init", "Incorrect image type");
  }  
  if (mType<GrayFloat)
   mRowSize=(((GetWidth()*GetBitCount()+31)>>5)<<2);
  else
   mRowSize=(GetWidth()*GetBitCount()+7)/8;
}

void CImageInfo::Free()
{
  if (mPalette) delete mPalette;
}

void CImageInfo::Duplicate(const CImageInfo& info)
{
  if (info.GetColorPalette())
    Init(info.GetWidth(),info.GetHeight(),info.GeEImageType(),info.GetXResolution(),info.GetYResolution(),new CColorPalette(*(info.GetColorPalette())));
  else
    Init(info.GetWidth(),info.GetHeight(),info.GeEImageType(),info.GetXResolution(),info.GetYResolution(),NULL);
}

int CImageInfo::GetSamplesPerPixel() const
{
  EImageType type=GeEImageType();
  if (type==ColorRGB || type==ColorFloat) return 3;
  return 1;
}

#if defined(_WIN32_IMPLEMENTATION)

LPBITMAPINFOHEADER CImageInfo::ConvertToBitmapInfoHeader() const
{
  if (!IsStandard()) throw CImageException("CImageInfo::ConvertToBitmapInfoHeader()", 
			"This method is not supported for this image type");

  int nbcol=GetNumColors();
  LPBITMAPINFOHEADER lpbi;
  int bisize;
  RGBQUAD *palette;
  /* Allocation de la structure BITMAPINFOHEADER avec de 
    l'espace pour la palette de couleurs, si ne'ce'ssaire */
  bisize = sizeof(BITMAPINFOHEADER)+nbcol*sizeof(RGBQUAD);
  lpbi = (LPBITMAPINFOHEADER) new TByte[bisize];
    /* Initialise BITMAPINFOHEADER */

  memset(lpbi,0, bisize);
	
  lpbi->biSize=sizeof(BITMAPINFOHEADER);
  lpbi->biWidth=GetWidth();
  lpbi->biHeight=GetHeight();
  lpbi->biPlanes=1;
  lpbi->biCompression=BI_RGB;
  lpbi->biXPelsPerMeter=(int) (((long) GetXResolution()*10000)/254);
  lpbi->biYPelsPerMeter=(int) (((long) GetYResolution()*10000)/254);
  lpbi->biBitCount=(WORD)GetBitCount();
  lpbi->biSizeImage=GetRowSize()*GetHeight();


  palette=(RGBQUAD *) (((unsigned char *) lpbi)+sizeof(BITMAPINFOHEADER));

  switch (GeEImageType()) {
  case Bilevel:     /* Noir & Blanc */
    lpbi->biClrUsed=lpbi->biClrImportant=2;
    palette[0].rgbRed	  =255;
    palette[0].rgbGreen	  =255;
    palette[0].rgbBlue	  =255;
    palette[0].rgbReserved=0;
    palette[1].rgbRed	  =0;
    palette[1].rgbGreen	  =0;
    palette[1].rgbBlue	  =0;
    palette[1].rgbReserved=0;
    break;
  case Gray16:     /* 16 niveaux de gris */
    {      
      lpbi->biClrUsed=lpbi->biClrImportant=16;
      for (int i=0;i<16;i++) {
	palette[i].rgbRed	  =
	palette[i].rgbGreen	  =
	palette[i].rgbBlue	  = (BYTE) (i*16+i);
	palette[i].rgbReserved	  =0;
      }
    }
    break;
  case Gray256:    /* 256 niveaux de gris */
    {
      lpbi->biClrUsed=lpbi->biClrImportant=256;
      for (int i=0;i<256;i++) {
	palette[i].rgbRed	  =
	palette[i].rgbGreen	  =
	palette[i].rgbBlue	  = (BYTE) i;
	palette[i].rgbReserved	  =0;
      }
    }
    break;
  case Color16:    /* 16 couleurs */
    {
      lpbi->biClrUsed=lpbi->biClrImportant=16;
      for (int i=0;i<16;i++) {
	palette[i].rgbRed	  = GetColorPalette()->GetColor(i).GetRed();
	palette[i].rgbGreen	  = GetColorPalette()->GetColor(i).GetGreen();
	palette[i].rgbBlue	  = GetColorPalette()->GetColor(i).GetBlue();
	palette[i].rgbReserved	  =0;
      }
    }
    break;
  case Color256:   /* 256 couleurs */
    {
      lpbi->biClrUsed=lpbi->biClrImportant=256;
      for (int i=0;i<256;i++) {

	palette[i].rgbRed	  = GetColorPalette()->GetColor(i).GetRed();
	palette[i].rgbGreen	  = GetColorPalette()->GetColor(i).GetGreen();
	palette[i].rgbBlue	  = GetColorPalette()->GetColor(i).GetBlue();
	palette[i].rgbReserved	  =0;
      }
    }
    break;
  case ColorRGB:   /* 24 bits True color */
    break;
  default:
	throw CImageException("CImageInfo::ConvertToBitmapInfoHeader()", 
		  "This method is not supported for this image type");
  }
  return lpbi;
}

#endif

CImageInfo::~CImageInfo()
{
  Free();
}

void CImageInfo::ConfinedRead(shared_ptr<CInputBinFile> ibf)
{
	ibf->Read(mWidth);
	ibf->Read(mHeight);
	
	long nType(0);
	ibf->Read(nType);
	mType = static_cast<CImageInfo::EImageType>(nType);

	ibf->Read(mXResolution);
	ibf->Read(mYResolution);

	switch (GeEImageType()) {
	case Bilevel:    /* Noir & Blanc */
		mBitCount=1;
		break;
	case Gray16:     /* 16 niveaux de gris */
		mBitCount=4;	    
		break;
	case Gray256:    /* 256 niveaux de gris */
		mBitCount=8;	    
		break;
	  }

	if (mType<GrayFloat)
		mRowSize=(((GetWidth()*GetBitCount()+31)>>5)<<2);
	else
		mRowSize=(GetWidth()*GetBitCount()+7)/8;

	if (mType == Color16 || mType == Color256) {
		std::auto_ptr<CColorPalette> pPalette(new CColorPalette());
		pPalette->JustRead(ibf);
	}   
}

void CImageInfo::ReadFromFile(CInputBinaryFile& ibf)
{
	ibf.Read(mWidth);
	ibf.Read(mHeight);

	long nType(0);
	ibf.Read(nType);
	mType = static_cast<CImageInfo::EImageType>(nType);

	ibf.Read(mXResolution);
	ibf.Read(mYResolution);

	Free();
	Init(mWidth, mHeight, mType, mXResolution, mYResolution, NULL);

	if (mType == Color16 || mType == Color256) {
		std::auto_ptr<CColorPalette> pPalette(new CColorPalette());
		pPalette->ReadFromFile(ibf);
		SetColorPalette(pPalette.release());
	}    
}

void CImageInfo::ReadFromFile(shared_ptr<CInputBinFile> ibf)
{
	ibf->Read(mWidth);
	ibf->Read(mHeight);

	long nType(0);
	ibf->Read(nType);
	mType = static_cast<CImageInfo::EImageType>(nType);

	ibf->Read(mXResolution);
	ibf->Read(mYResolution);

	Free();
	Init(mWidth, mHeight, mType, mXResolution, mYResolution, NULL);

	if (mType == Color16 || mType == Color256) {
		std::auto_ptr<CColorPalette> pPalette(new CColorPalette());
		pPalette->ReadFromFile(ibf);
		SetColorPalette(pPalette.release());
	}    
}

void CImageInfo::WriteToFile(shared_ptr<COutputBinFile> obf) const
{
	obf->Write(GetWidth());
	obf->Write(GetHeight());

	long nType = static_cast<long>(GeEImageType());
	obf->Write(nType);

	obf->Write(GetXResolution());
	obf->Write(GetYResolution());

	if (GetColorPalette()) {
		GetColorPalette()->WriteToFile(obf);
	}
}

void CImageInfo::WriteToFile(COutputBinaryFile& obf) const
{
	obf.Write(GetWidth());
	obf.Write(GetHeight());

	long nType = static_cast<long>(GeEImageType());
	obf.Write(nType);

	obf.Write(GetXResolution());
	obf.Write(GetYResolution());

	if (GetColorPalette()) {
		GetColorPalette()->WriteToFile(obf);
	}
}

//void CImageInfo::ReadFromStream (istream& stream)
//{
//  int width,height,xres,yres;
//  EImageType type;
//  stream >> width;
//  stream >> height;
//  stream >> type;
//  stream >> xres;
//  stream >> yres;
//  Free();
//  Init(width, height, type, xres, yres, NULL);
//  if (type==Color16 || type==Color256) {
//    CColorPalette *palette=new CColorPalette();
//    stream >> *palette;
//    SetColorPalette(palette);
//  }    
//}

//void CImageInfo::WriteToStream  (ostream& stream) const
//{
//  stream << GetWidth() << ' ' << GetHeight() << ' ' ;
//  stream << GeEImageType() << ' ';
//  stream << GetXResolution() << ' ' << GetYResolution() << ' ';
//  if (GetColorPalette()) 
//    stream << *GetColorPalette();  
//  else
//    stream << endl;
//}

//istream& operator>> (istream& s, CImageInfo& o)
//{
//  o.ReadFromStream(s);
//  return s;
//}
//
//ostream& operator<< (ostream& s, const CImageInfo& o)
//{
//  o.WriteToStream(s);
//  return s;
//}
//
//istream& operator>> (istream& stream, CImageInfo::EImageType& type)
//{
//  int value;
//  stream >> value;
//  type=(CImageInfo::EImageType) value;
//  return stream;
//}
//
//ostream& operator<< (ostream& stream, CImageInfo::EImageType type)
//{
//  int value=(int) type;
//  stream << value;
//  return stream;
//}



const CImageInfo& CImageInfo::operator= (const CImageInfo& info)
{
  if (this == &info) return *this; 
  Free();
  Duplicate(info);
  
  return *this;
}

bool operator== (CImageInfo const& lh, CImageInfo const& rh)
{
	bool bRes =
		lh.mWidth == rh.mWidth &&
		lh.mHeight == rh.mHeight &&
		lh.mType == rh.mType &&
		lh.mXResolution == rh.mXResolution &&
		lh.mYResolution == rh.mYResolution;

	if (0 != lh.mPalette && 0 != rh.mPalette) {
		bRes =
			bRes && *lh.mPalette == *rh.mPalette;
	} else if (0 != lh.mPalette || 0 != rh.mPalette) {
		bRes = false;
	}

	return bRes;
}

#if !defined(NO_NAMESPACE)
} //namespace ImgTk
#endif 
