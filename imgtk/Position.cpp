//////////////////////////////////////////////////////////////////////
// @doc POSITION

#include "Position.h"

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

using std::ostream;
using std::istream;

void CPosition::WriteToStream  (ostream& stream) const
{
  stream <<  GetX() << ' ' << GetY();
}

ostream& operator<< (ostream& s, const CPosition& o)
{
  o.WriteToStream(s);
  return s;
}


const CPosition& CPosition::operator= (const CPosition& position)
{
  if (this == &position) return *this; 
  mX=position.mX;  
  mY=position.mY;  
  return *this;
}
  
bool CPosition::operator< (const CPosition &position) const
{
  if (mY==position.mY)
    return mX<position.mX;
  else
    return mY<position.mY;

}

bool CPosition::operator== (const CPosition &position) const
{
  return mX==position.mX && mY==position.mY;
}



#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
