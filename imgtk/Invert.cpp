//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#include "Image.h"

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


void CImage::InvertBW()
{
  int i,length=GetRowSize()*GetHeight();
  TBitmap  bits=GetBitmap();
  
  for (i=length ;i;i--) *bits++=0xffu ^ *bits;
  
}

void CImage::Invert4()
{
  int i,length=GetRowSize()*GetHeight();
  TBitmap  bits=GetBitmap();
  
  for (i=0;i<length;i++) 
    *bits++=((15u-(*bits>>4))<<4) + (15u-(*bits & 0x0fu));
}

void CImage::Invert8()
{
  int i,length=GetRowSize()*GetHeight();
  TBitmap  bits=GetBitmap();
  
  for (i=0;i<length;i++) *bits++=0xffu - *bits;
}


void CImage::InvertFloat()
{
  int i,j;
  TFSample * p;
  
  for (i=0;i<GetHeight();i++) {
    p=(TFSample *) GetRow(0);
    for (j=0;j<GetWidth();j++) {
      *p++=(TFSample)255.0 - *p;
    }
  }
}

void CImage::Invert()
{
  switch(GeEImageType()) {
  case CImageInfo::Bilevel:
    InvertBW();
    break;
  case CImageInfo::Gray16:
  case CImageInfo::Color16:
    Invert4();
    break;
  case CImageInfo::Gray256:
  case CImageInfo::Color256:
  case CImageInfo::ColorRGB:
    Invert8();
    break;
  case CImageInfo::GrayFloat:
  case CImageInfo::ColorFloat:
    InvertFloat();
    break;
  default:
    assert(0);
  }
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
