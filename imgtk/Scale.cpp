//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#include "Image.h"

#if !defined(__IMAGE_EXCEPTION_H)
#include "ImageException.h"
#endif

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

#define SCALEX(x) (((x)*w)/neww)
#define SCALEY(y) (((y)*h)/newh)

static int XShrink[256][256];
static bool Built=false;

static void BuildXShrink()
{
  int i,j;
  unsigned int lum1,lum2,lum3,lum4;
  unsigned char *p;
  
  for(i=0;i<256;i++)
    for(j=0;j<256;j++) {
      lum1=lum2=lum3=lum4=0;
      if (!(i & 0x80)) lum1+=255;
      if (!(i & 0x40)) lum1+=255;
      if (!(j & 0x80)) lum1+=255;
      if (!(j & 0x40)) lum1+=255;
      lum1>>=2;
      if (!(i & 0x20)) lum2+=255;
      if (!(i & 0x10)) lum2+=255;
      if (!(j & 0x20)) lum2+=255;
      if (!(j & 0x10)) lum2+=255;
      lum2>>=2;
      if (!(i & 0x08)) lum3+=255;
      if (!(i & 0x04)) lum3+=255;
      if (!(j & 0x08)) lum3+=255;
      if (!(j & 0x04)) lum3+=255;
      lum3>>=2;
      if (!(i & 0x02)) lum4+=255;
      if (!(i & 0x01)) lum4+=255;
      if (!(j & 0x02)) lum4+=255;
      if (!(j & 0x01)) lum4+=255;
      lum4>>=2;
      p=(unsigned char * ) &XShrink[i][j];
      *p++=(TByte)lum1;
      *p++=(TByte)lum2;
      *p++=(TByte)lum3;
      *p++=(TByte)lum4;
    }
    Built=true;
}

CImage *CImage::HalfSizeToGrayscale() const
{
  CImage * img2;
  int i,j,w,h,neww,newh;
  TCBitmap bits1,bits11;
  TBitmap bits2;
  
  unsigned char chunk1,chunk2;
  unsigned int lum;
  int nextlineoffset;
  
  if (!Built) BuildXShrink();
  
  w=GetWidth();
  h=GetHeight();
  neww=w/2;
  newh=h/2;
  
  img2=new CImage(neww,newh,CImageInfo::Gray256);
  img2->CopyResolution(*this);
  
  nextlineoffset=GetRowOffset();
  for(i=0;i<newh;i++) {
    
    bits1=GetRow(i*2);
    bits11=bits1+nextlineoffset;
    bits2=img2->GetRow(i);
    
    for(j=0;j<neww/4;j++) {
      chunk1=*bits1++;
      chunk2=*bits11++;
      *((int *) bits2)=XShrink[chunk1][chunk2];
      bits2+=4;
    }
    j<<=2;

    if (j>=neww) continue;
    chunk1=*bits1++;
    chunk2=*bits11++;
    lum=0;
    if (!(chunk1 & 0x80)) lum+=255;
    if (!(chunk1 & 0x40)) lum+=255;
    if (!(chunk2 & 0x80)) lum+=255;
    if (!(chunk2 & 0x40)) lum+=255;
    *bits2++=lum>>2;
    if (++j>=neww) continue;
    lum=0;
    if (!(chunk1 & 0x20)) lum+=255;
    if (!(chunk1 & 0x10)) lum+=255;
    if (!(chunk2 & 0x20)) lum+=255;
    if (!(chunk2 & 0x10)) lum+=255;
    *bits2++=lum>>2;
    if (++j>=neww) continue;
    lum=0;    
    if (!(chunk1 & 0x08)) lum+=255;
    if (!(chunk1 & 0x04)) lum+=255;
    if (!(chunk2 & 0x08)) lum+=255;
    if (!(chunk2 & 0x04)) lum+=255;
    *bits2++=lum>>2;
    
  }
  return img2;
}


CImage *CImage::ShrinkBWToGRAY256(int neww,int newh) const
{
	CImage* img2;
	int i,j,w,h,m,n;
	TCBitmap bits1,bits4,bits3;
	TBitmap bits2;
	TByte omask,mask;
	int rectw,recth;
	int x,y,nextx,nexty;
	unsigned int nbpoint,nbnoir;
	int *scalex = 0;
	int *pscalex = 0;
	int nextlineoffset;
	int bitpos;

	assert(CImageInfo::Bilevel == this->GeEImageType());

	w=GetWidth();
	h=GetHeight();
	if (w/2==neww && h/2==newh) return HalfSizeToGrayscale();

	nextlineoffset=GetRowOffset();

	img2=new CImage(neww,newh,CImageInfo::Gray256);
	img2->CopyResolution(*this);

	scalex=new int[neww+1];
	if (0 == scalex) {
		throw CImageException("CImage::ShrinkBWToGRAY256", "Out of memory");
	}

	for(i=0,pscalex=scalex;i<neww;i++) *pscalex++=SCALEX(i);
	*pscalex=w-1;

	nexty=0;
	/* pour chaque ligne de la nouvelle image */
	for (i=0;i<newh;i++) {
		y=nexty;
		nexty=SCALEY(i+1);
		if (nexty>h) nexty=h;
		recth=nexty-y;
		if (recth<1) recth=1;
		bits2=img2->GetRow(i);
		bits1=GetRow(y);
		nextx=0;
		pscalex=scalex+1;
		/* pour chaque point de la nouvelle image */
		for (j=0;j<neww;j++) {
			x=nextx;
			nextx=*pscalex++;
			bits4=bits1+x/8;
			bitpos=x%8;
			omask=0x80u >> bitpos;
			nbnoir=0;
			/* on calcule le rectangle de taille rectw x rect h dans l'image
			de de'part. On va calculer le nombre de points noirs de ce rectangle */
			rectw=nextx-x;
			if (rectw<1) rectw=1;
			/* Nombre de points du rectangle */
			nbpoint=rectw*recth;
			m=recth;
			/* for each rectangle line */
			do {
				bits3=bits4;
				mask=omask;
				n=rectw;
				/* only white ? */
				if (*bits3==0) {
					/* all pixels in this line are white and inside current byte */
					if ((8-bitpos)>=rectw) {	      
						goto nextline;
					} else {	      
						/* pass 8-bitpos white pixels and work with next byte */
						bits3++;
						mask=0x80;
						n-=(8-bitpos);
						assert(n > 0);
						/* pass groups of 8 white pixels */
						while (*bits3==0) {
							n-=8;		
							/* contains this line only white pixels ? */
							if (n<=0) goto nextline;
							bits3++;
						}
					} //if ((8-bitpos)>=rectw)
				} //if (*bits3==0)
				/* for each pixel in rectangle */
				do {
					/* is current pixel black? */
					if ((*bits3 & mask))
						nbnoir++;
					/* no more pixels to testing? */
					if ((--n)<=0) goto nextline;
					/* goto next pixel. Is it last point in the byte? */
					if (mask==01) {
						bits3++;
						mask=0x80;
						/* pass groups of 8 white pixels */
						while (*bits3==0) {
							n-=8;		
							if (n<=0) goto nextline;
							bits3++;
						}
					} else mask>>=1;
				} while (1);	
		nextline:
				bits4+=nextlineoffset;
			} while(--m);
			*bits2++=255u-(nbnoir*255u)/nbpoint;
		} //for j
	} //for i
	delete [] scalex;
	return img2;
}

CImage *CImage::ResizeGRAY256(int neww,int newh) const
{
  if (0 >= neww) {
	  throw CImageException("CImage::ResizeGRAY256", "Incorrect width value");
  }
  if (0 >= newh) {
	  throw CImageException("CImage::ResizeGRAY256", "Incorrect height value");
  }
  CImage * img2 = 0;
  int i,j,w,h,m,n;
  TCBitmap bits1,bits3,bits4;
  TBitmap bits2;
  int rectw,recth;
  int x,y,nextx,nexty;
  unsigned int lum,nbpoint;
  int *scalex = 0;
  int *pscalex = 0;
  int nextlineoffset;
  
  w=GetWidth();
  h=GetHeight();
  nextlineoffset=GetRowOffset();
  
  img2=new CImage(neww,newh,CImageInfo::Gray256);
  img2->CopyResolution(*this);

  scalex=new int[neww+1];
  if (0 == scalex) {
	  if (0 != img2) {
		  delete img2;
	  }
	  throw CImageException("CImage::ResizeGRAY256", "Out of memory");
  }
  
  for(i=0,pscalex=scalex;i<neww;i++) *pscalex++=SCALEX(i);
  *pscalex=w-1;
  
  if (neww>w && newh>h) {
    for (i=0;i<newh;i++) {
      y=SCALEY(i);
      bits2=img2->GetRow(i);
      bits1=GetRow(y);
      pscalex=scalex;
      for (j=0;j<neww;j++) 
	*bits2++ = *(bits1+*pscalex++);
    }
  }
  else {
    nexty=0;
    for (i=0;i<newh;i++) {
      y=nexty;
      nexty=SCALEY(i+1);
      if (nexty>=h) nexty=h-1;
      recth=nexty-y;
      if (recth<1) recth=1;
      bits2=img2->GetRow(i);
      bits1=GetRow(y);
      nextx=0;
      pscalex=scalex+1;
      for (j=0;j<neww;j++) {
	x=nextx;
	nextx=*pscalex++;
	bits4=bits1+x;
	lum=0;
	rectw=nextx-x;
	if (rectw<1) rectw=1;
	
	nbpoint=rectw*recth;
	m=recth;
	do {
	  bits3=bits4;
	  n=rectw; do { lum+=*bits3++; } while (--n);
	  bits4+=nextlineoffset;
	} while(--m);
	*bits2++=lum/nbpoint;
      }
    }
  }
  delete [] scalex;
  return img2;
}

CImage *CImage::ResizeGRAYFLOAT(int neww,int newh) const
{
  if (0 >= neww) {
  	throw CImageException("CImage::ResizeGRAYFLOAT", "Incorrect width value");
  }
  if (0 >= newh) {
	throw CImageException("CImage::ResizeGRAYFLOAT", "Incorrect height value");
  }
  CImage* img2 = 0;
  int i,j,w,h,m,n;
  const TFSample *bits1,*bits3,*bits4;
  TFSample *bits2;
  int rectw,recth;
  int x,y,nextx,nexty;
  TFSample lum;
  int nbpoint;
  int *scalex = 0;
  int *pscalex = 0;
  int nextlineoffset;
  
  w=GetWidth();
  h=GetHeight();
  nextlineoffset=GetRowOffset();
  
  img2=new CImage(neww,newh,CImageInfo::GrayFloat);
  img2->CopyResolution(*this);

  scalex=new int[neww+1];
  if (0 == scalex) {
	  if (0 != img2) {
		  delete img2;
	  }
	  throw CImageException("CImage::ResizeGRAYFLOAT", "Out of memory");
  }
  
  for(i=0,pscalex=scalex;i<neww;i++) *pscalex++=SCALEX(i);
  *pscalex=w-1;
  
  if (neww>w && newh>h) {
    for (i=0;i<newh;i++) {
      y=SCALEY(i);
      bits2=img2->GetFRow(i);
      bits1=GetFRow(y);
      pscalex=scalex;
      for (j=0;j<neww;j++) 
	*bits2++ = *(bits1+*pscalex++);
    }
  }
  else {
    nexty=0;
    for (i=0;i<newh;i++) {
      y=nexty;
      nexty=SCALEY(i+1);
      if (nexty>=h) nexty=h-1;
      recth=nexty-y;
      if (recth<1) recth=1;
      bits2=img2->GetFRow(i);
      bits1=GetFRow(y);
      nextx=0;
      pscalex=scalex+1;
      for (j=0;j<neww;j++) {
	x=nextx;
	nextx=*pscalex++;
	bits4=bits1+x;
	lum=(TFSample) 0.0;
	rectw=nextx-x;
	if (rectw<1) rectw=1;
	
	nbpoint=rectw*recth;
	m=recth;
	do {
	  bits3=bits4;
	  n=rectw; do { lum+=*bits3++; } while (--n);
	  bits4+=nextlineoffset;
	} while(--m);
	*bits2++=lum/nbpoint;
      }
    }
  }
  delete [] scalex;
  return img2;
}

CImage *CImage::ResizeRGB(int neww,int newh) const
{
  if (0 >= neww) {
	throw CImageException("CImage::ResizeRGB", "Incorrect width value");
  }
  if (0 >= newh) {
	throw CImageException("CImage::ResizeRGB", "Incorrect height value");
  }
  CImage * img2 = 0;
  int i,j,w,h,m,n;
  TCBitmap bits1,bits3,bits4;
  TBitmap bits2;
  int rectw,recth;
  int x,y,nextx,nexty;
  unsigned int red,green,blue,nbpoint;
  int *scalex = 0;
  int *pscalex = 0;
  int nextlineoffset;
  
  w=GetWidth();
  h=GetHeight();
  nextlineoffset=GetRowOffset();
  
  img2=new CImage(neww,newh,CImageInfo::ColorRGB);
  img2->CopyResolution(*this);

  scalex=new int[neww+1];
  if (0 == scalex) {
	  if (0 != img2) {
		  delete img2;
	  }
	  throw CImageException("CImage::ResizeRGB", "Out of memory");
  }
  
  for(i=0,pscalex=scalex;i<neww;i++) *pscalex++=SCALEX(i);
  *pscalex=w-1;
  
  
  if (neww>w && newh>h) {
    for (i=0;i<newh;i++) {
      y=SCALEY(i);
      bits2=img2->GetRow(i);
      bits3=GetRow(y);
      pscalex=scalex;
      for (j=0;j<neww;j++) {
	bits1=bits3+(*pscalex++)*3;
	*bits2++=*bits1++;
	*bits2++=*bits1++;
	*bits2++=*bits1++;
      }
    }
  }
  else {
    nexty=0;
    for (i=0;i<newh;i++) {
      y=nexty;
      nexty=SCALEY(i+1);
      if (nexty>=h) nexty=h-1;
      recth=nexty-y;
      if (recth<1) recth=1;
      bits2=img2->GetRow(i);
      bits1=GetRow(y);
      nextx=0;
      pscalex=scalex+1;
      for (j=0;j<neww;j++) {
	x=nextx;
	nextx=*pscalex++;
	if (nextx>=w) nextx=w-1;
	bits4=bits1+x*3;
	red=green=blue=0;
	rectw=nextx-x;
	if (rectw<1) rectw=1;
	
	nbpoint=rectw*recth;
	for(m=recth;m;m--) {
	  bits3=bits4;
	  for (n=rectw;n;n--) {
	    red+=*bits3++;
	    green+=*bits3++;
	    blue+=*bits3++;
	  }
	  bits4+=nextlineoffset;
	}
	*bits2++ =red/nbpoint;
	*bits2++ =green/nbpoint;
	*bits2++ =blue/nbpoint;
      }
    }
  }

  delete [] scalex;
  return img2;
}

CImage *CImage::ResizeCOLORFLOAT(int neww,int newh) const
{
  if (0 >= neww) {
	throw CImageException("CImage::ResizeCOLORFLOAT", "Incorrect width value");
  }
  if (0 >= newh) {
	throw CImageException("CImage::ResizeCOLORFLOAT", "Incorrect height value");
  }
  CImage * img2 = 0;
  int i,j,w,h,m,n;
  const TFSample *bits1,*bits3,*bits4;
  TFSample *bits2;
  int rectw,recth;
  int x,y,nextx,nexty;
  TFSample red,green,blue;
  int nbpoint;
  int *scalex = 0;
  int *pscalex = 0;
  int nextlineoffset;
  
  w=GetWidth();
  h=GetHeight();
  nextlineoffset=GetRowOffset();
  
  img2=new CImage(neww,newh,CImageInfo::ColorFloat);
  img2->CopyResolution(*this);

  scalex=new int[neww+1];
  if (0 == scalex) {
	  if (0 != img2) {
		  delete img2;
	  }
	  throw CImageException("CImage::ResizeCOLORFLOAT", "Out of memory");
  }
  
  for(i=0,pscalex=scalex;i<neww;i++) *pscalex++=SCALEX(i);
  *pscalex=w-1;
  
  if (neww>w && newh>h) {
    for (i=0;i<newh;i++) {
      y=SCALEY(i);
      bits2=img2->GetFRow(i);
      bits3=GetFRow(y);
      pscalex=scalex;
      for (j=0;j<neww;j++) {
	bits1=bits3+(*pscalex++)*3;
	*bits2++=*bits1++;
	*bits2++=*bits1++;
	*bits2++=*bits1++;
      }
    }
  }
  else {
    nexty=0;
    for (i=0;i<newh;i++) {
      y=nexty;
      nexty=SCALEY(i+1);
      if (nexty>=h) nexty=h-1;
      recth=nexty-y;
      if (recth<1) recth=1;
      bits2=img2->GetFRow(i);
      bits1=GetFRow(y);
      nextx=0;
      pscalex=scalex+1;
      for (j=0;j<neww;j++) {
	x=nextx;
	nextx=*pscalex++;
	if (nextx>=w) nextx=w-1;
	bits4=bits1+x*3;
	red=green=blue=(TFSample) 0.0;
	rectw=nextx-x;
	if (rectw<1) rectw=1;
	
	nbpoint=rectw*recth;
	for(m=recth;m;m--) {
	  bits3=bits4;
	  for (n=rectw;n;n--) {
	    red+=*bits3++;
	    green+=*bits3++;
	    blue+=*bits3++;
	  }
	  bits4+=nextlineoffset;
	}
	*bits2++ =red/nbpoint;
	*bits2++ =green/nbpoint;
	*bits2++ =blue/nbpoint;
      }
    }
  }
  delete [] scalex;
  return img2;
}

CImage * CImage::TransformAux(EImageType tp,int neww,int newh) const
{
  if (0 >= neww) {
    throw CImageException("CImage::TransformAux", "Incorrect width value");
  }
  if (0 >= newh) {
	throw CImageException("CImage::TransformAux", "Incorrect height value");
  }	
  CImage* img2 = 0;
  CImage* img3 = 0;
  int w,h;
  EImageType oldtp;
  w=GetWidth();
  h=GetHeight();
  oldtp=GeEImageType();
  
  switch (oldtp) {
  case CImageInfo::Bilevel:
    if (neww < w && newh < h) {
      img2=ShrinkBWToGRAY256(neww,newh);
      return img2;
    } else {
      img2=ToGRAY256();
	  try {
		img3=img2->ResizeGRAY256(neww,newh);
	  } catch (...) {
		  if (0 != img2) {
			  delete img2;
		  }
		  throw;
	  }
      delete img2;
      return img3;
    }
  case CImageInfo::Gray16:
    img2=ToGRAY256();
	try {
		img3=img2->ResizeGRAY256(neww,newh);
	} catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
    delete img2;
    return img3;
  case CImageInfo::Gray256:
    return ResizeGRAY256(neww,newh);
  case CImageInfo::Color16:
  case CImageInfo::Color256:
    if (tp==CImageInfo::Bilevel || tp==CImageInfo::Gray16 
        || tp==CImageInfo::Gray256 || tp==CImageInfo::GrayFloat) {
		    img2=ToGRAY256();
			try {
				img3=img2->ResizeGRAY256(neww,newh);
			} catch (...) {
				if (0 != img2) {
					delete img2;
				}
				throw;
			}
		    delete img2;
		    return img3;
    } else {
		    img2=ToRGB();
			try {
				img3=img2->ResizeRGB(neww,newh);
			} catch (...) {
				if (0 != img2) {
					delete img2;
				}
				throw;
			}
		    delete img2;
		    return img3;
    }
  case CImageInfo::ColorRGB:
    if (tp==CImageInfo::Bilevel || tp==CImageInfo::Gray16 
        || tp==CImageInfo::Gray256 || tp==CImageInfo::GrayFloat) {
		    img2=ToGRAY256();
			try {
				img3=img2->ResizeGRAY256(neww,newh);
			} catch (...) {
				if (0 != img2) {
					delete img2;
				}
				throw;
			}
		    delete img2;
		    return img3;
    } else
      return ResizeRGB(neww,newh);
  case CImageInfo::GrayFloat:
    return ResizeGRAYFLOAT(neww,newh);
  case CImageInfo::ColorFloat:
    if (tp==CImageInfo::GrayFloat) {
      img2=ToGRAYFLOAT();
	  try {
		img3=img2->ResizeGRAYFLOAT(neww,newh);
	  } catch (...) {
		  if (0 != img2) {
			  delete img2;
		  }
		  throw;
	  }
      delete img2;
      return img3;
    } 
    else
    if (tp==CImageInfo::Bilevel || tp==CImageInfo::Gray16 
        || tp==CImageInfo::Gray256 || tp==CImageInfo::GrayFloat) {
	img2=ToGRAY256();
	try {
		img3=img2->ResizeGRAY256(neww,newh);
	} catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	delete img2;
	return img3;
      } else
	return ResizeCOLORFLOAT(neww,newh);
  default:
    throw CImageException("CImage::Resize","Unknown Type");
  }
  return NULL;
}


CImage *CImage::ConvertAndResize(EImageType newtype,  int neww, int newh) const

{
  CImage *img2 = 0;
  CImage *img3 = 0;
  try {
	img2=TransformAux(newtype,neww,newh);
	if (img2->GeEImageType()==newtype)
		return img2;
	img3=img2->Convert(newtype);
  } catch (...) {
	  delete img2;
	  throw;
  }
  delete img2;
  return img3;
}

CImage *CImage::Resize(int neww, int newh) const
{
  return ConvertAndResize(GeEImageType(),neww,newh);
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
