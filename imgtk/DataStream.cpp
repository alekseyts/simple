//////////////////////////////////////////////////////////////////////
// @doc STREAM

#include <sys/stat.h>

#include "DataStream.h"



#if !defined(__STREAM_EXCEPTION_H)
#include "StreamException.h"
#endif


#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CFileStream::CFileStream(const char *filename,TMode mode)
{
  switch(mode) {
  case In:
    mFile=fopen(filename,"rb");
    break;
  case Out:
    mFile=fopen(filename,"w+b");
    break;
  default:
    mFile=NULL;
  }
  if (mFile==NULL) 
    throw CStreamException("CFileStream constructor","Can't open file");

}

CFileStream::~CFileStream()
{
  if (mFile) Close();
}

size_t CFileStream::Read( void* lpBuf, size_t nCount )
{
  size_t realCount=fread(lpBuf,1,nCount,mFile);
  if (realCount!=nCount && ferror(mFile))
    throw CStreamException("CFileStream::Read","Read Error");
  return realCount;

}

size_t CFileStream::Write(const void* lpBuf, size_t nCount )
{
  size_t realCount=fwrite(lpBuf,1,nCount,mFile);
  if (realCount!=nCount)
    throw CStreamException("CFileStream::Write","Write Error");
  return realCount;

}

long CFileStream::Seek(long offset,TOrigin origin)
{
  int error;
  switch(origin) {
  case Begin:
    error=fseek(mFile,offset,SEEK_SET);
    break;
  case Current:
    error=fseek(mFile,offset,SEEK_CUR);
    break;
  case End:
    error=fseek(mFile,offset,SEEK_END);
    break;
  default:
    throw CStreamException("CFileStream::Seek","Illegal Origin");
  }
  if (error)
    throw CStreamException("CFileStream::Seek","Seek error");
  return ftell(mFile);    
}

void CFileStream::Close()
{
  int error=fclose(mFile);
  mFile=NULL;
  if (error) 
    throw CStreamException("CFileStream::Close","Error while closing file");
}

long CFileStream::Size()
{
  struct stat st;
  if (fstat(fileno(mFile),&st)==-1)
    throw CStreamException("CFileStream::Size","Error while getting file size");
  return st.st_size;
  
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
