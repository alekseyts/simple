//////////////////////////////////////////////////////////////////////
// @doc HISTOGRAM
//////////////////////////////////////////////////////////////////////

#if !defined(__HISTOBUILDER_H)
#define __HISTOBUILDER_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif


#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

class CImage;
class CHistogram;


//@class Histogram builder. Builds an histogram from an image
class IMGTK_EXPORT CHistoBuilder  {
 public:
    //@cmember Build vertical histogram
    static CHistogram *VertHisto(const CImage& image) ;
    //@cmember Build horizontal histogram
    static CHistogram *HorzHisto(const CImage& image) ;
 private:
    static CHistogram *VertHistoBilevel(const CImage& image) ;
    static CHistogram *VertHistoGray256(const CImage& image) ;
    static CHistogram *HorzHistoBilevel(const CImage& image) ;
    static CHistogram *HorzHistoGray256(const CImage& image) ;

 private:
    //@cmember No Constructor !
    CHistoBuilder();
};

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__HISTOBUILDER_H)
