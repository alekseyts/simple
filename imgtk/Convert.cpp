//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#include "Image.h"

#if !defined(__IMAGE_EXCEPTION_H)
#include "ImageException.h"
#endif


#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


/* ----------------------------------------------------------------*/

TSample CImage::mThreshold = 0; /* Use optimal threshold by default */

EColorReductionMethod CImage::mColorReductionMethod = CImage::Thresholding;

EColorReductionMethod 
CImage::UseColorReductionMethod(EColorReductionMethod method)
{
  EColorReductionMethod previous=mColorReductionMethod;
  mColorReductionMethod=method;
  return previous;
}

TSample CImage::UseThreshold(TSample threshold)
{
  TSample previous=mThreshold;
  mThreshold=threshold;
  return previous;
}

/* ---------------------------------------------------------------- */
/* Conversion vers le N&B */

CImage *CImage::GRAY16ToBW() const
{
  CImage * img2 = 0;
  int i,j,w,h;
  TCBitmap bits1;
  TBitmap bits2;
  unsigned char mask,umask;
  bool even;
  int threshd;
  w=GetWidth();
  h=GetHeight();

  if (mThreshold==0) 
    threshd=OptimalThreshold();
  else
    threshd=mThreshold;

  threshd/=16;
  
  img2=new CImage(w,h,CImageInfo::Bilevel);
  if (0 == img2) {
	  throw CImageException("CImage::GRAY16ToBW", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    mask=0x80;
    umask=0;
    even=true;
    for(j=0;j<w;j++) {
      if (even) {
	if (((*bits1)>>4) < threshd)
	  umask|=mask;
      } else {
	if (((*bits1)&0xF) < threshd)
	  umask|=mask;
	bits1++;
      }
      even=!even;
      if (mask==1) {
	mask=0x80;
	*bits2++=umask;
	umask=0;
      } else mask>>=1;
    }
    if (mask!=0x80) *bits2=umask;
  }
  return img2;
}

/* 256 niveaux de gris -> N&B
*/
CImage *CImage::GRAY256ToBW() const
{
  CImage * img2 = 0;
  int i,j,w,h;
  TCBitmap  bits1;
  TBitmap bits2;
  unsigned char mask,umask;
  unsigned char threshold;

  if (mThreshold==0) 
    threshold=OptimalThreshold();
  else
    threshold=mThreshold;

  w=GetWidth();
  h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::Bilevel);
  if (0 == img2) {
	  throw CImageException("CImage::GRAY256oBW", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    mask=128;
    umask=0;
    for(j=0;j<w;j++) {
      if (*bits1++<threshold)
	umask|=mask;
      if (mask==1) {
	mask=128;
	*bits2++=umask;
	umask=0;
      } else mask>>=1;
    }
    if (mask!=128) *bits2=umask;
  }
  return img2;
}

CImage * CImage::COLOR16ToBW() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=COLOR16ToGRAY16();
		img3=img2->GRAY16ToBW();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
  return img3;
}

CImage *CImage::COLOR256ToBW() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=COLOR256ToGRAY256();
		img3=img2->GRAY256ToBW();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
  return img3;
}

CImage *CImage::RGBToBW() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBToGRAY256();
		img3=img2->GRAY256ToBW();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::GRAYFLOATToBW() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAYFLOATToGRAY256();
		img3=img2->GRAY256ToBW();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBFLOATToBW() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBFLOATToGRAY256();
		img3=img2->GRAY256ToBW();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

/* ---------------------------------------------------------------- */
/* Conversions vers le gris 16 Niveaux */

CImage *CImage::BWToGRAY16() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  unsigned char mask;
  bool even;
  unsigned char p;
  int w=GetWidth(),h=GetHeight();
  
  
  img2=new CImage(w,h,CImageInfo::Gray16);
  if (0 == img2) {
	  throw CImageException("CImage::BWToGRAY16", "Out of memory");
  }
  img2->CopyResolution(*this);
  
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    mask=128;
    even=true;
    
    for(j=0;j<w;j++) {
      if (even) p=((*bits1 & mask) ? 0 : 0xF0);
      else *bits2++=((*bits1 & mask) ? p : p+15);
      even=!even;
      if (mask==1) {
	mask=128;
	bits1++;
      } else mask>>=1;
    }
    if (!even) *bits2=p;
  }
  return img2;
}

CImage *CImage::GRAY256ToGRAY16() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  bool even;
  unsigned char p;
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::Gray16);
  if (0 == img2) {
	  throw CImageException("CImage::GRAY256ToGRAY16", "Out of memory");
  }
  if (img2==NULL) return NULL;
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    even=true;
    for(j=0;j<w;j++) {
      if (even)
        p=((*bits1++)/16)<<4;
      else
        *bits2++=p+ (*bits1++)/16;
      even=!even;
    }
    if (!even) *bits2=p;
  }
  return img2;
}

CImage *CImage::COLOR16ToGRAY16() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  bool even;
  unsigned char p;
  int w=GetWidth(),h=GetHeight();
  const CColorPalette * pal=GetColorPalette();
  
  img2=new CImage(w,h,CImageInfo::Gray16);
  if (0 == img2) {
	  throw CImageException("CImage::COLOR16ToGRAY16", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    even=true;
    for(j=0;j<w;j++) {
      if (even) {
	p=pal->GetLuma((*bits1)>>4)/16;
      } else {
	*bits2++=p+pal->GetLuma((*bits1)>>4)/16;
	bits1++;
      }
      even=!even;
    }
    if (!even) *bits2=p;
  }
  return img2;
}

CImage *CImage::COLOR256ToGRAY16() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  bool even;
  unsigned char p;
  int w=GetWidth(),h=GetHeight();
  const CColorPalette * pal=GetColorPalette();
  
  img2=new CImage(w,h,CImageInfo::Gray16);
  if (0 == img2) {
	  throw CImageException("CImage::COLOR256ToGRAY16", "Out of memory");
  }
  if (img2==NULL) return NULL;
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    even=true;
    for(j=0;j<w;j++) {
      if (even)
        p=pal->GetLuma(*bits1++)<<4;
      else
        *bits2++=p+(pal->GetLuma(*bits1++)<<4);
      even=!even;
    }
    if (!even) *bits2=p;
  }
  return img2;
}

CImage *CImage::RGBToGRAY16() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBToGRAY256();
		img3=img2->GRAY256ToGRAY16();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::GRAYFLOATToGRAY16() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAYFLOATToGRAY256();
		img3=img2->GRAY256ToGRAY16();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBFLOATToGRAY16() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBFLOATToGRAY256();
		img3=img2->GRAY256ToGRAY16();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

/* ---------------------------------------------------------------- */
/* Conversions vers le gris 256 niveaux */


/* Conversion N&B -> 256 niveaux de gris
*/
CImage *CImage::BWToGRAY256() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;  
  TBitmap  bits2;
  unsigned char chunk;
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::Gray256);
  if (0 == img2) {
	  throw CImageException("CImage::BWToGRAY256", "Out of memory");
  }
  if (img2==NULL) return NULL;
  img2->CopyResolution(*this);
  /* memset(img2->Bits,img2->RowSize*img2->Height,0); */
  
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    chunk=*bits1;
    
    for(j=w/8;j;j--) {
      *bits2++= (chunk & 0x80) ? 0: 0xFF;
      *bits2++= (chunk & 0x40) ? 0: 0xFF;
      *bits2++= (chunk & 0x20) ? 0: 0xFF;
      *bits2++= (chunk & 0x10) ? 0: 0xFF;
      *bits2++= (chunk & 0x08) ? 0: 0xFF;
      *bits2++= (chunk & 0x04) ? 0: 0xFF;
      *bits2++= (chunk & 0x02) ? 0: 0xFF;
      *bits2++= (chunk & 0x01) ? 0: 0xFF;
	  if ((1 == j) && (0 == w%8)) {
		  goto nomore;
	  }
	  ++bits1;
      chunk=*bits1;
    }
    j=w%8;
    if (!j--) goto nomore;
    *bits2++= (chunk & 0x80) ? 0: 0xFF;
    if (!j--) goto nomore;
    *bits2++= (chunk & 0x40) ? 0: 0xFF;
    if (!j--) goto nomore;
    *bits2++= (chunk & 0x20) ? 0: 0xFF;
    if (!j--) goto nomore;
    *bits2++= (chunk & 0x10) ? 0: 0xFF;
    if (!j--) goto nomore;
    *bits2++= (chunk & 0x08) ? 0: 0xFF;
    if (!j--) goto nomore;
    *bits2++= (chunk & 0x04) ? 0: 0xFF;
    if (!j--) goto nomore;
    *bits2++= (chunk & 0x02) ? 0: 0xFF;
    if (!j--) goto nomore;
    *bits2++= (chunk & 0x01) ? 0: 0xFF;
nomore:
    ;
  }
  return img2;
}

CImage *CImage::GRAY16ToGRAY256() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  bool even;
  unsigned char p,chunk;
  int w,h;
  
  w=GetWidth();
  h=GetHeight();
  img2=new CImage(w,h,CImageInfo::Gray256);
  if (0 == img2) {
	  throw CImageException("CImage::GRAY16ToGRAY256", "Out of memory");
  }
  if (img2==NULL) return NULL;
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    even=true;
    chunk=*bits1;
    for(j=0;j<w;j++) {
      if (even) 
        p=chunk>>4;
      else {
        p=chunk%16;
        chunk=*(++bits1);
      }
      *bits2++=p*16+p;
      even=!even;
    }
  }
  return img2;
}

CImage *CImage::COLOR16ToGRAY256() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  bool even;
  int w=GetWidth(),h=GetHeight();
  const CColorPalette * pal=GetColorPalette();
  
  img2=new CImage(w,h,CImageInfo::Gray256);
  if (0 == img2) {
	  throw CImageException("CImage::COLOR16ToGRAY256", "Out of memory");
  }
  if (img2==NULL) return NULL;
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    even=true;
    for(j=0;j<w;j++) {
      if (even) 
	*bits2++=pal->GetLuma((*bits1)>>4);
      else 
	*bits2++=pal->GetLuma((*bits1++)%16);
      even=!even;
    }
  }
  return img2;
}

CImage *CImage::COLOR256ToGRAY256() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  int w=GetWidth(),h=GetHeight();

  const CColorPalette * pal=GetColorPalette();
  
  img2=new CImage(w,h,CImageInfo::Gray256);
  if (0 == img2) {
	  throw CImageException("CImage::COLOR256ToGRAY256", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    for(j=0;j<w;j++) 
      *bits2++=pal->GetLuma(*bits1++);
  }
  return img2;
}

CImage *CImage::RGBToGRAY256() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  unsigned long lum;
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::Gray256);
  if (0 == img2) {
	  throw CImageException("CImage::RGBToGRAY256", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    for(j=0;j<w;j++) {
      lum= *(bits1+CImage::ROffset)*CColor::GetRedFactor();
      lum+=*(bits1+CImage::GOffset)*CColor::GetGreenFactor();
      lum+=*(bits1+CImage::BOffset)*CColor::GetBlueFactor();
      *bits2++=(TSample) (lum/100);
      bits1+=3;
    }
  }
  return img2;
}

CImage *CImage::GRAYFLOATToGRAY256() const
{
  CImage * img2 = 0;
  int i,j;
  const TFSample * bits1;
  TFSample val;
  TBitmap  bits2;
  
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::Gray256);
  if (0 == img2) {
	  throw CImageException("CImage::GRAYFLOATToGRAY256", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetFRow(i);
    bits2=img2->GetRow(i);
    for(j=0;j<w;j++) {
      val=*bits1++;
      *bits2++=CLUMP(val);
    }
  }
  return img2;
}

CImage *CImage::RGBFLOATToGRAY256() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBFLOATToGRAYFLOAT();
		img3=img2->GRAYFLOATToGRAY256();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

/* ---------------------------------------------------------------- */
/* Conversions vers le 16 couleurs */

CImage *CImage::BWToCOLOR16() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  unsigned char mask;
  bool even;
  unsigned char p;
  CColorPalette * palette = 0;
  int w=GetWidth(),h=GetHeight();
  

  palette=new CColorPalette(16);
  if (0 == palette) {
	  throw CImageException("CImage::BWToCOLOR16", "Out of memory");
  } 

  palette->GetColor(0).SetRGB(0,0,0);
  palette->GetColor(1).SetRGB(255,255,255);

  img2=new CImage(w,h,CImageInfo::Color16,GetXResolution(),GetYResolution(),palette);
  if (0 == img2) {
	  if (0 != palette) {
		  delete palette;
	  }
	  throw CImageException("CImage::BWToCOLOR16", "Out of memory");
  } 
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    mask=128;
    even=true;
    
    for(j=0;j<w;j++) {
      if (even) p=((*bits1 & mask) ? 0 : 0x10);
      else *bits2++=((*bits1 & mask) ? p : p+1);
      even=!even;
      if (mask==1) {
	mask=128;
	bits1++;
      } else mask>>=1;
    }
    if (!even) *bits2=p;
  }
  return img2;
}

CImage *CImage::GRAY16ToCOLOR16() const
{
  CColorPalette * palette = 0;
  int i;
  CImage * img2 = 0;
  
  palette=new CColorPalette(16);
  if (0 == palette) {
	  throw CImageException("CImage::GRAY16ToCOLOR16", "Out of memory");
  }
  for(i=0;i<16;i++)
    palette->GetColor(i).SetRGB(16*i+i,16*i+i,16*i+i);
  img2=new CImage(GetWidth(),GetHeight(),CImageInfo::Color16,GetXResolution(),
		GetYResolution(),palette);
  if (0 == img2) {
	  delete palette;
	  throw CImageException("CImage::GRAY16ToCOLOR16", "Out of memory");
  }
  img2->CopyResolution(*this);
  
  memcpy(img2->GetBitmap(),GetBitmap(),GetBitmapSize());
  
  return img2;
}

CImage *CImage::GRAY256ToCOLOR16() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAY256ToGRAY16();
		img3=img2->GRAY16ToCOLOR16();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::COLOR256ToCOLOR16() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=COLOR256ToRGB();
		img3=img2->RGBToCOLOR16();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBToCOLOR16() const
{
  return Quantize(4,false);
}

CImage *CImage::RGBToCOLOR16Dither() const
{
  return Quantize(4,true);
}

CImage *CImage::GRAYFLOATToCOLOR16() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAYFLOATToGRAY256();
		img3=img2->GRAY256ToCOLOR16();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBFLOATToCOLOR16() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBFLOATToRGB();
		img3=img2->RGBToCOLOR16();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBFLOATToCOLOR16Dither() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBFLOATToRGB();
		img3=img2->RGBToCOLOR16Dither();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}


/* ---------------------------------------------------------------- */
/* Conversions vers le 256 couleurs */

CImage *CImage::BWToCOLOR256() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  unsigned char chunk;
  CColorPalette * palette = 0;
  int w=GetWidth(),h=GetHeight();
  
  palette=new CColorPalette(256);
  if (0 == palette) {
	  throw CImageException("CImage::BWToCOLOR256", "Out of memory");
  }
  palette->GetColor(0).SetRGB(0,0,0);
  palette->GetColor(1).SetRGB(255,255,255);
  
  img2=new CImage(w,h,CImageInfo::Color256,GetXResolution(),GetYResolution(),palette);
  if (0 == img2) {
	  delete palette;
	  throw CImageException("CImage::BWToCOLOR256", "Out of memory");
  }

  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    
    chunk=*bits1;
    
    for(j=w;;) {
      *bits2++= (chunk & 0x80) ? 0 : 1;
      if (!(--j)) break;
      *bits2++= (chunk & 0x40) ? 0 : 1;
      if (!(--j)) break;
      *bits2++= (chunk & 0x20) ? 0 : 1;
      if (!(--j)) break;
      *bits2++= (chunk & 0x10) ? 0 : 1;
      if (!(--j)) break;
      *bits2++= (chunk & 0x08) ? 0 : 1;
      if (!(--j)) break;
      *bits2++= (chunk & 0x04) ? 0 : 1;
      if (!(--j)) break;
      *bits2++= (chunk & 0x02) ? 0 : 1;
      if (!(--j)) break;
      *bits2++= (chunk & 0x01) ? 0 : 1;
      if (!(--j)) break;
      chunk=*(++bits1);
    }
  }
  return img2;
}

CImage *CImage::GRAY16ToCOLOR256() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAY16ToGRAY256();
		img3=img2->GRAY256ToCOLOR256();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::GRAY256ToCOLOR256() const
{
  CColorPalette * palette = 0;
  int i;  
  palette=new CColorPalette(256);
  if (0 == palette) {
	  throw CImageException("CImage::GRAY256ToCOLOR256", "Out of memory");
  }
  CImage * img2 = 0;

  for(i=0;i<256;i++)
    palette->GetColor(i).SetRGB(i,i,i);
  img2=new CImage(GetWidth(),GetHeight(),CImageInfo::Color256,
    GetXResolution(),GetYResolution(),palette);
  if (0 == img2) {
	  delete palette;
	  throw CImageException("CImage::GRAY256ToCOLOR256", "Out of memory");
  }
  
  memcpy(img2->GetBitmap(),GetBitmap(),GetBitmapSize());
  
  return img2;
}

CImage *CImage::COLOR16ToCOLOR256() const
{
  CColorPalette * palette = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  bool even;
  int w=GetWidth(),h=GetHeight();
  const CColorPalette * pal1=GetColorPalette();
  
  CImage * img2 = 0;
   
  palette=new CColorPalette(256);
  if (0 == palette) {
	  throw CImageException("CImage::COLOR16ToCOLOR256", "Out of memory");
  }

  for(i=0;i<16;i++) {
    palette->GetColor(i)=GetColorPalette()->GetColor(i);
  }		

  img2=new CImage(w,h,CImageInfo::Color256,GetXResolution(),GetYResolution(),palette);
  if (0 == img2) {
	  delete palette;
	  throw CImageException("CImage::COLOR16ToCOLOR256", "Out of memory");
  }
  
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    even=true;
    for(j=0;j<w;j++) {
      if (even) 
        *bits2++=(*bits1)>>4;
      else 
        *bits2++=(*bits1++)%16;
      even=!even;
    }
  }
  return img2;
}

CImage *CImage::RGBToCOLOR256() const
{
  return Quantize(8,false);
}

CImage *CImage::RGBToCOLOR256Dither() const
{
  return Quantize(8,true);
}

CImage *CImage::GRAYFLOATToCOLOR256() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAYFLOATToGRAY256();
		img3=img2->GRAY256ToCOLOR256();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBFLOATToCOLOR256() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBFLOATToRGB();
		img3=img2->RGBToCOLOR256();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBFLOATToCOLOR256Dither() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBFLOATToRGB();
		img3=img2->RGBToCOLOR256Dither();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

/* ---------------------------------------------------------------- */
/* Conversions vers le RGB */

CImage *CImage::BWToRGB() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  unsigned char mask;
  int w=GetWidth(),h=GetHeight();
  unsigned char lum;
  
  img2=new CImage(w,h,CImageInfo::ColorRGB);
  if (0 == img2) {
	  throw CImageException("CImage::BWToRGB", "Out of memory");
  }
  img2->CopyResolution(*this);
  
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    mask=128;
    
    for(j=w;j;j--) {
      lum=((*bits1 & mask) ? 0 : 255);
      *bits2++=lum;
      *bits2++=lum;
      *bits2++=lum;
      if (mask==1) {
	mask=128;
	bits1++;
      } else mask>>=1;
    }
  }
  return img2;
}

CImage *CImage::GRAY16ToRGB() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  bool even;
  unsigned char p;
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::ColorRGB);
  if (0 == img2) {
	  throw CImageException("CImage::GRAY16ToRGB", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    even=true;
    for(j=0;j<w;j++) {
      if (even) 
        p=(*bits1)>>4;
      else 
        p=(*bits1++)%16;
      p=p*16+p;
      *bits2++=p;
      *bits2++=p;
      *bits2++=p;
      even=!even;
    }
  }
  return img2;
}

CImage *CImage::GRAY256ToRGB() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::ColorRGB);
  if (0 == img2) {
	  throw CImageException("CImage::GRAY256ToRGB", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    for(j=0;j<w;j++) {
      *bits2++=*bits1;
      *bits2++=*bits1;
      *bits2++=*bits1++;
    }
  }
  return img2;
}

CImage *CImage::COLOR16ToRGB() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  bool even;
  int w=GetWidth(),h=GetHeight();
  const CColorPalette * pal=GetColorPalette();
  CColor col;
  
  img2=new CImage(w,h,CImageInfo::ColorRGB);
  if (0 == img2) {
	  throw CImageException("CImage::COLOR16ToRGB", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    even=true;
    for(j=0;j<w;j++) {
      if (even) 
	col=pal->GetColor((*bits1)>>4);
      else 
	col=pal->GetColor((*bits1++)%16);
      *(bits2+CImage::ROffset)=col.GetRed(); 
      *(bits2+CImage::GOffset)=col.GetGreen();
      *(bits2+CImage::BOffset)=col.GetBlue();
      bits2+=3;
      even=!even;
    }
  }
  return img2;
}

CImage *CImage::COLOR256ToRGB() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap bits1;
  TBitmap bits2;
  int w=GetWidth(),h=GetHeight();
  const CColorPalette * pal=GetColorPalette();
  CColor col;
  
  img2=new CImage(w,h,CImageInfo::ColorRGB);
  if (0 == img2) {
	  throw CImageException("CImage::COLOR256ToRGB", "Out of memory");
  }
  img2->CopyResolution(*this);

  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetRow(i);
    for(j=0;j<w;j++) {
      col=pal->GetColor(*bits1++);
      *(bits2+CImage::ROffset)=col.GetRed(); 
      *(bits2+CImage::GOffset)=col.GetGreen();
      *(bits2+CImage::BOffset)=col.GetBlue();
      bits2+=3;
    }
  }
  return img2;
}

CImage *CImage::GRAYFLOATToRGB() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAYFLOATToGRAY256();
		img3=img2->GRAY256ToRGB();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBFLOATToRGB() const
{
  CImage * img2 = 0;
  int i,j;
  const TFSample * bits1;
  TFSample val;
  TBitmap  bits2;
  
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::ColorRGB);
  if (0 == img2) {
	  throw CImageException("RGBFLOATToRGB", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetFRow(i);
    bits2=img2->GetRow(i);
    for(j=0;j<w;j++) {
      val=*bits1++;
      *bits2++=CLUMP(val);
      val=*bits1++;
      *bits2++=CLUMP(val);
      val=*bits1++;
      *bits2++=CLUMP(val);
    }
  }
  return img2;
}

/* ---------------------------------------------------------------- */
/* Conversions vers le niveaux de gris flottant */

CImage *CImage::GRAY16ToGRAYFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAY16ToGRAY256();
		img3=img2->GRAY256ToGRAYFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::GRAY256ToGRAYFLOAT() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap  bits1;
  TFSample * bits2;
  
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::GrayFloat);
  if (0 == img2) {
	  throw CImageException("GRAY256ToGRAYFLOAT", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetFRow(i);
    for(j=0;j<w;j++) {
      *bits2++=(TFSample) *bits1++;
    }
  }
  return img2;
}

CImage *CImage::COLOR16ToGRAYFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=COLOR16ToGRAY256();
		img3=img2->GRAY256ToGRAYFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::COLOR256ToGRAYFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=COLOR256ToGRAY256();
		img3=img2->GRAY256ToGRAYFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::BWToGRAYFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=BWToGRAY256();
		img3=img2->GRAY256ToGRAYFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBToGRAYFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=RGBToGRAY256();
		img3=img2->GRAY256ToGRAYFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::RGBFLOATToGRAYFLOAT() const
{
  CImage * img2 = 0;
  int i,j;
  const TFSample * bits1;
  TFSample * bits2;
  TFSample lum;
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::GrayFloat,w,h);
  if (0 == img2) {
	  throw CImageException("CImage::RGBFLOATToGRAYFLOAT", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetFRow(i);
    bits2=img2->GetFRow(i);
    for(j=0;j<w;j++) {
      lum= *(bits1+CImage::FROffset)*CColor::GetRedFactor(); 
      lum+=*(bits1+CImage::FGOffset)*CColor::GetGreenFactor();
      lum+=*(bits1+CImage::FBOffset)*CColor::GetBlueFactor();
      *bits2++=lum/(float) 100.0;
      bits1+=3;
    }
  }
  return img2;
}

/* ---------------------------------------------------------------- */
/* Conversions vers le RGB flottant */

CImage *CImage::GRAY16ToRGBFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAY16ToRGB();
		img3=img2->RGBToRGBFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::GRAY256ToRGBFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=GRAY256ToGRAYFLOAT();
		img3=img2->GRAYFLOATToRGBFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
	return img3;
}

CImage *CImage::COLOR16ToRGBFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=COLOR16ToRGB();
		img3=img2->RGBToRGBFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
  return img3;
}

CImage *CImage::COLOR256ToRGBFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=COLOR256ToRGB();
		img3=img2->RGBToRGBFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
    return img3;
}

CImage *CImage::BWToRGBFLOAT() const
{
	CImage* img2 = 0;
	CImage* img3 = 0;
	try {
		img2=BWToRGB();
		if (img2==NULL) return NULL;
		img3=img2->RGBToRGBFLOAT();
		if (0 != img2) {
			delete img2;
		}
	}catch (...) {
		if (0 != img2) {
			delete img2;
		}
		throw;
	}
    return img3;
}

CImage *CImage::RGBToRGBFLOAT() const
{
  CImage * img2 = 0;
  int i,j;
  TCBitmap  bits1;
  TFSample * bits2;
  
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::ColorFloat);
  if (0 == img2) {
	  throw CImageException("CImage::RGBToRGBFLOAT", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetRow(i);
    bits2=img2->GetFRow(i);
    for(j=0;j<w;j++) {
      *bits2++=(TFSample) *bits1++;
      *bits2++=(TFSample) *bits1++;
      *bits2++=(TFSample) *bits1++;
    }
  }
  return img2;
}

CImage *CImage::GRAYFLOATToRGBFLOAT() const
{
  CImage * img2 = 0;
  int i,j;
  const TFSample * bits1;
  TFSample * bits2;
  int w=GetWidth(),h=GetHeight();
  
  img2=new CImage(w,h,CImageInfo::ColorFloat);
  if (0 == img2) {
	  throw CImageException("CImage::GRAYFLOATToRGBFLOAT", "Out of memory");
  }
  img2->CopyResolution(*this);
  for (i=0;i<h;i++) {
    bits1=GetFRow(i);
    bits2=img2->GetFRow(i);
    for(j=0;j<w;j++) {
      *bits2++=*bits1;
      *bits2++=*bits1;
      *bits2++=*bits1++;
    }
  }
  return img2;
}

/* ---------------------------------------------------------------- */

CImage *CImage::ToDither(EColorReductionMethod algo) const
{
  CImage* img2 = 0;
  CImage* img3 = 0;
  TSample threshold;
  switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("CImage::ToDither", "Out of memory");
		}
      return img2;    
    case CImageInfo::Gray16:
      img2=GRAY16ToGRAY256();
      break;
    case CImageInfo::Gray256:
      if (mThreshold==0)
	threshold=OptimalThreshold();
      else
	threshold=mThreshold;

      return Dither(algo,threshold);
    case CImageInfo::Color16:
      img2=COLOR16ToGRAY256();
      break;
    case CImageInfo::Color256:
      img2=COLOR256ToGRAY256();
      break;
    case CImageInfo::ColorRGB:
      img2=RGBToGRAY256();
      break;
    case CImageInfo::GrayFloat:
      img2=GRAYFLOATToGRAY256();
      break;
    case CImageInfo::ColorFloat:
      img2=RGBFLOATToGRAY256();
      break;
    default:
      throw CImageException("CImage::Convert","Unknown type");
  }
  if (mThreshold==0)
    threshold=OptimalThreshold();
  else
    threshold=mThreshold;

  img3=img2->Dither(algo,threshold);
  delete img2;
  return img3;
}

CImage * CImage::ToBW(EColorReductionMethod algo) const
{
  CImage* img2 = 0;
  if (algo!=Thresholding) return ToDither(algo);
  switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("ToBW", "Out of memory");
		}
		return img2;
    case CImageInfo::Gray16:
      return GRAY16ToBW();
    case CImageInfo::Gray256:
      return GRAY256ToBW();
    case CImageInfo::Color16:
      return COLOR16ToBW();
    case CImageInfo::Color256:
      return COLOR256ToBW();
    case CImageInfo::ColorRGB:
      return RGBToBW();
    case CImageInfo::GrayFloat:
      return GRAYFLOATToBW();
    case CImageInfo::ColorFloat:
      return RGBFLOATToBW();
    default:
      throw CImageException("CImage::Convert", "Unknown type");
  }
  return NULL;
}

CImage * CImage::ToGRAY16() const
{
	CImage* img2 = 0;
	switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
      return BWToGRAY16();
    case CImageInfo::Gray16:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("CImage::ToGRAY16", "Out of memory");
		}
		return img2;
    case CImageInfo::Gray256:
      return GRAY256ToGRAY16();
    case CImageInfo::Color16:
      return COLOR16ToGRAY16();
    case CImageInfo::Color256:
      return COLOR256ToGRAY16();
    case CImageInfo::ColorRGB:
      return RGBToGRAY16();
    case CImageInfo::GrayFloat:
      return GRAYFLOATToGRAY16();
    case CImageInfo::ColorFloat:
      return RGBFLOATToGRAY16();   
    default:
      throw CImageException("CImage::Convert", "Unknown type");
  }
  return NULL;
}

CImage * CImage::ToGRAY256() const
{
  CImage* img2 = 0;
  switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
      return BWToGRAY256();
    case CImageInfo::Gray16:
      return GRAY16ToGRAY256();
    case CImageInfo::Gray256:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("CImage::ToGRAY256", "Out of memory");
		}
		return img2;
    case CImageInfo::Color16:
      return COLOR16ToGRAY256();
    case CImageInfo::Color256:
      return COLOR256ToGRAY256();
    case CImageInfo::ColorRGB:
      return RGBToGRAY256();
    case CImageInfo::GrayFloat:
      return GRAYFLOATToGRAY256();
    case CImageInfo::ColorFloat:
      return RGBFLOATToGRAY256();    
    default:
      throw CImageException("CImage::Convert", "Unknown type");
  }
  return NULL;
}

CImage * CImage::ToCOLOR16(EColorReductionMethod algo) const
{
  CImage* img2 = 0;
  switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
      return BWToCOLOR16();
    case CImageInfo::Gray16:
      return GRAY16ToCOLOR16();
    case CImageInfo::Gray256:
      return GRAY256ToCOLOR16();
    case CImageInfo::Color16:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("CImage::ToCOLOR16", "Out of memory");
		}
		return img2;
    case CImageInfo::Color256:
      return COLOR256ToCOLOR16();
    case CImageInfo::ColorRGB:
      if (algo==Thresholding)
	return RGBToCOLOR16();
      return RGBToCOLOR16Dither();
    case CImageInfo::GrayFloat:
      return GRAYFLOATToCOLOR16();
    case CImageInfo::ColorFloat:
      if (algo==Thresholding)
	return RGBFLOATToCOLOR16();
      return RGBFLOATToCOLOR16Dither();    
    default:
      throw CImageException("CImage::Convert", "Unknown type");
  }
  return NULL;
}

CImage * CImage::ToCOLOR256(EColorReductionMethod algo) const
{
  CImage* img2 = 0;
  switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
      return BWToCOLOR256();
    case CImageInfo::Gray16:
      return GRAY16ToCOLOR256();
    case CImageInfo::Gray256:
      return GRAY256ToCOLOR256();
    case CImageInfo::Color16:
      return COLOR16ToCOLOR256();
    case CImageInfo::Color256:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("CImage::ToCOLOR256", "Out of memory");
		}
		return img2;
    case CImageInfo::ColorRGB:
      if (algo==Thresholding)
	return RGBToCOLOR256();
      return RGBToCOLOR256Dither();
    case CImageInfo::GrayFloat:
      return GRAYFLOATToCOLOR256();
    case CImageInfo::ColorFloat:
      if (algo==Thresholding)
	return RGBFLOATToCOLOR256();
      return RGBFLOATToCOLOR256Dither();    
    default:
      throw CImageException("CImage::Convert", "Unknown type");
  }
  return NULL;
}


CImage * CImage::ToRGB() const
{
  CImage* img2 = 0;
  switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
      return BWToRGB();
    case CImageInfo::Gray16:
      return GRAY16ToRGB();
    case CImageInfo::Gray256:
      return GRAY256ToRGB();
    case CImageInfo::Color16:
      return COLOR16ToRGB();
    case CImageInfo::Color256:
      return COLOR256ToRGB();
    case CImageInfo::ColorRGB:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("CImage::ToRGB", "Out of memory");
		}
		return img2;
     case CImageInfo::GrayFloat:
      return GRAYFLOATToRGB();
    case CImageInfo::ColorFloat:
      return RGBFLOATToRGB();    
    default:
      throw CImageException("CImage::Convert", "Unknown type");
  }
  return NULL;
}

CImage * CImage::ToGRAYFLOAT() const
{
  CImage* img2 = 0;
  switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
      return BWToGRAYFLOAT();
    case CImageInfo::Gray16:
      return GRAY16ToGRAYFLOAT();
    case CImageInfo::Gray256:
      return GRAY256ToGRAYFLOAT();
    case CImageInfo::Color16:
      return COLOR16ToGRAYFLOAT();
    case CImageInfo::Color256:
      return COLOR256ToGRAYFLOAT();
    case CImageInfo::ColorRGB:
      return RGBToGRAYFLOAT();
    case CImageInfo::GrayFloat:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("CImage::ToGRAYFLOAT", "Out of memory");
		}
		return img2;
    case CImageInfo::ColorFloat:
      return RGBFLOATToGRAYFLOAT();    
    default:
      throw CImageException("CImage::Convert", "Unknown type");

  }
  return NULL;
}

CImage * CImage::ToRGBFLOAT() const
{
  CImage* img2 = 0;
  switch( GeEImageType() ) {
    case CImageInfo::Bilevel:
      return BWToRGBFLOAT();
    case CImageInfo::Gray16:
      return GRAY16ToRGBFLOAT();
    case CImageInfo::Gray256:
      return GRAY256ToRGBFLOAT();
    case CImageInfo::Color16:
      return COLOR16ToRGBFLOAT();
    case CImageInfo::Color256:
      return COLOR256ToRGBFLOAT();
    case CImageInfo::ColorRGB:
      return RGBToRGBFLOAT();
    case CImageInfo::GrayFloat:
      return GRAYFLOATToRGBFLOAT();
    case CImageInfo::ColorFloat:
		img2 = new CImage(*this);
		if (0 == img2) {
			throw CImageException("CImage::ToRGBFLOAT", "Out of memory");
		}
		return img2;
    default:
      throw CImageException("CImage::Convert", "Unknown type");
  }
  return NULL;
}


CImage *CImage::Convert(EImageType newtype) const 
{
  switch( newtype ) {
  case CImageInfo::Bilevel:
    return ToBW(mColorReductionMethod);
  case CImageInfo::Gray16:
    return ToGRAY16();
  case CImageInfo::Gray256:
    return ToGRAY256();
  case CImageInfo::Color16:
    return ToCOLOR16(mColorReductionMethod);
  case CImageInfo::Color256:
    return ToCOLOR256(mColorReductionMethod);
  case CImageInfo::ColorRGB:
    return ToRGB();
  case CImageInfo::GrayFloat:
    return ToGRAYFLOAT();
  case CImageInfo::ColorFloat:
    return ToRGBFLOAT();
  default:
    throw CImageException("CImage::Convert", "Unknown type");
  }
  return NULL;
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
