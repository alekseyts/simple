//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#include "Image.h"

#if !defined(__IMAGE_EXCEPTION_H)
#include "ImageException.h"
#endif

#include <time.h>

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


/*---------------------------------------------------------------------------
  Matrices de dithering
  ---------------------------------------------------------------------------*/

int CImage::dither6[8][8] = {
     1, 59, 15, 55,  2, 56, 12, 52,
    33, 17, 47, 31, 34, 18, 44, 28,
     9, 49,  5, 63, 10, 50,  6, 60,
    41, 25, 37, 21, 42, 26, 38, 22,
     3, 57, 13, 53,  0, 58, 14, 54,
    35, 19, 45, 29, 32, 16, 46, 30,
    11, 51,  7, 61,  8, 48,  4, 62,
    43, 27, 39, 23, 40, 24, 36, 20 };

int CImage::dither8[16][16] = {
      1,235, 59,219, 15,231, 55,215,  2,232, 56,216, 12,228, 52,212,
    129, 65,187,123,143, 79,183,119,130, 66,184,120,140, 76,180,116,
     33,193, 17,251, 47,207, 31,247, 34,194, 18,248, 44,204, 28,244,
    161, 97,145, 81,175,111,159, 95,162, 98,146, 82,172,108,156, 92,
      9,225, 49,209,  5,239, 63,223, 10,226, 50,210,  6,236, 60,220,
    137, 73,177,113,133, 69,191,127,138, 74,178,114,134, 70,188,124,
     41,201, 25,241, 37,197, 21,255, 42,202, 26,242, 38,198, 22,252,
    169,105,153, 89,165,101,149, 85,170,106,154, 90,166,102,150, 86,
      3,233, 57,217, 13,229, 53,213,  0,234, 58,218, 14,230, 54,214,
    131, 67,185,121,141, 77,181,117,128, 64,186,122,142, 78,182,118,
     35,195, 19,249, 45,205, 29,245, 32,192, 16,250, 46,206, 30,246,
    163, 99,147, 83,173,109,157, 93,160, 96,144, 80,174,110,158, 94,
     11,227, 51,211,  7,237, 61,221,  8,224, 48,208,  4,238, 62,222,
    139, 75,179,115,135, 71,189,125,136, 72,176,112,132, 68,190,126,
     43,203, 27,243, 39,199, 23,253, 40,200, 24,240, 36,196, 20,254,
    171,107,155, 91,167,103,151, 87,168,104,152, 88,164,100,148, 84 };

int CImage::cluster3[6][6] = {
     9,11,10, 8, 6, 7,
    12,17,16, 5, 0, 1,
    13,14,15, 4, 3, 2,
     8, 6, 7, 9,11,10,
     5, 0, 1,12,17,16,
     4, 3, 2,13,14,15 };


int CImage::cluster4[8][8] = {
    18,20,19,16,13,11,12,15,
    27,28,29,22, 4, 3, 2, 9,
    26,31,30,21, 5, 0, 1,10,
    23,25,24,17, 8, 6, 7,14,
    13,11,12,15,18,20,19,16,
     4, 3, 2, 9,27,28,29,22,
     5, 0, 1,10,26,31,30,21,
     8, 6, 7,14,23,25,24,17 };

int CImage::cluster8[16][16] = {
     64, 69, 77, 87, 86, 76, 68, 67, 63, 58, 50, 40, 41, 51, 59, 60,
     70, 94,100,109,108, 99, 93, 75, 57, 33, 27, 18, 19, 28, 34, 52,
     78,101,114,116,115,112, 98, 83, 49, 26, 13, 11, 12, 15, 29, 44,
     88,110,123,124,125,118,107, 85, 39, 17,  4,  3,  2,  9, 20, 42,
     89,111,122,127,126,117,106, 84, 38, 16,  5,  0,  1, 10, 21, 43,
     79,102,119,121,120,113, 97, 82, 48, 25,  8,  6,  7, 14, 30, 45,
     71, 95,103,104,105, 96, 92, 74, 56, 32, 24, 23, 22, 31, 35, 53,
     65, 72, 80, 90, 91, 81, 73, 66, 62, 55, 47, 37, 36, 46, 54, 61,
     63, 58, 50, 40, 41, 51, 59, 60, 64, 69, 77, 87, 86, 76, 68, 67,
     57, 33, 27, 18, 19, 28, 34, 52, 70, 94,100,109,108, 99, 93, 75,
     49, 26, 13, 11, 12, 15, 29, 44, 78,101,114,116,115,112, 98, 83,
     39, 17,  4,  3,  2,  9, 20, 42, 88,110,123,124,125,118,107, 85,
     38, 16,  5,  0,  1, 10, 21, 43, 89,111,122,127,126,117,106, 84,
     48, 25,  8,  6,  7, 14, 30, 45, 79,102,119,121,120,113, 97, 82,
     56, 32, 24, 23, 22, 31, 35, 53, 71, 95,103,104,105, 96, 92, 74,
     62, 55, 47, 37, 36, 46, 54, 61, 65, 72, 80, 90, 91, 81, 73, 66 };

int CImage::lineart[7][1] = {
  0,
  2,
  4,
  6,
  5,
  3,
  1 };


int CImage::lineart2[6][6] = {
  35,33,31,30,32,34,
  23,21,19,18,20,22,
  11, 9, 7, 6, 8,10,
   5, 3, 1, 0, 2, 4,
  17,15,13,12,14,16,
  29,27,25,24,26,28 
};

int CImage::lineart3[4][4] = {
   0, 3, 8,15,
   2, 7,14,11,
   6,13,10, 5,
  12, 9, 4, 1 }; 


#define FS_SCALE 256
#define HALF_FS_SCALE 128

/* Dithering par la me'thode de Floyd-Steinberg */
CImage * CImage::FSDithering(TSample threshold)  const
{
  CImage * img2 = 0;
  
  TCBitmap inptr;
  TBitmap  outptr;
  unsigned char bit;
  long *thiserr, *nexterr, *temperr;
  int fs_direction = 1;
  
  int col, limitcol, row;
  int imagewidth;
  int imagelength;
  long sum,threshval;
  
  threshval=threshold;
  
  imagewidth=GetWidth();
  imagelength=GetHeight();
  img2=new CImage(imagewidth,imagelength,CImageInfo::Bilevel);
  if (0 == img2) {
	  throw CImageException("CImage::FSDithering", "Out of memory");
  }
  memset(img2->GetBitmap(),0,img2->GetBitmapSize());
  img2->CopyResolution(*this);
  
  thiserr = new long[imagewidth+2];
  nexterr = new long[imagewidth+2];
  /* Seed the random-number generator avec current time */
  srand( (unsigned)time( NULL ) );        
  for ( col = 0; col < imagewidth + 2; ++col )
    thiserr[col] = ( rand( ) % FS_SCALE - HALF_FS_SCALE ) / 4;
  
  for (row = 0; row < imagelength; ++row) {
    memset(nexterr,0,(imagewidth+2) * sizeof (long));
    if ( fs_direction ) {
      col = 0;
      limitcol = imagewidth;
      inptr = GetRow(row);
      outptr = img2->GetRow(row);
      bit=0x80;
    } else {
      col = imagewidth - 1;
      limitcol = -1;
      inptr = GetRow(row)+col;
      outptr = img2->GetRow(row)+col/8;
      bit = 0x80 >> (col%8);
    }
    do {
      sum = ( (long) *inptr * FS_SCALE ) / 255 + thiserr[col + 1];
      if ( sum >= threshval ) {
	/* white point */
	sum = sum - threshval - HALF_FS_SCALE;
      }
      else {
	*outptr |= bit;
	/* black point */
      }
      if ( fs_direction ) {
	thiserr[col + 2] += ( sum * 7 ) / 16;
	nexterr[col    ] += ( sum * 3 ) / 16;
	nexterr[col + 1] += ( sum * 5 ) / 16;
	nexterr[col + 2] += ( sum     ) / 16;
	++col;
	++inptr;
	if (bit==0x01) {
	  bit=0x80;
	  ++outptr;
	} else bit>>=1;
      } else {
	thiserr[col    ] += ( sum * 7 ) / 16;
	nexterr[col + 2] += ( sum * 3 ) / 16;
	nexterr[col + 1] += ( sum * 5 ) / 16;
	nexterr[col    ] += ( sum     ) / 16;
	--col;
	--inptr;
	if (bit==0x80) {
	  bit=0x01;
	  --outptr;
	} else bit<<=1;
      }
    } while ( col != limitcol );
    temperr = thiserr;
    thiserr = nexterr;
    nexterr = temperr;
    fs_direction = ! fs_direction;
  }
  
  delete [] thiserr;
  delete [] nexterr;
  
  return img2;
}

        
/* Dithering par matrice */
CImage * 
CImage::MatrixDithering(const int *matrix, int wsize, int hsize, int maxval, TSample threshold)  const
{
  CImage * img2 = 0;
  
  TCBitmap inptr;
  TBitmap  outptr;
  unsigned char bit;
  int col, row;
  int imagewidth;
  int imagelength;
  int *matrix2,*p2;
  const int *p;
  int matrow,matcol;
  
  p=matrix;
  p2=matrix2=new int[wsize * hsize];
  
  for (matrow=0;matrow<hsize;matrow++) 
    for (matcol=0;matcol<wsize;matcol++) 
      *p2++ = (*p++ * 256 * 128) / (maxval * threshold);
    
    imagewidth=GetWidth();
    imagelength=GetHeight();
    img2=new CImage(imagewidth,imagelength,CImageInfo::Bilevel);
	if (0 == img2) {
		throw CImageException("CImage::MatrixDithering", "Out of memory");
	}
    memset(img2->GetBitmap(),0,img2->GetBitmapSize());
    img2->CopyResolution(*this);
    
    for (row = 0; row < imagelength; ++row)  {
      inptr = GetRow(row);
      outptr = img2->GetRow(row);
      bit = 0x80;
      matrow = (row % hsize);
      matcol = 0;
      for (col = 0; col < imagewidth; ++col) {
	if (*inptr++ < matrix2[ matrow * wsize + matcol])
	  *outptr |= bit;
	if (bit==0x01) {
	  bit=0x80;
	  ++outptr;
	} else bit>>=1;
	matcol++;
	if (matcol==wsize) matcol = 0;
      }
    }
    
    delete [] matrix2;
    return img2;
}

/* Dithering par la me'thode de Hilbert */

#define MAXORD 18

class CHilbert {
public:
  CHilbert(int w,int h);
  int Plot(int *px,int *py);    
private:
  // Hilbert data
  int hil_order,hil_ord;
  int hil_turn;
  int hil_dx,hil_dy;
  int hil_x,hil_y;
  int hil_stage[MAXORD];
  int hil_width,hil_height;
};



/* Initialise le traceur de courbes de Hilbert */
CHilbert::CHilbert(int w, int h)
{
  int big,ber;
  hil_width = w;
  hil_height = h;
  big = w > h ? w : h;
  for(ber = 2, hil_order = 1; ber < big; ber <<= 1, hil_order++);
  assert (hil_order <= MAXORD);
  hil_ord = hil_order;
  hil_order--;
}

/* Retourne non-zero if got another point */
int CHilbert::Plot(int *px,int *py)
{
  int temp;
  if (hil_ord > hil_order)    /* have to do first point */
  {
    hil_ord--;
    hil_stage[hil_ord] = 0;
    hil_turn = -1;
    hil_dy = 1;
    hil_dx = hil_x = hil_y = 0;
    *px = *py = 0;
    return 1;
  }
  for(;;) /* Operate the state machine */
  {
    switch (hil_stage[hil_ord])
    {
    case 0:
      hil_turn = -hil_turn;
      temp = hil_dy;
      hil_dy = -hil_turn * hil_dx;
      hil_dx = hil_turn * temp;
      if (hil_ord > 0) 
      {
	hil_stage[hil_ord] = 1;
	hil_ord--;
	hil_stage[hil_ord]=0;
	continue;
      }
	  break;
    case 1:
      hil_x += hil_dx;
      hil_y += hil_dy;
      if (hil_x < hil_width && hil_y < hil_height)
      {
	hil_stage[hil_ord] = 2;
	*px = hil_x;
	*py = hil_y;
	return 1;
      }
	  break;
    case 2:
      hil_turn = -hil_turn;
      temp = hil_dy;
      hil_dy = -hil_turn * hil_dx;
      hil_dx = hil_turn * temp;
      if (hil_ord > 0) /* recurse */
      {
	hil_stage[hil_ord] = 3;
	hil_ord--;
	hil_stage[hil_ord]=0;
	continue;
      }
	  break;
    case 3:
      hil_x += hil_dx;
      hil_y += hil_dy;
      if (hil_x < hil_width && hil_y < hil_height)
      {
	hil_stage[hil_ord] = 4;
	*px = hil_x;
	*py = hil_y;
	return 1;
      }
	  break;
    case 4:
      if (hil_ord > 0) /* recurse */
      {
	hil_stage[hil_ord] = 5;
	hil_ord--;
	hil_stage[hil_ord]=0;
	continue;
      }
	  break;
    case 5:
      temp = hil_dy;
      hil_dy = -hil_turn * hil_dx;
      hil_dx = hil_turn * temp;
      hil_turn = -hil_turn;
      hil_x += hil_dx;
      hil_y += hil_dy;
      if (hil_x < hil_width && hil_y < hil_height)
      {
	hil_stage[hil_ord] = 6;
	*px = hil_x;
	*py = hil_y;
	return 1;
      }
	  break;
    case 6:
      if (hil_ord > 0) /* recurse */
      {
	hil_stage[hil_ord] = 7;
	hil_ord--;
	hil_stage[hil_ord]=0;
	continue;
      }
	  break;
    case 7:
      temp = hil_dy;
      hil_dy = -hil_turn * hil_dx;
      hil_dx = hil_turn * temp;
      hil_turn = -hil_turn;
      /* Retourne from a recursion */
      if (hil_ord < hil_order)
	hil_ord++;
      else
	return 0;
	  break;
    }
  }
}

/* Dithering de hilbert */

CImage * CImage::HilbertDithering() const
{
  CImage * img2 = 0;
  
  int imagewidth;
  int imagelength;
  
  int end;
  int *x,*y;
  int sum = 0;
  int clump_size=5;
  
  imagewidth=GetWidth();
  imagelength=GetHeight();
  img2=new CImage(imagewidth,imagelength,CImageInfo::Bilevel);
  if (0 == img2) {
	  throw CImageException("CImage::HilbertDithering", "Out of memory");
  }
  memset(img2->GetBitmap(),0,img2->GetBitmapSize());
  img2->CopyResolution(*this);  
  
  x = new int[clump_size];
  y = new int[clump_size];
  
  CHilbert hilbert(imagewidth,imagelength);
  
  end = clump_size;
  while (end == clump_size) {
    int i;
    /* compute the next clust co-ordinates along hilbert path */
    for (i = 0; i < end; i++) {
      if (hilbert.Plot(&x[i],&y[i])==0)
	end = i;/* we reached the end */
    }
    /* sum levels */
    for (i = 0; i < end; i++)
      sum += *(GetRow(y[i])+x[i]);
    /* dither half and half along path */
    for (i = 0; i < end; i++) {
      if (sum >= 255) {
	sum -= 255;
      }
      else
	img2->SetLuma(x[i], y[i],0);
    }
  }
  delete [] x;
  delete [] y;
  return img2;
}


/* Dithering d'une image */
CImage *
CImage::Dither(EColorReductionMethod algo, TSample threshold) const
{
  switch ( algo ) {
  case FloydSteinberg:
    return FSDithering(threshold);
  case Dither8:
    return MatrixDithering((const int *) dither8,16,16,256,threshold);
  case Cluster3:
    return MatrixDithering((const int *) cluster3,6,6,18,threshold);
  case Cluster4:
    return MatrixDithering((const int *) cluster4,8,8,32,threshold);
  case Cluster8:
    return MatrixDithering((const int *) cluster8,16,16,128,threshold);  
  case LineArt:
    return MatrixDithering((const int *) lineart,1,7,7,threshold);
  case LineArt2:
    return MatrixDithering((const int *) lineart2,6,6,36,threshold);
  case LineArt3:  
    return MatrixDithering((const int *) lineart3,4,4,16,threshold);
  case Hilbert:
    return HilbertDithering();
  default:
    throw CImageException("CImage::Dither","Unknown dithering algorithm");
  }              
  return NULL;
}
                


#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
