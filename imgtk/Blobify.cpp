//////////////////////////////////////////////////////////////////////
// @doc BLOB

#include "ImgTk.h"

typedef queue<CPosition> CQueuePosition;

CBlobList *CImage::Blobify() const
{
  int oldheight,oldwidth;
  CPosition tmpnode;
  int newx,newy,totx,toty;    
  CBlob mytmpblob;
  CBlobList *bloblist= new CBlobList();
  int xminval,xmaxval,yminval,ymaxval;
  CQueuePosition TheQueue;
  CQueuePosition holding;                                 
  int maxheight,maxwidth;
  CImage *workingimage=Convert(CImageInfo::Bilevel);
  CBilevelImageIterator i1(workingimage),i2(workingimage);

  oldwidth  = GetWidth();
  oldheight = GetHeight();

  const TSample background=255;
  const TSample foreground=0;
      
  for( ;i1.GetY()<i1.GetHeight();i1.NextRow() )
    for ( i1.SetX(0) ;i1.GetX()<i1.GetWidth();i1.Next()  ) {       
       if ( i1.GetLuma() == foreground ) {
         tmpnode.SetX(i1.GetX());
	 tmpnode.SetY(i1.GetY());
	 xminval = xmaxval = i1.GetX();
	 yminval = ymaxval = i1.GetY();

	 TheQueue.push(tmpnode);

	 i1.SetLuma(background);

         while ( !TheQueue.empty() ) {
	   tmpnode =  TheQueue.front();
	   TheQueue.pop();
	   
           holding.push(tmpnode);

	   newx = tmpnode.GetX();
	   newy = tmpnode.GetY();	   	   

	   newx--;
	   newy--;
	   
	   totx = newx + 3;
	   toty = newy + 3;
           
	   if (newx < 0) newx = 0;
           if (newy < 0) newy = 0;
           
	   if (totx >= oldwidth)  totx=oldwidth-1;
           if (toty >= oldheight) toty=oldheight-1;
           
	   for ( i2.SetY(newy) ; i2.GetY() < toty ; i2.NextRow() )
	     for( i2.SetX(newx); i2.GetX() < totx ; i2.Next() )
	       if ( i2.GetLuma() == foreground ) {
		 tmpnode.SetX(i2.GetX());
		 tmpnode.SetY(i2.GetY());		 

		 if ( tmpnode.GetX() < xminval )
	            xminval = tmpnode.GetX();
                 else if ( tmpnode.GetX() > xmaxval )
	            xmaxval = tmpnode.GetX();

                 if ( tmpnode.GetY() < yminval )
	            yminval = tmpnode.GetY();
                  else if ( tmpnode.GetY() > ymaxval )
	            ymaxval = tmpnode.GetY();                 

		 TheQueue.push(tmpnode);
	         i2.SetLuma(background); 
               }
         } 

         maxheight = ymaxval - yminval+1;  /* max height of current blob */
	 maxwidth = xmaxval - xminval+1;   /* max width of current blob */

	 CImage *image=new CImage(maxwidth,maxheight,GetImageType(),GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
	 
	 mytmpblob.SetImage(image);
	 mytmpblob.SetX1(xminval);
	 mytmpblob.SetY1(yminval);
	 mytmpblob.SetWidth(maxwidth);
	 mytmpblob.SetHeight(maxheight);
	 mytmpblob.SetPointCount(holding.size());
	 
	 // Fill Image with background color
	 image->Erase(background);
	 
	 // Retrieve points from original image and plot them in the blob
	 while (!holding.empty()) {
	   CColor color;
	   GetColor(holding.front().GetX(),holding.front().GetY(),color);
	   image->SetColor(holding.front().GetX()-xminval,holding.front().GetY()-yminval,color);
	     
	   holding.pop();
	 }
	 // Add the new blob to the list
	 bloblist->push_back(mytmpblob);
         

       }  
    }     

  delete workingimage;
  return bloblist;

} 
