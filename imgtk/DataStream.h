//////////////////////////////////////////////////////////////////////
// @doc STREAM

#if !defined(__DATA_STREAM_H)
#define __DATA_STREAM_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif


#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


//@class Virtual stream
class IMGTK_EXPORT CStream 
{
public:  
  //@cmember,menum Seek reference
  enum TOrigin {
    Begin,    //@@emem seek from the beginning
    Current,  //@@emem seek from current position
    End	      //@@emem seek from the end
  };
  //@cmember constructor
  CStream() {};
  //@cmember destructor
  virtual ~CStream() {};
  //@cmember Read data
  virtual size_t Read( void* lpBuf, size_t nCount ) = 0;
  //@cmember Write data
  virtual size_t Write(const void* lpBuf, size_t nCount ) = 0;	
  //@cmember Random access
  virtual long Seek(long offset,TOrigin origin) = 0;
  //@cmember Close stream
  virtual void Close() = 0;
  //@cmember Get stream length
  virtual long Size() = 0;
};


#include <cstdio> // for FILE

//@class File stream
//@base public | CStream
class IMGTK_EXPORT CFileStream : public CStream  
{
public:
  //@cmember,menum Stream mode  
  enum TMode { 
    In, //@@emem Input mode
    Out //@@emem Output mode
  };  
  //@cmember Constructor
  CFileStream(const char *filename,TMode mode);
  //@cmember Destructor
  virtual ~CFileStream();
  //@cmember Read data
  virtual size_t Read( void* lpBuf, size_t nCount );
  //@cmember Write data
  virtual size_t Write(const void* lpBuf, size_t nCount );	
  //@cmember Random access
  virtual long Seek(long offset,TOrigin origin);
  //@cmember Close stream
  virtual void Close();
  //@cmember Get stream length
  virtual long Size();
private:
  FILE *mFile;
};

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__DATA_STREAM_H)
