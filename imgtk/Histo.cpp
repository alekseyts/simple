//////////////////////////////////////////////////////////////////////
// @doc HISTOGRAM

#include "Histo.h"

#include <cstring> // for memset

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


CHistogram::CHistogram(int size)
{
  mSize=size;
  mValue=new int[size];
  memset(mValue,0,sizeof(int)*size);
}

CHistogram::~CHistogram()
{
  delete[] mValue;
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
