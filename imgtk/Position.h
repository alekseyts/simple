//////////////////////////////////////////////////////////////////////
// @doc POSITION

#if !defined(__POSITION_H)
#define __POSITION_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif


#if !defined(__IOSTREAM)
#include <iostream>
#define __IOSTREAM
#endif

#include <assert.h>

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

using namespace std;

//@class Position
class IMGTK_EXPORT CPosition  {
public:
  //@cmember Default Constructor
  CPosition();
  //@cmember Constructor
  CPosition(int x,int y);
  //@cmember Copy Constructor
  CPosition(const CPosition& pos);
  //@cmember Get X-coordinate  
  int GetX() const;
  //@cmember Get Y-coordinate  
  int GetY() const;
  //@cmember Set X-coordinate  
  void SetX(int val);
  //@cmember Set Y-coordinate  
  void SetY(int val);
  //@cmember Set both coordinates
  void Set(int x,int y);
  //@cmember offset both coordinates
  void Offset(int x,int y);
  //@cmember Write a position to a stream
  void WriteToStream  (ostream& stream) const;
  //@cmember assignment operator
  const CPosition& operator= (const CPosition& position);
  //@cmember < operator (compares weigth magnitudes)
  bool operator< (const CPosition &position) const;
  //@cmember equality operator 
  bool operator== (const CPosition &position) const;
  
  // fix all coordinates by factor (used after changing resolution)
  void FixByFactor(const double& dXFactor, const double& dYFactor) { 
	  assert(0 != dXFactor); 
	  assert(0 != dYFactor); 
	  mX = static_cast<int>(mX*dXFactor); 
	  mY = static_cast<int>(mY*dYFactor);
  }

private:
  int mX;
  int mY;
};

//@mfunc Default constructor
inline CPosition::CPosition() : mX(0),mY(0) {}
//@mfunc Constructor
//@parm x-coordinate
//@parm y-coordinate
inline CPosition::CPosition(int x,int y) : mX(x),mY(y) {}
//@mfunc Copy constructor
//@parm Position to copy
inline CPosition::CPosition(const CPosition& pos) : mX(pos.mX), mY(pos.mY) {}

//@mfunc Get X-coordinate  
//@rdesc Coordinate
inline int CPosition::GetX() const { return mX; }
//@mfunc Get Y-coordinate  
//@rdesc Coordinate
inline int CPosition::GetY() const { return mY; }  
//@mfunc Set X-coordinate
//@parm new X-coordinate
inline void CPosition::SetX(int val) { mX=val; }
//@mfunc Set Y-coordinate
//@parm new Y-coordinate
inline void CPosition::SetY(int val) { mY=val; }
//@mfunc Set both coordinates
//@parm new X-coordinate new Y-coordinate
inline void CPosition::Set(int x,int y) {mX = x;mY = y;};
//@mfunc Offset both coordinates
//@parm new X-coordinate new Y-coordinate
inline void CPosition::Offset(int x,int y) {mX += x;mY += y;};

IMGTK_EXPORT ostream& operator<< (ostream& s, const CPosition& o);

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // __POSITION_H
