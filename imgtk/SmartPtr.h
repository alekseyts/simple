//////////////////////////////////////////////////////////////////////
// @doc BLOB

#if !defined(_SMARTPTR_H)
#define _SMARTPTR_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ImgTk.h"

//@class Template class for smart pointers
//@tcarg class | T | The class the pointer points to
template<class T> class IMGTK_EXPORT CSmartPtr {
public:
  //@cmember Constructor
  CSmartPtr();  
  //@cmember Constructor
  CSmartPtr(T *obj);
  //@cmember Destructor
  ~CSmartPtr();
  //@cmember,mfunc Data access
  T& operator* (CSmartPtr& ptr) { return *mData; }
  //@cmember,mfunc Data access
  const T& operator* (const CSmartPtr& ptr) { return *mData; }
  //@cmember Data access
  operator T* () { return mData; };
  //@cmember,mfunc Increment reference counter
  void IncRefCount() { mRefcount++; }
  //@cmember,mfunc Decrement reference counter
  void DecRefCount();
private:
  CSmartPtr(const CSmartPtr& ptr); // no copy
  void operator= (const CSmartPtr& ptr); // no assignment

  T *mData;
  int mRefcount;
};

template<class T> inline CSmartPtr<T>::CSmartPtr()
{
  mRefcount=0;
  mData=NULL;
}

template<class T> inline CSmartPtr<T>::CSmartPtr(T* obj)
{
  mRefcount=0;
  mData=obj;
}

template<class T> inline CSmartPtr<T>::~CSmartPtr()
{
  if (mData) delete mData;
}

template<class T> inline void CSmartPtr<T>::DecRefCount()
{ 
  mRefcount--; 
  if (mRefcount==0)
    delete this; // Suicide
}


#endif