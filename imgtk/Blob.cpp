//////////////////////////////////////////////////////////////////////
// @doc BLOB

#include "ImgTk.h"

//@mfunc Constructor
CBlob::CBlob()
{
  mImgPtr=NULL; 
  mPointCount=0;
}

//@mfunc Copy constructor
CBlob::CBlob(const CBlob& blob)
{
  mImgPtr=blob.mImgPtr;
  mImgPtr->IncRefCount();

  mPosition=blob.mPosition;
  mWidth=blob.mWidth;
  mHeight=blob.mHeight;
  mPointCount=blob.mPointCount;
}

//@mfunc Destructor
CBlob::~CBlob()
{  
  if (mImgPtr) 
    mImgPtr->DecRefCount();
}

//@mfunc Set the image associated with the blob
//@parm The image
void CBlob::SetImage(CImage *img)
{
  if (mImgPtr) 
    mImgPtr->DecRefCount();

  mImgPtr=new CImagePtr(img);
  mImgPtr->IncRefCount();
  mWidth=img->GetWidth();
  mHeight=img->GetHeight();
  mPointCount= -1; // unknown!
}

//@cfunc Get total width for a group of two blobs
//@parm Reference to a Blob
//@rdesc Total width in pixels
int CBlob::GetTotalWidth(const CBlob& blob) const
{
  return max(GetX2(),blob.GetX2()) - min(GetX1(),blob.GetX1())+1;
}

//@cfunc Get total height for a group of two blobs
//@parm Reference to a Blob
//@rdesc Total height in pixels
int CBlob::GetTotalHeight(const CBlob& blob) const
{
    return max(GetY2(),blob.GetY2()) - min(GetY1(),blob.GetY1())+1;
}

//@cfunc Get Horizontal Inclusivity (in percentage) for a group of two blobs
//@parm Reference to a Blob
//@rdesc Inclusivity in percentage, between 0 and 100
int CBlob::GetHInclusivity(const CBlob& blob) const 
{
  int d,d2;
  d=min(GetWidth(),blob.GetWidth());
  d2=min(GetX2(),blob.GetX2())-max(GetX1(),blob.GetX1())+1;
  if (d2<=0) return 0;
  return (d2*100)/d;
}

//@cfunc Get Vertical Inclusivity (in percentage) for a group of two blobs
//@parm Reference to a Blob
//@rdesc Inclusivity in percentage, between 0 and 100
int CBlob::GetVInclusivity(const CBlob& blob) const 
{
  int d,d2;
  d=min(GetHeight(),blob.GetHeight());
  d2=min(GetY2(),blob.GetY2())-max(GetY1(),blob.GetY1())+1;
  if (d2<=0) return 0;
  return (d2*100)/d;
}

//@cfunc Add to blobs
//@parm Reference to a Blob
//@rdesc Resulting blob
CBlob& CBlob::Add(const CBlob& blob)
{
  // Add to himself -> no effect
  if (this == &blob) return *this; 
  // Compute new dimension
  int newwidth=GetTotalWidth(blob);
  int newheight=GetTotalHeight(blob);
  // Get blobs images
  CImage *img=GetImage();
  const CImage *img2=blob.GetImage();
  // Create resulting image
  CImage *newimg=new CImage(newwidth,newheight,img->GetImageType(),
    img->GetXResolution(),img->GetYResolution(),img->GetColorPalette()?new CColorPalette(*(img->GetColorPalette())):NULL);
  // Erase it
  newimg->Erase();
  // New position
  int newx=min(GetX1(),blob.GetX1());
  int newy=min(GetY1(),blob.GetY1());
  // Delta positions for both blobs relatives to resulting blob
  int dx1=GetX1()-newx;
  int dy1=GetY1()-newy;
  int dx2=blob.GetX1()-newx;
  int dy2=blob.GetY1()-newy;  
  // Blit images
  newimg->Blit(dx1,dy1,img);
  newimg->Blit(dx2,dy2,img2);
  // Set new image in current blob
  SetImage(newimg);
  // Increment points count
  mPointCount+=blob.mPointCount;
  // remember new position and size
  SetX1(newx);
  SetY1(newy);
  SetWidth(newwidth);
  SetHeight(newheight);
  return *this;
}



//@cfunc + operator
CBlob& CBlob::operator+= (const CBlob& blob)
{
  return Add(blob);
}

//@cfunc + operator
CBlob CBlob::operator+ (const CBlob& blob) const
{
  return CBlob(*this)+=blob;
}


//@cmember Write a blob to a stream

//@mfunc Assignment operator
//@parm Reference to a blob
//@rdesc Reference to the assigned blob
const CBlob& CBlob::operator= (const CBlob& blob)
{
  if (this == &blob) return *this; 
  if (mImgPtr) 
    mImgPtr->DecRefCount();
  
  mImgPtr=blob.mImgPtr;
  mImgPtr->IncRefCount();

  mPosition=blob.mPosition;
  mWidth=blob.mWidth;
  mHeight=blob.mHeight;
  return *this;
}

//@mfunc Write a blob to a stream
//@parm A stream
void CBlob::WriteToStream  (ostream& stream) const
{
  stream << mPosition << ' ' << mWidth << ' ' << mHeight;
}

//@func Write a blob to a stream
//@parm A stream
//@parm A blob
ostream& operator<< (ostream& s, const CBlob& o)
{
  o.WriteToStream(s);
  return s;
}


