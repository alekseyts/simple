//////////////////////////////////////////////////////////////////////
// @doc SKEW

#include "Image.h"
#include <cmath> // for fabs, sin & cos

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

/* Structure de codage d'une image afin de gagner du temps
   Par image on a un tableau de lignes
   pour chaque ligne on connait le nombre de points noirs
   et on a un tableau pour stocker les abscisses
   de ces points */

class CSkewingAnalyzer {
private:
  typedef unsigned short ABSCISSE;
  struct SLine {
    int nbpoints;  /* nombre de points noir dans la ligne */
    ABSCISSE *x;   /* abscisses des points noirs */
  };
public:  
  CSkewingAnalyzer(const CImage &img);
  ~CSkewingAnalyzer();
  double ComputeAngle() const;
private:
  int mWidth;         /* largeur de l'image */
  int mHeight;         /* hauteur de l'image */
  SLine *mLine;   /* tableau des lignes de l'image */

  int VertHistoAngle(double angle,int **phisto) const;
  double CalcCrit(double angle) const;
  void mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb, 
        double *fc) const;
  double brent(double ax, double bx, double cx, double fb,double tol,
	double *xmin) const;
  void InitBilevel(const CImage &img);
  void InitOther(const CImage &img);
  void Init(const CImage &img);

  static const double GOLD;
  static const double GLIMIT;
  static const double TINY;
  static const int ITMAX;
  static const double CGOLD;
  static const double ZEPS;

  static double SIGN(double a,double b) { return b >= 0.0 ? fabs(a) : -fabs(b) ; };
  static double FMAX(double a, double b) { return a>b ? a : b; };
    
};

CSkewingAnalyzer::CSkewingAnalyzer(const CImage &img): mWidth(0), mHeight(0), mLine(NULL)
{
  if ( img.GeEImageType() == CImageInfo::Bilevel)
    InitBilevel(img);
  else
    InitOther(img);
}


void CSkewingAnalyzer::Init(const CImage &img)
{  
  int nbpt,i,j;
  TCBitmap bits;
  TByte mask,data;
  ABSCISSE *tmpx;
  
  mWidth=img.GetWidth();
  mHeight=img.GetHeight();
  mLine=new SLine[mHeight];

  /* buffer pour recevoir les abscisses de la ligne courante */
  tmpx=new ABSCISSE[mWidth];
  
  for(i=0;i<mHeight;i++) {    
    bits=img.GetRow(i);
    mask=0x01;
    nbpt=0;
    for(j=0;j<mWidth;j++) {
      if (mask==0x01) {
	mask=0x80;
      	data=*bits++;
	while (data==0) {
	  j+=8;
	  if (j>=mWidth) goto exitloop;
	  data=*bits++; 
	}
      } else
	  mask>>=1;
      if (data & mask)
	  tmpx[nbpt++]=j;    
    }
  exitloop: ;
    mLine[i].nbpoints=nbpt;
    if (nbpt==0) 
      mLine[i].x=NULL;
    else {
      mLine[i].x=new ABSCISSE[nbpt];
      memcpy(mLine[i].x,tmpx,sizeof(ABSCISSE)*nbpt);
    } 
  }
  delete [] tmpx;
}

CSkewingAnalyzer::~CSkewingAnalyzer()
{
  int i;
  /* liberation de toutes les lignes */
  for(i=0;i<mHeight;i++) 
    if (mLine[i].x) delete [] mLine[i].x;
  delete [] mLine;
}


/* -------------------------------------------------------------------
  FONCTION PRIVEE VertHistoAngle
  DESCRIPTION 
    Calcul d'un histogramme vertical sur une ligne incline'e d'un 
    angle donne'
  PARAMETRES
    img        image au format ptimg
    angle      angle de la ligne en degree par rapport a` l'axe horizontal
    phisto     pointeur sur un tableau d'entiers destine' a` recevoir
               l'histogramme
  VALEUR DE RETOUR
    Taille de l'histogramme
    ou code d'erreur
*/

int CSkewingAnalyzer::VertHistoAngle(double angle,int **phisto) const
{
  int newheight,oldheight,oldwidth,i;
  double halfnewheight;
  double halfoldheight,halfoldwidth;
  double radians;
  double cosval,sinval;
  double mycosval;
  ABSCISSE* abscisse;
  int *histo;
  int nbpt;

  /* Cas special de l'angle nul */
  if (angle==0.0) {
    histo=new int[mHeight];
    *phisto=histo;
    for(i=0;i<mHeight;i++)
      histo[i]=mLine[i].nbpoints;
    return mHeight;
  }
  radians =  (angle) / ((180 / M_PI));
  cosval = cos(radians);
  sinval = sin(radians);

  oldheight = mHeight;
  oldwidth = mWidth;

  newheight = (int)(fabs(-oldwidth*sinval) + fabs(oldheight*cosval)+0.5);

  halfnewheight = newheight / 2.0;
  halfoldwidth = oldwidth /2.0;
  halfoldheight = oldheight /2.0 ;

  histo=new int[newheight];
  *phisto=histo;
  memset(histo,0,newheight*sizeof(int));

  for(i=0;i<oldheight;i++) {
    if ((nbpt=mLine[i].nbpoints)!=0) {
      abscisse=mLine[i].x;
      mycosval=(i-halfoldheight)*cosval+halfnewheight;
      for(;nbpt;nbpt--) 
	histo[(int) (mycosval-(*abscisse++-halfoldwidth)*sinval)]++;
    }
  }
  return newheight;
}

/* -------------------------------------------------------------------
  FONCTION PRIVEE intcompare
  DESCRIPTION 
    Comparaison de 2 entiers pour tri dans le sens de'croissant 
  PARAMETRES
    i,j      pointeur sur des entiers a` comparer
  VALEUR DE RETOUR
    egalite' -> 0
    *i<*j    -> 1
    *i>*j    -> -1
*/
static  int intcompare(int *i, int *j)
{
  if (*i < *j)
    return (1);
  if (*i > *j)
    return (-1);
  return (0);
}


/* -------------------------------------------------------------------
  FONCTION PRIVEE CalcCrit
  DESCRIPTION 
    Calcul du crite`re de bonne inclinaison pour un angle donne' 
  PARAMETRES
    img        image au format ptimg
    angle      angle de rotation pre'sume' en degre's
  VALEUR DE RETOUR
    Crite`re calcule'
    ou code d'erreur
*/

double CSkewingAnalyzer::CalcCrit(double angle) const
{
  int hhisto;
  int *vhisto;
  double crit;
  int i;
  int nb;

  /* Calcul de l'histogramme */
  hhisto=VertHistoAngle(angle,&vhisto);
  /* si ne'gatif, c'est un code d'erreur */
  if (hhisto<0) return hhisto;

  /* tri de l'histogramme par ordre decroissant */
  qsort((void *) vhisto,hhisto,sizeof(int),(int (*)(const void*,const void*))intcompare);
  nb=0;
  
  for(crit=0,i=0;i<hhisto && vhisto[i];i++) {    
    crit+=vhisto[i]*(hhisto-i);
    nb+= (hhisto-i);    
  }
  /* libe`re histo */
  delete [] vhisto;
  return  -crit/nb;
}

const double CSkewingAnalyzer::GOLD = 1.618034;
const double CSkewingAnalyzer::GLIMIT = 100.0;
const double CSkewingAnalyzer::TINY = 1.0e-20;

#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

/* -------------------------------------------------------------------
  FONCTION PRIVEE mnbrak
	      
  DESCRIPTION 
    Recherche d'un intervalle encadrant l'angle optimal pour
    lequel le crite`re est minimal
  PARAMETRES
    ax		triplet encadrant le minimum tq f(bx)< f(ax) et f(bx)
    bx
    cx
    fa		Crit(ax)
    fb		Crit(bx)
    fc		Crit(cx)
    img		image au format ptimg    
  VALEUR DE RETOUR
    Sans
*/

void 
CSkewingAnalyzer::mnbrak(double *ax, double *bx, double *cx, double *fa, double *fb, double *fc) const 

{
  double ulim,u,r,q,fu,dum;
  
  *fa=CalcCrit(*ax);
  *fb=CalcCrit(*bx);
  if (*fb > *fa) {
    SHFT(dum,*ax,*bx,dum)
      SHFT(dum,*fb,*fa,dum)
  }
  *cx=(*bx)+GOLD*(*bx-*ax);
  *fc=CalcCrit(*cx);
  while (*fb > *fc) {
    r=(*bx-*ax)*(*fb-*fc);
    q=(*bx-*cx)*(*fb-*fa);
    u=(*bx)-((*bx-*cx)*q-(*bx-*ax)*r)/
      (2.0*SIGN(FMAX(fabs(q-r),TINY),q-r));
    ulim=(*bx)+GLIMIT*(*cx-*bx);
    if ((*bx-u)*(u-*cx) > 0.0) {
      fu=CalcCrit(u);
      if (fu < *fc) {
	*ax=(*bx);
	*bx=u;
	*fa=(*fb);
	*fb=fu;
	return;
      } else if (fu > *fb) {
	*cx=u;
	*fc=fu;
	return;
      }
      u=(*cx)+GOLD*(*cx-*bx);
      fu=CalcCrit(u);
    } else if ((*cx-u)*(u-ulim) > 0.0) {
      fu=CalcCrit(u);
      if (fu < *fc) {
	SHFT(*bx,*cx,u,*cx+GOLD*(*cx-*bx))
	  SHFT(*fb,*fc,fu,CalcCrit(u))
      }
    } else if ((u-ulim)*(ulim-*cx) >= 0.0) {
      u=ulim;
      fu=CalcCrit(u);
    } else {
      u=(*cx)+GOLD*(*cx-*bx);
      fu=CalcCrit(u);
    }
    SHFT(*ax,*bx,*cx,u)
      SHFT(*fa,*fb,*fc,fu)
  }
}
#undef SHFT

const int CSkewingAnalyzer::ITMAX = 100;
const double CSkewingAnalyzer::CGOLD = 0.3819660;
const double CSkewingAnalyzer::ZEPS  = 1.0e-10;

#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);

/* -------------------------------------------------------------------
  FONCTION PRIVEE brent
  DESCRIPTION 
    Recherche de l'angle qui minimise le crite`re pour la 
    recherche de l'angle optimal
    -> Recherche d'un minimum par la me'thode de Brent
  PARAMETRES
    ax	       triplet encadrant le minimum tq f(bx)< f(ax) et f(bx)
    bx
    cx
    img        image au format ptimg
    tol	       tolérance
    xmin       angle pour lequel le critere est minimal
  VALEUR DE RETOUR
    Critere au point minimal
*/

double CSkewingAnalyzer::brent(double ax, double bx, double cx, double fb,double tol,
	double *xmin) const
{
	int iter;
	double a,b,d,etemp,fu,fv,fw,fx,p,q,r,tol1,tol2,u,v,w,x,xm;
	double e=0.0;

	a=(ax < cx ? ax : cx);
	b=(ax > cx ? ax : cx);
	x=w=v=bx;
	fw=fv=fx=fb;
	for (iter=1;iter<=ITMAX;iter++) {
		xm=0.5*(a+b);
		tol2=2.0*(tol1=tol*fabs(x)+ZEPS);
		if (fabs(x-xm) <= (tol2-0.5*(b-a))) {
			*xmin=x;
			return fx;
		}
		if (fabs(e) > tol1) {
			r=(x-w)*(fx-fv);
			q=(x-v)*(fx-fw);
			p=(x-v)*q-(x-w)*r;
			q=2.0*(q-r);
			if (q > 0.0) p = -p;
			q=fabs(q);
			etemp=e;
			e=d;
			if (fabs(p) >= fabs(0.5*q*etemp) || p <= q*(a-x) || p >= q*(b-x))
				d=CGOLD*(e=(x >= xm ? a-x : b-x));
			else {
				d=p/q;
				u=x+d;
				if (u-a < tol2 || b-u < tol2)
					d=SIGN(tol1,xm-x);
			}
		} else {
			d=CGOLD*(e=(x >= xm ? a-x : b-x));
		}
		u=(fabs(d) >= tol1 ? x+d : x+SIGN(tol1,d));
		fu=CalcCrit(u);
		if (fu <= fx) {
			if (u >= x) a=x; else b=x;
			SHFT(v,w,x,u)
			SHFT(fv,fw,fx,fu)
		} else {
			if (u < x) a=u; else b=u;
			if (fu <= fw || w == x) {
				v=w;
				w=u;
				fv=fw;
				fw=fu;
			} else if (fu <= fv || v == x || v == w) {
				v=u;
				fv=fu;
			}
		}
	}
	*xmin=x;
	return fx;
}

#undef SHFT


/* -------------------------------------------------------------------
  FONCTION PRIVEE SearchAngle
  DESCRIPTION 
    Recherche de l'angle d'inclinaison optimal pour redresser l'image
  PARAMETRES
    img        image au format ptimg
  VALEUR DE RETOUR
    angle optimal
*/
double CSkewingAnalyzer::ComputeAngle() const
{

  double fmin,fa,fb,fc;
  double xmin;
  double ax,bx,cx;
  
  ax=-1.0;
  bx=0;
  
  /* Encadre le minimum dans un intervalle ax,bx,cx tq f(bx)<f(ax) && f(bx)<f(cx) */
  mnbrak(&ax,&bx,&cx,&fa,&fb,&fc);
  
  /* Recherche minimum par la me'thode de Brent */
  fmin=brent(ax,bx,cx,fb,0.01,&xmin);
  return xmin;
} 
	    

void CSkewingAnalyzer::InitBilevel(const CImage &img)
{
  CImage *img2;

  /* si l'image est "grande" */
  if (img.GetWidth()>1000 && img.GetHeight()>100) {
    /* reduction de l'image de 50% pour accelerer le traitement
       (on perd un peu en precision... */
    img2=img.Resize(img.GetWidth()/2,img.GetHeight()/2);
    Init(*img2);
    delete img2;
  } else {
    Init(img);
  }
}

void CSkewingAnalyzer::InitOther(const CImage &img)
{
  CImage *img2;
  CImage::EColorReductionMethod oldmethod;
  unsigned char oldthreshold;
 
  oldmethod=CImage::UseColorReductionMethod(CImage::Thresholding);
  oldthreshold=CImage::UseThreshold(0);

  /* conversion de l'image en noir et blanc */
  img2=img.Convert(CImageInfo::Bilevel);

  CImage::UseColorReductionMethod(oldmethod);
  CImage::UseThreshold(oldthreshold);

  Init(*img2);

  delete img2;
}

double CImage::FindSkewing() const
{
  CSkewingAnalyzer analyzer(*this);
  return analyzer.ComputeAngle();
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
