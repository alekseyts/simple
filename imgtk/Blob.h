//////////////////////////////////////////////////////////////////////
// @doc BLOB


#if !defined(_BLOB_H)
#define _BLOB_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "ImgTk.h"

typedef CSmartPtr<CImage> CImagePtr;

//@class CBlob
class IMGTK_EXPORT CBlob  {
public:
  //@cmember Constructor
  CBlob();  
  //@cmember Copy Constructor
  CBlob(const CBlob& blob);
  //@cmember Destructor
  ~CBlob();
  //@cmember Get a constant pointer to the image associated with the blob
  const CImage *GetImage() const;
  //@cmember Get a pointer the image associated with the blob
  CImage *GetImage();
  //@cmember Get a constant reference to Blob's position
  const CPosition& GetPosition() const;
  //@cmember Get a reference to Blob's position
  CPosition& GetPosition();  
  //@cmember Get X coordinate of the upper-left corner
  int GetX1() const;
  //@cmember Get Y coordinate of the upper-left corner
  int GetY1() const;
  //@cmember Get X coordinate of the bottom-right corner
  int GetX2() const;
  //@cmember Get Y coordinate of the bottom-right corner
  int GetY2() const;
  //@cmember Get X coordinate of the center
  int GetCenterX() const;  
  //@cmember Get Y coordinate of the center
  int GetCenterY() const;  
  //@cmember Get blob's width
  int GetWidth() const;
  //@cmember Get blob's height
  int GetHeight() const;
  //@cmember Set Blob's image  
  void SetImage(CImage *img);
  //@cmember Set Blob's position  
  void SetPosition(CPosition& pos);
  //@cmember Set X coordinate of the upper-left corner  
  void SetX1(int x);
  //@cmember Set Y coordinate of the upper-left corner  
  void SetY1(int y);
  //@cmember Set blob's width  
  void SetWidth(int w);
  //@cmember Set blob's height  
  void SetHeight(int h);
  //@cmember Set number of points in blob
  void SetPointCount(int count);
  //@cmember Get number of points in blob
  int GetPointCount() const;
  //@cmember Get total width for a group of two blobs
  int GetTotalWidth(const CBlob& blob) const;
  //@cmember Get total height for a group of two blobs
  int GetTotalHeight(const CBlob& blob) const ;
  //@cmember Get Horizontal Inclusivity (in percentage) for a group of two blobs
  int GetHInclusivity(const CBlob& blob) const ;
  //@cmember Get Vertical Inclusivity (in percentage) for a group of two blobs
  int GetVInclusivity(const CBlob& blob) const ;
  //@cmember Add to blobs
  CBlob& Add(const CBlob& blob);  
  //@cmember assignment operator
  const CBlob& operator= (const CBlob& blob);
  //@cmember += operator
  CBlob& operator+=(const CBlob& blob);
  //@cmember + operator
  CBlob operator+(const CBlob& blob) const;
  //@cmember Write a blob to a stream
  void WriteToStream  (ostream& stream) const;

private:
  // Blob's position
  CPosition mPosition;  
  // Width
  int mWidth;
  // Height
  int mHeight;  
  // Smart pointer to the blob's image
  CImagePtr *mImgPtr;
  // number of points in blob
  int mPointCount;
};


//@mfunc Get a constant pointer to the image associated with the blob
//@rdesc A constant pointer to the associated image
inline const CImage *CBlob::GetImage() const { return (CImage *) (*mImgPtr); }
//@mfunc Get a pointer to the image associated with the blob
//@rdesc A pointer to the associated image
inline CImage *CBlob::GetImage() { return (CImage *) (*mImgPtr); }
//@mfunc Get a constant reference to Blob's position
//@rdesc A constant reference to Blob's position
inline const CPosition& CBlob::GetPosition() const { return mPosition; }
//@mfunc Get a reference to Blob's position (upperleft corner)
//@rdesc A reference to Blob's position
inline CPosition& CBlob::GetPosition() { return mPosition; }
//@mfunc Get X coordinate of the upper-left corner
//@rdesc Coordinate
inline int CBlob::GetX1() const { return mPosition.GetX(); }
//@mfunc Get Y coordinate of the upper-left corner
//@rdesc Coordinate
inline int CBlob::GetY1() const { return mPosition.GetY(); }
//@mfunc Get X coordinate of the bottom-right corner
//@rdesc Coordinate
inline int CBlob::GetX2() const { return mPosition.GetX()+mWidth-1; }
//@mfunc Get Y coordinate of the bottom-right corner
//@rdesc Coordinate
inline int CBlob::GetY2() const { return mPosition.GetY()+mHeight-1; }
//@mfunc Get X coordinate of the blob's center
//@rdesc Coordinate
inline int CBlob::GetCenterX() const { return mPosition.GetX()+mWidth/2; }
//@mfunc Get Y coordinate of the blob's center
//@rdesc Coordinate
inline int CBlob::GetCenterY() const { return mPosition.GetY()+mHeight/2; }
//@mfunc Get blob's width
//@rdesc Width in pixels
inline int CBlob::GetWidth() const { return mWidth; }
//@mfunc Get blob's height
//@rdesc Height in pixels
inline int CBlob::GetHeight() const { return mHeight; }  
//@mfunc Set number of points in blob
//@parm Number of points in blob
inline void CBlob::SetPointCount(int count) {  mPointCount=count; }
//@mfunc Get number of points in blob
inline int CBlob::GetPointCount() const { return mPointCount; }

//@mfunc Set Blob's image
//@parm new image
inline void CBlob::SetImage(CImage *img);
//@mfunc Set Blob's position
//@parm new position
inline void CBlob::SetPosition(CPosition& pos) { mPosition=pos; }
//@mfunc Set X coordinate of the upper-left corner
//@parm new coordinate
inline  void CBlob::SetX1(int x) { mPosition.SetX(x); }
//@mfunc Set Y coordinate of the upper-left corner
//@parm new coordinate
inline  void CBlob::SetY1(int y) { mPosition.SetY(y); }
//@mfunc Set blob's width
//@parm new width
inline  void CBlob::SetWidth(int w) { mWidth=w; }
//@mfunc Set blob's height
//@parm new height
inline  void CBlob::SetHeight(int h) { mHeight=h; }
  
IMGTK_EXPORT ostream& operator<< (ostream& s, const CBlob& o);

#endif
