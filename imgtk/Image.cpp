//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#include "Image.h"
#include "ImageException.h"
#include "ImageIterator.h"
#include "CTiff.h"
#include <CBinaryFile.h>

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

#if defined(_WIN32_IMPLEMENTATION)
bool CImage::mUseDIBSections = false;
#endif

#if defined(_WIN32_IMPLEMENTATION)
const int CImage::ROffset = 2;
const int CImage::GOffset = 1;
const int CImage::BOffset = 0;
#else
const int CImage::ROffset = 0;
const int CImage::GOffset = 1;
const int CImage::BOffset = 2;
#endif

const int CImage::FROffset = 0;
const int CImage::FGOffset = 1;
const int CImage::FBOffset = 2;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImage::CImage(): mBitmap(NULL), m_eCreationState(CImage::mUseDIBSections ? CS_UsingDIBSection : CS_NotUsingDIBSection)
#if defined(_WIN32_IMPLEMENTATION)
, mFirstRow(NULL), mHBitmap(NULL)
#endif
{
  Init();  
}

//@mfunc Image constructor
//@parm int | width | image width
//@parm int | height | image height
//@parm EImageType | type | image type (cf. <t EImageType>)
//@parm int | xres | horizontal resolution
//@parm int | yres | vertical resolution
//@parm CColorPalette * | palette | A pointer to a color palette or NULL if no palette is needed
//@comm For Windows Platform, Image can implemented as DIB Sections (cf.
//    <mf CImage::UseDIBSections> class method). When an image has been implemented 
//    with a DIB section, you can get a HBITMAP object mapped on the 
//    image (cf. <mf CImage::GetHBitmap>). 

CImage::CImage(int width,int height,EImageType type,int xres,int yres, CColorPalette *palette)
	  : mInfo(width,height,type,xres,yres,palette), mBitmap(NULL), m_eCreationState(CImage::mUseDIBSections ? CS_UsingDIBSection : CS_NotUsingDIBSection)
#if defined(_WIN32_IMPLEMENTATION)
	, mFirstRow(NULL), mHBitmap(NULL)
#endif
{
  Init();    
}

CImage::CImage(const CImage &img) 
: mInfo(img.GetImageInfo()), mBitmap(NULL), m_eCreationState(CImage::mUseDIBSections ? CS_UsingDIBSection : CS_NotUsingDIBSection)
#if defined(_WIN32_IMPLEMENTATION)
, mFirstRow(NULL), mHBitmap(NULL)
#endif
{  
  Init();  
  Duplicate(img);
}

CImage::CImage(const CImageInfo &imgInfo)
: mInfo(imgInfo), mBitmap(NULL), m_eCreationState(CImage::mUseDIBSections ? CS_UsingDIBSection : CS_NotUsingDIBSection)
#if defined(_WIN32_IMPLEMENTATION)
, mFirstRow(NULL), mHBitmap(NULL)
#endif
{  
  Init();    
}

void CImage::Init()
{
  mBitmap=NULL;
  AllocBitmap();
}

void CImage::Free()
{
#if defined(_WIN32_IMPLEMENTATION)
  /* DIB Section ? */
  if (CS_UsingDIBSection == m_eCreationState) {
	if (mHBitmap) {
		DeleteObject(mHBitmap);	
		mHBitmap=NULL;
	}
  } else {
	if (mHBitmap) {
		DeleteObject(mHBitmap);	
		mHBitmap = NULL;
	} 
	if (mBitmap) delete [] mBitmap;
	mBitmap = NULL;
  }
#else
  if (mBitmap) delete [] mBitmap;
  mBitmap = NULL;
#endif
}

void CImage::Duplicate(const CImage &image)
{
  // Copy bitmap
  memcpy(GetBitmap(),image.GetBitmap(),GetBitmapSize());
}

CImage *CImage::DoubleHorizontal() const
{
	// Compute new dimension
	int w = GetWidth()*2;
	int h = GetHeight();
	CImage *img2 = 0;
	img2=new CImage(w,h,GeEImageType(),GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
	if (0 == img2) {
			throw CImageException("CImage::ExtractAreaOther", "Out of memory");
	}

	img2->Blit(0,0,this);
	img2->Blit(GetWidth(),0,this);
	return img2;
}

bool operator== (CImage const& lh, CImage const& rh)
{
	assert(lh.m_eCreationState == rh.m_eCreationState);
	assert(lh.m_eCreationState == CImage::CS_NotUsingDIBSection);
	assert(0 != lh.mBitmap);
	assert(0 != rh.mBitmap);

	return
		lh.mInfo == rh.mInfo &&
		0 == memcmp(lh.mBitmap, rh.mBitmap, lh.GetHeight() * lh.GetRowSize());
}

#if defined(_WIN32_IMPLEMENTATION)
void CImage::MaskLimits(DWORD mask,int *pstart,int *pend)
{
  int i;
  DWORD m=(DWORD)1<<31;	
  int start=-1;

  for(i=0; i<32 ; i++,m>>=1) {
    if (start==-1) {
      if (mask & m) start=i;
    } else {
      if (!(mask & m)) {
        *pstart=start;
        *pend=i-1;
        return;
      }
    }
  }
  *pstart=start;
  *pend=31;
}

TSample CImage::ApplyMask(DWORD val,DWORD mask,int maskstart,int maskend)
{
  return (TSample) (((val & mask)>>(31-maskend))*256)/(1<<(maskend-maskstart+1));
}


int CImage::WidthBytes(int bits)
{
  return ((bits + 31) / 32 * 4);
}


CImage::CImage(HGLOBAL hNewDIB): mBitmap(NULL), m_eCreationState(CImage::mUseDIBSections ? CS_UsingDIBSection : CS_NotUsingDIBSection)
#if defined(_WIN32_IMPLEMENTATION)
, mFirstRow(NULL), mHBitmap(NULL)
#endif
{
  LPBITMAPINFO lpbi= (LPBITMAPINFO) GlobalLock(hNewDIB);
  if (NULL == lpbi) {
	  throw CImageException("CImage(HGLOBAL)", "Cannot lock object");
  }
  int width=lpbi->bmiHeader.biWidth;
  int height=lpbi->bmiHeader.biHeight;
  int bc=lpbi->bmiHeader.biBitCount;
  RGBQUAD *pal=lpbi->bmiColors;
  int numcolors=(lpbi->bmiHeader.biClrUsed ? lpbi->bmiHeader.biClrUsed : 1<<bc);			
  TBitmap bitmap;
  EImageType imagetype;
  int offsetmask;
    
  switch (bc) {
    case 1:
      imagetype=CImageInfo::Bilevel;
      bitmap=(TBitmap) 
        lpbi+lpbi->bmiHeader.biSize+numcolors*sizeof(RGBQUAD);
      break;
    case 4:
      imagetype=CImageInfo::Color16;
      bitmap=(TBitmap) 
        lpbi+lpbi->bmiHeader.biSize+numcolors*sizeof(RGBQUAD);
      break;
    case 8:
      imagetype=CImageInfo::Color256;
      bitmap=(TBitmap) 
        lpbi+lpbi->bmiHeader.biSize+numcolors*sizeof(RGBQUAD);
      break;
    case 16:			
    case 32:
      imagetype=CImageInfo::ColorRGB;
      if (lpbi->bmiHeader.biCompression==BI_BITFIELDS)
        bitmap=(TBitmap) lpbi+lpbi->bmiHeader.biSize+3*sizeof(DWORD);
      else
        bitmap=(TBitmap) lpbi+lpbi->bmiHeader.biSize;
      break;
    case 24:			
      imagetype=CImageInfo::ColorRGB;
      bitmap=(TBitmap) lpbi+lpbi->bmiHeader.biSize;
      break;
    default:
      GlobalUnlock(hNewDIB);      
      throw CImageException("CImage::CImage","Can't handle this type of DIB");
  }
  offsetmask=(bitmap-((TBitmap) lpbi))+WidthBytes(bc*width)*height;
  mInfo.SetWidth(width);
  mInfo.SetHeight(height);
  mInfo.SeEImageType(imagetype);
  Init();

  
  if (bc!=16 && bc!=32)
    memcpy(GetBitmap(),bitmap,GetBitmapSize());
  else {
    DWORD *masks=(DWORD *) lpbi->bmiColors;
    DWORD redMask,greenMask,blueMask;
    int		redMaskStart,greenMaskStart,blueMaskStart;
    int		redMaskEnd,greenMaskEnd,blueMaskEnd;
    int i,j;			
    TBitmap bits;
    DWORD val;
    if (lpbi->bmiHeader.biCompression==BI_BITFIELDS) {
      redMask=masks[0];			
      greenMask=masks[1];
      blueMask=masks[2];							
    } else {
      if (bc==16) {
        blueMask=0x1f;
        greenMask=0x3e0;
        redMask=0x7c00;
      } else {
        blueMask=0xff;
        greenMask=0xff00;
        redMask=0xff0000;
      }
    }
      
    MaskLimits(redMask,&redMaskStart,&redMaskEnd);
    MaskLimits(greenMask,&greenMaskStart,&greenMaskEnd);
    MaskLimits(blueMask,&blueMaskStart,&blueMaskEnd);
    for(i=0;i<height;i++) {
      bits=GetRow(height-i-1);
      for(j=0;j<width;j++) {
        if (bc==16) {
          val=(DWORD) *((WORD *) bitmap);
          bitmap+=2;
        } else {
          val= *((DWORD *) bitmap);
          bitmap+=4;
        }
          *bits++=ApplyMask(val,blueMask,blueMaskStart,blueMaskEnd);
          *bits++=ApplyMask(val,greenMask,greenMaskStart,greenMaskEnd);
          *bits++=ApplyMask(val,redMask,redMaskStart,redMaskEnd);
      }
      if (((int) bitmap)%4) bitmap+=2;
    }
  }

  if (bc==4) {
    mInfo.SetColorPalette(new CColorPalette(16));
    for(int i=0;i<16;i++) {
      GetColorPalette()->GetColor(i).SetRGB(pal[i].rgbRed,pal[i].rgbGreen,pal[i].rgbBlue);      
    }
  }
  else
    if (bc==8) {
      mInfo.SetColorPalette(new CColorPalette(256));
      for(int i=0;i<256;i++) {
	GetColorPalette()->GetColor(i).SetRGB(pal[i].rgbRed,pal[i].rgbGreen,pal[i].rgbBlue);
    }
  }
    
  else if (bc==1) {
    
    
    int c1=pal[0].rgbRed+pal[0].rgbGreen+pal[0].rgbBlue;
    int c2=pal[1].rgbRed+pal[1].rgbGreen+pal[1].rgbBlue;
    
    if (c1 < c2) {        
       /* Inversion des bits */
       int i,length=GetRowSize()*GetHeight();
       TBitmap bits=GetBitmap();
	for (i=length ;i;i--) *bits++=(TByte)255 ^ *bits;
    }
    
  }
  
  GlobalUnlock(hNewDIB);  

}

CImage::CImage(HBITMAP hBitmap, HPALETTE hPal, int xres, int yres): mBitmap(NULL), m_eCreationState(CImage::mUseDIBSections ? CS_UsingDIBSection : CS_NotUsingDIBSection)
#if defined(_WIN32_IMPLEMENTATION)
, mFirstRow(NULL), mHBitmap(NULL)
#endif
{
   BITMAP             aBitmap;
   TBitmap              lpBits;
   HDC                hMemDC;
   HPALETTE           hOldPal = NULL;
   int                BPP;
   EImageType           type;
   LPBITMAPINFOHEADER lpbi = NULL;

   try {
		/* Ve'rification de la validite' du bitmap passe' en parame`tre */
		if (!GetObject (hBitmap, sizeof (aBitmap), (LPSTR) &aBitmap)) {
			throw CImageException("CImage::CImage", "Invalid HBITMAP");
		}
		   
		BPP = aBitmap.bmPlanes * aBitmap.bmBitsPixel;
		   
		switch (BPP) {
		case 1:
			type=CImageInfo::Bilevel;
			break;
		case 4:
			type = hPal ? CImageInfo::Color16 : CImageInfo::Gray16;
			break;
		case 8:
			type= hPal ? CImageInfo::Color256 : CImageInfo::Gray256;
			break;
		case 24:
			type=CImageInfo::ColorRGB;
			break;
		default:
			type=CImageInfo::ColorRGB;
		}


		CColorPalette* pPalette = 0;
		if ((CImageInfo::Color16 == type) || (CImageInfo::Color256 == type)) {
			pPalette = new CColorPalette(hPal);
		}

		mInfo.Init(aBitmap.bmWidth, aBitmap.bmHeight, type, xres, yres, pPalette);
		Init();

		lpbi = ConvertToBitmapInfoHeader();
		if (NULL == lpbi) {
			throw CImageException("CImage(HBITMAP, HPALETTE)", "Cannot get BITMAPINFOHEADER");
		}
		lpBits       = GetBitmap();
		/* On a besoin d'un DC me'moire pour contenir le bitmap.
			Si une palette a e'te' passe'e en parame`tre, il faut la
			se'lectionner dans le DC */

		hMemDC       = GetDC (NULL);

		if (hPal) {
			hOldPal = SelectPalette (hMemDC, hPal, FALSE);
			RealizePalette (hMemDC);
		}

		/*  On est maintenant pre`s a` obtenir notre bitmap DIB */

		if (!GetDIBits (hMemDC,
						hBitmap,
						0,
						aBitmap.bmHeight,
						lpBits,
						(BITMAPINFO *)lpbi,
						DIB_RGB_COLORS)) {
			throw CImageException("CImage::CImage", "Can't extract DIBBits from this HBITMAP");
		}

			if (NULL != lpbi) {
				delete [] lpbi;
			}
		  
		if (hOldPal)
			SelectPalette (hMemDC, hOldPal, FALSE);

		ReleaseDC (NULL, hMemDC);
   } catch (...) {
	   if (NULL != lpbi) {
		   delete [] lpbi;
	   }
	   throw;
   }
}

#endif // defined(_WIN32_IMPLEMENTATION)


CImage::~CImage()
{
  Free();
}


void CImage::AllocBitmap()
{
#if defined(_WIN32_IMPLEMENTATION)
  m_eCreationState = mUseDIBSections ? CS_UsingDIBSection : CS_NotUsingDIBSection;
  if (CS_UsingDIBSection == m_eCreationState)
    AllocDIBSection();
  else
    AllocMemory();
#else
  AllocMemory();
#endif
}

void CImage::AllocMemory()
{
  int h=GetHeight(); /* nombre de lignes */
  int rowsize=GetRowSize(); /* taille d'une ligne en octets */
  
  /* allocation dynamique du bitmap en me'moire */
  if (0 != mBitmap) {
	  delete [] mBitmap;
	  mBitmap = 0;
  }
  mBitmap=new TByte[h*rowsize];
  if (0 == mBitmap) {
	  throw CImageException("CImage::AllocMemory", "Out of memory");
  }
  /* adresse de la 1e`re ligne en me'moire */
#if defined(_WIN32_IMPLEMENTATION)
  mFirstRow=mBitmap+(h-1)*rowsize;
  mHBitmap=NULL;
#endif
  
}

#if defined(_WIN32_IMPLEMENTATION)
void CImage::AllocDIBSection()
{
  HDC hdc=GetDC(NULL);
  LPBITMAPINFOHEADER lpbi = 0;
  lpbi = ConvertToBitmapInfoHeader();
  
  /* Cre'ation de la section DIB */
  mHBitmap = 0;
  mHBitmap = CreateDIBSection(hdc,(CONST BITMAPINFO *) (lpbi),
              DIB_RGB_COLORS, (VOID **) &(mBitmap),
				NULL,0);

  /* teste re'ussite */
  if (0 == mHBitmap) {
	  if (0 != lpbi) {
		  delete [] lpbi;
	  }
	  ReleaseDC(NULL,hdc);

#if defined(_WIN32_IMPLEMENTATION)
	  //detect whether max allowed GDI objects (in Win2k and later - 9999 per process) exceeded
	  HANDLE hProcess;
	  hProcess = GetCurrentProcess();
	  if (NULL != hProcess) {
		  DWORD dwGdiObjectCount = GetGuiResources(hProcess, GR_GDIOBJECTS);
		  CloseHandle(hProcess);
		  if (dwGdiObjectCount > 9990) {
			  throw CImageException("CImage::AllocDIBSection", "Maximum allowed GDI objects exceeded");
		  }
	  }
#endif
	  throw CImageException("CImage::AllocDIBSection", "Cannot create image: invalid image parameters");
  }
  if (0 != lpbi) {
	  delete [] lpbi;
  }

  ReleaseDC(NULL,hdc);

  /* adresse de la 1e`re ligne en me'moire */
  mFirstRow=mBitmap+(GetHeight()-1)*GetRowSize();
}

LPBITMAPINFOHEADER CImage::ConvertToBitmapInfoHeader() const
{
  return mInfo.ConvertToBitmapInfoHeader();
}

HGLOBAL	CImage::ConvertToDIB() const
{
  if (!IsStandard()) {
    throw CImageException("CImage::ConvertToDIB", "Can't convert this type of image to DIB");
  }

  int nbcol;
  int bisize;
  int size;
  HGLOBAL hMem;
  TBitmap s;
  LPBITMAPINFOHEADER lpbi = 0;
  lpbi = ConvertToBitmapInfoHeader();
  
  nbcol=mInfo.GetNumColors(); /* number of colors in palette */
  /* taille du BITMAPINFO avec l'espace pour la palette */
  bisize = sizeof(BITMAPINFO)+nbcol*sizeof(RGBQUAD); 
  /* Taille totale avec le bitmap */
  size = bisize+GetBitmapSize();        
  /* Allocation du bloc */
  hMem = GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,size);  
  
  if (hMem==0) {
	  if (0 != lpbi) {
		  delete [] lpbi;
	  }
	  return NULL;
  }
  /* Ve'rouillage du block me'moire */
  s=(TBitmap) GlobalLock(hMem);
  if (s==NULL) {
	  GlobalUnlock(hMem);
	  if (0 != lpbi) {
		  delete [] lpbi;
	  }
	  return NULL;
  }
  /* Copie du BITMAPINFO avec la palette de couleurs */
  memcpy(s,lpbi,bisize);
  /* Copie du bitmap */
  memcpy(s+bisize,GetBitmap(),GetBitmapSize());
  /* De've'rouillage */
  GlobalUnlock(hMem);      
  if (0 != lpbi) {
	  delete [] lpbi;
  }
  return hMem;
}

HBITMAP	CImage::ConvertToDDB() const
{
  if (!IsStandard()) {
    throw CImageException("CImage::ConvertToDDB", "Can't convert this type of image to DDB");
  }

  LPBITMAPINFOHEADER lpbi = 0;
  lpbi = ConvertToBitmapInfoHeader();
  TCBitmap bits;
  int w,h;
  HDC hdc;
  HBITMAP hBmp,obmp;          
  HDC hdcbmp;
   
  bits=GetBitmap();
  w=GetWidth();
  h=GetHeight();
  try {
	hdc=GetDC(NULL);
	if (0 == hdc) {
		throw CImageException("CImage::ConvertToDD", "Cannot get device context");
	}
	hBmp=CreateCompatibleBitmap(hdc,w,h);
	if (0 == hBmp) {
		throw CImageException("CImage::ConvertToDD", "Cannot create compatible bitmap");
	}
	hdcbmp=CreateCompatibleDC(hdc);      
	if (0 == hdcbmp) {
		throw CImageException("CImage::ConvertToDD", "Cannot create device context");
	}
	obmp=(HBITMAP) SelectObject(hdcbmp,(HGDIOBJ) hBmp);
	if (0 == obmp) {
		throw CImageException("CImage::ConvertToDD", "An error occured while selecting object into device context");
	}

	int nRes = StretchDIBits(hdcbmp,0,0,w,h,0,0,w,h,bits,(BITMAPINFO *)lpbi,DIB_RGB_COLORS,SRCCOPY);
	if (GDI_ERROR == nRes) {
		throw CImageException("CImage::ConvertToDD", "An error occured while coping image data");
	}
	SelectObject(hdcbmp,obmp);      
  } catch (...) {
		if (0 != lpbi) {
			delete [] lpbi;
		}
		throw;	
  }

  DeleteDC(hdcbmp);
  ReleaseDC(NULL,hdc);
  if (0 != lpbi) {
	delete [] lpbi;
  }
  return hBmp;
}

#endif // defined(_WIN32_IMPLEMENTATION)

//@mfunc Return the luminosity of a given pixel
//@parm  x-coordinate of the pixel
//@parm  y-coordinate of the pixel
//@rdesc Pixel luminosity between 0 (black) and 255 (white)
    
TSample	  CImage::GetLuma(int x, int y) const
{
  CheckPointInside("GetLuma", x, y);

  int val;
  
  switch (GeEImageType()) {
  case CImageInfo::Bilevel:
    if (*(GetRow(y)+x/8) & (0x80 >> (x%8)))    
      return 0;
    return 255;
  case CImageInfo::Gray16:
    if (x%2==0) 
      val=*(GetRow(y)+x/2) >> 4;
    else
      val=*(GetRow(y)+x/2) & (TByte)0xf;
    return val*16+val;
  case CImageInfo::Gray256:
    return *(GetRow(y)+x);
  case CImageInfo::Color16:
    if (x%2==0) 
      val=*(GetRow(y)+x/2) >> 4;
    else
      val=*(GetRow(y)+x/2) & 0xf;
    
    return GetColorPalette()->GetColor(val).GetLuma();
  case CImageInfo::Color256:
    val=*(GetRow(y)+x);
    return GetColorPalette()->GetColor(val).GetLuma();
  case CImageInfo::ColorRGB: {
    TCBitmap p=GetRow(y)+3*x;
    
    return CColor(*(p+ROffset),
      *(p+GOffset),
      *(p+BOffset)).GetLuma();
			     }
  case CImageInfo::GrayFloat:
    return (TSample) *(GetFRow(y)+x);
  case CImageInfo::ColorFloat: {
    const TFSample *p=GetFRow(y)+3*x;
    
    return CColor(  (unsigned char) *(p+FROffset),
      (unsigned char) *(p+FGOffset),        
      (unsigned char) *(p+FBOffset)).GetLuma();
			      }
  default:
    throw CImageException("CImage::GetLuma","Unknown Image type");
  }
  return 0;
}

//@mfunc Return the luminosity of a given pixel as a real sample
//@parm  x-coordinate of the pixel
//@parm  y-coordinate of the pixel
//@rdesc Pixel luminosity between 0.0 (black) and 255.9999999999 (white)

TFSample  CImage::GetFLuma	(int x, int y) const
{
  switch (GeEImageType()) {
  case CImageInfo::GrayFloat:
    return *(GetFRow(y)+x);
  case CImageInfo::ColorFloat: {
    const TFSample *p=GetFRow(y)+3*x;
    TFSample r,g,b;	
    r=*(p+FROffset);
    g=*(p+FGOffset);
    b=*(p+FBOffset);
    return (r*CColor::GetRedFactor()+g*CColor::GetGreenFactor()+b*CColor::GetBlueFactor())/100;
			       }
  default:
    throw CImageException("CImage::GetFLuma", "This image type is not supported by GetFLuma function");
  }
  return 0;
}

//@mfunc Set the luminosity of a given pixel
//@parm  x-coordinate of the pixel
//@parm  y-coordinate of the pixel
//@parm  Pixel luminosity between 0 (black) and 255 (white)
//@rdesc none
void	  CImage::SetLuma	(int x, int y, TSample lum)
{
  CheckPointInside("SetLuma", x, y);

  switch (GeEImageType()) {
  case CImageInfo::Bilevel:
    if (lum>=127) 
      *(GetRow(y)+x/8) &= ~(0x80 >> x%8);
    else
      *(GetRow(y)+x/8) |= (0x80 >> x%8);
    break;
  case CImageInfo::Gray16: {
    TSample *p=GetRow(y)+x/2;
    if (x%2==0) 
      *p = (*p & (TByte)0xf) + ((lum>>4)<<4);
    else
      *p = (lum>>4) + (*p & (TByte)0xf0);
    break;
			   }
  case CImageInfo::Gray256:
    *(GetRow(y)+x)=lum;
    break;
  case CImageInfo::Color16: {
    TSample *p=GetRow(y)+x/2;
    lum=(TSample) GetColorPalette()->GetNearestLumaIndex(lum);
    if (x%2==0) 
      *p = (*p & (TByte)0xf) + (lum<<4);
    else
      *p = lum + (*p & (TByte) 0xf0);
    break;
			    }
  case CImageInfo::Color256:
    lum=(TSample) GetColorPalette()->GetNearestLumaIndex(lum);
    *(GetRow(y)+x)=lum;
    break;
  case CImageInfo::ColorRGB: {
    TSample *p=GetRow(y)+3*x;
    *p=lum;
    *(p+1)=lum;
    *(p+2)=lum;
    break;
			     }
  case CImageInfo::GrayFloat:
    *(GetFRow(y)+x)=(TFSample) lum;
    break;
  case CImageInfo::ColorFloat: {
    TFSample *p=GetFRow(y)+3*x;
    *p= *(p+1) = *(p+2) = (TFSample) lum;	
    break;
			       }
  default:
    throw CImageException("CImage::SetLuma","Unknown Image type");
  }    
}

//@mfunc Set the luminosity of a given pixel with a real sample
//@parm  x-coordinate of the pixel
//@parm  y-coordinate of the pixel
//@parm  Pixel luminosity between 0.0 (black) and 255.9999999999 (white)
//@rdesc none
void	  CImage::SetFLuma	(int x, int y, TFSample lum)
{
  switch (GeEImageType()) {
  case CImageInfo::GrayFloat:
    *(GetFRow(y)+x)=lum;
    break;
  case CImageInfo::ColorFloat: {
    TFSample *p=GetFRow(y) + 3*x;
    *(p) = *(p+1) = *(p+2) = lum;	
    break;
			       }
  default:    
    SetLuma(x,y,(TSample)
      (lum<(TFSample)0.0?0:(lum>(TFSample)255.0?255: lum))); 
  }
}

void CImage::CheckPointInside(const string& sData, const int& x, const int& y) const
{
	 if ((x < 0) || (x >= GetWidth())) {
		 throw CImageException(sData.c_str(), "Incorrect x value. The value should be between %d and %d", 0, GetWidth());
	 }
	 if ((y < 0) || (y >= GetHeight())) {
		 throw CImageException(sData.c_str(), "Incorrect y value. The value should be between %d and %d", 0, GetHeight());
	 }
}

//@mfunc Get the color of a given pixel
//@parm  x-coordinate of the pixel
//@parm  y-coordinate of the pixel
//@parm  A reference to a color
//@rdesc none
void	  CImage::GetColor (int x, int y, CColor &color) const
{
  CheckPointInside("GetColor", x, y);

  TByte val;
  TSample lum;
  
  switch (GeEImageType()) {
  case CImageInfo::Bilevel:
    if (*(GetRow(y)+x/8) & (0x80 >> x%8))
      color.SetRGB(0,0,0);
    else
      color.SetRGB(255,255,255);
    break;
  case CImageInfo::Gray16:
    if (x%2==0) 
      val=*(GetRow(y)+x/2) >> 4;
    else
      val=*(GetRow(y)+x/2) & (TByte) 0xf;
    lum=(val<<4)+val;
    color.SetRGB(lum,lum,lum);
    break;
  case CImageInfo::Gray256:
    lum=*(GetRow(y)+x);
    color.SetRGB(lum,lum,lum);
    break;
  case CImageInfo::Color16:
    if (x%2==0) 
      val=*(GetRow(y)+x/2) >> 4;
    else
      val=*(GetRow(y)+x/2) & (TByte)0xf;
    color=GetColorPalette()->GetColor(val);
    break;
  case CImageInfo::Color256:
    val=*(GetRow(y)+x);
    color=GetColorPalette()->GetColor(val);
    break;
  case CImageInfo::ColorRGB: {
    TCBitmap p=GetRow(y) + 3*x;
    color.SetRGB(*(p+ROffset),*(p+GOffset),*(p+BOffset));
    break;
			     }
  case CImageInfo::GrayFloat:
    lum = (TSample) *(GetFRow(y) + x);
    color.SetRGB(lum,lum,lum);
    break;
  case CImageInfo::ColorFloat: {
    const TFSample *p=GetFRow(y) + 3*x;
    color.SetRGB(
      (TSample) *(((TFSample *) p)+FROffset),
      (TSample) *(((TFSample *) p)+FGOffset),
      (TSample) *(((TFSample *) p)+FBOffset));
    break;
			       }
  default:
    throw CImageException("CImage::GetColor", "Unknown Image type");
  }  
}

//@mfunc Get the color of a given pixel, retrieving color components as real values
//@parm  x-coordinate of the pixel
//@parm  y-coordinate of the pixel
//@parm  A reference to returning the value of the red component
//@parm  A reference to returning the value of the green component
//@parm  A reference to returning the value of the blue component
//@rdesc none
void	  CImage::GetFColor(int x, int y, TFSample &r, TFSample &g, TFSample &b) const
{
  switch (GeEImageType()) {
  case CImageInfo::GrayFloat:
    r=g=b= *(GetFRow(y)+x);
    break;
  case CImageInfo::ColorFloat: {
    const TFSample *p= GetFRow(y) + 3*x;
    r=*(p+FROffset);
    g=*(p+FGOffset);
    b=*(p+FBOffset);
    break;
			       }
  default: {
    CColor color;
    GetColor(x,y,color);
    r=(TFSample) color.GetRed();
    g=(TFSample) color.GetGreen();
    b=(TFSample) color.GetBlue();
	 }
  }  
}

//@mfunc Set the color of a given pixel
//@parm  x-coordinate of the pixel
//@parm  y-coordinate of the pixel
//@parm  A constant reference to a color
//@rdesc none

void	  CImage::SetColor (int x, int y, const CColor &color)
{    
  CheckPointInside("SetColor", x, y);

  TSample lum;
  
  switch (GeEImageType()) {
  case CImageInfo::Bilevel:
    lum=color.GetLuma();
    if (lum>=127) 
      *(GetRow(y)+x/8) &= ~(0x80 >> x%8);
    else
      *(GetRow(y)+x/8) |= (0x80 >> x%8);
    break;
  case CImageInfo::Gray16: {
    TSample *p=GetRow(y)+x/2;
    lum=color.GetLuma();
    if (x%2==0) 
      *p = (*p & (TByte)0xf) + ((lum>>4)<<4);
    else
      *p = (lum>>4) + (*p & (TByte)0xf0);
    break;
			   }
  case CImageInfo::Gray256:
    lum=color.GetLuma();
    *(GetRow(y)+x)=lum;
    break;
  case CImageInfo::Color16: {
    TSample *p=GetRow(y)+x/2;
    lum=(TSample) GetColorPalette()->GetNearestColorIndex(color);
	assert((0 <= lum ) && (lum < 16));
    if (x%2==0) 
	  *p = (*p & (TByte)0xf) + (lum<<4);
    else
	  *p = lum + (*p & (TByte)0xf0);
    break;
			    }
  case CImageInfo::Color256:
    *(GetRow(y)+x)=(TSample) GetColorPalette()->GetNearestColorIndex(color);
    break;
  case CImageInfo::ColorRGB: {
    TSample *p = GetRow(y) + 3*x;
    *(p+ROffset)=color.GetRed();
    *(p+GOffset)=color.GetGreen();
    *(p+BOffset)=color.GetBlue();
    break;
			     }
  case CImageInfo::GrayFloat:
    lum=color.GetLuma();
    *(GetFRow(y)+x)=(TFSample) lum;
    break;
  case CImageInfo::ColorFloat: {
    TFSample *p = GetFRow(y) + 3*x;
    *(((TFSample *) p)+3*x+FROffset)=(TFSample) color.GetRed();
    *(((TFSample *) p)+3*x+FGOffset)=(TFSample) color.GetGreen();
    *(((TFSample *) p)+3*x+FBOffset)=(TFSample) color.GetBlue();
    break;
			       }
  default:
    throw CImageException("CImage::SetColor","Unknown Image type");
  }  
}

//@mfunc Set the color of a given pixel, using real values for the color components
//@parm  x-coordinate of the pixel
//@parm  y-coordinate of the pixel
//@parm  Value of the red component
//@parm  Value of the green component
//@parm  Value of the blue component
//@rdesc none

void	  CImage::SetFColor(int x, int y, TFSample r, TFSample g, TFSample b)
{
  switch (GeEImageType()) {
  case CImageInfo::GrayFloat:
    *(GetFRow(y) + x)=(r*CColor::GetRedFactor()+g*CColor::GetGreenFactor()+b*CColor::GetBlueFactor())/100;
    break;
  case CImageInfo::ColorFloat: {
    TFSample *p = GetFRow(y) + 3*x;
    *(p+FROffset)= r;
    *(p+FGOffset)= g;
    *(p+FBOffset)= b;
    break;
			       }
  default:
    SetColor(x,y,CColor((TSample) r,(TSample) g,(TSample) b));
  }
}

#if defined(_WIN32_IMPLEMENTATION)
void CImage::RGBToBGR()
{
  TBitmap p;
  TSample bol;
  int i,j;
  int w,h;
	
  if (CImageInfo::ColorRGB != GeEImageType()) {
	  throw CImageException("RGBToBGR", "This method is supported only for true color images");
  }
  
  w=GetWidth();
  h=GetHeight();
	
  for(i=0;i<h;i++) {
    p=GetRow(i);
    for(j=0;j<w;j++) {
      bol=*p;
      *p=*(p+2);
      *(p+2)=bol;
      p+=3;
    }
  }
}

#endif


//@cfunc Fill the whole image with a given luminosity (default, white)
//@parm Luminosity (default 255=white)
void CImage::Erase(TSample luma)
{
  switch (GeEImageType()) {
  case CImageInfo::Bilevel:
    if (luma>127)
      memset(GetBitmap(),0,GetBitmapSize());
    else	     
      memset(GetBitmap(),255,GetBitmapSize());
    break;
  case CImageInfo::Gray256:
  case CImageInfo::ColorRGB:
    memset(GetBitmap(),luma,GetBitmapSize());
    break;
  default: 
    {
	CImageIterator *pit = 0;
	pit = CreateIterator();
	if (0 == pit) {
		throw CImageException("CImage::Erase", "Out of memory");
	}
      for(; pit->GetY() < GetHeight() ; pit->NextRow() )
	for(pit->SetX(0) ; pit->GetX() < GetWidth() ; pit->Next() )
	  pit->SetLuma(luma);
      delete pit;
    }
  }
}

//@cfunc Fill the whole image with a given color 
//@parm Color
void CImage::Erase(const CColor &color)
{
  switch (GeEImageType()) {
    case CImageInfo::Bilevel:
    case CImageInfo::Gray16:
    case CImageInfo::Gray256:
      Erase(color.GetLuma());
      break;
    default:
      {
	CImageIterator *pit = 0;
	pit = CreateIterator();
	if (0 == pit) {
		throw CImageException("CImage::Erase", "Out of memory");
	}
	for(; pit->GetY() < GetHeight() ; pit->NextRow() )
	  for(pit->SetX(0) ; pit->GetX() < GetWidth() ; pit->Next() )
	    pit->SetColor(color);      
	  delete pit;
      }
  }
}

  
CImageIterator *CImage::CreateIterator() const
{
  switch (GeEImageType()) {
  case CImageInfo::Bilevel:
    return new CBilevelImageIterator((CImage *)this);
  case CImageInfo::Gray16: 
    return new CGray16ImageIterator((CImage *)this);
  case CImageInfo::Gray256:    
    return new CGray256ImageIterator((CImage *)this);
  case CImageInfo::Color16: 
    return new CColor16ImageIterator((CImage *)this);
  case CImageInfo::Color256:
    return new CColor256ImageIterator((CImage *)this);
  case CImageInfo::ColorRGB: 
    return new CColorRGBImageIterator((CImage *)this);
  case CImageInfo::GrayFloat:
    return new CGrayFloatImageIterator((CImage *)this);
  case CImageInfo::ColorFloat: 
    return new CColorFloatImageIterator((CImage *)this);
  default:
    throw CImageException("CImage::CreateIterator","Unknown Image type");
  }  
  return NULL;

}

void CImage::WriteAsTIFF(std::string fileName)
{
	CTiff tiff;
	tiff.Open(fileName.c_str(),"w");
	tiff.WriteImage(this);
	tiff.Flush();
	tiff.Close();
}

void CImage::ReadFromFile(CInputBinaryFile& ibf)
{
	// Free current bitmap
	Free();
	// Read info block
	mInfo.ReadFromFile(ibf);
	// Initialize (allocate the bitmap)
	Init();
	// OK! We can read the bitmap now!
	ibf.Read(GetBitmap(), GetBitmapSize());
}

void CImage::ReadFromFile(shared_ptr<CInputBinFile> ibf)
{
	// Free current bitmap
	Free();
	// Read info block
	mInfo.ReadFromFile(ibf);
	// Initialize (allocate the bitmap)
	Init();
	// OK! We can read the bitmap now!
	ibf->Read(GetBitmap(), GetBitmapSize());
}


void CImage::IgnoreRead(shared_ptr<CInputBinFile> ibf)
{
	// Read info block
	mInfo.ConfinedRead(ibf);
	// Free current bitmap
	//Free();
	// Read info block
	//mInfo.ReadFromFile(ibf);
	// Initialize (allocate the bitmap)
	//Init();
	// Offset pointer
	ibf->OffsetPointerPosition(GetBitmapSize());
}

void CImage::WriteToFile(shared_ptr<COutputBinFile> obf) const
{
	mInfo.WriteToFile(obf);
	obf->Write(GetBitmap(), GetBitmapSize());
}

void CImage::WriteToFile(COutputBinaryFile& obf) const
{
	mInfo.WriteToFile(obf);
	obf.Write(GetBitmap(), GetBitmapSize());
}

//void CImage::ReadFromStream (istream& stream)
//{
//  // Free current bitmap
//  Free();
//  // Read info block
//  stream >> mInfo;
//  // Initialize (allocate the bitmap)
//  Init();
//  // OK! We can read the bitmap now!
//  TBitmap p=GetBitmap();
//  int nValue;
//  for(int i=0;i<GetBitmapSize();i++) {
//    stream >> nValue;
//	*p++ = nValue;    
//  }
//}
//
//void CImage::WriteToStream  (ostream& stream) const
//{
//  stream << mInfo;
//  TCBitmap p=GetBitmap();
//  for(int i=0;i<GetBitmapSize();i++) {
//    stream << int(*p++);
//    if (i%10==9) stream << endl; else stream << ' ';
//  }
//}
//
//istream& operator>> (istream& s, CImage& o)
//{
//  o.ReadFromStream(s);
//  return s;
//}
//
//ostream& operator<< (ostream& s, const CImage& o)
//{
//  o.WriteToStream(s);
//  return s;
//}

const CImage& CImage::operator= (const CImage& img)
{
  if (this == &img) return *this; 
  // not the same size and type ? We need to reallocate
  if (GetWidth()!=img.GetWidth() || GetHeight()!=img.GetHeight() || GeEImageType()!=img.GeEImageType()) {
    // Free the image
    Free();
    // Initialize info block
    mInfo=img.GetImageInfo();
    // Allocate bitmap
    Init();
  } else {
    // if the image has a palette, assign it too
	  if (img.GetColorPalette()) {
		CColorPalette *pColorPalette = 0;
		pColorPalette = new CColorPalette(*img.GetColorPalette());
		if (0 == pColorPalette) {
			throw CImageException("CImage::operator=", "Out of memory");
		}
		SetColorPalette(pColorPalette);
	  }
  }
  // Duplicate bitmap
  Duplicate(img);
  
  return *this;
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
