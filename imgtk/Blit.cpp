//////////////////////////////////////////////////////////////////////
// @doc IMAGE 

#include "Image.h"

#if !defined(__IMAGE_ITERATOR_H)
#include "ImageIterator.h"
#endif


#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

//@mfunc blit an image
void CImage::Blit(int x,int y,const CImage *src)
{
	Blit(x, y, src, 0, 0, src->GetWidth(), src->GetHeight());
}
  
//@mfunc blit a portion of an image
void CImage::Blit(int x,int y,const CImage *src,int xSrc,int ySrc,int widthSrc,int heightSrc)
{
	try {
		CheckPointInside("Blit", x, y);
	} catch (CImgTkException& ex) {
		throw CImgTkException(ex.Data(), "Incorrect point coordinates on the original blit image.\n%s", ex.Message());
	}
	try {
		src->CheckPointInside("Blit", xSrc, ySrc);
	} catch (CImgTkException& ex) {
		throw CImgTkException(ex.Data(), "Incorrect point coordinates on the source image.\n%s", ex.Message());
	}

	if (xSrc+widthSrc > src->GetWidth()) {
		throw CImageException("Blit", "This part of image is too large to blit at this point");
	}
	if (ySrc+heightSrc > src->GetHeight()) {
		throw CImageException("Blit", "This part of image is too large to blit at this point");
	}
	if (x+widthSrc > GetWidth()) {
		throw CImageException("Blit", "This image is too large to blit at this point");
	}
	if (y+heightSrc > GetHeight()) {
		throw CImageException("Blit", "This image is too large to blit at this point");
	}

	CImageIterator *pSrc = 0;
	CImageIterator *pDest = 0;

	try {
		pSrc = src->CreateIterator();
		pDest = CreateIterator();
		int maxx=xSrc+widthSrc;
		int maxy=ySrc+heightSrc;

		switch (GeEImageType()) {
		case CImageInfo::Bilevel:
		case CImageInfo::Gray16:
		case CImageInfo::Gray256:
			{  
				for(pDest->SetY(y),pSrc->SetY(ySrc); pSrc->GetY() < maxy ; pSrc->NextRow(),pDest->NextRow() )
					for(pDest->SetX(x),pSrc->SetX(xSrc) ; pSrc->GetX() < maxx ; pSrc->Next(),pDest->Next() )
						pDest->SetLuma(pSrc->GetLuma());
			}
			break;
		case CImageInfo::Color16:
		case CImageInfo::Color256:
		case CImageInfo::ColorRGB:
			{  
				for(pDest->SetY(y),pSrc->SetY(ySrc); pSrc->GetY() < maxy ; pSrc->NextRow(),pDest->NextRow() )
					for(pDest->SetX(x),pSrc->SetX(xSrc) ; pSrc->GetX() < maxx ; pSrc->Next(),pDest->Next() )
						pDest->SetColor(pSrc->GetColor());
			}
			break;
		case CImageInfo::GrayFloat:    
			{  
				for(pDest->SetY(y),pSrc->SetY(ySrc); pSrc->GetY() < maxy ; pSrc->NextRow(),pDest->NextRow() )
					for(pDest->SetX(x),pSrc->SetX(xSrc) ; pSrc->GetX() < maxx ; pSrc->Next(),pDest->Next() )
						pDest->SetFLuma(pSrc->GetFLuma());
			}
			break;
		case CImageInfo::ColorFloat:    
			{  
				TFSample r,g,b;
				for(pDest->SetY(y),pSrc->SetY(ySrc); pSrc->GetY() < maxy ; pSrc->NextRow(),pDest->NextRow() )
					for(pDest->SetX(x),pSrc->SetX(xSrc) ; pSrc->GetX() < maxx ; pSrc->Next(),pDest->Next() ) {
						pSrc->GetFColor(r,g,b);
						pDest->SetFColor(r,g,b);
					}
			}
			break;
		default:
			assert(0);
		}
	} catch (CImgTkException&) {
		if (0 != pSrc) {
			delete pSrc;
		}
		if (0 != pDest) {
			delete pDest;
		}
		throw;
	}
	if (0 != pSrc) {
		delete pSrc;
	}
	if (0 != pDest) {  
		delete pDest;
	}
}
  
#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
