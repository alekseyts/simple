//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#if !defined(__IMAGE_ITERATOR_H)
#define __IMAGE_ITERATOR_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif

#if !defined(__POSITION_H)
#include "Position.h"
#endif

#if !defined(__IMAGE_H)
#include "Image.h"
#endif

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

//@class virtual Image iterator
class IMGTK_EXPORT CImageIterator {
public:  
  virtual ~CImageIterator() {};
  // Accessors
  CImage *GetImage() const { return mImg; }
  virtual int GetWidth() const { return mImg->GetWidth();  }
  virtual int GetHeight() const { return mImg->GetHeight(); }
  // Accessors & modifiers for position
  int GetX() const { return mX; }
  int GetY() const { return mY; }
  virtual void SetX(int newX) = 0;
  virtual void SetY(int newY) = 0;
  virtual void SetPosition(int newX,int newY) = 0;
  void SetPosition(CPosition &pos) { SetPosition(pos.GetX(),pos.GetY()); }
  virtual void Next() = 0;
  virtual void Previous() = 0;
  virtual void NextRow() = 0; 
  virtual void PreviousRow() = 0;
  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const = 0;
  virtual void	    SetLuma(TSample value) = 0;

  virtual TFSample GetFLuma() const = 0;
  virtual void	   SetFLuma(TFSample value)  = 0;
  virtual CColor  GetColor()  const = 0;
  virtual void SetColor(const CColor &color)  = 0;
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b)  const = 0;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b)  = 0;

protected:
	CImage *mImg;
	int mX;
	int mY;  

	virtual void CheckXInside(int x) {	
		if (x<0 || x>=GetWidth()) {
			throw CImageException("CImageIterator", "Incorrect x value. This value should be between %d and %d", 0, GetWidth());
		}
	};
	virtual bool CheckYInside(int y) {	
		if (y<0 || y>=GetHeight()) {
		return false;//	throw CImageException("CImageIterator", "Incorrect y value. This value should be between %d and %d", 0, GetHeight());
		}
		return true;
	};
};

//@class Image iterator for bilevel images
//@base public | CImageIterator
class IMGTK_EXPORT CBilevelImageIterator : public CImageIterator {
public:
  CBilevelImageIterator(CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual void	    SetLuma(TSample value) ;

  virtual TFSample GetFLuma() const;
  virtual void	   SetFLuma(TFSample value);
  virtual CColor  GetColor()  const;
  virtual void SetColor(const CColor &color);
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b) ;

  TBitmap 	get() const {return mBits;};

protected:  
  TByte	  mMask;
  int mRowOffset;
  TBitmap mBits;
  TBitmap mBitsCurrentRow;
};

inline CBilevelImageIterator::CBilevelImageIterator(CImage *img) 
{
  mImg=img;
  mRowOffset=img->GetRowOffset();
  SetPosition(0,0);   
}

inline void CBilevelImageIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX/8;
  mMask=0x80 >> (mX%8);
}

inline void CBilevelImageIterator::SetX(int newX) 
{
	CheckXInside(newX);
	mX=newX;  
    mBits=mBitsCurrentRow+mX/8;
    mMask=0x80 >> (mX%8);
}

inline void CBilevelImageIterator::SetY(int newY) 
{
  bool ret = CheckYInside(newY);
  if(ret == false){
	  return;
  }

  mY=newY;  
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX/8;  
}

inline void CBilevelImageIterator::Next()
{
  mX++;

  if (mMask==0x01u) {
    mMask=0x80u;
    mBits++;
  } else mMask>>=1;
}

inline void CBilevelImageIterator::Previous()
{
  mX--;

  if (mMask==0x80u) {
    mMask=0x01u;
    mBits--;
  } else mMask<<=1;
}

inline void CBilevelImageIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CBilevelImageIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CBilevelImageIterator::GetLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  if (*mBits & mMask) return 0;
  else return 255;
}

inline void CBilevelImageIterator::SetLuma(TSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());

  if (value>=127) 
    *mBits &= ~mMask;
  else
    *mBits |=  mMask;
}

inline  TFSample CBilevelImageIterator::GetFLuma() const
{
  return (TFSample) GetLuma();
}

inline  void	   CBilevelImageIterator::SetFLuma(TFSample value) 
{
  SetLuma(CImage::CLUMP(value));
}

inline  CColor  CBilevelImageIterator::GetColor()  const
{
  TSample luma=GetLuma();
  return CColor(luma,luma,luma);
}

inline  void CBilevelImageIterator::SetColor(const CColor &color)
{
  SetLuma(color.GetLuma());
}

inline  void CBilevelImageIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  r=g=b=(TFSample) GetLuma();
}

inline  void CBilevelImageIterator::SetFColor(TFSample r, TFSample g, TFSample b) 
{
  SetColor(CColor(CImage::CLUMP(r),CImage::CLUMP(g),CImage::CLUMP(b)));
}


class IMGTK_EXPORT CBilevelImageConstIterator : public CImageIterator {
public:
  CBilevelImageConstIterator(const CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }
  
  virtual int GetWidth() const { return m_pImage->GetWidth();  }
  virtual int GetHeight() const { return m_pImage->GetHeight(); }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual TFSample GetFLuma() const;
  virtual CColor  GetColor()  const;
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;

  virtual void	    SetLuma(TSample value) {};
  virtual void	   SetFLuma(TFSample value)  {};
  virtual void SetColor(const CColor &color)  {};
  virtual void SetFColor(TFSample r, TFSample g, TFSample b)  {};
   
  TCBitmap 	get() const {return mBits;};

protected:  
  const CImage * m_pImage;
  TByte	  mMask;
  int mRowOffset;
  TCBitmap mBits;
  TCBitmap mBitsCurrentRow;
};

inline CBilevelImageConstIterator::CBilevelImageConstIterator(const CImage *img) 
{
  m_pImage=img;
  mRowOffset=img->GetRowOffset();
  SetPosition(0,0);   
}

inline void CBilevelImageConstIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=m_pImage->GetRow(mY);
  mBits=mBitsCurrentRow+mX/8;
  mMask=0x80 >> (mX%8);
}

inline void CBilevelImageConstIterator::SetX(int newX) 
{
	CheckXInside(newX);
	mX=newX;  
    mBits=mBitsCurrentRow+mX/8;
    mMask=0x80 >> (mX%8);
}

inline void CBilevelImageConstIterator::SetY(int newY) 
{
  CheckYInside(newY);

  mY=newY;  
  mBitsCurrentRow=m_pImage->GetRow(mY);
  mBits=mBitsCurrentRow+mX/8;  
}

inline void CBilevelImageConstIterator::Next()
{
  mX++;

  if (mMask==0x01u) {
    mMask=0x80u;
    mBits++;
  } else mMask>>=1;
}

inline void CBilevelImageConstIterator::Previous()
{
  mX--;

  if (mMask==0x80u) {
    mMask=0x01u;
    mBits--;
  } else mMask<<=1;
}

inline void CBilevelImageConstIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CBilevelImageConstIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CBilevelImageConstIterator::GetLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  if (*mBits & mMask) return 0;
  else return 255;
}

inline  TFSample CBilevelImageConstIterator::GetFLuma() const
{
  return (TFSample) GetLuma();
}

inline  CColor  CBilevelImageConstIterator::GetColor()  const
{
  TSample luma=GetLuma();
  return CColor(luma,luma,luma);
}

inline  void CBilevelImageConstIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  r=g=b=(TFSample) GetLuma();
}



//@class Image iterator for 16 level grayscale images
//@base public | CImageIterator
class IMGTK_EXPORT CGray16ImageIterator : public CImageIterator {
public:
  CGray16ImageIterator(CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual void	    SetLuma(TSample value) ;

  virtual TFSample GetFLuma() const;
  virtual void	   SetFLuma(TFSample value) ;
  virtual CColor  GetColor()  const;
  virtual void SetColor(const CColor &color);
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b) ;

protected:    
  int mRowOffset;
  TBitmap mBits;
  TBitmap mBitsCurrentRow;
};

inline CGray16ImageIterator::CGray16ImageIterator(CImage *img) 
{
  mImg=img;
  mRowOffset=img->GetRowOffset();
  SetPosition(0,0);   
}

inline void CGray16ImageIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX/2;  
}

inline void CGray16ImageIterator::SetX(int newX) 
{
	CheckXInside(newX);
	mX=newX;  
    mBits=mBitsCurrentRow+mX/2;  
}

inline void CGray16ImageIterator::SetY(int newY) 
{
  CheckYInside(newY);

  mY=newY;  
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX/2;  
}

inline void CGray16ImageIterator::Next()
{
  if ((mX%2)!=0) mBits++;
  mX++;
    
}

inline void CGray16ImageIterator::Previous()
{
  mX--;
  if ((mX%2)==0) mBits--;
}

inline void CGray16ImageIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CGray16ImageIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CGray16ImageIterator::GetLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  TByte val;
  if ((mX%2) == 0) 
    val=*mBits >> 4;
  else
    val=*mBits & (TByte) 0xfu;
  return (val<<4)+val;
}

inline void CGray16ImageIterator::SetLuma(TSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());

  if ((mX%2)==0) 
    *mBits = (*mBits & (TByte)0xfu) + ((value>>4)<<4);
  else
    *mBits = (value>>4) + (*mBits & (TByte)0xf0u);
}

inline  TFSample CGray16ImageIterator::GetFLuma() const
{
  return (TFSample) GetLuma();
}

inline  void	   CGray16ImageIterator::SetFLuma(TFSample value) 
{
  SetLuma(CImage::CLUMP(value));
}

inline  CColor  CGray16ImageIterator::GetColor()  const
{
  TSample luma=GetLuma();
  return CColor(luma,luma,luma);
}

inline  void CGray16ImageIterator::SetColor(const CColor &color)
{
  SetLuma(color.GetLuma());
}

inline  void CGray16ImageIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  r=g=b=(TFSample) GetLuma();
}

inline  void CGray16ImageIterator::SetFColor(TFSample r, TFSample g, TFSample b) 
{
  SetColor(CColor(CImage::CLUMP(r),CImage::CLUMP(g),CImage::CLUMP(b)));
}

//@class Image iterator for 256 level grayscale images
//@base public | CImageIterator
class IMGTK_EXPORT CGray256ImageIterator : public CImageIterator {
public:
  CGray256ImageIterator(CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual void	    SetLuma(TSample value) ;

  virtual TFSample GetFLuma() const;
  virtual void	   SetFLuma(TFSample value) ;
  virtual CColor  GetColor()  const;
  virtual void SetColor(const CColor &color);
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b) ;

protected:    
  int mRowOffset;
  TSample *mBits;
  TSample *mBitsCurrentRow;
};

inline CGray256ImageIterator::CGray256ImageIterator(CImage *img) 
{
  mImg=img;
  mRowOffset=img->GetRowOffset();
  SetPosition(0,0);   
}

inline void CGray256ImageIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX;  
}

inline void CGray256ImageIterator::SetX(int newX) 
{
	CheckXInside(newX);
	mX=newX;  
    mBits=mBitsCurrentRow+mX;  
}

inline void CGray256ImageIterator::SetY(int newY) 
{
  CheckYInside(newY);

  mY=newY;  
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX;  
}

inline void CGray256ImageIterator::Next()
{
  mX++;
  mBits++;
}

inline void CGray256ImageIterator::Previous()
{
  mX--;
  mBits--;
}

inline void CGray256ImageIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CGray256ImageIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CGray256ImageIterator::GetLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  return *mBits;
}

inline void CGray256ImageIterator::SetLuma(TSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());

  *mBits=value;
}

inline  TFSample CGray256ImageIterator::GetFLuma() const
{
  return (TFSample) GetLuma();
}

inline  void	   CGray256ImageIterator::SetFLuma(TFSample value) 
{
  SetLuma(CImage::CLUMP(value));
}

inline  CColor  CGray256ImageIterator::GetColor()  const
{
  TSample luma=GetLuma();
  return CColor(luma,luma,luma);
}

inline  void CGray256ImageIterator::SetColor(const CColor &color)
{
  SetLuma(color.GetLuma());
}

inline  void CGray256ImageIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  r=g=b=(TFSample) GetLuma();
}

inline  void CGray256ImageIterator::SetFColor(TFSample r, TFSample g, TFSample b) 
{
  SetColor(CColor(CImage::CLUMP(r),CImage::CLUMP(g),CImage::CLUMP(b)));
}

//@class Image iterator for 16 color images
//@base public | CImageIterator
class IMGTK_EXPORT CColor16ImageIterator : public CImageIterator {
public:
  CColor16ImageIterator(CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual void	    SetLuma(TSample value) ;

  virtual TFSample GetFLuma() const;
  virtual void	   SetFLuma(TFSample value) ;
  virtual CColor  GetColor()  const;
  virtual void SetColor(const CColor &color);
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b) ;

protected:    
  int mRowOffset;
  TBitmap mBits;
  TBitmap mBitsCurrentRow;
  CColorPalette *mPalette;
};

inline CColor16ImageIterator::CColor16ImageIterator(CImage *img) 
{
  mImg=img;
  mRowOffset=img->GetRowOffset();
  mPalette=img->GetColorPalette();
  SetPosition(0,0);   
}

inline void CColor16ImageIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX/2;  
}

inline void CColor16ImageIterator::SetX(int newX) 
{
  CheckXInside(newX);
  mX=newX;  
  mBits=mBitsCurrentRow+mX/2;  
}

inline void CColor16ImageIterator::SetY(int newY) 
{
  CheckYInside(newY);

  mY=newY;  
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX/2;  
}

inline void CColor16ImageIterator::Next()
{
  if ((mX%2)!=0) mBits++;
  mX++;
    
}

inline void CColor16ImageIterator::Previous()
{
  mX--;
  if ((mX%2)==0) mBits--;
}

inline void CColor16ImageIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CColor16ImageIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CColor16ImageIterator::GetLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  TByte val;
  if ((mX%2) == 0) 
    val=*mBits >> 4;
  else
    val=*mBits & (TByte) 0xfu;
  return mPalette->GetColor(val).GetLuma();
}

inline void CColor16ImageIterator::SetLuma(TSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());

  value=(TSample) mPalette->GetNearestLumaIndex(value);

  if ((mX%2)==0) 
    *mBits = (*mBits & (TByte)0xfu) + ((value>>4)<<4);
  else
    *mBits = (value>>4) + (*mBits & (TByte)0xf0u);
}

inline  TFSample CColor16ImageIterator::GetFLuma() const
{
  return (TFSample) GetLuma();
}

inline  void	   CColor16ImageIterator::SetFLuma(TFSample value) 
{
  SetLuma(CImage::CLUMP(value));
}

inline  CColor  CColor16ImageIterator::GetColor()  const
{
 assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  TByte val;
  if ((mX%2) == 0) 
    val=*mBits >> 4;
  else
    val=*mBits & (TByte) 0xfu;
  return mPalette->GetColor(val);
}

inline  void CColor16ImageIterator::SetColor(const CColor &color)
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());

  TSample value=mPalette->GetNearestColorIndex(color);

  if ((mX%2)==0) 
    *mBits = (*mBits & (TByte)0xfu) + (value << 4);
  else
    *mBits = value + (*mBits & (TByte)0xf0u);
}

inline  void CColor16ImageIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  CColor col=GetColor();
  r=(TFSample) col.GetRed();
  g=(TFSample) col.GetGreen();
  b=(TFSample) col.GetBlue();
}

inline  void CColor16ImageIterator::SetFColor(TFSample r, TFSample g, TFSample b) 
{
  SetColor(CColor(CImage::CLUMP(r),CImage::CLUMP(g),CImage::CLUMP(b)));
}

//@class Image iterator for 256 color images
//@base public | CImageIterator
class IMGTK_EXPORT CColor256ImageIterator : public CImageIterator {
public:
  CColor256ImageIterator(CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual void	    SetLuma(TSample value) ;

  virtual TFSample GetFLuma() const;
  virtual void	   SetFLuma(TFSample value) ;
  virtual CColor  GetColor()  const;
  virtual void SetColor(const CColor &color);
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b) ;

protected:    
  int mRowOffset;
  TSample *mBits;
  TSample *mBitsCurrentRow;
  CColorPalette *mPalette;
};

inline CColor256ImageIterator::CColor256ImageIterator(CImage *img) 
{
  mImg=img;
  mRowOffset=img->GetRowOffset();
  mPalette=img->GetColorPalette();
  SetPosition(0,0);   
}

inline void CColor256ImageIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX;  
}

inline void CColor256ImageIterator::SetX(int newX) 
{
  CheckXInside(newX);

  mX=newX;  
  mBits=mBitsCurrentRow+mX;  
}

inline void CColor256ImageIterator::SetY(int newY) 
{
  CheckYInside(newY);

  mY=newY;  
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX;  
}

inline void CColor256ImageIterator::Next()
{
  mX++;
  mBits++;
}

inline void CColor256ImageIterator::Previous()
{
  mX--;
  mBits--;
}

inline void CColor256ImageIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CColor256ImageIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CColor256ImageIterator::GetLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  return mPalette->GetColor(*mBits).GetLuma();
}

inline void CColor256ImageIterator::SetLuma(TSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());

  *mBits=(TSample) mPalette->GetNearestLumaIndex(value);
}

inline  TFSample CColor256ImageIterator::GetFLuma() const
{
  return (TFSample) GetLuma();
}

inline  void	   CColor256ImageIterator::SetFLuma(TFSample value) 
{
  SetLuma(CImage::CLUMP(value));
}

inline  CColor  CColor256ImageIterator::GetColor()  const
{
  TSample luma=GetLuma();
  return mPalette->GetColor(*mBits);
}

inline  void CColor256ImageIterator::SetColor(const CColor &color)
{
  *mBits=(TSample) mPalette->GetNearestColorIndex(color);
}

inline  void CColor256ImageIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  CColor col=GetColor();
  r=(TFSample) col.GetRed();
  g=(TFSample) col.GetGreen();
  b=(TFSample) col.GetBlue();
}

inline  void CColor256ImageIterator::SetFColor(TFSample r, TFSample g, TFSample b) 
{
  SetColor(CColor(CImage::CLUMP(r),CImage::CLUMP(g),CImage::CLUMP(b)));
}
//@class Image iterator for truecolor 24 bit images
//@base public | CImageIterator
class IMGTK_EXPORT CColorRGBImageIterator : public CImageIterator {
public:
  CColorRGBImageIterator(CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual void	    SetLuma(TSample value) ;

  virtual TFSample GetFLuma() const;
  virtual void	   SetFLuma(TFSample value) ;
  virtual CColor  GetColor()  const;
  virtual void SetColor(const CColor &color);
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b) ;

protected:    
  int mRowOffset;
  TSample *mBits;
  TSample *mBitsCurrentRow;
};

inline CColorRGBImageIterator::CColorRGBImageIterator(CImage *img) 
{
  mImg=img;
  mRowOffset=img->GetRowOffset();
  SetPosition(0,0);   
}

inline void CColorRGBImageIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX*3;  
}

inline void CColorRGBImageIterator::SetX(int newX) 
{
  CheckXInside(newX);

  mX=newX;  
  mBits=mBitsCurrentRow+mX*3;  
}

inline void CColorRGBImageIterator::SetY(int newY) 
{
  CheckYInside(newY);

  mY=newY;  
  mBitsCurrentRow=mImg->GetRow(mY);
  mBits=mBitsCurrentRow+mX*3;  
}

inline void CColorRGBImageIterator::Next()
{
  mX++;
  mBits+=3;
}

inline void CColorRGBImageIterator::Previous()
{
  mX--;
  mBits-=3;
}

inline void CColorRGBImageIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CColorRGBImageIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CColorRGBImageIterator::GetLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  return GetColor().GetLuma();
}

inline void CColorRGBImageIterator::SetLuma(TSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());

  *mBits=*(mBits+1)=*(mBits+2)=value;
}

inline  TFSample CColorRGBImageIterator::GetFLuma() const
{
  return (TFSample) GetLuma();
}

inline  void	   CColorRGBImageIterator::SetFLuma(TFSample value) 
{
  SetLuma(CImage::CLUMP(value));
}

inline  CColor  CColorRGBImageIterator::GetColor()  const
{
  
  return CColor(*(mBits+CImage::ROffset),*(mBits+CImage::GOffset),*(mBits+CImage::BOffset));
}

inline  void CColorRGBImageIterator::SetColor(const CColor &color)
{
  *(mBits+CImage::ROffset)=color.GetRed();
  *(mBits+CImage::GOffset)=color.GetGreen();
  *(mBits+CImage::BOffset)=color.GetBlue();
}

inline  void CColorRGBImageIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  *(mBits+CImage::ROffset)=CImage::CLUMP(r);
  *(mBits+CImage::GOffset)=CImage::CLUMP(g);
  *(mBits+CImage::BOffset)=CImage::CLUMP(b);
}

inline  void CColorRGBImageIterator::SetFColor(TFSample r, TFSample g, TFSample b) 
{
  SetColor(CColor(CImage::CLUMP(r),CImage::CLUMP(g),CImage::CLUMP(b)));
}

//@class Image iterator for grayscale images with real samples
//@base public | CImageIterator
class IMGTK_EXPORT CGrayFloatImageIterator : public CImageIterator {
public:
  CGrayFloatImageIterator(CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual void	    SetLuma(TSample value) ;

  virtual TFSample GetFLuma() const;
  virtual void	   SetFLuma(TFSample value) ;
  virtual CColor  GetColor()  const;
  virtual void SetColor(const CColor &color);
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b) ;

protected:    
  int mRowOffset;
  TFSample *mBits;
  TFSample *mBitsCurrentRow;
};

inline CGrayFloatImageIterator::CGrayFloatImageIterator(CImage *img) 
{
  mImg=img;
  mRowOffset=img->GetRowOffset();
  SetPosition(0,0);   
}

inline void CGrayFloatImageIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=mImg->GetFRow(mY);
  mBits=mBitsCurrentRow+mX;  
}

inline void CGrayFloatImageIterator::SetX(int newX) 
{
  CheckXInside(newX);

  mX=newX;  
  mBits=mBitsCurrentRow+mX;  
}

inline void CGrayFloatImageIterator::SetY(int newY) 
{
  CheckYInside(newY);

  mY=newY;  
  mBitsCurrentRow=mImg->GetFRow(mY);
  mBits=mBitsCurrentRow+mX;  
}

inline void CGrayFloatImageIterator::Next()
{
  mX++;
  mBits++;
}

inline void CGrayFloatImageIterator::Previous()
{
  mX--;
  mBits--;
}

inline void CGrayFloatImageIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CGrayFloatImageIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CGrayFloatImageIterator::GetLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  return CImage::CLUMP(*mBits);
}

inline void CGrayFloatImageIterator::SetLuma(TSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());

  *mBits=(TFSample) value;
}

inline  TFSample CGrayFloatImageIterator::GetFLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  return *mBits;
}

inline  void	   CGrayFloatImageIterator::SetFLuma(TFSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  *mBits=value;
}

inline  CColor  CGrayFloatImageIterator::GetColor()  const
{
  TSample luma=GetLuma();
  return CColor(luma,luma,luma);
}

inline  void CGrayFloatImageIterator::SetColor(const CColor &color)
{
  SetLuma(color.GetLuma());
}

inline  void CGrayFloatImageIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  r=g=b=GetFLuma();
}

inline  void CGrayFloatImageIterator::SetFColor(TFSample r, TFSample g, TFSample b) 
{
	*mBits =
		static_cast<TFSample>(
			(r * CColor::GetRedFactor() + g * CColor::GetGreenFactor() + b * CColor::GetBlueFactor()) / 100.0
		);
}

//@class Image iterator for color images with real samples
//@base public | CImageIterator
class IMGTK_EXPORT CColorFloatImageIterator : public CImageIterator {
public:
  CColorFloatImageIterator(CImage *img) ;  
  int GetX() const { return mX; }
  int GetY() const { return mY; }

  virtual void SetX(int newX) ;
  virtual void SetY(int newY) ;
  virtual void SetPosition(int newX,int newY) ;
  virtual void Next();
  virtual void Previous();
  virtual void NextRow();
  virtual void PreviousRow();

  // Accessors for current Luma & Color
  virtual TSample   GetLuma() const;
  virtual void	    SetLuma(TSample value) ;

  virtual TFSample GetFLuma() const;
  virtual void	   SetFLuma(TFSample value) ;
  virtual CColor  GetColor() const;
  virtual void SetColor(const CColor &color);
  virtual void GetFColor(TFSample &r, TFSample &g, TFSample &b) const ;
  virtual void SetFColor(TFSample r, TFSample g, TFSample b) ;

protected:    
  int mRowOffset;
  TFSample *mBits;
  TFSample *mBitsCurrentRow;
};

inline CColorFloatImageIterator::CColorFloatImageIterator(CImage *img) 
{
  mImg=img;
  mRowOffset=img->GetRowOffset();
  SetPosition(0,0);   
}

inline void CColorFloatImageIterator::SetPosition(int newX,int newY) 
{
  CheckXInside(newX);
  CheckYInside(newY);

  mX=newX;
  mY=newY;
  mBitsCurrentRow=mImg->GetFRow(mY);
  mBits=mBitsCurrentRow+mX*3;  
}

inline void CColorFloatImageIterator::SetX(int newX) 
{
  CheckXInside(newX);

  mX=newX;  
  mBits=mBitsCurrentRow+mX*3;  
}

inline void CColorFloatImageIterator::SetY(int newY) 
{
  CheckYInside(newY);

  mY=newY;  
  mBitsCurrentRow=mImg->GetFRow(mY);
  mBits=mBitsCurrentRow+mX*3;  
}

inline void CColorFloatImageIterator::Next()
{
  mX++;
  mBits+=3;
}

inline void CColorFloatImageIterator::Previous()
{
  mX--;
  mBits-=3;
}

inline void CColorFloatImageIterator::NextRow()
{
  mY++;
  mBits+=mRowOffset;
  mBitsCurrentRow+=mRowOffset;
}


inline void CColorFloatImageIterator::PreviousRow()
{
  mY--;
  mBits-=mRowOffset;
  mBitsCurrentRow-=mRowOffset;
}

inline TSample CColorFloatImageIterator::GetLuma() const
{  
  return GetColor().GetLuma();
}

inline void CColorFloatImageIterator::SetLuma(TSample value) 
{
  *mBits=*(mBits+1)=*(mBits+2)=(TFSample) value;  
}

inline  TFSample CColorFloatImageIterator::GetFLuma() const
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  
  TFSample r,g,b;	
  r=*(mBits+CImage::FROffset);
  g=*(mBits+CImage::FGOffset);
  b=*(mBits+CImage::FBOffset);
  return
		static_cast<TFSample>(
			(r * CColor::GetRedFactor() + g * CColor::GetGreenFactor() + b * CColor::GetBlueFactor()) / 100.0
		);
}

inline void CColorFloatImageIterator::SetFLuma(TFSample value) 
{
  assert(mX>=0 && mX<GetWidth());
  assert(mY>=0 && mY<GetHeight());
  *mBits=*(mBits+1)=*(mBits+2)=value;  
}

inline  CColor  CColorFloatImageIterator::GetColor()  const
{
  TSample luma=GetLuma();
  TSample rColor = static_cast<TSample>(*(mBits+CImage::FROffset));
  TSample gColor = static_cast<TSample>(*(mBits+CImage::FGOffset));
  TSample bColor = static_cast<TSample>(*(mBits+CImage::FBOffset));
  return CColor(rColor, gColor, bColor);
}

inline  void CColorFloatImageIterator::SetColor(const CColor &color)
{
  *(mBits+CImage::FROffset)=(TFSample) color.GetRed();
  *(mBits+CImage::FGOffset)=(TFSample) color.GetGreen();
  *(mBits+CImage::FBOffset)=(TFSample) color.GetBlue();
}

inline  void CColorFloatImageIterator::GetFColor(TFSample &r, TFSample &g, TFSample &b) const
{
  r=*(mBits+CImage::FROffset);
  g=*(mBits+CImage::FGOffset);
  b=*(mBits+CImage::FBOffset);
}

inline  void CColorFloatImageIterator::SetFColor(TFSample r, TFSample g, TFSample b) 
{
  *(mBits+CImage::FROffset)=r;
  *(mBits+CImage::FGOffset)=g;
  *(mBits+CImage::FBOffset)=b;
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // __IMAGE_ITERATOR_H
