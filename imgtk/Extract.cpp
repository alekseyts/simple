//////////////////////////////////////////////////////////////////////
// @doc IMAGE 

#include "Image.h"

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

CImage *CImage::ExtractAreaOther(int x,int y,int w,int h) const
{
  int i;
  CImage *img2 = 0;
  
  img2=new CImage(w,h,GeEImageType(),GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
  if (0 == img2) {
	  throw CImageException("CImage::ExtractAreaOther", "Out of memory");
  }

  int pixelSize=GetBitCount()/8;
  /* extraction des points */
  for(i=0;i<h;i++) 
    memcpy(img2->GetRow(i),GetRow(i+y)+x*pixelSize,w*pixelSize);
  return img2;
}

CImage *CImage::ExtractAreaNibble(int x,int y,int w,int h) const
{
  int i,j;
  CImage *img2 = 0;
  CColor color;
  
  img2=new CImage(w,h,GeEImageType(),GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
  if (0 == img2) {
	  throw CImageException("CImage::ExtractAreaNibble", "Out of memory");
  }

  /* extraction des points */
  for(i=0;i<h;i++) 
    for(j=0;j<w;j++) {
      GetColor(x+j,y+i,color);
      img2->SetColor(j,i,color);
    }
  return img2;
}


CImage *CImage::ExtractAreaBilevel(int x,int y,int w,int h) const
{
  int i,j;
  CImage *img2 = 0;
  
  img2=new CImage(w,h,GeEImageType(),GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
  if (0 == img2) {
	  throw CImageException("CImage::ExtractAreaBilevel", "Out of memory");
  }

  TCBitmap bits;
  TBitmap  bits2;
  TByte mask,mask2;

  for(i=0;i<h;i++) {
    bits=GetRow(i+y)+(x>>3);
    mask=0x80 >> (x & 0x7);
    bits2=img2->GetRow(i);
    memset(bits2,0,img2->GetRowSize());
    mask2=0x80;
    for(j=0;j<w;j++) {
      if (*bits & mask) 
	*bits2 |= mask2;
      if (mask==0x01) {
	mask=0x80;
	bits++;
      } else mask>>=1;
      if (mask2==0x01) {
	mask2=0x80;
	bits2++;
      } else mask2>>=1;
    }
  }
  return img2;
}

//@mfunc Extract an area from an image
//@parm x-coordinate of the area upper left corner
//@parm y-coordinate of the area upper left corner
//@parm x-coordinate of the area bottom right corner
//@parm y-coordinate of the area bottom right corner
//@rdesc Extracted image
CImage *CImage::ExtractArea(int x,int y,int w,int h) const
{
	//if (!(x>=0 && w>0 && x+w<=GetWidth()) || !(y>=0 && h>0 && y+h<=GetHeight())) {
	// throw CImageException("CImage::ExtractArea", "Incorrect parameters: specified area should be entirely inside the image");
	//}
	if (!(x>=0)) {
		throw CImageException(
			"CImage::ExtractArea",
			"Incorrect x parameter: specified area should be entirely inside the image",
			x
		);
	}
	if (!(w>0)) {
		//throw CImageException(
		//	"CImage::ExtractArea",
		//	"Incorrect w parameter: specified area should be entirely inside the image",
		//	w
		//);
		return NULL;
	}
	if (!(x+w<=GetWidth())) {
		throw CImageException(
			"CImage::ExtractArea",
			"Incorrect x+w parameters: specified area should be entirely inside the image",
			x+w
		);
	}
	if (!(y>=0)) {
		throw CImageException(
			"CImage::ExtractArea",
			"Incorrect y parameter: specified area should be entirely inside the image",
			y
		);
	}
	if (!(h>0)) {
		throw CImageException(
			"CImage::ExtractArea",
			"Incorrect h parameter: specified area should be entirely inside the image",
			h
		);
	}
	if (!(y+h<=GetHeight())) {
		throw CImageException(
			"CImage::ExtractArea",
			"Incorrect y+h parameters: specified area should be entirely inside the image",
			y+h
		);
	}

	switch(GeEImageType()) {
	case CImageInfo::Bilevel:
		return ExtractAreaBilevel(x,y,w,h);
	case CImageInfo::Color16:
	case CImageInfo::Gray16:
		return ExtractAreaNibble(x,y,w,h);
		break;
	default:
		return ExtractAreaOther(x,y,w,h);
	}
	return NULL;
}

void CImage::GetExtractAreaCoordinatesBilevel(int& startx, int& starty, int& neww, int& newh) const
{
	assert(GeEImageType() == CImageInfo::Bilevel);
  int i;
  int endx,endy;
  int w=GetWidth();
  int h=GetHeight();
  int offset;
  TCBitmap bits;
  TByte mask;

  for(starty=0;starty<h;starty++) {
    bits=GetRow(starty);
    for(i=0;i<w/8;i++) 
      if (*bits++!=0) goto startyfound;
    mask=0x80;
    for(i=(w/8)*8;i<w;i++) {
      if (*bits&mask) goto startyfound;
      mask>>=1;
    }
  }
  return;
startyfound:
  for(endy=h-1;endy>=0;endy--) {
    bits=GetRow(endy);
    for(i=0;i<w/8;i++) 
      if (*bits++!=0) goto endyfound;
    mask=0x80;
    for(i=(w/8)*8;i<w;i++) {
      if (*bits&mask) goto endyfound;
      mask>>=1;
    }
  }
endyfound:
  for(startx=0;startx<w;startx++) {
    mask=0x80 >> (startx & 0x7);
    offset=startx>>3;
    for(i=starty;i<=endy;i++) {
      if (*(GetRow(i)+offset)&mask) goto startxfound;
    }
  }
startxfound:
  for(endx=w-1;endx>=0;endx--) {
    mask=0x80 >> (endx & 0x7);
    offset=endx>>3;
    for(i=starty;i<=endy;i++) {
      if (*(GetRow(i)+offset)&mask) goto endxfound;
    }
  }
endxfound:
  neww=endx-startx+1;
  newh=endy-starty+1;
}

void CImage::GetExtractAreaCoordinatesGray256(int& startx, int& starty, int& neww, int& newh, TSample threshold) const
{
	assert(GeEImageType() == CImageInfo::Gray256);
  int i;
  int endx,endy;
  int w=GetWidth();
  int h=GetHeight();
  const TSample *bits;

  for(startx=0;startx<w;startx++) {
    for(i=0;i<h;i++) {
      if (*(GetRow(i)+startx)<threshold) goto startxfound;
    }
  }
  throw CImgTkException("CImage::CropGray256", "Threshold is too small");
startxfound:
  for(endx=w-1;endx>=0;endx--) {
    for(i=0;i<h;i++) {
      if (*(GetRow(i)+endx)<threshold) goto endxfound;
    }
  }
endxfound:
  for(starty=0;starty<h;starty++) {
    bits=GetRow(starty);
    for(i=startx;i<=endx;i++) {
      if (*bits++<threshold) goto startyfound;
    }
  }
startyfound:
  for(endy=h-1;endy>=0;endy--) {
    bits=GetRow(endy);
    for(i=startx;i<=endx;i++) {
      if (*bits++<threshold) goto endyfound;
    }
  }
endyfound:
  neww=endx-startx+1;
  newh=endy-starty+1;
}


void CImage::GetExtractAreaCoordinatesOther(int& startx, int& starty, int& neww, int& newh, TSample threshold) const
{
  int i;
  int endx,endy;
  int w=GetWidth();
  int h=GetHeight();

  for(startx=0;startx<w;startx++) {
    for(i=0;i<h;i++) {
      if (GetLuma(startx,i)<threshold) goto startxfound;
    }
  }
  throw CImgTkException("CImage::CropOther", "Threshold is too small");
startxfound:
  for(endx=w-1;endx>=0;endx--) {
    for(i=0;i<h;i++) {
      if (GetLuma(endx,i)<threshold) goto endxfound;
    }
  }
endxfound:
  for(starty=0;starty<h;starty++) {
    for(i=startx;i<=endx;i++) {
      if (GetLuma(i,starty)<threshold) goto startyfound;
    }
  }
startyfound:
  for(endy=h-1;endy>=0;endy--) {
    for(i=startx;i<=endx;i++) {
      if (GetLuma(i,endy)<threshold) goto endyfound;
    }
  }
endyfound:
  neww=endx-startx+1;
  newh=endy-starty+1;
}

void CImage::GetExtractAreaCoordinates(int& startx, int& starty, int& neww, int& newh, TSample threshold) const
{
	switch(GeEImageType()) {
	case CImageInfo::Bilevel:
		GetExtractAreaCoordinatesBilevel(startx, starty, neww, newh);
		break;
	case CImageInfo::Gray256:
		GetExtractAreaCoordinatesGray256(startx, starty, neww, newh, threshold);
		break;
	default:
		GetExtractAreaCoordinatesOther(startx, starty, neww, newh, threshold);
	}
}

CImage *CImage::Crop(TSample threshold) const
{
	int startx = 0; 
	int starty = 0;
	int neww = 0;
	int newh = 0;
	GetExtractAreaCoordinates(startx, starty, neww, newh, threshold);
	return ExtractArea(startx, starty, neww, newh);
}



#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
