//////////////////////////////////////////////////////////////////////
// @doc TKEXCEPTION

#if !defined(__IMGTK_EXCEPTION_H)
#define __IMGTK_EXCEPTION_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif

#include <string>

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

	//@class Exception
	class IMGTK_EXPORT CImgTkException
	{
	public:
		//@cmember Constructor
		CImgTkException(const std::string sData, const std::string sMessage);
		//additional constructor, shows that value should be in specified range
		CImgTkException(const std::string sData, const std::string sMessage, int nMinValue, int nMaxValue);
		//additional constructor, shows that value should compared with specified constant
		CImgTkException(const std::string sData, const std::string sMessage, int nValue);
		//additional constructor, shows additional description data
		CImgTkException(const std::string sData, const std::string sMessage, const std::string szDescription);

		//@cmember The module where exception occurs
		const char* Data() const { return m_sData.c_str(); }
		//@cmember Error message
		const char* Message() const { return m_sMessage.c_str(); }

	private:
		std::string format_int(int nValue) const;

		const std::string m_sData;
		const std::string m_sMessage;
	};

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__IMGTK_EXCEPTION_H)
