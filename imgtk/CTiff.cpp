//////////////////////////////////////////////////////////////////////
// @doc TIFF

#include "ctiff.h"

#if !defined(__DATA_STREAM_H)
#include "DataStream.h"
#endif

#if !defined(__TIFF_EXCEPTION_H)
#include "TiffException.h"
#endif

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

bool CTiff::mFirstCreation = true;

CTiff::CTiff()
{
  mTiff=NULL;
  if (mFirstCreation) {
    mFirstCreation=false;
    SetErrorHandler(StandardErrorProcessing);
    SetWarningHandler(StandardWarningProcessing);
  }
}

CTiff::~CTiff()
{
  // be sure the TIFF file is closed
  if (mTiff) Close();
}

void CTiff::Close() 
{ 
	::TIFFClose(mTiff); 
	mTiff = NULL;
}

int CTiff::Flush() 
{ 
  return ::TIFFFlush(mTiff); 
}

int CTiff::FlushData() 
{ 
  return ::TIFFFlushData(mTiff); 
}

int CTiff::GetField(ttag_t tag, ...) 
{ 
  va_list l;
  int res;
  va_start( l, tag );     
  res = ::TIFFVGetField(mTiff, tag, l);
  va_end(l);
  return res;
}

int CTiff::VGetField(ttag_t tag, va_list l) 
{ 
  return ::TIFFVGetField(mTiff, tag, l); 
}

int CTiff::GetFieldDefaulted(ttag_t tag, ...) 
{ 
  va_list l;
  int res;
  va_start( l, tag );     
  res = ::TIFFVGetFieldDefaulted(mTiff, tag, l);
  va_end(l);
  return res;
}

int CTiff::VGetFieldDefaulted(ttag_t tag, va_list l) 
{ 
  return ::TIFFVGetFieldDefaulted(mTiff, tag, l); 
}

int CTiff::ReadDirectory() 
{ 
  return ::TIFFReadDirectory(mTiff); 
}

tsize_t CTiff::ScanlineSize() 
{ 
  return ::TIFFScanlineSize(mTiff); 
}

tsize_t CTiff::RasterScanlineSize() 
{ 
  return ::TIFFRasterScanlineSize(mTiff); 
}

tsize_t CTiff::StripSize() 
{ 
  return ::TIFFStripSize(mTiff); 
}

tsize_t CTiff::VStripSize(uint32 v) 
{ 
  return ::TIFFVStripSize(mTiff, v); 
}

tsize_t CTiff::TileRowSize() 
{ 
  return ::TIFFTileRowSize(mTiff); 
}

tsize_t CTiff::TileSize() 
{ 
  return ::TIFFTileSize(mTiff); 
}

tsize_t CTiff::VTileSize(uint32 v) 
{ 
  return ::TIFFVTileSize(mTiff, v); 
}

uint32 CTiff::DefaultStripSize(int v) 
{ 
  return ::TIFFDefaultStripSize(mTiff, v); 
}

void CTiff::DefaultTileSize(uint32 * v1,uint32 *v2) 
{ 
  ::TIFFDefaultTileSize(mTiff, v1, v2); 
}

int CTiff::Fileno() 
{ 
  return ::TIFFFileno(mTiff); 
}

int CTiff::GetMode() 
{ 
  return ::TIFFGetMode(mTiff); 
}

int CTiff::IsTiled() 
{ 
  return ::TIFFIsTiled(mTiff); 
}

int CTiff::IsByteSwapped() 
{ 
  return ::TIFFIsByteSwapped(mTiff); 
}

int CTiff::IsUpSampled() 
{ 
  return ::TIFFIsUpSampled(mTiff); 
}

int CTiff::IsMSB2LSB()
{ 
  return ::TIFFIsMSB2LSB(mTiff); 
}

uint32 CTiff::CurrentRow() 
{ 
  return ::TIFFCurrentRow(mTiff); 
}

tdir_t CTiff::CurrentDirectory() 
{ 
  return ::TIFFCurrentDirectory(mTiff); 
}

uint32 CTiff::CurrentDirOffset() 
{ 
  return ::TIFFCurrentDirOffset(mTiff); 
}

tstrip_t CTiff::CurrentStrip()
{ 
  return ::TIFFCurrentStrip(mTiff); 
}

ttile_t CTiff::CurrentTile() 
{ 
  return ::TIFFCurrentTile(mTiff); 
}

int CTiff::ReadBufferSetup(tdata_t data,tsize_t size) 
{ 
  return ::TIFFReadBufferSetup(mTiff, data, size); 
}

int CTiff::WriteBufferSetup(tdata_t data,tsize_t size) 
{ 
  return ::TIFFWriteBufferSetup(mTiff, data, size); 
}

int CTiff::LastDirectory() 
{ 
  return ::TIFFLastDirectory(mTiff); 
}

int CTiff::SetDirectory(tdir_t dir) 
{ 
  return ::TIFFSetDirectory(mTiff, dir); 
}

int CTiff::SetSubDirectory(uint32 sub) 
{ 
  return ::TIFFSetSubDirectory(mTiff, sub); 
}

int CTiff::UnlinkDirectory(tdir_t dir) 
{ 
  return ::TIFFUnlinkDirectory(mTiff, dir); 
}

int CTiff::SetField(ttag_t tag,...) 
{ 
  va_list l;
  int res;
  va_start( l, tag );     
  res = ::TIFFVSetField(mTiff, tag, l);
  va_end(l);
  return res;
}

int CTiff::VSetField(ttag_t tag, va_list l) 
{ 
  return ::TIFFVSetField(mTiff, tag, l); 
}

int CTiff::WriteDirectory() 
{ 
  return ::TIFFWriteDirectory(mTiff); 
}

int CTiff::ReadScanline(tdata_t data, uint32 v, tsample_t s ) 
{ 
  return ::TIFFReadScanline(mTiff, data, v, s); 
}

int CTiff::WriteScanline(tdata_t data, uint32 v, tsample_t s ) 
{ 
  return ::TIFFWriteScanline(mTiff, data, v, s); 
}

int CTiff::ReadRGBAImage(uint32 v1, uint32 v2, uint32* p, int v3 ) 
{
  return ::TIFFReadRGBAImage(mTiff, v1, v2, p, v3); 
}

int CTiff::RGBAImageOK(char tab[1024]) 
{ 
  return ::TIFFRGBAImageOK(mTiff, tab); 
}

int CTiff::RGBAImageBegin(TIFFRGBAImage* img,int v, char tab [1024]) 
{ 
  return ::TIFFRGBAImageBegin(img,mTiff, v, tab); 
}

int CTiff::RGBAImageGet(TIFFRGBAImage*img,uint32* p, uint32 v1, uint32 v2) 
{
  return ::TIFFRGBAImageGet(img, p, v1, v2); 
}

void CTiff::RGBAImageEnd(TIFFRGBAImage*img) 
{ 
  ::TIFFRGBAImageEnd(img); 
}

void CTiff::Open(const char *file, const char *mode) 
{ 
	mTiff = ::TIFFOpen(file,mode);		
}

void CTiff::FdOpen(int no,const char *file, const char *mode) { 
  mTiff = ::TIFFFdOpen(no,file,mode);  
}

void CTiff::ClientOpen(const char* file, const char* mode,
	    thandle_t h,
	    TIFFReadWriteProc rp, TIFFReadWriteProc wp,
	    TIFFSeekProc sp, TIFFCloseProc cp,
	    TIFFSizeProc szp,
	    TIFFMapFileProc mp, TIFFUnmapFileProc ump) 
{
  mTiff = ::TIFFClientOpen(file,mode,h, rp, wp, sp, cp, szp, mp, ump);  
}

const char* CTiff::FileName() 
{ 
  return ::TIFFFileName(mTiff); 
}

TIFFErrorHandler CTiff::SetErrorHandler(TIFFErrorHandler h) 
{ 
  return ::TIFFSetErrorHandler(h); 
}

TIFFErrorHandler CTiff::SetWarningHandler(TIFFErrorHandler h) 
{ 
  return ::TIFFSetWarningHandler(h); 
}

TIFFExtendProc CTiff::SetTagExtender(TIFFExtendProc ep)
{ 
  return ::TIFFSetTagExtender(ep); 
}

ttile_t CTiff::ComputeTile(uint32 v1, uint32 v2, uint32 v3, tsample_t s) 
{ 
  return ::TIFFComputeTile(mTiff,v1,v2,v3,s);
}

int CTiff::CheckTile(uint32 v1, uint32 v2, uint32 v3, tsample_t s) 
{ 
  return ::TIFFCheckTile(mTiff, v1, v2, v3, s); 
}

ttile_t CTiff::NumberOfTiles() 
{ 
  return ::TIFFNumberOfTiles(mTiff); 
}

tsize_t CTiff::ReadTile(tdata_t data, uint32 v1, uint32 v2, uint32 v3, tsample_t s) 
{
  return ::TIFFReadTile(mTiff, data, v1, v2, v3, s);
}

tsize_t CTiff::WriteTile(tdata_t data, uint32 v1, uint32 v2, uint32 v3, tsample_t s) 
{
  return ::TIFFWriteTile(mTiff,data, v1, v2, v3, s);
}

tstrip_t CTiff::ComputeStrip(uint32 v,tsample_t s) 
{ 
  return ::TIFFComputeStrip(mTiff, v, s); 
}

tstrip_t CTiff::NumberOfStrips() 
{ 
  return ::TIFFNumberOfStrips(mTiff); 
}

tsize_t CTiff::ReadEncodedStrip(tstrip_t strip, tdata_t data, tsize_t size) 
{ 
  return ::TIFFReadEncodedStrip(mTiff, strip, data, size); 
}

tsize_t CTiff::ReadRawStrip(tstrip_t strip, tdata_t data, tsize_t size) 
{ 
  return ::TIFFReadRawStrip(mTiff, strip, data, size); 
}

tsize_t CTiff::ReadEncodedTile(ttile_t tile, tdata_t data, tsize_t size) 
{ 
  return ::TIFFReadEncodedTile(mTiff, tile, data, size); 
}

tsize_t CTiff::ReadRawTile(ttile_t tile, tdata_t data, tsize_t size) 
{ 
  return ::TIFFReadRawTile(mTiff, tile, data, size); 
}

tsize_t CTiff::WriteEncodedStrip(tstrip_t strip, tdata_t data, tsize_t size) 
{ 
  return ::TIFFWriteEncodedStrip(mTiff, strip, data, size); 
}

tsize_t CTiff::WriteRawStrip(tstrip_t strip, tdata_t data, tsize_t size) 
{ 
  return ::TIFFWriteRawStrip(mTiff, strip, data, size); 
}

tsize_t CTiff::WriteEncodedTile(ttile_t tile, tdata_t data, tsize_t size) 
{ 
  return ::TIFFWriteEncodedTile(mTiff, tile, data, size); 
}

tsize_t CTiff::WriteRawTile(ttile_t tile, tdata_t data, tsize_t size) 
{ 
  return ::TIFFWriteRawTile(mTiff, tile, data, size); 
}

void CTiff::SetWriteOffset(toff_t off) 
{ 
  ::TIFFSetWriteOffset(mTiff, off); 
}

void CTiff::SwabShort(uint16 *p) 
{ 
  ::TIFFSwabShort(p); 
}

void CTiff::SwabLong(uint32 *p)
{ 
  ::TIFFSwabLong(p); 
}

void CTiff::SwabDouble(double *p) 
{ 
  ::TIFFSwabDouble(p); 
}

void CTiff::TIFFSwabArrayOfShort(uint16* p, unsigned long n) 
{ 
	::TIFFSwabArrayOfShort(p, n); 
}

void CTiff::TIFFSwabArrayOfLong(uint32* p, unsigned long n) 
{ 
  ::TIFFSwabArrayOfLong(p, n); 
}

//void CTiff::TIFFSwabArrayOfDouble(double* p, unsigned long n) 
//{
//	::TIFFSwabArrayOfDouble(p, n);
//}

void CTiff::ReverseBits(unsigned char *p, unsigned long n) 
{ 
  ::TIFFReverseBits(p, n); 
}

const unsigned char* CTiff::GetBitRevTable(int i) 
{
  return ::TIFFGetBitRevTable(i); 
}

const char* CTiff::GetVersion(void) 
{ 
  return ::TIFFGetVersion(); 
}

const TIFFCodec* CTiff::FindCODEC(uint16 c) 
{ 
  return ::TIFFFindCODEC(c); 
}

TIFFCodec* CTiff::RegisterCODEC(uint16 c, const char* n, TIFFInitMethod m) 
{
  return ::TIFFRegisterCODEC(c,n,m);
}

void CTiff::UnRegisterCODEC(TIFFCodec *c) 
{ 
  ::TIFFUnRegisterCODEC(c); 
}

EImageType CTiff::GeEImageType()
{
	/* determine un type d'image */
	if (GetSampleFormat()==SAMPLEFORMAT_IEEEFP) {
		if (GetSamplesPerPixel()==3)
			return CImageInfo::ColorFloat;
		else
			return CImageInfo::GrayFloat;
	} else {
		uint16 nCompression = GetCompression();
		uint16 nBitsPerSample = GetBitsPerSample();
		if ((COMPRESSION_CCITTFAX3 == nCompression) || (COMPRESSION_CCITTFAX4 == nCompression)) {
			return CImageInfo::Bilevel;
		} else {
			switch (GetPhotometric()) {
			case PHOTOMETRIC_MINISWHITE:
			case PHOTOMETRIC_MINISBLACK:
				switch(GetBitsPerSample()) {
				case 1:
					return CImageInfo::Bilevel;        
				case 4:
					return CImageInfo::Gray16;                
				case 8:
					return CImageInfo::Gray256;                        
				default:
					throw CTiffException("Image Type","Can't handle this kind of image");
				}
				break;
			case PHOTOMETRIC_PALETTE:
				switch(GetBitsPerSample()) {
				case 4:
					return CImageInfo::Color16;                
					break;
				case 8:
					return CImageInfo::Color256;                
					break;
				default:
					throw CTiffException("Image Type","Can't handle this kind of image");
				}	
				break;
			case PHOTOMETRIC_RGB:
				if (16 == nBitsPerSample) {
					throw CTiffException("Image Type","Can't handle this kind of image");
				}
				return CImageInfo::ColorRGB;                
				break;
			case PHOTOMETRIC_SEPARATED:
				return CImageInfo::ColorRGB;                
				break;
			case PHOTOMETRIC_YCBCR:
				{
				if (GetCompression() == COMPRESSION_JPEG &&
						GetPlanarConfig() == PLANARCONFIG_CONTIG) {
					/* can rely on libjpeg to convert to RGB */
					/* XXX should restore current state on exit */
					SetField(TIFFTAG_JPEGCOLORMODE, JPEGCOLORMODE_RGB);
				}
				return CImageInfo::ColorRGB;                
				}      
			case PHOTOMETRIC_UNDEFINED:
				throw CTiffException("PhotometricInterpretation tag is undefined in this TIFF", "Can't handle this kind of image");
			default:
				throw CTiffException("Image Type","Can't handle this kind of image");
			}
		}
	}
	return CImageInfo::Bilevel;
}

CColorPalette *CTiff::GetColorPalette()
{
  uint16 *red,*green,*blue;
  int size=1<<GetBitsPerSample();

  GetColorMap(&red,&green,&blue);
  CColorPalette *palette=new CColorPalette(size);
  for(int i=0;i<size;i++)
    palette->GetColor(i).SetRGB(*red++>>8,*green++>>8,*blue++>>8);
  return palette;
}

void CTiff::SetColorPalette(const CColorPalette& palette)
{
  #define PREPARE_LEVEL(r) (r)*256+(r)
  uint16 *red,*green,*blue;
  uint16 *bufred,*bufgreen,*bufblue;
  int nbcolor=1<<GetBitsPerSample();
  
  red=new uint16[nbcolor];
  green=new uint16[nbcolor];
  blue=new uint16[nbcolor];
  bufred=red;bufgreen=green;bufblue=blue;
  for (int i=0;i< nbcolor; i++) {
    *bufred++	=(uint16) PREPARE_LEVEL(palette[i].GetRed());
    *bufgreen++ =(uint16) PREPARE_LEVEL(palette[i].GetGreen());
    *bufblue++  =(uint16) PREPARE_LEVEL(palette[i].GetBlue());
  }
  SetColorMap(red,green,blue);
  delete [] red;
  delete [] green;
  delete [] blue;
}


CImageInfo *CTiff::ReadImageInfo()
{
  
  uint16 resunit;
  float xres,yres;
  uint16 photometric=GetPhotometric();  

  if (GetField(TIFFTAG_XRESOLUTION, &xres)==0)
    xres=(float) CImageInfo::DefaultXResolution;

  if (GetField(TIFFTAG_YRESOLUTION, &yres)==0)
    yres=(float) CImageInfo::DefaultYResolution;

  if (GetField(TIFFTAG_RESOLUTIONUNIT, &resunit)) {
    /* Proportion ? */
    if (resunit==1) {       
      float cx=(xres/(xres+yres));
      float cy=(yres/(xres+yres));
      xres=CImageInfo::DefaultXResolution*cx;
      yres=CImageInfo::DefaultXResolution*cy;      
    } else
    /* Centimeters ? */
    if (resunit==3) {
      /* convert to inches */
      xres*=(float) 2.54;
      yres*=(float) 2.54;
    }
  } 

  EImageType type=GeEImageType();

  CColorPalette *palette;

  if (type==CImageInfo::Color16 || type==CImageInfo::Color256)
    palette=GetColorPalette();
  else
    palette=NULL;
  
  return new CImageInfo(GetImageWidth(),GetImageLength(),
    type,(int) xres,(int) yres,palette);
}


CImage *CTiff::ReadImage()
{
	CImage *img = 0;
	CImageInfo *imgInfo = 0;
	try {
		uint16 photometric=GetPhotometric();  

		imgInfo=ReadImageInfo();

		img=new CImage(*imgInfo);

		delete imgInfo;

		if (GetSamplesPerPixel()>3 || GetPlanarConfig()!=PLANARCONFIG_CONTIG ||
				!(photometric==PHOTOMETRIC_MINISBLACK ||
				photometric==PHOTOMETRIC_MINISWHITE ||
				photometric==PHOTOMETRIC_PALETTE ||
				photometric==PHOTOMETRIC_RGB ||
				photometric==PHOTOMETRIC_UNDEFINED))
			ReadUniversal(img);
		else    
			ReadContig(img);

		EImageType type=GeEImageType();

		/* Inverse les points si ne'ce'ssaire */
		if ((photometric==PHOTOMETRIC_MINISBLACK && type==CImageInfo::Bilevel) ||
		(photometric==PHOTOMETRIC_MINISWHITE && type!=CImageInfo::Bilevel) )      
		img->Invert();
	} catch (...) {
		if (0 != img) {
			delete img;
		}
		throw;
	}
	return img;
}

void CTiff::WriteImage(const CImage *img)
{
  SetImageWidth(img->GetWidth());
  SetImageLength(img->GetHeight());
  SetPlanarConfig(PLANARCONFIG_CONTIG);
  SetBitsPerSample(img->GetBitsPerSample());
  SetSamplesPerPixel(img->GetSamplesPerPixel());
  
  // Do not compress small images 
  if (img->GetBitmapSize()<=1024)
    SetCompression(COMPRESSION_NONE);
  else
  if (img->GetBitCount()==1)
    SetCompression(COMPRESSION_CCITTFAX4);
  else
    SetCompression(COMPRESSION_LZW);

  SetXResolution((float) img->GetXResolution());
  SetYResolution((float) img->GetYResolution());
  SetResolutionUnit(RESUNIT_INCH);

  switch(img->GeEImageType()) {
  case CImageInfo::Bilevel:
    SetPhotometric(PHOTOMETRIC_MINISWHITE);
    break;
  case CImageInfo::Gray16:
  case CImageInfo::Gray256:  
    SetPhotometric(PHOTOMETRIC_MINISBLACK);
    break;
  case CImageInfo::Color16:
  case CImageInfo::Color256:
    SetPhotometric(PHOTOMETRIC_PALETTE);
    SetColorPalette(*(img->GetColorPalette()));
    break;
  case CImageInfo::ColorRGB:  
    SetPhotometric(PHOTOMETRIC_RGB);
    break;
  case CImageInfo::GrayFloat:  
    SetSampleFormat(SAMPLEFORMAT_IEEEFP);
    SetPhotometric(PHOTOMETRIC_MINISWHITE);    
    break;
  case CImageInfo::ColorFloat:
    SetSampleFormat(SAMPLEFORMAT_IEEEFP);
    SetPhotometric(PHOTOMETRIC_RGB);    
    break;
  default:
     throw CTiffException("CTiff::WriteImage","Can't handle this kind of image");
  }
  
  SetField(TIFFTAG_ROWSPERSTRIP, img->GetHeight());
    
  WriteContig(img);
  WriteDirectory();
}


void CTiff::ReadContig(CImage *img)
{   
    TBitmap  bits;
    int row=0;
    int imagelength=GetImageLength();

    switch (GetOrientation()) {
	case ORIENTATION_BOTLEFT:
	    for (; row < imagelength; row++ ) {
	    	bits=img->GetRow(imagelength-row-1);
	    	ReadScanline( bits, row, 0);
	    	//ShowProgress((100*(row+1))/imagelength);
	    }
	    break;

    	case ORIENTATION_TOPLEFT:
	    for (; row < imagelength; row++ ) {
	    	bits=img->GetRow(row);
		ReadScanline(bits, row, 0);
		//ShowProgress((100*(row+1))/imagelength);
	    }
	    break;
	default:	   
	    for (; row < imagelength; row++) {
	    	bits=img->GetRow(row);
		ReadScanline(bits, row, 0);
		//ShowProgress((100*(row+1))/imagelength);
	    }
	    break;
    }

#ifdef _WIN32_IMPLEMENTATION
    if (img->GetBitCount()==24)
    	img->RGBToBGR();
#endif    

}	

void CTiff::ReadUniversal(CImage *image)
{
  char msg[1024];

  if (!RGBAImageOK(msg)) {
    throw CTiffException("CTiff::ReadUniversal",msg);
  }
  
  CImage *img2,*img3;
  
  TBitmap  buf;
  uint32 *p,slot;
  int w,h,i,j;
  TBitmap  bits;
  EImageType ImageType;
  
  w=image->GetWidth();
  h=image->GetHeight();
  ImageType=image->GeEImageType();
  
  buf=new TByte[4*w*h];
  
  try {
	if (ReadRGBAImage(w,h,(uint32 *) buf,FALSE)!=1) {
		throw CTiffException("CTiff::ReadUniversal","Error while reading image");
	}
	      
	if (ImageType!=CImageInfo::ColorRGB) {
		img2=new CImage(w,h,CImageInfo::ColorRGB);    
	} else img2=image;
			
	p=(uint32 *) buf;
	for(i=0;i<h;i++) {
		bits=img2->GetRow(h-i-1);
		for(j=0;j<w;j++) {
		slot=*p++;
		*(bits+CImage::ROffset)=(TSample) TIFFGetR(slot);
		*(bits+CImage::GOffset)=(TSample) TIFFGetG(slot);
		*(bits+CImage::BOffset)=(TSample) TIFFGetB(slot);
		bits+=3;			
		}
	}
	delete [] buf;
	if (ImageType!=CImageInfo::ColorRGB) {
		img3=img2->Convert(ImageType);    
		memcpy(image->GetBitmap(),img3->GetBitmap(),image->GetBitmapSize());
		delete img2;
		delete img3;  
	} 
  } catch (...) {
	delete[] buf;
	throw;
  }	    
}

void CTiff::WriteContig(const CImage *img)
{
	TCBitmap  buf;
	int row;
	TBitmap  buf2=NULL;
	uint32 scanlinesize;
        int w=img->GetWidth();
	int h=img->GetHeight();

#ifdef _WIN32_IMPLEMENTATION
	TBitmap  p;
	TCBitmap  p2;
	int i;
	if (img->GetBitCount()==24 ||  GetPredictor() ) {
#else
	if( GetPredictor() ) {
#endif
	  scanlinesize=ScanlineSize();
	  buf2=new TByte[scanlinesize];
	}
	
	for(row=0;row<h;row++) {
		buf=img->GetRow(row);
#ifdef _WIN32_IMPLEMENTATION
		if (img->GetBitCount()==24) {
			/* remet dans l'ordre RGB */
			p=buf2; p2=buf;
			for(i=0;i<w;i++) {
			  *p++=*(p2+CImage::ROffset);
			  *p++=*(p2+CImage::GOffset);
			  *p++=*(p2+CImage::BOffset);				
			  p2+=3;
			}
			WriteScanline(buf2, row, 0);
		} else
#endif
		if ( GetPredictor() ) {
			memcpy(buf2,buf,scanlinesize);
			WriteScanline(buf2, row, 0);
		} else
			WriteScanline((void *) buf, row, 0);
		//TLL_ShowProgress((100*(row+1))/h);
	}

	if (buf2)
		delete [] buf2;
}

void CTiff::StandardErrorProcessing(const char *module,const char *fmt,va_list ap)
{
  char message[1024];
  vsprintf(message,fmt,ap);
  throw CTiffException(module,message);
}

void CTiff::StandardWarningProcessing(const char *module,const char *fmt,va_list ap)
{
}

int     CTiff::MapProc(thandle_t a, tdata_t* b, toff_t* c)
{
  a=(thandle_t)0;
  b=NULL;
  c=NULL;
  return (0);
}

void    CTiff::UnmapProc(thandle_t a, tdata_t b, toff_t c)
{
  a=(thandle_t)0;
  b=(tdata_t)0;
  c=(toff_t)0;
}

tsize_t CTiff::ReadProc(thandle_t handle, tdata_t data, tsize_t size)
{
  CStream *stream=(CStream *) handle;
  return stream->Read(data,size);
}

tsize_t CTiff::WriteProc(thandle_t handle, tdata_t data, tsize_t size)
{
  CStream *stream=(CStream *) handle;
  return stream->Write(data,size);
}

toff_t  CTiff::SeekProc(thandle_t handle, toff_t offset , int whence)
{
  CStream *stream=(CStream *) handle;
  switch(whence) {
  case SEEK_SET:
    return stream->Seek(offset,CStream::Begin);
  case SEEK_CUR:
     return stream->Seek(offset,CStream::Current);
  case SEEK_END:
    return stream->Seek(offset,CStream::End);
  }
  return -1;
}

int     CTiff::CloseProc(thandle_t handle)
{
  CStream *stream=(CStream *) handle;
  stream->Close();
  return 0;
}


toff_t  CTiff::SizeProc(thandle_t handle)
{
  CStream *stream=(CStream *) handle;
  return stream->Size();
}


void CTiff::AttachInputStream(CStream &stream)
{  
  ClientOpen("stream","r",(thandle_t) (&stream),
    ReadProc,WriteProc,SeekProc,CloseProc,SizeProc,MapProc,UnmapProc);
}

void CTiff::AttachOutputStream(CStream &stream)
{
  ClientOpen("stream","w",(thandle_t) (&stream),
    ReadProc,WriteProc,SeekProc,CloseProc,SizeProc,MapProc,UnmapProc);
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
