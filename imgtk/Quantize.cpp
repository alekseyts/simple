//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#include "Image.h"

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


typedef unsigned char u_char;
typedef unsigned short u_short;
typedef unsigned long u_long;

#define MAX_CMAP_SIZE   256
#define howmany(x, y)   (((x)+((y)-1))/(y))

#define COLOR_DEPTH     8
#define MAX_COLOR       256

#define B_DEPTH         5               /* # bits/pixel to use */
#define B_LEN           (1<<B_DEPTH)

#define C_DEPTH         2
#define C_LEN           (1<<C_DEPTH)    /* # cells/color to use */

#define COLOR_SHIFT     (COLOR_DEPTH-B_DEPTH)


class CColorBox;
class CCell;
class CQuantizer;

class CColorBox {
  friend class CQuantizer;
  private:
        CColorBox *next, *prev;
        int     rmin, rmax;
        int     gmin, gmax;
        int     bmin, bmax;
        int     total;
};

class CCell {
  friend class CQuantizer;
  private:
        int     num_ents;
        int     entries[MAX_CMAP_SIZE][2];
};

class CQuantizer {
  public:
    CQuantizer(int bitcount);
    ~CQuantizer();
    void SubmitImage(const CImage *img);
    CColorPalette *GetColorPalette();
    CImage *Quantize(const CImage *img,bool dither);
  private:
    bool colormapped;
    const CImage *image;
    u_short rm[MAX_CMAP_SIZE], gm[MAX_CMAP_SIZE], bm[MAX_CMAP_SIZE];
    int     num_colors;
    int     histogram[B_LEN][B_LEN][B_LEN];
    CColorBox *freeboxes;
    CColorBox *usedboxes;
    CCell  **ColorCells;
    u_long  image_width;
    u_long  image_length;
    int bitcount;
    int rmin,gmin,bmin,rmax,gmax,bmax;
    CColorBox *box_list, *ptr;
    
    CColorBox *largest_box();
    void init_histogram();
    void get_histogram(CColorBox *box);
    CCell *create_colorcell(int red, int green, int blue);
    void splitbox(CColorBox *);
    void shrinkbox(CColorBox *);
    void map_colortable();
    int quant(CImage *);
    int quant_fsdither(CImage * );
};


/* ---------------------------------------------------------------- */


void CQuantizer::init_histogram()
{
	rmin=gmin=bmin=999;
	rmax=gmax=bmax=-1;
        { int *ptr = &histogram[0][0][0];
	  int i;
          for (i = B_LEN*B_LEN*B_LEN; i-- > 0;)
                *ptr++ = 0;
        }
}

void CQuantizer::get_histogram(CColorBox *box)
{
  int red, green, blue;
  u_long j, i;
  TCBitmap  bits;
  
  for (i = 0; i < image_length; i++) {
    bits=image->GetRow(i);
    for (j = image_width; j-- > 0;) {
      red =   *(bits+CImage::ROffset) >> COLOR_SHIFT;
      green = *(bits+CImage::GOffset) >> COLOR_SHIFT;
      blue =  *(bits+CImage::BOffset) >> COLOR_SHIFT;
      bits+=3;
      if (red < rmin)
	rmin = red;
      if (red > rmax)
	rmax = red;
      if (green < gmin)
	gmin = green;
      if (green > gmax)
	gmax = green;
      if (blue < bmin)
	bmin = blue;
      if (blue > bmax)
	bmax = blue;
      histogram[red][green][blue]++;
    }
  }
  box->rmin=rmin;
  box->gmin=gmin;
  box->bmin=bmin;
  box->rmax=rmax;
  box->gmax=gmax;
  box->bmax=bmax;
  box->total += image_width * image_length;
}

CColorBox *
CQuantizer::largest_box()
{
  CColorBox *p, *b;
  int size;
  
  b = NULL;
  size = -1;
  for (p = usedboxes; p != NULL; p = p->next)
    if ((p->rmax > p->rmin || p->gmax > p->gmin ||
      p->bmax > p->bmin) &&  p->total > size)
      size = (b = p)->total;
    return (b);
}

void CQuantizer::shrinkbox(CColorBox *box)
{
  int *histp, ir, ig, ib;
  
  if (box->rmax > box->rmin) {
    for (ir = box->rmin; ir <= box->rmax; ++ir)
      for (ig = box->gmin; ig <= box->gmax; ++ig) {
	histp = &histogram[ir][ig][box->bmin];
	for (ib = box->bmin; ib <= box->bmax; ++ib)
	  if (*histp++ != 0) {
	    box->rmin = ir;
	    goto have_rmin;
	  }
      }
have_rmin:
      if (box->rmax > box->rmin)
	for (ir = box->rmax; ir >= box->rmin; --ir)
	  for (ig = box->gmin; ig <= box->gmax; ++ig) {
	    histp = &histogram[ir][ig][box->bmin];
	    ib = box->bmin;
	    for (; ib <= box->bmax; ++ib)
	      if (*histp++ != 0) {
		box->rmax = ir;
		goto have_rmax;
	      }
	  }
  }
have_rmax:
  if (box->gmax > box->gmin) {
    for (ig = box->gmin; ig <= box->gmax; ++ig)
      for (ir = box->rmin; ir <= box->rmax; ++ir) {
	histp = &histogram[ir][ig][box->bmin];
	for (ib = box->bmin; ib <= box->bmax; ++ib)
	  if (*histp++ != 0) {
	    box->gmin = ig;
	    goto have_gmin;
	  }
      }
have_gmin:
      if (box->gmax > box->gmin)
	for (ig = box->gmax; ig >= box->gmin; --ig)
	  for (ir = box->rmin; ir <= box->rmax; ++ir) {
	    histp = &histogram[ir][ig][box->bmin];
	    ib = box->bmin;
	    for (; ib <= box->bmax; ++ib)
	      if (*histp++ != 0) {
		box->gmax = ig;
		goto have_gmax;
	      }
	  }
  }
have_gmax:
  if (box->bmax > box->bmin) {
    for (ib = box->bmin; ib <= box->bmax; ++ib)
      for (ir = box->rmin; ir <= box->rmax; ++ir) {
	histp = &histogram[ir][box->gmin][ib];
	for (ig = box->gmin; ig <= box->gmax; ++ig) {
	  if (*histp != 0) {
	    box->bmin = ib;
	    goto have_bmin;
	  }
	  histp += B_LEN;
	}
      }
have_bmin:
      if (box->bmax > box->bmin)
	for (ib = box->bmax; ib >= box->bmin; --ib)
	  for (ir = box->rmin; ir <= box->rmax; ++ir) {
	    histp = &histogram[ir][box->gmin][ib];
	    ig = box->gmin;
	    for (; ig <= box->gmax; ++ig) {
	      if (*histp != 0) {
		box->bmax = ib;
		goto have_bmax;
	      }
	      histp += B_LEN;
	    }
	  }
  }
have_bmax:
  ;
}


void CQuantizer::splitbox(CColorBox *ptr)
{
  int             hist2[B_LEN];
  int             first, last;
  CColorBox       *newbox;
  int    *iptr, *histp;
  int    i, j;
  int    ir,ig,ib;
  int sum, sum1, sum2;
  enum { RED, GREEN, BLUE } axis;
  
  /*
  * See which axis is the largest, do a histogram along that
  * axis.  Split at median point.  Contract both new boxes to
  * fit points and return
  */
  i = ptr->rmax - ptr->rmin;
  if (i >= ptr->gmax - ptr->gmin  && i >= ptr->bmax - ptr->bmin)
    axis = RED;
  else if (ptr->gmax - ptr->gmin >= ptr->bmax - ptr->bmin)
    axis = GREEN;
  else
    axis = BLUE;
  /* get histogram along longest axis */
  switch (axis) {
  case RED:
    histp = &hist2[ptr->rmin];
    for (ir = ptr->rmin; ir <= ptr->rmax; ++ir) {
      *histp = 0;
      for (ig = ptr->gmin; ig <= ptr->gmax; ++ig) {
	iptr = &histogram[ir][ig][ptr->bmin];
	for (ib = ptr->bmin; ib <= ptr->bmax; ++ib)
	  *histp += *iptr++;
      }
      histp++;
    }
    first = ptr->rmin;
    last = ptr->rmax;
    break;
  case GREEN:
    histp = &hist2[ptr->gmin];
    for (ig = ptr->gmin; ig <= ptr->gmax; ++ig) {
      *histp = 0;
      for (ir = ptr->rmin; ir <= ptr->rmax; ++ir) {
	iptr = &histogram[ir][ig][ptr->bmin];
	for (ib = ptr->bmin; ib <= ptr->bmax; ++ib)
	  *histp += *iptr++;
      }
      histp++;
    }
    first = ptr->gmin;
    last = ptr->gmax;
    break;
  case BLUE:
    histp = &hist2[ptr->bmin];
    for (ib = ptr->bmin; ib <= ptr->bmax; ++ib) {
      *histp = 0;
      for (ir = ptr->rmin; ir <= ptr->rmax; ++ir) {
	iptr = &histogram[ir][ptr->gmin][ib];
	for (ig = ptr->gmin; ig <= ptr->gmax; ++ig) {
	  *histp += *iptr;
	  iptr += B_LEN;
	}
      }
      histp++;
    }
    first = ptr->bmin;
    last = ptr->bmax;
    break;
  }
  /* find median point */
  sum2 = ptr->total / 2;
  histp = &hist2[first];
  sum = 0;
  for (i = first; i <= last && (sum += *histp++) < sum2; ++i)
    ;
  if (i == first)
    i++;
  
  /* Create new box, re-allocate points */
  newbox = freeboxes;
  freeboxes = newbox->next;
  if (freeboxes)
    freeboxes->prev = NULL;
  if (usedboxes)
    usedboxes->prev = newbox;
  newbox->next = usedboxes;
  usedboxes = newbox;
  
  histp = &hist2[first];
  for (sum1 = 0, j = first; j < i; j++)
    sum1 += *histp++;
  for (sum2 = 0, j = i; j <= last; j++)
    sum2 += *histp++;
  newbox->total = sum1;
  ptr->total = sum2;
  
  newbox->rmin = ptr->rmin;
  newbox->rmax = ptr->rmax;
  newbox->gmin = ptr->gmin;
  newbox->gmax = ptr->gmax;
  newbox->bmin = ptr->bmin;
  newbox->bmax = ptr->bmax;
  switch (axis) {
  case RED:
    newbox->rmax = i-1;
    ptr->rmin = i;
    break;
  case GREEN:
    newbox->gmax = i-1;
    ptr->gmin = i;
    break;
  case BLUE:
    newbox->bmax = i-1;
    ptr->bmin = i;
    break;
  }
  shrinkbox(newbox);
  shrinkbox(ptr);
}


CCell *
CQuantizer::create_colorcell(int red, int green, int blue)
{
  int ir, ig, ib, i;
  CCell *ptr;
  int mindist, next_n;
  int tmp, dist, n;
  
  ir = red >> (COLOR_DEPTH-C_DEPTH);
  ig = green >> (COLOR_DEPTH-C_DEPTH);
  ib = blue >> (COLOR_DEPTH-C_DEPTH);
  ptr = new CCell;
  
  *(ColorCells + ir*C_LEN*C_LEN + ig*C_LEN + ib) = ptr;
  ptr->num_ents = 0;
  
  /*
  * Step 1: find all couleurs inside this cell, while we're at
  *         it, find distance of centermost point to furthest corner
  */
  mindist = 99999999;
  for (i = 0; i < num_colors; ++i) {
    if ((rm[i]>>(COLOR_DEPTH-C_DEPTH)) != ir  ||
      (gm[i]>>(COLOR_DEPTH-C_DEPTH)) != ig  ||
      (bm[i]>>(COLOR_DEPTH-C_DEPTH)) != ib)
      continue;
    ptr->entries[ptr->num_ents][0] = i;
    ptr->entries[ptr->num_ents][1] = 0;
    ++ptr->num_ents;
    tmp = rm[i] - red;
    if (tmp < (MAX_COLOR/C_LEN/2))
      tmp = MAX_COLOR/C_LEN-1 - tmp;
    dist = tmp*tmp;
    tmp = gm[i] - green;
    if (tmp < (MAX_COLOR/C_LEN/2))
      tmp = MAX_COLOR/C_LEN-1 - tmp;
    dist += tmp*tmp;
    tmp = bm[i] - blue;
    if (tmp < (MAX_COLOR/C_LEN/2))
      tmp = MAX_COLOR/C_LEN-1 - tmp;
    dist += tmp*tmp;
    if (dist < mindist)
      mindist = dist;
  }
  
  /*
  * Step 3: find all points within that distance to cell.
  */
  for (i = 0; i < num_colors; ++i) {
    if ((rm[i] >> (COLOR_DEPTH-C_DEPTH)) == ir  &&
      (gm[i] >> (COLOR_DEPTH-C_DEPTH)) == ig  &&
      (bm[i] >> (COLOR_DEPTH-C_DEPTH)) == ib)
      continue;
    dist = 0;
    if ((tmp = red - rm[i]) > 0 ||
      (tmp = rm[i] - (red + MAX_COLOR/C_LEN-1)) > 0 )
      dist += tmp*tmp;
    if ((tmp = green - gm[i]) > 0 ||
      (tmp = gm[i] - (green + MAX_COLOR/C_LEN-1)) > 0 )
      dist += tmp*tmp;
    if ((tmp = blue - bm[i]) > 0 ||
      (tmp = bm[i] - (blue + MAX_COLOR/C_LEN-1)) > 0 )
      dist += tmp*tmp;
    if (dist < mindist) {
      ptr->entries[ptr->num_ents][0] = i;
      ptr->entries[ptr->num_ents][1] = dist;
      ++ptr->num_ents;
    }
  }
  
  /*
  * Sort color cells by distance, use cheap exchange sort
  */
  for (n = ptr->num_ents - 1; n > 0; n = next_n) {
    next_n = 0;
    for (i = 0; i < n; ++i)
      if (ptr->entries[i][1] > ptr->entries[i+1][1]) {
	tmp = ptr->entries[i][0];
	ptr->entries[i][0] = ptr->entries[i+1][0];
	ptr->entries[i+1][0] = tmp;
	tmp = ptr->entries[i][1];
	ptr->entries[i][1] = ptr->entries[i+1][1];
	ptr->entries[i+1][1] = tmp;
	next_n = i;
      }
  }
  return (ptr);
}


void CQuantizer::map_colortable()
{
  int *histp = &histogram[0][0][0];
  CCell *cell;
  int j, tmp, d2, dist;
  int ir, ig, ib, i;
  
  for (ir = 0; ir < B_LEN; ++ir)
    for (ig = 0; ig < B_LEN; ++ig)
      for (ib = 0; ib < B_LEN; ++ib, histp++) {
	if (*histp == 0) {
	  *histp = -1;
	  continue;
	}
	cell = *(ColorCells +
	  (((ir>>(B_DEPTH-C_DEPTH)) << C_DEPTH*2) +
	  ((ig>>(B_DEPTH-C_DEPTH)) << C_DEPTH) +
	  (ib>>(B_DEPTH-C_DEPTH))));
	if (cell == NULL ) 
	  cell = create_colorcell(
	    ir << COLOR_SHIFT,
	    ig << COLOR_SHIFT,
	    ib << COLOR_SHIFT);
	
	  dist = 9999999;
	  for (i = 0; i < cell->num_ents && dist > cell->entries[i][1]; ++i) {
	    j = cell->entries[i][0];
	    d2 = rm[j] - (ir << COLOR_SHIFT);
	    d2 *= d2;
	    tmp = gm[j] - (ig << COLOR_SHIFT);
	    d2 += tmp*tmp;
	    tmp = bm[j] - (ib << COLOR_SHIFT);
	    d2 += tmp*tmp;
	    if (d2 < dist) {
	      dist = d2;
	      *histp = j;
	    }
	  }
      }
}

/*
 * straight quantization.  Each pixel is mapped to the colors
 * closest to it.  Color values are rounded to the la couleur la plus proche
 * table entry.
 */

int CQuantizer::quant(CImage * img2)
{
  TCBitmap bits1;
  TBitmap  bits2;
  unsigned char pix;
  bool even;
  u_long i, j;
  int red, green, blue;
  
  if (num_colors==256) {
    for (i = 0; i < image_length; i++) {
      bits1=image->GetRow(i);
      bits2=img2->GetRow(i);
      for (j = 0; j < image_width; j++) {
	red = *(bits1+CImage::ROffset) >> COLOR_SHIFT;
	green = *(bits1+CImage::GOffset) >> COLOR_SHIFT;
	blue = *(bits1+CImage::BOffset) >> COLOR_SHIFT;
	bits1+=3;
	*bits2++ = histogram[red][green][blue];
      }
    }
  } else {
    for (i = 0; i < image_length; i++) {
      bits1=image->GetRow(i);
      bits2=img2->GetRow(i);
      even=true;
      for (j = 0; j < image_width; j++) {
	red = *(bits1+CImage::ROffset) >> COLOR_SHIFT;
	green = *(bits1+CImage::GOffset) >> COLOR_SHIFT;
	blue = *(bits1+CImage::BOffset) >> COLOR_SHIFT;
	bits1+=3;
	if (even) 
	  pix= histogram[red][green][blue] << 4;
	else {
	  pix+= histogram[red][green][blue];
	  *bits2++=pix;
	}
	even=!even;
      }
      if (!even) *bits2=pix;
    }
  }
  return 0;
}

#define SWAP(type,a,b)  { type p; p = a; a = b; b = p; }

#define GetInputLine(img, row) inptr = (img)->GetRow(row); nextptr = nextline; \
        for (j = 0; j < image_width; ++j) {                      \
                *nextptr++ = *(inptr+CImage::ROffset);                            \
                *nextptr++ = *(inptr+CImage::GOffset);                            \
                *nextptr++ = *(inptr+CImage::BOffset);                            \
                inptr+=3; \
        }

        
#define GetComponent(raw, cshift, c)                            \
        cshift = raw;                                           \
        if (cshift < 0)                                         \
                cshift = 0;                                     \
        else if (cshift >= MAX_COLOR)                           \
                cshift = MAX_COLOR-1;                           \
        c = cshift;                                             \
        cshift >>= COLOR_SHIFT;


int CQuantizer::quant_fsdither(CImage * img2)
{
  short *thisline, *nextline;
  TCBitmap inptr;
  short *thisptr, *nextptr;
  u_long i, j;
  u_long imax, jmax;
  int lastline, lastpixel;
  TBitmap  bits2;
  bool even;
  
  imax = image_length - 1;
  jmax = image_width - 1;
  
  thisline = new short[image_width * 3];
  nextline = new short[image_width * 3];
  
  GetInputLine(image,0);
  for (i = 1; i < image_length; ++i) {
    even=true;
    SWAP(short *, thisline, nextline);
    lastline = (i == imax);
    GetInputLine(image, i);
    thisptr = thisline;
    nextptr = nextline;
    bits2=img2->GetRow(i-1);
    for (j = 0; j < image_width; ++j) {
      int red, green, blue;
      int oval, r2, g2, b2;
      
      lastpixel = (j == jmax);
      GetComponent(*thisptr++, r2, red);
      GetComponent(*thisptr++, g2, green);
      GetComponent(*thisptr++, b2, blue);
      oval = histogram[r2][g2][b2];
      if (oval == -1) {
	int ci;
	int cj, tmp, d2, dist;
	CCell *cell;
	
	cell = *(ColorCells +
	  (((r2>>(B_DEPTH-C_DEPTH)) << C_DEPTH*2) +
	  ((g2>>(B_DEPTH-C_DEPTH)) << C_DEPTH ) +
	  (b2>>(B_DEPTH-C_DEPTH))));
	if (cell == NULL)
	  cell = create_colorcell(red,
	  green, blue);
	dist = 9999999;
	for (ci = 0; ci < cell->num_ents && dist > cell->entries[ci][1]; ++ci) {
	  cj = cell->entries[ci][0];
	  d2 = (rm[cj] >> COLOR_SHIFT) - r2;
	  d2 *= d2;
	  tmp = (gm[cj] >> COLOR_SHIFT) - g2;
	  d2 += tmp*tmp;
	  tmp = (bm[cj] >> COLOR_SHIFT) - b2;
	  d2 += tmp*tmp;
	  if (d2 < dist) {
	    dist = d2;
	    oval = cj;
	  }
	}
	histogram[r2][g2][b2] = oval;
      }
      if (num_colors<256) {
	if (even) *bits2 = oval<<4;
	else {
	  *bits2 = *bits2+oval;
	  bits2++;
	}
	even=!even;
      } else
	*bits2++ = oval;
      red -= rm[oval];
      green -= gm[oval];
      blue -= bm[oval];
      if (!lastpixel) {
	thisptr[0] += blue * 7 / 16;
	thisptr[1] += green * 7 / 16;
	thisptr[2] += red * 7 / 16;
      }
      if (!lastline) {
	if (j != 0) {
	  nextptr[-3] += blue * 3 / 16;
	  nextptr[-2] += green * 3 / 16;
	  nextptr[-1] += red * 3 / 16;
	}
	nextptr[0] += blue * 5 / 16;
	nextptr[1] += green * 5 / 16;
	nextptr[2] += red * 5 / 16;
	if (!lastpixel) {
	  nextptr[3] += blue / 16;
	  nextptr[4] += green / 16;
	  nextptr[5] += red / 16;
	}
	nextptr += 3;
      }
    }
  }
  /* Duplicate last line */
  memcpy(img2->GetRow(image_length-1),img2->GetRow(image_length-2),
    img2->GetRowSize());
  
  delete [] thisline;
  delete [] nextline;
  return 0;
}

CQuantizer::CQuantizer(int bc): colormapped(false), image(NULL), num_colors(0), ColorCells(NULL),
		image_width(0), image_length(0)
{
  int i;

  for (i = 0; i < MAX_CMAP_SIZE; i++) {
	rm[i] = 0;
	gm[i] = 0;
	bm[i] = 0;
  }

  bitcount=bc;
  num_colors = 1<<bitcount;
  colormapped=false;
  /*
   * STEP 1:  create empty boxes
   */
  usedboxes = NULL;
  box_list = freeboxes = new CColorBox[num_colors];
  freeboxes[0].next = &freeboxes[1];
  freeboxes[0].prev = NULL;
  for (i = 1; i < num_colors-1; ++i) {
    freeboxes[i].next = &freeboxes[i+1];
    freeboxes[i].prev = &freeboxes[i-1];
  }
  freeboxes[num_colors-1].next = NULL;
  freeboxes[num_colors-1].prev = &freeboxes[num_colors-2];
  /*
   * STEP 2a: initialize first box
   */
  ptr = freeboxes;
  freeboxes = ptr->next;
  if (freeboxes)
    freeboxes->prev = NULL;
  ptr->next = usedboxes;
  usedboxes = ptr;
  if (ptr->next)
    ptr->next->prev = ptr;
  ptr->total=0;
  init_histogram();
}

void CQuantizer::SubmitImage(const CImage *img)
{
  image=img;
  image_width=image->GetWidth();
  image_length=image->GetHeight();
  
  /*
   * STEP 2b: initialize first box
   */
  get_histogram(ptr);
}


CColorPalette *CQuantizer::GetColorPalette()
{
   
  CColorPalette *palette;
  int i;

  if (!colormapped) {  
    /*
     * STEP 3: continually subdivide boxes until no more free
     * boxes remain or until all couleurs assigned.
     */
    while (freeboxes != NULL) {
      ptr = largest_box();
      if (ptr != NULL)
	splitbox(ptr);
      else
	freeboxes = NULL;
    }
  
    /*
     * STEP 4: assign couleurs to all boxes
     */
     for (i = 0, ptr = usedboxes; ptr != NULL; ++i, ptr = ptr->next) {
       rm[i] = ((ptr->rmin + ptr->rmax) << COLOR_SHIFT) / 2;
       gm[i] = ((ptr->gmin + ptr->gmax) << COLOR_SHIFT) / 2;
       bm[i] = ((ptr->bmin + ptr->bmax) << COLOR_SHIFT) / 2;
     }
     /* put random couleurs in unused palette couleurs */
     for (;i<num_colors;i++) {
       rm[i] = rand() & 0xff;
       gm[i] = rand() & 0xff;
       bm[i] = rand() & 0xff;
     }
		
     /* We're done avec the boxes now */
     delete [] box_list;
     freeboxes = usedboxes = NULL;
		
     /*
      * STEP 5: scan histogram and map all values to closest color
      */
     /* 5a: create cell list as described in Heckbert[2] */
     ColorCells = new CCell *[C_LEN*C_LEN*C_LEN];		
     memset(ColorCells,0,(C_LEN*C_LEN*C_LEN * sizeof (CCell *)));
     /* 5b: create mapping from truncated pixel space to color
        table entries */
     map_colortable();
     colormapped=true;
  }
		
  /*
   * Set Palette
   */
  palette=new CColorPalette(num_colors);
		
  for (i=0;i<num_colors;i++) 
    palette->GetColor(i).SetRGB((TSample)rm[i],(TSample)gm[i],(TSample)bm[i]);
  return palette;
}

CImage *CQuantizer::Quantize(const CImage *img,bool dither)
{
  CColorPalette * palette= GetColorPalette();
  CImage *img2;
  image=img;
  image_width=image->GetWidth();
  image_length=image->GetHeight();
  
  /*
   * STEP 6: scune image, match input values to table entries
   */
  if (bitcount==4)
    img2=new CImage(image->GetWidth(),image->GetHeight(),CImageInfo::Color16,
      image->GetXResolution(),image->GetYResolution(),palette);
  else
    img2=new CImage(image->GetWidth(),image->GetHeight(),CImageInfo::Color256,
    image->GetXResolution(),image->GetYResolution(),palette);
  
  if (dither) 
    quant_fsdither(img2);
  else 
    quant(img2);
  
  return img2;
}

CQuantizer::~CQuantizer()
{
  for(int i=0;i<C_LEN*C_LEN*C_LEN;i++) {
    if (ColorCells[i]) delete ColorCells[i];
  }
  delete [] ColorCells;
}

CImage * CImage::Quantize(int bitcount,bool dit) const
{
  
  CImage * img2;
  
  CQuantizer quantizer(bitcount);
  quantizer.SubmitImage(this);  
  img2=quantizer.Quantize(this,dit);
  
  return img2;
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
