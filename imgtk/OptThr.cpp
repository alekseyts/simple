//////////////////////////////////////////////////////////////////////
// @doc IMAGE

#include "Image.h"
#include <cmath> // for sqrt

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


void CImage::ComputePg(double *pg) const
{
  int i,j;
  int w=GetWidth(),h=GetHeight();
  TCBitmap bits;
  double nbpt=(double) w * (double) h;

  for(i=0;i<=255;i++) pg[i]=0.0;

  if (GeEImageType()==CImageInfo::Gray256) {
    for (i=0;i<h;i++) {
      bits=GetRow(i);   
      for (j=0;j<w;j++) {
	pg[*bits++]++;
      }
    }
  } else {
    for (i=0;i<h;i++) {
        for (j=0;j<w;j++) {
	pg[GetLuma(j,i)]++;
      }
    }

  }
  for(i=0;i<=255;i++) pg[i]/=nbpt;  
}



double CImage::ComputeMicro0(int T,const double *pg)
{
  int g;
  double s1,s2;

  s1=s2=0.0;
  for(g=0;g<=T;g++) {
    s1+=g*pg[g];
    s2+=pg[g];
  }  
  if (s2==0.0) return 0.0;
  return s1/s2;
}

double CImage::ComputeMicro1(int T,const double *pg)
{
  int g;
  double s1,s2;

  s1=s2=0.0;
  for(g=T+1;g<=255;g++) {    
    s1+=g*pg[g];
    s2+=pg[g];
  }  
  if (s2==0.0) return 255.0;
  return s1/s2;
}

void CImage::ComputeMicro(const double *pg,double *micro0,double *micro1)
{
  int T;
  for(T=0;T<=255;T++) {
    micro0[T]=ComputeMicro0(T,pg);
    micro1[T]=ComputeMicro1(T,pg);
  }  
}

double CImage::ComputeEx(const double *pg)
{
  double s=0.0;
  int g;

  for(g=0;g<=255;g++) {
    s+=g*pg[g];
  }
  return s;
}

double CImage::ComputeExx(const double *pg)
{
  double s=0.0;
  int g;

  for(g=0;g<=255;g++) {
    s+=g*g*pg[g];
  }
  return s;
}

double CImage::ComputeEy(const double *pg,const double *micro0,const double *micro1,int T)
{
  double s=0.0;
  int g;

  for(g=0;g<=T;g++) {
    s+=micro0[T]*pg[g];
  }
  for(g=T+1;g<=255;g++) {
    s+=micro1[T]*pg[g];
  }
  return s;
}

double CImage::ComputeEyy(const double *pg,const double *micro0,const double *micro1,int T)
{
  double s=0.0;
  int g;

  for(g=0;g<=T;g++) {
    s+=micro0[T]*micro0[T]*pg[g];
  }
  for(g=T+1;g<=255;g++) {
    s+=micro1[T]*micro1[T]*pg[g];
  }
  return s;
}

double CImage::ComputeExy(const double *pg,const double *micro0,const double *micro1,int T)
{
  double s=0.0;
  int g;

  for(g=0;g<=T;g++) {
    s+=g*micro0[T]*pg[g];
  }
  for(g=T+1;g<=255;g++) {
    s+=g*micro1[T]*pg[g];
  }
  return s;
}

/* Correlation entre l'image niveaux de gris et l'image NB apres seuillage
    avec la valeur T */

double CImage::ComputeCorrelation(int T,const double *pg,const double *micro0,const double *micro1,double Ex,double Vx)
{
  double Exy,Ey,Eyy,Vy,VxVy;

  Ey=ComputeEy(pg,micro0,micro1,T);
  Eyy=ComputeEyy(pg,micro0,micro1,T);
  Exy=ComputeExy(pg,micro0,micro1,T);

  Vy=Eyy-Ey*Ey;
  VxVy=Vx*Vy;
  if (VxVy<=0.0) return 0.0;

  return (Exy-Ex*Ey)/sqrt(VxVy);
}

 
TSample CImage::OptimalThreshold() const
{
  double pg[256],micro0[256],micro1[256];
  int bestT;
  double Correl;
  double bestCorrel;
  int T;
  double Ex,Exx,Vx;
  
  ComputePg(pg);
  ComputeMicro(pg,micro0,micro1);
  Ex=ComputeEx(pg);
  Exx=ComputeExx(pg);
  Vx=Exx-Ex*Ex;

  bestT=127;
  bestCorrel=ComputeCorrelation(bestT,pg,micro0,micro1,Ex,Vx);
  /* Maximise la correlation */
  for(T=0;T<=255;T++) {
    Correl=ComputeCorrelation(T,pg,micro0,micro1,Ex,Vx);    
    if (Correl>bestCorrel) {
      bestCorrel=Correl;
      bestT=T; 
    }
  }
  return (unsigned char) bestT;
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
