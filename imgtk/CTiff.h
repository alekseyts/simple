//////////////////////////////////////////////////////////////////////
// @doc TIFF

#if !defined(__CTIFF_H)
#define __CTIFF_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif


#if !defined(__IMAGE_H)
#include "Image.h"
#endif

#include <tiffio.h>

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

class CStream;

//@class TIFF writer/reader
class IMGTK_EXPORT CTiff
{
public:
  //@cmember Constructor
  CTiff();
  //@cmember Destructor
  ~CTiff();
  
  //@access Mapping of the standard TIFF functions
  //@cmember Close current TIFF stream
  void Close() ;
  //@cmember cf. Libtiff documentation
  int Flush() ;
  //@cmember cf. Libtiff documentation
  int FlushData() ;
  //@cmember Get Tag value
  int GetField(ttag_t tag, ...) ;
  //@cmember cf. Libtiff documentation
  int VGetField(ttag_t tag, va_list l) ;
  //@cmember cf. Libtiff documentation
  int GetFieldDefaulted(ttag_t tag, ...); 
  //@cmember cf. Libtiff documentation
  int VGetFieldDefaulted(ttag_t tag, va_list l) ;
  //@cmember cf. Libtiff documentation
  int ReadDirectory() ;
  //@cmember cf. Libtiff documentation
  tsize_t ScanlineSize() ;
  //@cmember cf. Libtiff documentation
  tsize_t RasterScanlineSize() ;
  //@cmember cf. Libtiff documentation
  tsize_t StripSize() ;
  //@cmember cf. Libtiff documentation
  tsize_t VStripSize(uint32 v) ;
  //@cmember cf. Libtiff documentation
  tsize_t TileRowSize() ;
  //@cmember cf. Libtiff documentation
  tsize_t TileSize() ;
  //@cmember cf. Libtiff documentation
  tsize_t VTileSize(uint32 v) ;
  //@cmember cf. Libtiff documentation
  uint32 DefaultStripSize(int v) ;
  //@cmember cf. Libtiff documentation
  void DefaultTileSize(uint32 * v1,uint32 *v2) ;
  //@cmember cf. Libtiff documentation
  int Fileno() ;
  //@cmember cf. Libtiff documentation
  int GetMode() ;
  //@cmember cf. Libtiff documentation
  int IsTiled() ;
  //@cmember cf. Libtiff documentation
  int IsByteSwapped() ;
  //@cmember cf. Libtiff documentation
  int IsUpSampled() ;
  //@cmember cf. Libtiff documentation
  int IsMSB2LSB() ;
  //@cmember cf. Libtiff documentation
  uint32 CurrentRow() ;
  //@cmember cf. Libtiff documentation
  tdir_t CurrentDirectory() ;
  //@cmember cf. Libtiff documentation
  uint32 CurrentDirOffset() ;
  //@cmember cf. Libtiff documentation
  tstrip_t CurrentStrip() ;
  //@cmember cf. Libtiff documentation
  ttile_t CurrentTile() ;
  //@cmember cf. Libtiff documentation
  int ReadBufferSetup(tdata_t data,tsize_t size) ;
  //@cmember cf. Libtiff documentation
  int WriteBufferSetup(tdata_t data,tsize_t size) ;
  //@cmember cf. Libtiff documentation
  int LastDirectory() ;
  //@cmember cf. Libtiff documentation
  int SetDirectory(tdir_t dir) ;
  //@cmember cf. Libtiff documentation
  int SetSubDirectory(uint32 sub) ;
  //@cmember cf. Libtiff documentation
  int UnlinkDirectory(tdir_t dir) ;
  //@cmember cf. Libtiff documentation
  int SetField(ttag_t tag,...) ;
  //@cmember cf. Libtiff documentation
  int VSetField(ttag_t tag, va_list l) ;
  //@cmember cf. Libtiff documentation
  int WriteDirectory() ;
  //@cmember cf. Libtiff documentation
  int ReadScanline(tdata_t data, uint32 v, tsample_t s = 0) ;
  //@cmember cf. Libtiff documentation
  int WriteScanline(tdata_t data, uint32 v, tsample_t s = 0) ;
  //@cmember cf. Libtiff documentation
  int ReadRGBAImage(uint32 v1, uint32 v2, uint32* p, int v3 = 0) ;
  //@cmember cf. Libtiff documentation
  int RGBAImageOK(char tab[1024]) ;
  //@cmember cf. Libtiff documentation
  int RGBAImageBegin(TIFFRGBAImage* img,int v, char tab [1024]) ;
  //@cmember cf. Libtiff documentation
  static int RGBAImageGet(TIFFRGBAImage*img,uint32* p, uint32 v1, uint32 v2) ;
  //@cmember cf. Libtiff documentation
  static void RGBAImageEnd(TIFFRGBAImage*img) ;
  //@cmember cf. Libtiff documentation
  void Open(const char *file, const char *mode) ;
  //@cmember cf. Libtiff documentation
  void FdOpen(int no,const char *file, const char *mode) ;
  //@cmember cf. Libtiff documentation
  void ClientOpen(
    const char* file, 
    const char* mode,
    thandle_t h,
    TIFFReadWriteProc rp, TIFFReadWriteProc wp,
    TIFFSeekProc sp, TIFFCloseProc cp,
    TIFFSizeProc szp,
    TIFFMapFileProc mp, TIFFUnmapFileProc ump) ;
  //@cmember Returns current file name
  const char* FileName() ;
  //@cmember cf. Libtiff documentation
  static TIFFErrorHandler SetErrorHandler(TIFFErrorHandler h) ;
  //@cmember cf. Libtiff documentation
  static TIFFErrorHandler SetWarningHandler(TIFFErrorHandler h) ;
  //@cmember cf. Libtiff documentation
  static TIFFExtendProc SetTagExtender(TIFFExtendProc ep) ;
  //@cmember cf. Libtiff documentation
  ttile_t ComputeTile(uint32 v1, uint32 v2, uint32 v3, tsample_t s) ;
  //@cmember cf. Libtiff documentation
  int CheckTile(uint32 v1, uint32 v2, uint32 v3, tsample_t s) ;
  //@cmember cf. Libtiff documentation
  ttile_t NumberOfTiles() ;
  //@cmember cf. Libtiff documentation
  tsize_t ReadTile(tdata_t data, uint32 v1, uint32 v2, uint32 v3, tsample_t s) ;
  //@cmember cf. Libtiff documentation
  tsize_t WriteTile(tdata_t data, uint32 v1, uint32 v2, uint32 v3, tsample_t s) ;
  //@cmember cf. Libtiff documentation
  tstrip_t ComputeStrip(uint32 v,tsample_t s) ;
  //@cmember cf. Libtiff documentation
  tstrip_t NumberOfStrips() ;
  //@cmember cf. Libtiff documentation
  tsize_t ReadEncodedStrip(tstrip_t strip, tdata_t data, tsize_t size) ;
  //@cmember cf. Libtiff documentation
  tsize_t ReadRawStrip(tstrip_t strip, tdata_t data, tsize_t size) ;
  //@cmember cf. Libtiff documentation
  tsize_t ReadEncodedTile(ttile_t tile, tdata_t data, tsize_t size) ;
  //@cmember cf. Libtiff documentation
  tsize_t ReadRawTile(ttile_t tile, tdata_t data, tsize_t size) ;
  //@cmember cf. Libtiff documentation
  tsize_t WriteEncodedStrip(tstrip_t strip, tdata_t data, tsize_t size) ;
  //@cmember cf. Libtiff documentation
  tsize_t WriteRawStrip(tstrip_t strip, tdata_t data, tsize_t size) ;
  //@cmember cf. Libtiff documentation
  tsize_t WriteEncodedTile(ttile_t tile, tdata_t data, tsize_t size) ;
  //@cmember cf. Libtiff documentation
  tsize_t WriteRawTile(ttile_t tile, tdata_t data, tsize_t size) ;
  //@cmember cf. Libtiff documentation
  void SetWriteOffset(toff_t off) ;
  //@cmember cf. Libtiff documentation
  static void SwabShort(uint16 *p) ;
  //@cmember cf. Libtiff documentation
  static void SwabLong(uint32 *p) ;
  //@cmember cf. Libtiff documentation
  static void SwabDouble(double *p) ;
  //@cmember cf. Libtiff documentation
  static void TIFFSwabArrayOfShort(uint16* p, unsigned long n) ;
  //@cmember cf. Libtiff documentation
  static void TIFFSwabArrayOfLong(uint32* p, unsigned long n) ;
  //@cmember cf. Libtiff documentation
  //static void TIFFSwabArrayOfDouble(double* p, unsigned long n) ;
  //@cmember cf. Libtiff documentation
  static void ReverseBits(unsigned char *p, unsigned long n) ;
  //@cmember cf. Libtiff documentation
  static const unsigned char* GetBitRevTable(int i) ;
  //@cmember cf. Libtiff documentation
  static const char* GetVersion(void) ;
  //@cmember cf. Libtiff documentation
  static const TIFFCodec* FindCODEC(uint16 c) ;
  //@cmember cf. Libtiff documentation
  static TIFFCodec* RegisterCODEC(uint16 c, const char* n, TIFFInitMethod m) ;
  //@cmember cf. Libtiff documentation
  static void UnRegisterCODEC(TIFFCodec *c) ;
  //@cmember cf. Libtiff documentation
  
  //@access Access to Common fields
  //@cmember Get image height
  uint32 GetImageLength () { 
	   uint32 imagelength;
	   GetField(TIFFTAG_IMAGELENGTH, &imagelength);
	   return imagelength;
  }
  //@cmember Set image height
  int SetImageLength (uint32 imagelength) { 	   
	   return SetField(TIFFTAG_IMAGELENGTH, imagelength);	   
  }
  //@cmember Get image width
  uint32 GetImageWidth () { 
	   uint32 imagewidth;
	   GetField(TIFFTAG_IMAGEWIDTH, &imagewidth);
	   return (int) imagewidth;
  }
  //@cmember Set image width
  int SetImageWidth (uint32 imagewidth) { 
	   return SetField(TIFFTAG_IMAGEWIDTH, imagewidth);
  }
  //@cmember Get tile height
  uint32 GetTileHeight () { 
	   uint32 tilelength;
	   GetField(TIFFTAG_TILELENGTH, &tilelength);
	   return tilelength;
  }
  //@cmember Set tile height
  int SetTileHeight (uint32 tilelength) { 
	   return SetField(TIFFTAG_TILELENGTH, tilelength);
  }
  //@cmember Get tile width
  uint32 GetTileWidth () { 
	   uint32 tilewidth;
	   GetField(TIFFTAG_TILEWIDTH, &tilewidth);
	   return tilewidth;
  }
  //@cmember Set tile width
  int SetTileWidth (uint32 tilewidth) { 
	   return SetField(TIFFTAG_TILEWIDTH, tilewidth);
  }
  //@cmember Get horizontal resolution
  float GetXResolution () { 
	   float resolution;
	   GetField(TIFFTAG_XRESOLUTION, &resolution);
	   return resolution;
  }
  //@cmember Set horizontal resolution
  int SetXResolution (float resolution) { 
	   return SetField(TIFFTAG_XRESOLUTION, resolution);
  }
  //@cmember Get vertical resolution
  float GetYResolution () { 
	   float resolution;
	   GetField(TIFFTAG_YRESOLUTION, &resolution);
	   return resolution;
  }
  //@cmember Set vertical resolution
  int SetYResolution (float resolution) { 
	   return SetField(TIFFTAG_YRESOLUTION, resolution);
  }
  //@cmember Get photometric interpretation
  uint16 GetPhotometric () { 
	   uint16 photometric = PHOTOMETRIC_UNDEFINED;
	   if (!GetField(TIFFTAG_PHOTOMETRIC, &photometric)) {
		   return PHOTOMETRIC_UNDEFINED;
	   }
	   return photometric;
  }
  //@cmember Set photometric interpretation
  int SetPhotometric (uint16 photometric) { 
	   return SetField(TIFFTAG_PHOTOMETRIC, photometric);
  }
  //@cmember Get number of bits per sample
  uint16 GetBitsPerSample () { 
	   uint16 bps;
	   GetFieldDefaulted(TIFFTAG_BITSPERSAMPLE, &bps);
	   return bps;
  }
  //@cmember Set number of bits per sample
  int SetBitsPerSample (uint16 bps) { 
	   return SetField(TIFFTAG_BITSPERSAMPLE, bps);
  }
  //@cmember Get number of samples per pixel
  uint16 GetSamplesPerPixel () { 
	   uint16 spp;
	   GetFieldDefaulted(TIFFTAG_SAMPLESPERPIXEL, &spp);
	   return spp;
  }
  //@cmember Set number of samples per pixel
  int SetSamplesPerPixel (uint16 spp) { 
	   return SetField(TIFFTAG_SAMPLESPERPIXEL, spp);
  }
  //@cmember Get planar configuration
  uint16 GetPlanarConfig () { 
	   uint16 pc;
	   GetFieldDefaulted(TIFFTAG_PLANARCONFIG, &pc);
	   return pc;
  }
  //@cmember Set planar configuration
  int SetPlanarConfig (uint16 pc) { 
	   return SetField(TIFFTAG_PLANARCONFIG, pc);
  }
  //@cmember Get sample format
  uint16 GetSampleFormat () { 
	   uint16 sf;
	   GetFieldDefaulted(TIFFTAG_SAMPLEFORMAT, &sf);
	   return sf;
  }
  //@cmember Set sample format
  int SetSampleFormat (uint16 sf) { 
	   return SetField(TIFFTAG_SAMPLEFORMAT, sf);
  }
  //@cmember Get Resolution unit
  uint16 GetResolutionUnit () { 
	   uint16 ru;
	   GetFieldDefaulted(TIFFTAG_RESOLUTIONUNIT, &ru);
	   return ru;
  }
  //@cmember Set Resolution unit
  int SetResolutionUnit (uint16 ru) { 
	   return SetField(TIFFTAG_RESOLUTIONUNIT, ru);
  }
  //@cmember Get compression scheme
  uint16 GetCompression () { 
	   uint16 c;
	   if (!GetField(TIFFTAG_COMPRESSION, &c))
	     return COMPRESSION_NONE;
	   return c;
  }
  //@cmember Set compression scheme
  int SetCompression (uint16 c) { 
	   return SetField(TIFFTAG_COMPRESSION, c);
  }
  //@cmember Get image orientation
  uint16 GetOrientation () { 
	   uint16 o;
	   GetFieldDefaulted(TIFFTAG_ORIENTATION, &o);	     
	   return o;
  }
  //@cmember Set image orientation
  int SetOrientation (uint16 o) { 
	   return SetField(TIFFTAG_ORIENTATION, o);	     
  }
  //@cmember Get horizontal predictor
  uint16 GetPredictor () { 
	   uint16 p;
	   if(!GetField(TIFFTAG_PREDICTOR, &p)) return 0;
	   return p;
  }
  //@cmember Set horizontal predictor
  int SetPredictor (uint16 p) { 
	   return SetField(TIFFTAG_PREDICTOR, p);
  }
  //@cmember Get color map
  int GetColorMap(uint16 **red,uint16 **green,uint16 **blue) {
	   return GetField(TIFFTAG_COLORMAP, red,green,blue);
  }
  //@cmember Set color map
  int SetColorMap(uint16 *red,uint16 *green,uint16 *blue) {
	   return SetField(TIFFTAG_COLORMAP, red,green,blue);
  }
  
  // IO with CImage
  //@cmember Get Image type
  EImageType GeEImageType();
  //@cmember Get Color palette
  CColorPalette *GetColorPalette();
  //@cmember Set Color palette
  void SetColorPalette(const CColorPalette& palette);
  //@cmember Read image information block
  CImageInfo *ReadImageInfo();
  //@cmember Read image
  CImage *ReadImage();
  //@cmember Write image
  void WriteImage(const CImage *img);
  
  //@cmember Attach an input stream (cf <c CStream>)
  void AttachInputStream(CStream &stream);
  //@cmember Attach an output stream (cf <c CStream>)
  void AttachOutputStream(CStream &stream);
  
private:
  TIFF *mTiff;
  
  void ReadContig(CImage *image);
  void ReadUniversal(CImage *image);
  void WriteContig(const CImage *image);

  static void StandardErrorProcessing(const char *module,const char *fmt,va_list ap);
  static void StandardWarningProcessing(const char *module,const char *fmt,va_list ap);

  static bool mFirstCreation;

  static int     MapProc(thandle_t a, tdata_t* b, toff_t* c);
  static void    UnmapProc(thandle_t a, tdata_t b, toff_t c);
  static tsize_t ReadProc(thandle_t, tdata_t, tsize_t);
  static tsize_t WriteProc(thandle_t, tdata_t, tsize_t);
  static toff_t  SeekProc(thandle_t, toff_t, int);  
  static int     CloseProc(thandle_t);
  static toff_t  SizeProc(thandle_t);
};

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__CTIFF_H)
