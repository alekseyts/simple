//////////////////////////////////////////////////////////////////////
// @doc ROTATE

#include "Image.h"
#include <cmath> // for cos, fabs, & sin

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

      
CImage *CImage::Rotate90() const
{
  CImage *rotimage = 0;
  int height,width,i,j,rowoffset;
  TCBitmap bits;
  TBitmap  bits2;
  TFSample r,g,b;
  unsigned char mask,mask2,data;
  int xoff;

  height = GetHeight();
  width = GetWidth();

  rotimage = new CImage(height,width,GeEImageType(),
    GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
  try {
	switch(GeEImageType()) {
	case CImageInfo::Bilevel:
		memset(rotimage->GetBitmap(),0,rotimage->GetBitmapSize());
		for(i=0;i < height ;i++) {
		bits=GetRow(i);
		mask=0x01;
		xoff= i>>3;
		mask2= 0x80 >> (i & 0x7);
	      
		for(j=0;j<width;j++) {
		if (mask==0x01) {
		mask=0x80;
		data=*bits++;
		while (data==0) {
			j+=8;
			if (j>=width) goto exitloop;
			data=*bits++;
		}
		} else mask>>=1;
		if (data & mask)
		*(rotimage->GetRow(width-j-1) + xoff) |= mask2;
		}
	exitloop: ;
		}
		break;
	case CImageInfo::Gray256:
		rowoffset=GetRowOffset();
		for(i=0;i < width;i++) {
		bits2=rotimage->GetRow(i);
		bits=GetRow(0)+width-i-1;
		for(j=height;j;j--) {
			*bits2++ = *bits;
			bits+=rowoffset;
		}
		}
		break;
	case CImageInfo::ColorRGB:
		rowoffset=GetRowOffset();
		for(i=0;i < width;i++) {
		bits2=rotimage->GetRow(i);
		bits=GetRow(0)+(width-i-1)*3;
		for(j=height;j;j--) {
			*bits2++ = *bits;
			*bits2++ = *(bits+1);
			*bits2++ = *(bits+2);
			bits+=rowoffset;
		}
		}
		break;
	default:
		for(i=0;i < width;i++) {
		for(j=height;j;j--) {
			GetFColor(width-i-1,j,r,g,b);
			rotimage->SetFColor(i,j,r,g,b);
		}
		}
	}
  } catch (...) {
	  if (0 != rotimage) {
		  delete rotimage;
	  }
	  throw;
  }
  return (rotimage);

}

CImage *CImage::Rotatem90() const
{
  CImage *rotimage = 0;
  int height,width,i,j,rowoffset;
  TCBitmap bits;
  TBitmap  bits2;
  TFSample r,g,b;
  
  unsigned char mask,mask2,data;
  int xoff;

  height = GetHeight();
  width = GetWidth();

  rotimage = new CImage(height,width,GeEImageType(),
    GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
  
  try {
	switch(GeEImageType()) {
	case CImageInfo::Bilevel:
		memset(rotimage->GetBitmap(),0,rotimage->GetBitmapSize());
		for(i=0;i < height ;i++) {
		bits=GetRow(i);
		mask=0x01;
		xoff= (height-i-1)>>3;
		mask2= 0x80 >> ((height-i-1) & 0x7);
		for(j=0;j<width;j++) {
		if (mask==0x01) {
		mask=0x80;
		data=*bits++;
		while (data==0) {
			j+=8;
			if (j>=width) goto exitloop;
			data=*bits++;
		}
		} else mask>>=1;
		if (data & mask)
		*(rotimage->GetRow(j) + xoff) |= mask2;
		}
	exitloop: ;
		}  
		break;  
	case CImageInfo::Gray256:
		rowoffset=GetRowOffset();
		for(i=0;i < width;i++) {
		bits2=rotimage->GetRow(i);
		bits=GetRow(height-1)+i;
		for(j=height;j;j--) {
			*bits2++ = *bits;
			bits-=rowoffset;
		}
		}
		break;
	case CImageInfo::ColorRGB:
		rowoffset=GetRowOffset();
		for(i=0;i < width;i++) {
		bits2=rotimage->GetRow(i);
		bits=GetRow(height-1)+i*3;
		for(j=height;j;j--) {
			*bits2++ = *bits;
			*bits2++ = *(bits+1);
			*bits2++ = *(bits+2);
			bits-=rowoffset;
		}
		}
		break;
	default:
		for(i=0;i < width;i++) {
		for(j=height;j;j--) {
			GetFColor(i,height-j-1,r,g,b);
			rotimage->SetFColor(i,j,r,g,b);
		}
		}
	}
  } catch (...) {
	  if (0 != rotimage) {
		  delete rotimage;
	  }
	  throw;
  }
  return (rotimage);

}

static unsigned char fastr[] = {
0x0, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0, 
0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0, 
0x8, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8, 
0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8, 
0x4, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4, 
0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4, 
0xc, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec, 
0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc, 
0x2, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2, 
0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2, 
0xa, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea, 
0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa, 
0x6, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6, 
0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6, 
0xe, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee, 
0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe, 
0x1, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1, 
0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1, 
0x9, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9, 
0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9, 
0x5, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5, 
0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5, 
0xd, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed, 
0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd, 
0x3, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3, 
0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3, 
0xb, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb, 
0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb, 
0x7, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7, 
0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7, 
0xf, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef, 
0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff
};

CImage *CImage::Rotate180() const
{
  CImage *rotimage = 0;
  int height,width,i,j;
  TCBitmap bits;
  TBitmap  bits2;
  TFSample r,g,b;
  unsigned char mask,mask2,val,val2;

  height = GetHeight();
  width = GetWidth();

  rotimage = new CImage(width,height,GeEImageType(),
    GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
  
  try {
	switch(GeEImageType()) {
	case CImageInfo::Bilevel:
		if (width%8==0) {
      		for(i=0;i < height;i++) {
		bits2=rotimage->GetRow(i);
		bits=GetRow(height-i-1)+(width-1)/8;
		for(j=width/8;j;j--) 
	  		*bits2++=fastr[*bits--];
		}      	
		} else {
		for(i=0;i < height;i++) {
		bits2=rotimage->GetRow(i);
		mask2=0x80;
		bits=GetRow(height-i-1)+(width-1)/8;
		mask=(0x80 >> ((width-1)%8));
		val2=0;
		val=*bits;
		for(j=width;j;j--) {
			if ( val & mask )
	      		val2 |= mask2;
			if (mask2==0x01) {
			mask2=0x80;
			*bits2++=val2;
			val2=0;
			} else 
			mask2>>=1;
			if (mask==0x80) {
			mask=0x01;
			bits--;
			val=*bits;
			} else 
			mask<<=1;
		}
		}
		if (val2) *bits2=val2;
		}
		break;
	case CImageInfo::Gray256:
		for(i=0;i < height;i++) {
		bits2=rotimage->GetRow(i);
		bits=GetRow(height-i-1)+width-1;
		for(j=width;j;j--) {
			*bits2++ = *bits--;
		}
		}
		break;
	case CImageInfo::ColorRGB:
		for(i=0;i < height;i++) {
		bits2=rotimage->GetRow(i);
		bits=GetRow(height-i-1)+(width-1)*3;
		for(j=width;j;j--) {
			*bits2++ = *bits;
			*bits2++ = *(bits+1);
			*bits2++ = *(bits+2);
			bits-=3;
		}
		}
		break;
	default:
		for(i=0;i < height;i++) {
		for(j=width;j;j--) {
			GetFColor(width-j-1,height-i-1,r,g,b);
			rotimage->SetFColor(i,j,r,g,b);
		}
		}
	}
  } catch (...) {
	  if (0 != rotimage) {
		  delete rotimage;
	  }
	  throw;
  }
  return (rotimage);

}

CImage *CImage::FastRotateBW(double angle) const
{
  CImage *rotimage;
  int newheight,newwidth,oldheight,oldwidth,i,j,nx,ny;
  double halfnewheight,halfnewwidth;
  double halfoldheight,halfoldwidth;
  double radians;
  double cosval,sinval,abscosval,abssinval;
  double mysinval,mycosval;
  TCBitmap bits;
  TByte mask,data=0;


  radians =  (angle) / ((180 / M_PI));
  cosval = cos(radians);
  abscosval = fabs(cosval);
  sinval = sin(radians);
  abssinval = fabs(sinval);

  oldheight = GetHeight();
  oldwidth = GetWidth();

  halfoldwidth = oldwidth /2.0;
  newwidth = (int)(oldwidth*abscosval + oldheight*abssinval + 0.5);
  halfnewwidth = newwidth / 2.0;

  halfoldheight = oldheight /2.0 ;
  newheight = (int)(oldheight*abscosval + oldwidth*abssinval + 0.5);  
  halfnewheight = newheight / 2.0;
  
  /* rotation non-significante ? */
  if (newwidth==oldwidth && newheight==oldheight)
      return new CImage(*this);

  rotimage = new CImage(newwidth,newheight,GeEImageType(),
    GetXResolution(),GetYResolution(),GetColorPalette()?new CColorPalette(*GetColorPalette()):NULL);
  
  
  memset(rotimage->GetBitmap(),0,rotimage->GetBitmapSize());
  for(i=0;i<oldheight;i++) {
    bits=GetRow(i);
    mask=0x01;
    mysinval=(i-halfoldheight)*sinval+halfnewwidth;
    mycosval=(i-halfoldheight)*cosval+halfnewheight;
    for(j=0;j<oldwidth;j++) {
      if (mask==0x01) {
      	mask=0x80;
	data=*bits++;
	while (data==0) {
	  j+=8;
	  if (j>=oldwidth) goto exitloop;
	  data=*bits++;
	}
      } else mask>>=1;
      if (data & mask) {
      	nx=(int) ((j-halfoldwidth)*cosval+mysinval);
	ny=(int) (mycosval-(j-halfoldwidth)*sinval);	
	*(rotimage->GetRow(ny)+nx/8) |= (0x80 >> (nx%8));
      }
    }
    exitloop: ;
  }
  
  return rotimage;
}

/*---------------------------------------------------------------------------
  EXPORTED FUNCTION      CImage::Rotate
  DESCRIPTION
    Rotate and image by a given angle
  PARAMETERS
    img           original image, left unchanged
    angle         rotation angle, given in degrees
    whitefill     fill borders with white pixels (black otherwise)
  RETURN VALUES
    converted image or NULL if function fails
  ---------------------------------------------------------------------------*/

CImage *CImage::Rotate(double angle,const CColor &fill) const
{
  CImage *rotimage = 0;
  int nx,ny,newheight,newwidth,oldheight,oldwidth,i,j,halfnewheight,halfnewwidth;
  int halfoldheight,halfoldwidth;
  double radians; 
  double cosval,sinval;
  TBitmap bits;
  TCBitmap bits2,bits3;
  TFSample r,g,b;
  TByte mask;
  int icosval,isinval;
  int mx,my,mypersin,mypercos;
  int rowoffset;


  if (angle>180.0) angle-=360.0;
  else
  if (angle<=-180.0) angle+=360.0;
   
  if (angle==0.0) return new CImage(*this);
  if (angle==-90.0) return Rotatem90();
  if (angle==90.0) return Rotate90();
  if (angle==180.0) return Rotate180();

  radians =  -(angle) / ((180 / M_PI));
  cosval = cos(radians);
  sinval = sin(radians);
  icosval = (int) (cosval * 65536.0 + 0.5);
  isinval = (int) (sinval * 65536.0 + 0.5);
  
  oldheight = GetHeight();
  oldwidth = GetWidth();

  newwidth = (int)abs((int)(oldwidth*cosval)) + (int)abs((int)(oldheight*sinval));
  newheight = (int)abs((int)(-oldwidth*sinval)) + (int)abs((int)(oldheight*cosval));

  halfnewheight = newheight / 2;
  halfnewwidth = newwidth / 2;
  halfoldwidth = oldwidth /2;
  halfoldheight = oldheight /2 ;

  rotimage = new CImage(newwidth,newheight,GeEImageType(), GetXResolution(),GetYResolution(), 
		  GetColorPalette() ? new CColorPalette(*GetColorPalette()) : 0);
  try {
	switch(GeEImageType()) {
	case CImageInfo::Bilevel: {
		bits2=GetRow(0);
		rowoffset=GetRowOffset();
		my=-halfnewheight;
		memset(rotimage->GetBitmap(),0,rotimage->GetBitmapSize());
		bool blackfill = fill.GetLuma()<128;
		for(i=0;i < newheight;i++,my++) {
		bits=rotimage->GetRow(i);
		mask=0x80;
		mx=-halfnewwidth;
		mypersin=my*isinval;
		mypercos=my*icosval;
		for(j=newwidth;j;j--,mx++) {
			nx =((mx*icosval + mypersin + 32767)>>16) + halfoldwidth;
			if ((nx >= oldwidth) || (nx < 0)) {
	      		if (blackfill) 
				*bits |= mask;
			goto nextbwrow;
			}
			ny =((mypercos - mx*isinval + 32767)>>16) + halfoldheight;
			if ((ny >= oldheight) || (ny < 0)) {
	      		if (blackfill) 
				*bits |= mask;
			goto nextbwrow;
			}
			if ( *(bits2+ny*rowoffset+(nx>>3)) & (0x80 >> (nx & 0x7)))
	      		*bits |= mask;
			nextbwrow:
			if (mask==0x01) {
			mask=0x80;
			bits++;
			} else 
			mask>>=1;
		}
		}
		}
		break;
	case CImageInfo::Gray256: {
		TSample background=fill.GetLuma();
		bits2=GetRow(0);
		rowoffset=GetRowOffset();
		my=-halfnewheight;
		for(i=0;i < newheight;i++,my++) {
		bits=rotimage->GetRow(i);
		mx=-halfnewwidth;
		mypersin=my*isinval;
		mypercos=my*icosval;
		for(j=newwidth;j;j--,mx++) {
			nx =((mx*icosval + mypersin)>>16) + halfoldwidth;
			if ((nx >= oldwidth) || (nx < 0)) {
			*bits++ = background;
			continue;
			}
			ny =((mypercos - mx*isinval)>>16) + halfoldheight;
			if ((ny >= oldheight) || (ny < 0)) {
			*bits++ = background;
			continue;
			}
			*bits++ = *(bits2+ny*rowoffset+nx);
		}
		}
	}
		break;
	case CImageInfo::ColorRGB:
		bits2=GetRow(0);
		rowoffset=GetRowOffset();
		my=-halfnewheight;
		for(i=0;i < newheight;i++,my++) {
		bits=rotimage->GetRow(i);
		mx=-halfnewwidth;
		mypersin=my*isinval;
		mypercos=my*icosval;
		for(j=newwidth;j;j--,mx++) {
			nx =((mx*icosval + mypersin)>>16) + halfoldwidth;
			if ((nx >= oldwidth) || (nx < 0)) {
			*(bits+CImage::ROffset) = fill.GetRed();
			*(bits+CImage::GOffset) = fill.GetGreen();
			*(bits+CImage::BOffset) = fill.GetBlue();		  
			bits+=3;
			continue;
			}
			ny =((mypercos - mx*isinval)>>16) + halfoldheight;
			if ((ny >= oldheight) || (ny < 0)) {
			*(bits+CImage::ROffset) = fill.GetRed();
			*(bits+CImage::GOffset) = fill.GetGreen();
			*(bits+CImage::BOffset) = fill.GetBlue();		  
			bits+=3;
			continue;
			}
			bits3 = (bits2+ny*rowoffset+nx*3);
			*bits++ = *bits3++;
			*bits++ = *bits3++;
			*bits++ = *bits3;
		}
		}
		break;
	default:
			my=-halfnewheight;
			for(i=0;i < newheight;i++,my++) {
				mx=-halfnewwidth;
				mypersin=my*isinval;
				mypercos=my*icosval;
				for(j=0;j<newwidth;j++,mx++) {
					nx =((mx*icosval + mypersin)>>16) + halfoldwidth;
					if ((nx >= oldwidth) || (nx < 0)) {
						rotimage->SetColor(j,i,fill);
						continue;
					}
					ny =((mypercos - mx*isinval)>>16) + halfoldheight;
					if ((ny >= oldheight) || (ny < 0)) {
						rotimage->SetColor(j,i,fill);
						continue;
					}
					GetFColor(nx,ny,r,g,b);
					rotimage->SetFColor(j,i,r,g,b);
				}
			}
	}
  } catch (...) {
	  if (0 != rotimage) {
		  delete rotimage;
	  }
	  throw;
  }
  return (rotimage);
}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
