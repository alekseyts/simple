//////////////////////////////////////////////////////////////////////
// @doc HISTOGRAM
//////////////////////////////////////////////////////////////////////

#if !defined(__HISTO_H)
#define __HISTO_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

//@class Histogram
class IMGTK_EXPORT CHistogram  {
  public:
    //@cmember Constructor
    CHistogram(int size);
    //@cmember Destructor
    ~CHistogram();
    //@cmember Histogram size
    int GetSize() const { return mSize; };
    //@cmember Operator for returning a reference on an histogram value
    int& operator[](int i) { return mValue[i]; };    
    //@cmember Operator for an histogram value
    int operator[](int i) const { return mValue[i]; };    
    
  private:
    // Histogram size
    int mSize;
    // Histogram value
    int *mValue;
};

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__HISTO_H)
