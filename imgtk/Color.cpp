//////////////////////////////////////////////////////////////////////
// @doc COLOR

#include "Color.h"
#include <CBinaryFile.h>

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

using namespace std;

int CColor::mRedFactor	  = 30;
int CColor::mGreenFactor  = 59;
int CColor::mBlueFactor	  = 11;

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CColor::ReadFromFile(CInputBinaryFile& ibf)
{
  TSample r(0);
  TSample g(0);
  TSample b(0);

  ibf.Read(r);
  ibf.Read(g);
  ibf.Read(b);

  SetRed  (r);
  SetGreen(g);
  SetBlue (b);
}

void CColor::ReadFromFile(shared_ptr<CInputBinFile> ibf)
{
  TSample r(0);
  TSample g(0);
  TSample b(0);

  ibf->Read(r);
  ibf->Read(g);
  ibf->Read(b);

  SetRed  (r);
  SetGreen(g);
  SetBlue (b);
}

void CColor::WriteToFile(COutputBinaryFile& obf) const
{
  obf.Write(GetRed());
  obf.Write(GetGreen());
  obf.Write(GetBlue());
}

void CColor::WriteToFile(shared_ptr<COutputBinFile> obf) const
{
  obf->Write(GetRed());
  obf->Write(GetGreen());
  obf->Write(GetBlue());
}

//void CColor::ReadFromStream (istream& stream)
//{
//  int r,g,b;
//
//  stream >> r;
//  stream >> g;
//  stream >> b;
//
//  SetRed  ((TSample) r);
//  SetGreen((TSample) g);
//  SetBlue ((TSample) b);
//}
//
//void CColor::WriteToStream  (ostream& stream) const
//{
//  int r,g,b;
//
//  r=GetRed();
//  g=GetGreen();
//  b=GetBlue();
//
//  stream << r << ' ' << g << ' ' << b << endl;
//}

//istream& operator>> (istream& s, CColor& o)
//{
//  o.ReadFromStream(s);
//  return s;
//}
//
//ostream& operator<< (ostream& s, const CColor& o)
//{
//  o.WriteToStream(s);
//  return s;
//}

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
