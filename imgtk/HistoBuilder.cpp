//////////////////////////////////////////////////////////////////////
// @doc HISTOGRAM

#include "HistoBuilder.h"

#if !defined(__HISTO_H)
#include "Histo.h"
#endif

#if !defined(__IMAGE_H)
#include "Image.h"
#endif

#if !defined(__IMAGE_EXCEPTION_H)
#include "ImageException.h"
#endif


#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 

CHistogram *CHistoBuilder::VertHistoBilevel(const CImage& image) 
{
  int w=image.GetWidth();
  int h=image.GetHeight();
  int i,j;
  TCBitmap bits;
  TByte mask,data=0;
  CHistogram *histo;

  histo=new CHistogram(h);

  for (i=0;i<h;i++) {
    bits=image.GetRow(i);
    mask=0x01;
    for (j=0;j<w;j++) {
      if (mask==0x01) {
      	mask=0x80;
	data=*bits++;
        while(data==0) {
	  j+=8;
	  if (j>=w) goto exitloop;
	  data=*bits++;
        }
      } else mask>>=1;

      if (data & mask) (*histo)[i]++;
    }
    exitloop: ;
  }
  return histo;
}


CHistogram *CHistoBuilder::VertHistoGray256(const CImage& image)
{
  int w=image.GetWidth();
  int h=image.GetHeight();
  int i,j;
  TCBitmap bits;

  CHistogram *histo;

  histo=new CHistogram(h);

  for (i=0;i<h;i++) {
    bits=image.GetRow(i);
    for (j=0;j<w;j++) {
      (*histo)[i]+=*bits++;
    }
  }
  return histo;
    
}

CHistogram *CHistoBuilder::VertHisto(const CImage& image)
{
  switch(image.GeEImageType()) {
  case CImageInfo::Bilevel:
    return VertHistoBilevel(image);
  case CImageInfo::Gray256:
    return VertHistoGray256(image);
  default:
    throw CImageException("CHistoBuilder::VertHisto","Image type not handled");
  }
  return NULL;
}


CHistogram *CHistoBuilder::HorzHistoBilevel(const CImage& image)
{
  int w=image.GetWidth();
  int h=image.GetHeight();
  int i,j;
  TCBitmap bits;
  TByte mask,data=0;
  CHistogram *histo;

  histo=new CHistogram(w);

  for (i=0;i<h;i++) {
    bits=image.GetRow(i);
    mask=0x01;
    for (j=0;j<w;j++) {
      if (mask==0x01) {
      	mask=0x80;
	data=*bits++;
        while(data==0) {
	  j+=8;
	  if (j>=w) goto exitloop;
	  data=*bits++;
	}
      } else mask>>=1;
      if (data & mask) (*histo)[j]++;
    }
    exitloop: ;
  }
  return histo;

}

CHistogram *CHistoBuilder::HorzHistoGray256(const CImage& image)
{
  int w=image.GetWidth();
  int h=image.GetHeight();
  int i,j;
  const TSample *bits;
  CHistogram *histo;

  histo=new CHistogram(w);

  for (i=0;i<h;i++) {
    bits=image.GetRow(i);
    for (j=0;j<w;j++) {
      (*histo)[j]+=*bits++;
    }
  }
  return histo;

}

CHistogram *CHistoBuilder::HorzHisto(const CImage& image)
{
  switch(image.GeEImageType()) {
  case CImageInfo::Bilevel:
    return HorzHistoBilevel(image);
  case CImageInfo::Gray256:
    return HorzHistoGray256(image);
  default:
    throw CImageException("CHistoBuilder::HorzHisto","Image type not handled");
  }
  return NULL;
}


#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 
