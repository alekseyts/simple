//////////////////////////////////////////////////////////////////////
// @doc IMAGE
#if !defined(__IMAGE_EXCEPTION_H)
#define __IMAGE_EXCEPTION_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(__IMGTK_H)
#include "ImgTk.h"
#endif

#if !defined(__IMGTK_EXCEPTION_H)
#include "ImgTkException.h"
#endif

#if !defined(NO_NAMESPACE)
namespace ImgTk
{ 
#endif 


	//@class Image exception
	//@base public | CImgTkException
	class IMGTK_EXPORT CImageException : public CImgTkException {
	public:
		CImageException(const char *data, const char *message) :
		  CImgTkException(data, message) {};

		  CImageException(const char* szData, const char* szMessage, int nMinValue, int nMaxValue):
		  CImgTkException(szData, szMessage, nMinValue, nMaxValue) {};

		  CImageException(const char* szData, const char* szMessage, int nValue):
		  CImgTkException(szData, szMessage, nValue) {};

	};

#if !defined(NO_NAMESPACE)
}; //namespace ImgTk
#endif 

#endif // !defined(__IMAGE_EXCEPTION_H)
