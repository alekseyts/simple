#if !defined(__CPHYSICAL_LAYOUT_PROFILE_H)
#define __CPHYSICAL_LAYOUT_PROFILE_H

#if !defined(__cplusplus)
#error CPhysicalLayoutProfile.h requires C++ compilation (use a .cpp suffix) 
#endif 

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "CLDef.h"
#include "CRBlock.h"
#include "CRBlob.h"
#include <list>
#include <boost/pool/pool_alloc.hpp>
//#include "CRBlobList.h"
typedef std::list< CBlob, boost::fast_pool_allocator<CBlob> > CBlobList;

_LAYOUT_BEGIN

class CHistogram;

class LAYOUT_EXPORT CPhysicalLayoutProfile
{
public:
	CPhysicalLayoutProfile();

	void SetImage(shared_ptr<CImage> pImg);
	void Analyze();
	std::vector<RECT> GetPageBlockRects();
	shared_ptr<CRBlock> GetPageBlock() {return m_Page;};
	std::vector<CRBlock>& GetImageBlocks();

	CRBlockArray& GetContentBlocks() {return m_ContentBlocks;};

private:	
	typedef CBlobList::iterator CBlobIter;

	void Analyze(shared_ptr<CRBlock> block);
	void FusionAreas(CBlobList,int iW,int iH);
	int CountLines(CHistogram* histo,int& iMaxInterLine);
	bool QualifyBlock(shared_ptr<CRBlock> block,shared_ptr<CImage> pBlockImage);
	bool CalcFusionCondition(CBlobIter i,
							 CBlobIter j,
							 double dVInclus,
							 double dHInclus,
							 int iW,
							 int iH
							 );
	
	bool Included(CBlobIter i,CBlobIter j);
	int NoteArea(CBlob& area,CBlobIter shape);
	void GetNextArea(CBlobList& areaList,CBlob& area,std::vector<bool>& parsed);
	shared_ptr<CRBlock> ExtractArea(shared_ptr<CImage> pPage,CBlob& area);
	bool DoSmearing(shared_ptr<CImage>,CBlobList& aBlobList);
	bool TryToClassify(shared_ptr<CRBlock> block,
						CRBlockArray& aContent,
						CRBlockArray& aImages);

	void PageSegmentation(CRBlockArray& aContent,
						  CRBlockArray& aImages);
   
private:
	shared_ptr<CRBlock>	m_Page;
	shared_ptr<CImage> m_pImg;
	CRBlockArray m_IntroducedBlocks;
	CRBlockArray m_ContentBlocks;
	
	int	m_nExX;
	int m_nExY;
	int m_nLevel;
};

_LAYOUT_END
#endif