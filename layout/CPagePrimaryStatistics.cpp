#include "CPagePrimaryStatistics.h"
#include "CColumnSeparatorSearch.h"
#include "CRBlobify.h"
#include "STAT.h"
#include <boost/bind.hpp>


//-----------------------------------------------------------
CPagePrimaryStatistics::CPagePrimaryStatistics(shared_ptr<CImage> pPage):
 m_pPage(pPage)
{
 
}

void CPagePrimaryStatistics::Process()
{
  CBlobifier blober;
  CBlobList blobList;
  blober.Blobify(m_pPage.get(), blobList);

  double meanW,meanH;
  computeStats(blobList,meanW,meanH);

  filterBlobsList(blobList,meanW,meanH);
  computeStats(blobList,meanW,meanH);
  m_ExX = meanW;
  m_ExY = meanH;
}

void CPagePrimaryStatistics::computeStats(CBlobList& blobList,
										  double& dMeanWidth,
										  double& dMeanHeight)
{
	vector<int> x;
	x.reserve(blobList.size());
	vector<int> y;
	y.reserve(blobList.size());

	for (CBlobList::const_iterator it = blobList.begin(); it != blobList.end(); ++it) {
		const CBlob& blob = *it;
		if (blob.GetWidth() > 3 && blob.GetHeight() > 3) {
				x.push_back(blob.GetWidth());
				y.push_back(blob.GetHeight());
		}
	}
		assert(x.size() == y.size());

		if (x.empty()) {
			assert(y.empty());
			transform(blobList.begin(), blobList.end(), back_inserter(x), std::mem_fun_ref(&CBlob::GetWidth));
			transform(blobList.begin(), blobList.end(), back_inserter(y), std::mem_fun_ref(&CBlob::GetHeight));
		}

		dMeanWidth = stable_mean_and_var(x.begin(), x.end(), 0);
		dMeanHeight = stable_mean_and_var(y.begin(), y.end(), 0);
}

void CPagePrimaryStatistics::filterBlobsList(CBlobList& blobList,
											double  meanW,
											double  meanH)
{
  	blobList.remove_if(boost::bind(&CBlob::IsBigObject,_1,meanW,meanH));
    blobList.remove_if(boost::bind(&CBlob::IsLine,_1,max(1,meanH/4)));
}
