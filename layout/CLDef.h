#if !defined(__CLDEF_H)
#define __CLDEF_H

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#if !defined(NO_NAMESPACE)
#define _LAYOUT_BEGIN \
namespace LAYOUT {
#else
#define _LAYOUT_BEGIN
#endif 

#if !defined(NO_NAMESPACE)
#define _LAYOUT_END \
} //namespace LAYOUT
#else
#define _LAYOUT_END
#endif

#if defined(_MSC_VER) /* MicroSoft compiler ? */
//disable warnings on extern before template instantiation
#pragma warning (disable : 4231 4251)
// To disable warning about long identifiers with STL
#pragma warning (disable:   4786 4788)
#endif // defined(_MSC_VER)

/* a macro to export library symbols (OCR_EXPORT) 
   and the type for exported functions (OCR_API) */   
#if defined(WIN32) /* Compiling for 32 bit Windows  ? */
#if defined(_MSC_VER) /* MicroSoft compiler ? */
//#if defined(OCR_COMPILING) /* Compiling the DLL ? */
#define LAYOUT_EXPORT __declspec(dllexport) 
//#else // using the DLL
//#define LAYOUT_EXPORT __declspec(dllimport) 
//#endif /* defined(OCR_COMPILING) */
#define LAYOUT_API __stdcall
#else /* Other compilers */
#define LAYOUT_EXPORT export
#define LAYOUT_API pascal
#endif /* _MSC_VER */
#else /* WIN32 */
#define LAYOUT_EXPORT
#define LAYOUT_API
#endif /* WIN32 */

#endif