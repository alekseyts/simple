#include "CLRect.h"
#include "assert.h"
//---------------------------------------------------------
CLRect::CLRect(unsigned int iLeft,
			   unsigned int iRight,
			   unsigned int iTop,
			   unsigned int iBottom):
	m_iLeft(iLeft),
	m_iRight(iRight),
	m_iTop(iTop),
	m_iBottom(iBottom)
{ 
	assert(iLeft<=iRight);
    assert(iTop<=iBottom);
}

CLRect::CLRect(unsigned int iWidth,unsigned int iHeight):
	m_iLeft(0),
	m_iRight(iWidth),
	m_iTop(0),
	m_iBottom(iHeight)
{

}

bool CLRect::IsIntersected(const CLRect& rect)
{
	bool bRet = false;
	if(m_iRight>rect.Left() && m_iLeft<rect.Right()){
		if(m_iBottom>rect.Top() && m_iTop<rect.Bottom()){
			bRet = true;
		}
	}
	return bRet;
}