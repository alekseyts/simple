
#if !defined(__C_COLUMN_SEPARATOR_SEARCH_H)
#define __C_COLUMN_SEPARATOR_SEARCH_H

#if !defined(__cplusplus)
#error CColumnSeparatorSearch.h requires C++ compilation (use a .cpp suffix) 
#endif 

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "CLRect.h"
#include "Image.h"

class CColumnSeparatorSearch
{
public:
  
	void Run();
private:
	struct CRectAssociation
	{
		CRectAssociation(CLRect rBox,
						 CLRectArray aBlackRects,
						 float fQuality):
			m_aWhiteSpace(rBox),
			m_aObstacles(aBlackRects),
			m_fQuality(fQuality)	
		{};

		CLRect	m_aWhiteSpace;
        CLRectArray m_aObstacles;
		float m_fQuality;
	};

	friend bool operator<(const CColumnSeparatorSearch::CRectAssociation& op1,
						  const CColumnSeparatorSearch::CRectAssociation& op2);


	float Quality(const CLRect& rect) const;
	CLRectArray ComposeWordLikeBoxes();
	CLRectArray FindIntersectedObstacles(const CLRect& clRect,
															 CLRectArray& aObstacles);
	CLRect ObtainObstacle(CLRect aRect,CLRectArray& aObstacles);

	CLRectArray m_GlyphBoxes;
	CLRectArray	m_Separators; 
	shared_ptr<CImage> m_pImg;
};


#endif //__C_COLUMN_SEPARATOR_SEARCH_H