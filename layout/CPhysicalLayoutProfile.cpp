#include "CPhysicalLayoutProfile.h"

#if !defined(__HISTO_H)
#include "Histo.h"
#endif

#include "HistoBuilder.h"
#include "CRBlobify.h"
#include <boost/bind.hpp>
#include "ImageIterator.h"
#include <CTiff.h>
#include "scoped_assign.h"
#include "CPagePrimaryStatistics.h"

#undef max

_LAYOUT_BEGIN
//----------------------------------------------------------------
CPhysicalLayoutProfile::CPhysicalLayoutProfile()
{
	m_Page.reset();
	m_pImg.reset();
}

void CPhysicalLayoutProfile::SetImage(shared_ptr<CImage> pImg)
{

	m_pImg = pImg;
	RECT r = {0,0,m_pImg->GetWidth(),m_pImg->GetHeight()};
	m_Page.reset(new CRBlock(m_pImg,r));

	m_nExX = 14;//std::max(8,(8*m_pImg->GetXResolution())/100);
	m_nExY = 7;//std::max(8,(8*m_pImg->GetYResolution())/100);
}

void CPhysicalLayoutProfile::Analyze()
{
	assert(m_pImg.get()!=NULL);
	m_Page->Nullify();
	m_nLevel = 1;

	CPagePrimaryStatistics statGenerator(m_pImg);
    statGenerator.Process();
    m_nExX = statGenerator.GetMeanGlyphW();///2.0;
    m_nExY = statGenerator.GetMeanGlyphH();///4.0;

	//Analyze(m_Page);

	m_ContentBlocks.clear();
	m_IntroducedBlocks.clear();
    m_ContentBlocks.push_back(m_Page);
	PageSegmentation(m_ContentBlocks,m_IntroducedBlocks);

	CRBlockArray::iterator iter = m_ContentBlocks.begin();
	for(;iter!=m_ContentBlocks.end();iter++){
		shared_ptr<CRBlock> block = *iter;
		RECT r = block->GetBox();
		r =r;
	}
}

#define NOLINE 16
#define ISLINE 100
#define MINHEIGHTLINE 8
#define MAXHEIGHTLINE 64

int CPhysicalLayoutProfile::CountLines(CHistogram* histo,int& iMaxInterLine)
{
  int iStartLine=-1;
  int iMaxx=0;
  int i;
  int iNumLines=0;
  int iInterline;

  iMaxInterLine=0;

  for(int i=0;i<histo->GetSize();i++) {
    if ((*histo)[i]>NOLINE) {
      if (iStartLine==-1) {
        iStartLine = i;
        iMaxx = (*histo)[i];
        if (iNumLines) {
          if (iInterline > iMaxInterLine)
            iMaxInterLine=iInterline;
        }
      } else {
		  if (i - iStartLine >= MAXHEIGHTLINE){
			  return 0;
		  }
		  if ((*histo)[i] > iMaxx) { 
			  iMaxx = (*histo)[i];
		  }
      }
    } else {
      if (iStartLine != -1) {
		 if (i-iStartLine >= MINHEIGHTLINE && iMaxx >= ISLINE){
			iNumLines++;
		 }
        iStartLine=-1;
        iInterline=0;
	  } else if (iNumLines){
          iInterline++;
      }
    }
  }

  if (iStartLine!=-1) {
	  if (i-iStartLine>=MAXHEIGHTLINE){
		  return 0;
	  }
	  if ((i-iStartLine)>=MINHEIGHTLINE && iMaxx>=ISLINE){
		iNumLines++;
	  }
  }

  return iNumLines;
}
namespace{
	class CFDensity:public std::unary_function<const CHistogram&,double>
	{
	public:
		CFDensity(int max_value):
			m_iMaxVal(max_value)
		{
		};

		result_type operator()(argument_type histo)
		{
		  int h = histo.GetSize();	
		  result_type density;

		  for(int i=0;i<h;i++){
			 density+=(result_type) histo[i];
		  }
		  density=density/(m_iMaxVal*h);
		  return density;
		}
	private:
		int m_iMaxVal;
	};
}

bool CPhysicalLayoutProfile::QualifyBlock(shared_ptr<CRBlock> block,shared_ptr<CImage> pBlockImage)
{
  int iW = block->GetWidth();
  int iH = block->GetHeight();
  std::auto_ptr<CHistogram> vertHisto;
  vertHisto.reset(CHistoBuilder::VertHisto(*pBlockImage.get()));

  double fDensity=0.0;
  int i;

  int iMaxInterLine;
  int iNumLines = CountLines(vertHisto.get(),iMaxInterLine);

  const int iSuffLineCount = 2;
  const int iAdmissibleGap = 64;

  if (iNumLines >= iSuffLineCount && iMaxInterLine < iAdmissibleGap) {
	block->SetType(CRBlock::BT_Carea);
    return true;
  }

  CFDensity ufDensity(iW);
  fDensity = ufDensity(*vertHisto.get());	

  const float fImageDensityThr = 0.8;
  if (fDensity > fImageDensityThr) {
	  block->SetType(CRBlock::BT_Float);
	  return true;
  } else {
	  block->SetType(CRBlock::BT_Unspecified);
	  return false;
  }
	
}

namespace {
	class CFTransformer:public unary_function<void,auto_ptr<CImage> >
	{
	public:
		CFTransformer(CImage* img):
		  m_pImg(img)
		{	
		};

		result_type operator()(int iExtX,int iExtY)
		{
			int iWidth = m_pImg->GetWidth();
			int iHeight = m_pImg->GetHeight();
			int iWidth2 = (iWidth + iExtX - 1)/iExtX;
			int iHeight2 = (iHeight + iExtY - 1)/iExtY;

			result_type  res(new CImage(iWidth2,iHeight2,m_pImg->GeEImageType()));

			CBilevelImageIterator imgIter(res.get());
			for( ;imgIter.GetY()<imgIter.GetHeight();imgIter.NextRow() ) {
				for ( imgIter.SetX(0) ;imgIter.GetX()<imgIter.GetWidth();imgIter.Next()  ) { 
					if (isAnyPixel(imgIter.GetX()*iExtX,imgIter.GetY()*iExtY,iExtX,iExtY)) {
						imgIter.SetLuma(0);
					}
				}
			}
			return res;
		}
	private:
		
		bool isAnyPixel(int x,int y,int iExtX,int iExtY)
		{
		CBilevelImageConstIterator imgIter(m_pImg);
		for(imgIter.SetY(y) ;imgIter.GetY()< min(imgIter.GetHeight(),y+iExtY);imgIter.NextRow() ) {
			for ( imgIter.SetX(x) ;imgIter.GetX()<min(imgIter.GetWidth(),x+iExtX);imgIter.Next()  ) { 
				if (imgIter.GetLuma() == 0) {
					return true;
				}
			}
		}	
		return false;
		}
		CImage* m_pImg;
	};
}

shared_ptr<CRBlock> CPhysicalLayoutProfile::ExtractArea(
			shared_ptr<CImage> pPage,
			CBlob& area
			)
{
  int iW = pPage->GetWidth();
  int iH = pPage->GetHeight();

  int w = area.GetWidth()*m_nExX;
  int h = area.GetHeight()*m_nExY;

  shared_ptr<CImage> pRes(new CImage(w,h,pPage->GetImageInfo().GeEImageType()) );

  int iSrcX1 = area.GetX1()*m_nExX; 
  int iSrcY1 = area.GetY1()*m_nExY;

  int blockW = min(w,iW - iSrcX1);
  int blockH = min(h,iH - iSrcY1);
  CBilevelImageIterator imgIter(area.GetImage().get());
  for( ;imgIter.GetY()<imgIter.GetHeight();imgIter.NextRow() ) {
	int iStartY = imgIter.GetY()*m_nExY;
	int iY = iSrcY1 + iStartY;	
	int iBlitH = min(m_nExY,iH - iY);
				  
	for ( imgIter.SetX(0) ;imgIter.GetX()<imgIter.GetWidth();imgIter.Next()  ) { 
		if (imgIter.GetLuma() == 0) {
			  int iStartX = imgIter.GetX()*m_nExX;
			  int iX = iSrcX1 + iStartX;
			  int iBlitW = min(m_nExX,iW - iX);
			  pRes->Blit(iStartX,iStartY,pPage.get(),iX,iY,iBlitW,iBlitH);	
		}
	}
  }


	RECT r = {iSrcX1,iSrcY1,iSrcX1+blockW,iSrcY1+blockH};
	shared_ptr<CRBlock> block(new CRBlock(pRes,r));

  return block;
}

int CPhysicalLayoutProfile::NoteArea(CBlob& area,CBlobIter shape)
{
	if (area.GetY2()<shape->GetY1()){
		return std::max(shape->GetY1()-area.GetY2(),abs(shape->GetX1()-area.GetX1()));
	}

	if (area.GetVInclusivity(*shape)>0.8 && area.GetX2()<shape->GetX1()){
		return (shape->GetX1()-area.GetX2());
	}

	if (shape->GetY2()<area.GetY1() && !(shape->GetX1()>area.GetX2() )){
		return 0;
	}

	return std::numeric_limits<int>::max();
}


void  CPhysicalLayoutProfile::GetNextArea(CBlobList& areaList,
										CBlob& area,
										std::vector<bool>& aParsed
										)
{
  if (area.IsEmpty()) {
    CBlobIter besti;
	CBlobIter i = areaList.begin();
	CBlobIter j = areaList.begin();
    for(j++;j!=areaList.end();j++) {
      if( j->GetY1()<i->GetY1()-2 ||
          (i->GetVInclusivity(*j)>0.5 && j->GetX1()<i->GetX1()) ) {
          i=j;
      }
    }
	int iDist = distance(areaList.begin(),i);
    aParsed[iDist]=true;
	area = *i;
	return; 
  }

  CBlobIter res = areaList.end();
  int bestnote = std::numeric_limits<int>::max();
  for(CBlobIter shape=areaList.begin();shape!=areaList.end();shape++) {
	int idx = distance(areaList.begin(),shape);
    if (!aParsed[idx]) {
      if(res==areaList.end()) {
			res = shape;
			bestnote = NoteArea(area,shape);
      } else {
			int note=NoteArea(area,shape);
			if (note<bestnote) {
				res=shape;
				bestnote=note;
			}
      }
    }
  }

  if (res != areaList.end() && bestnote>4) {
    res=areaList.end();
	CBlobIter i = areaList.begin();
    for(;i!=areaList.end();i++) {
      int idx = distance(areaList.begin(),i);
      if (!aParsed[idx]) {
        if(res == areaList.end()) {
          res = i;
        } else {
          if(i->GetY1()<res->GetY1()-2 ||
            (res->GetVInclusivity(*i)>0.5 && i->GetX1()<res->GetX1())) {
				res=i;
           }
        }
      }
    }
  }

  int idx = distance(areaList.begin(),res);
  aParsed[idx]=1;
  area = *res;
}


void CPhysicalLayoutProfile::Analyze(shared_ptr<CRBlock> block)
{
 shared_ptr<CImage> pBlockImg = block->GetBlockImage();


 while(true){
    if(m_nLevel>1){
	    if(block->GetHeight() < 500){
		  block->SetType(CRBlock::BT_Carea);
		  return;
		}

		bool bSpecified = QualifyBlock(block,pBlockImg);
		if(bSpecified){
			return;
		}
    }

	if(m_nExX <= 7 || m_nExY <= 7){
		block->SetType(CRBlock::BT_Float);
		return;
	}

	CFTransformer transformer(pBlockImg.get());
	auto_ptr<CImage> pTransImg = transformer(m_nExX,m_nExY);
 
	//pTransImg->WriteAsTIFF("C:\\tmp\\aTrans.tiff");

	CBlobifier blober;
	std::list< CBlob, boost::fast_pool_allocator<CBlob> > areaList;
	blober.Blobify(pTransImg.get(), areaList);

	areaList.remove_if(boost::bind(&CBlob::IsBadArea,_1,pBlockImg.get(),m_nExX,m_nExY));

	int iWidth = pBlockImg->GetWidth();
	int iHeight = pBlockImg->GetHeight();
	FusionAreas(areaList,iWidth,iHeight);

	if (areaList.size()==1) {
		m_nExX--;
		m_nExY--;
        m_nLevel++;
		areaList.clear();
		continue;
	}
    
	if (m_nLevel>1 && areaList.size()>25) {
		block->SetType(CRBlock::BT_Noise);
		return;
	}

   CBlob area;
   std::vector<bool> aParsed(areaList.size());
   for(int i=0;i<areaList.size();i++) {
      GetNextArea(areaList,area,aParsed);
	  shared_ptr<CRBlock> subBlock = ExtractArea(pBlockImg,area);

	  //--subBlock->GetBlockImage()->WriteAsTIFF("C:\\tmp\\aCheck.tiff");		

	  scoped_assign<int> iExX(m_nExX,m_nExX);
	  scoped_assign<int> iExY(m_nExY,m_nExY);	
	  scoped_assign<int> iLevel(m_nLevel,m_nLevel+1);	
	  Analyze(subBlock);
	 // subBlock.DeleteImage();

	  block->Unite(subBlock);	
	  block->SetType(CRBlock::BT_Complex);
   }
   return;
 }
}

bool CPhysicalLayoutProfile::TryToClassify(shared_ptr<CRBlock> block,
										   CRBlockArray& aContent,
										   CRBlockArray& aImages)
{
    if(m_nLevel>1){
	    if(block->GetHeight() < 500){
		  block->SetType(CRBlock::BT_Carea);
          aContent.push_back(block);
		  return true;
		}

		shared_ptr<CImage> pBlockImg = block->GetBlockImage();
		bool bSpecified = QualifyBlock(block,pBlockImg);
		if(bSpecified){
			if(block->GetBlockType() != CRBlock::BT_Carea){
				aImages.push_back(block);
			} else {
				aContent.push_back(block);
			}
			return true;
		}
    }

	if(m_nExX <= 7 || m_nExY <= 7){
		block->SetType(CRBlock::BT_Float);
		aImages.push_back(block);
		return true;
	}

	return false;
}

bool CPhysicalLayoutProfile::DoSmearing(shared_ptr<CImage> pBlockImg,CBlobList& areaList)
{
  	CFTransformer transformer(pBlockImg.get());
	auto_ptr<CImage> pTransImg = transformer(m_nExX,m_nExY);
 
	//pTransImg->WriteAsTIFF("C:\\tmp\\aTrans.tiff");

	CBlobifier blober;
	blober.Blobify(pTransImg.get(), areaList);

	areaList.remove_if(boost::bind(&CBlob::IsBadArea,_1,pBlockImg.get(),m_nExX,m_nExY));

	int iWidth = pBlockImg->GetWidth();
	int iHeight = pBlockImg->GetHeight();
	FusionAreas(areaList,iWidth,iHeight);

	if (areaList.size()==1) {
		m_nExX--;
		m_nExY--;
        m_nLevel++;
		areaList.clear();
		return true;
	}

    return false;
}

void CPhysicalLayoutProfile::PageSegmentation(CRBlockArray& aContent,CRBlockArray& aImages)
{
 scoped_assign<int> iExX(m_nExX,m_nExX);
 scoped_assign<int> iExY(m_nExY,m_nExY);
 scoped_assign<int> iLevel(m_nLevel,m_nLevel);	

 CRBlockArray::iterator iter = aContent.begin();
 CRBlockArray procContent;
 for(;iter!=aContent.end();iter++){
    CRBlockArray aBlockContent;
	if(TryToClassify(*iter,aBlockContent,aImages)==false){
		CBlobList areaList;
		shared_ptr<CImage> pBlockImg = (*iter)->GetBlockImage();

		pBlockImg->WriteAsTIFF("C:\\tmp\\aBlock.tiff");

		bool bEnlarge = DoSmearing(pBlockImg,areaList); 

		if (m_nLevel>1 && areaList.size()>25) {
			(*iter)->SetType(CRBlock::BT_Noise);
			aImages.push_back((*iter));
			continue;
		}

		if(bEnlarge){
			PageSegmentation(aContent,aImages);
			break;
		} else {
			CBlob area;
			std::vector<bool> aParsed(areaList.size());
			for(int i=0;i<areaList.size();i++) {
				GetNextArea(areaList,area,aParsed);
				shared_ptr<CRBlock> subBlock = ExtractArea(pBlockImg,area);

				subBlock->GetBlockImage()->WriteAsTIFF("C:\\tmp\\aCheck.tiff");		

				scoped_assign<int> iExX(m_nExX,m_nExX);
				scoped_assign<int> iExY(m_nExY,m_nExY);	
				scoped_assign<int> iLevel(m_nLevel,m_nLevel+1);	

				CRBlockArray aSubBlockContent(1,subBlock);
				PageSegmentation(aSubBlockContent,aImages);
            
				aBlockContent.insert(aBlockContent.end(),
									aSubBlockContent.begin(),
									aSubBlockContent.end());
			}
		}
	}
    procContent.insert(procContent.end(),aBlockContent.begin(),aBlockContent.end());
 }//end for

 aContent.clear();
 aContent.insert(aContent.begin(),procContent.begin(),procContent.end());
}

void CPhysicalLayoutProfile::FusionAreas(CBlobList aBlock,int iW,int iH)
{
  double vi,hi;
  
  return;

  CBlobIter i = aBlock.begin();
  while(i!=aBlock.end()){
	CBlobIter j = i;
	bool bFuse = false;
		for(j++;j!=aBlock.end();j++) {
			double dVertInclus = i->GetVInclusivity(*j);
			double dHorzInclus = i->GetHInclusivity(*j);
            bool bCombine = CalcFusionCondition(i,j,dVertInclus,dHorzInclus,iW,iH);
			if (bCombine){
				CBlob fuseBlob;
				fuseBlob.Add(*i);
				fuseBlob.Add(*j);
				swap(*i,fuseBlob);
				aBlock.erase(j);
				bFuse = true;
				break;
			}
		}
		if(bFuse == false){
			i++;
		}
  }
}

bool CPhysicalLayoutProfile::Included(CBlobIter i,CBlobIter j)
{
  return (i->GetX1()+4>=j->GetX1() &&
		  i->GetX2()<=j->GetX2()+4 && 
		  i->GetY1()+4>=j->GetY1() && 
		  i->GetY2()<=j->GetY2()+4);
}

bool CPhysicalLayoutProfile::CalcFusionCondition(CBlobIter i,
												 CBlobIter j,
												 double dVInclus,
												 double dHInclus,
												 int iW,
												 int iH)
{
  bool cond = dVInclus > 0.8;
  bool cond1 = (i->GetX2()<j->GetX1() && j->GetX1()-i->GetX2()<=4) ||
				(j->GetX2()<i->GetX1() && i->GetX1()-j->GetX2()<=4);
  
  bool cond2 =  (i->GetHeight()*m_nExY < 50 ||
				 j->GetHeight()*m_nExY<50 ||
				 i->GetWidth()*m_nExX<iW/6 ||
				 j->GetWidth()*m_nExX<iW/6);

  bool cond3 =  Included(i,j) ||
				Included(j,i) ||
				(dVInclus > 0.6 && dHInclus>0.1) ||
				(dHInclus>.6 && dVInclus>0.1);

  return (cond && cond1 && cond2) || cond3;
}

std::vector<RECT>  CPhysicalLayoutProfile::GetPageBlockRects()
{
	std::vector<RECT>  rects = m_Page->GetBlockRects(0,0);
	return rects;
}	

_LAYOUT_END
