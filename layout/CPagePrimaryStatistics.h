#if !defined(__C_PAGE_PIMARY_STATISTICS_H)
#define __C_PAGE_PIMARY_STATISTICS_H

#if !defined(__cplusplus)
#error CPagePrimaryStatistics.h requires C++ compilation (use a .cpp suffix) 
#endif 

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Image.h"
#include "CRBlobList.h"


class CPagePrimaryStatistics
{
public:
	CPagePrimaryStatistics(shared_ptr<CImage> pPage);
    void Process();
	int GetMeanGlyphW() const {return m_ExX;};
	int GetMeanGlyphH() const {return m_ExY;};

private:
	void computeStats(CBlobList& blobList,double& dMeanWidth,double& dMeanHeight);
	void filterBlobsList(CBlobList& blobList,double dMeanWidth,double dMeanHeight);

    shared_ptr<CImage> m_pPage;
	int m_ExX;
	int m_ExY;
};

#endif //__C_PAGE_PIMARY_STATISTICS_H