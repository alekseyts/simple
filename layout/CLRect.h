#if !defined(__CL_RECT_H)
#define __CL_RECT_H

#include <vector>

class CLRect
{
public:
  
  CLRect(unsigned int iLeft,
		 unsigned int iRight,
		 unsigned int iTop,
		 unsigned int iBottom);

  CLRect(unsigned int iWidth,unsigned int iHeght);
  unsigned int	GetArea() const {return (m_iRight - m_iLeft)*(m_iBottom-m_iTop);}; 
  bool IsIntersected(const CLRect& rect);
  unsigned int Left() const {return m_iLeft;};
  unsigned int Right() const {return m_iRight;};
  unsigned int Top() const {return m_iTop;};
  unsigned int Bottom() const {return m_iBottom;};

private:
  unsigned int m_iLeft;
  unsigned int m_iRight;
  unsigned int m_iTop;
  unsigned int m_iBottom;
};

typedef std::vector<CLRect> CLRectArray;

#endif //__CL_RECT_H