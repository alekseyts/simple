# Microsoft Developer Studio Project File - Name="gz" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Dynamic-Link Library" 0x0102

CFG=gz - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "gz.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "gz.mak" CFG="gz - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "gz - Win32 Release" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE "gz - Win32 Debug" (based on "Win32 (x86) Dynamic-Link Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/sirocco/gz", DLAAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "gz - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "..\Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /YX /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x40c /d "NDEBUG"
# ADD RSC /l 0x40c /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /machine:I386
# ADD LINK32 /nologo /subsystem:windows /dll /machine:I386

!ELSEIF  "$(CFG)" == "gz - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "..\Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /Zi /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /YX /FD /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /FD /c
# SUBTRACT CPP /YX
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /o "NUL" /win32
# ADD BASE RSC /l 0x40c /d "_DEBUG"
# ADD RSC /l 0x40c /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /dll /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "gz - Win32 Release"
# Name "gz - Win32 Debug"
# Begin Group "Source files"

# PROP Default_Filter "*.c"
# Begin Source File

SOURCE=.\ADLER32.C
# End Source File
# Begin Source File

SOURCE=.\COMPRESS.C
# End Source File
# Begin Source File

SOURCE=.\CRC32.C
# End Source File
# Begin Source File

SOURCE=.\DEFLATE.C
# End Source File
# Begin Source File

SOURCE=.\DLLGZ.C
# End Source File
# Begin Source File

SOURCE=.\GZIO.C
# End Source File
# Begin Source File

SOURCE=.\INFBLOCK.C
# End Source File
# Begin Source File

SOURCE=.\INFCODES.C
# End Source File
# Begin Source File

SOURCE=.\INFFAST.C
# End Source File
# Begin Source File

SOURCE=.\INFLATE.C
# End Source File
# Begin Source File

SOURCE=.\INFTREES.C
# End Source File
# Begin Source File

SOURCE=.\INFUTIL.C
# End Source File
# Begin Source File

SOURCE=.\TREES.C
# End Source File
# Begin Source File

SOURCE=.\UNCOMPR.C
# End Source File
# Begin Source File

SOURCE=.\ZUTIL.C
# End Source File
# End Group
# Begin Group "Header files"

# PROP Default_Filter "*.h"
# Begin Source File

SOURCE=.\DEFLATE.H
# End Source File
# Begin Source File

SOURCE=.\INFBLOCK.H
# End Source File
# Begin Source File

SOURCE=.\INFCODES.H
# End Source File
# Begin Source File

SOURCE=.\INFFAST.H
# End Source File
# Begin Source File

SOURCE=.\INFTREES.H
# End Source File
# Begin Source File

SOURCE=.\INFUTIL.H
# End Source File
# Begin Source File

SOURCE=.\ZCONF.H
# End Source File
# Begin Source File

SOURCE=.\ZLIB.H
# End Source File
# Begin Source File

SOURCE=.\ZUTIL.H
# End Source File
# End Group
# Begin Group "Definition file"

# PROP Default_Filter "*.def"
# Begin Source File

SOURCE=.\DLLGZ.DEF
# End Source File
# End Group
# End Target
# End Project
